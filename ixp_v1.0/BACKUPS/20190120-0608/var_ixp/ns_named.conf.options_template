acl "acl4" {
        <<ipv4-man-net>>;
        <<ipv4-peer-net>>;
};
acl "acl6" {
        <<ipv6-man-net>>;
        <<ipv6-peer-net>>;
};

server 0.0.0.0/0 {
     edns no;
};

server ::/0 {
     edns no;
};

options {
      directory "/var/cache/bind";

      recursion yes;
      allow-recursion { acl4;
                        acl6; 
                        127.0.0.1/32; 
      };
      allow-query { acl4;
                    acl6; 
                    ::1/128; 
      };
      listen-on { <<ipv4-man>>;
                  <<ipv4-peer>>; 
      };
      listen-on-v6 { <<ipv6-man>>;
                     <<ipv6-peer>>; 
      };
      allow-transfer { none; };
      dnssec-validation no;
      auth-nxdomain no;    # conform to RFC1035

      forwarders {
      <<resolvers>>
      };
};

logging {
    channel "bind_log" {
        file "/var/log/named/bind9.log" versions 2 size 50m;
        # severity (one of: critical | error | warning | notice |
        #                   info | debug [ level ] | dynamic );
        severity debug 2;
        print-time yes;
        print-severity yes;
        print-category yes;
    };
    #category general { bind_log; };
    #category default { bind_log; };
    category queries { bind_log; };
    category resolver { bind_log; };
    #category delegation-only { bind_log; };
    #category network { bind_log; };
    #category notify { bind_log; };
    #category security { bind_log; };
    #category dispatch { bind_log; };
    #category client { bind_log; };
};
