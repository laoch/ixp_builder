#!/bin/bash

### ixp-install.sh: Installer script for IXP Builder. ###

# author     : "Diarmuid O'Briain"
# copyright  : "Copyright 2019, C²S Consulting"
# license    : "European Union Public Licence v1.2"
# version    : "Phase 1, version 2.2"

# // Declarations //

DIRECTORIES=('/opt/ixp/' '/var/ixp/' '/var/ixp/code/' '/var/log/ixp/')
APPLICATIONS=('python3' 'python3-pip' 'bind9' 'bind9-doc' 'bind9utils' 'bird'  
              'dnsutils' 'fping' 'lxd' 'mtr' 'snmp' 'vlan' 'python3-lxc')
PACKAGES=('pyroute2' 'python-apt' 'geocoder' 'validators')
LOGFILE='/var/log/ixp/ixp.log'

echo -e "\nSetup script for IXP builder package"
printf "%0.s-" {1..36}; echo


###################### // TEMP WHILE DEVELOPING // ####################

# // Delete directories //

echo "Removing directories temporarily."
for dirs in "${DIRECTORIES[@]}"; do
   if [ -d "$dirs" ]; then
       rm -r "$dirs"
   fi
done

groupdel ixp

###################### // TEMP // #########################


# // Check if the script is being ran by root user //

USER=$(whoami)
 
if [ "$USER" != 'root' ]
then
    echo -e "\nThis script must be ran using 'sudo' not $USER\n\n$ sudo $0\n"
    exit 1
fi

# // Create IXP group //
 
if [ $(getent group 'ixp') ]; then
    echo "IXP group 'ixp' already exists."
else
    echo "Creating group 'ixp'."
    groupadd ixp
fi

if [ $(cat '/etc/group' | grep ixp | grep $SUDO_USER) ]; then
    echo "$SUDO_USER is already part of the 'ixp' group."
else
    echo "Modifying user '$SUDO_USER' to add to the group 'ixp'."
    usermod -a -G ixp "$SUDO_USER"
fi

# // Make directories and change ownerships //

for dirs in "${DIRECTORIES[@]}"; do
   if [ -d "$dirs" ]; then
       echo "Directory $dirs already exists."
       ownership=$(stat -c "%U:%G" "$dirs")
       if [ "$ownership" != 'root:ixp' ];then
           echo "The ownership of the directory $dirs is $ownership, stopping install."
       fi
   else
       echo "Creating the directory $dirs."
       mkdir -p "$dirs"
       chown root:ixp "$dirs" 
       if [[ "$dirs" =~ ^/var/* ]]; then
           chmod 770 "$dirs" 
       else      
           chmod 550 "$dirs"    
       fi
   fi
done

#: <<'TEMPBLOCK'

# // Install applications //

echo "Installing GNU/Linux applications."
for apps in "${APPLICATIONS[@]}"; do
    echo "  - Installing $apps."
    apt-get -y install "$apps"
done

# // Install python packages //

echo "Installing Python3 packages"
for packs in "${PACKAGES[@]}"; do
    echo "  - Installing $packs."
    sudo -H python3 -m pip install "$packs"
done

#TEMPBLOCK

# // Copy files to directories //

# // Main IXP files //

# // /opt/ixp/ files //

cp '../files/ixp' '/opt/ixp/'
chown root:ixp '/opt/ixp/ixp'
# //temp while developing
#chmod 750 '/opt/ixp/ixp'
chmod 770 '/opt/ixp/ixp'

# // /var/ixp/templates files //

cp ../files/templates/* '/var/ixp/'
cp '/var/ixp/ixp_schema_template.yaml' '/var/ixp/ixp_schema.yaml'
chown -R root:ixp '/var/ixp/'
# //temp while developing
#chmod -R 750 '/var/ixp/'
chmod -R 770 '/var/ixp/'

# // /var/ixp/code files //

cp ../files/code/* '/var/ixp/code/'
chown -R root:ixp '/var/ixp/'
# //temp while developing
#chmod -R 750 '/var/ixp/code/'
chmod -R 770 '/var/ixp/code/'

# // link for main file //

if [ -L '/usr/local/bin/ixp' ]; then
    echo "IXP softlink already exists."
else
    echo "Creating IXP softlink in /usr/local/bin/ixp."
    ln -s '/opt/ixp/ixp' '/usr/local/bin/ixp'
fi
chown root:ixp '/usr/local/bin/ixp'

# // Python3 IXP package //

py3_site=$(python3 -m site | grep '/usr/local/lib' | awk -v FS="('|')" '{print $2}')
echo "Copying ixp.py package to $py3_site."
cp '../files/ixp.py' "$py3_site"
chown root:ixp "$py3_site/ixp.py"
# //temp while developing
#chmod 550 "$py3_site/ixp.py"
chmod 660 "$py3_site/ixp.py"

# // LXD init //

# // Preseed YAML file for LXD //

cat <<EOF | lxd init --preseed
config: {}
cluster: null
networks: []
storage_pools:
- config: {}
  description: ""
  name: default
  driver: dir
profiles:
- config: {}
  description: ""
  devices:
    root:
      path: /
      pool: default
      type: disk
  name: default

EOF

echo "LXD Hypervisor preceeded."

# // LXC profile //

# // Create Dual NIC profile for containers //
lxc profile create dualnic

cat <<EOF | lxc profile edit dualnic
# // Create default LXD profile //

### This is a yaml representation of the profile.
# Added for DualNIC configuration.

config: {}
description: LXD Dual NIC profile
devices:
  ens3:
    name: ens3
    nictype: bridged
    parent: br100
    type: nic
  ens4:
    name: ens4
    nictype: bridged
    parent: br900
    type: nic
  root:
    path: /
    pool: default
    type: disk
name: dualnic
used_by: []

EOF

echo "LXC Dualnic profile created."

# // Change ownership of the ~/.config/lxc directory //
chown -R $SUDO_USER: /home/$SUDO_USER/.config/lxc

# End

