#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""ixp_Server builder: Builds IXP on LXD/LXC platform."""

__author__      = "Diarmuid O'Briain"
__copyright__   = "Copyright 2019, C²S Consulting"
__license__     = "European Union Public Licence v1.2"
__version__     = "Phase 1.0, version 2.2"

#
# Copyright 2019 C²S Consulting
# 
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved 
# by the European Commission - subsequent versions of the EUPL (the "Licence");
#
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
# 
# https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
# 
# Unless required by applicable law or agreed to in writing, software distributed
# under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR 
# CONDITIONS OF ANY KIND, either express or implied.
# 
# See the Licence for the specific language governing permissions and limitations 
# under the Licence.
#

'''
Notes: Traditional IXP site with virtualised backend.

'''

from grp import getgrgid
from operator import itemgetter
from os import listdir
from os import path
from os import remove
from pwd import getpwuid
from pyroute2 import IPRoute
import datetime
import geocoder
import inspect
import ipaddress
import math
import os
import re
import requests
import subprocess
import sys
import tempfile
import time
import validators
import yaml

# // Global variable declarations //

container_yaml_template = 'ixp_container_template.yaml'
gai_file = '/etc/gai.conf'
help_list = {'?', 'help'}
ixp_servers = {'lxd':'200', 'ns':'201', 'cs':'203', 'rs':'202', 'bs':'204'}
local_host = subprocess.getstatusoutput('hostname')[1] 
log_dir = '/var/log/ixp/'
netplan_dir = '/etc/netplan/'
server_template_bak = 'ub-18-04-bak'
netplan_file = '/etc/netplan/10-netplan.yaml'
no_actual_download_test = False
public_ipv6_net = False
schema_yaml_file = 'ixp_schema.yaml'
schema_yaml_template = 'ixp_schema_template.yaml'
server_template = ('ub-18-04', 'ubuntu:18.04')
site_info_yaml_file = 'ixp_site_info.yaml'
site_info_yaml_template = 'ixp_site_info_template.yaml'
test_websites = ['http://www.google.com',
                 'http://www.ubuntu.com'
                ]
bird_root = '/etc/bird/'
bird_file = 'bird.conf'
bird6_file = 'bird6.conf'
upstream_ns = ('8.8.8.8', '8.8.8.4')   # Quad9: PCH/IBM/GCA  Quad8: Google
var_dir = '/var/ixp/'
file_perm = 770

# // Test if this file is being ran directly //

if (__name__ == '__main__'):
    print('This module cannot be run directly')
    sys.exit(1)

##############################################
# //              FUNCTIONS               // #
##############################################

#============================================#
# //       _function_call_location        // #
#============================================#

def _function_call_location():
    ''' Checks if a function is called via module or locally '''
    if (inspect.stack()[2][3] == '<module>'):
        print('Sneaky attempt to access internal function.\n')
        exit(1)    

# End _function_call_location

#============================================#
# //       _ixp_timestamp function        // #
#============================================#

def _ixp_timestamp():
    ''' Presents current time, date and timestamp when requested '''

    _function_call_location()
 
    ttime = '{:%H:%M:%S}'.format(datetime.datetime.now())
    tdate = '{:%Y%m%d}'.format(datetime.datetime.now())
    tstamp = '{:%Y%m%d-%H%M%S}'.format(datetime.datetime.now())

    return (ttime, tdate, tstamp)

# End _ixp_timestamp function

#============================================#
# //     _get_file_dir_info function      // #
#============================================#

def _get_file_dir_info(arg):
    ''' Get information from file or directory '''

    _function_call_location()
 
    # // Variables //
    l = list()
    d = dict()

    # showing stat information for files
    d['name'] = arg

    # Get type
    if (os.path.isdir(arg)):
            d['type'] = 'directory'
    elif (os.path.isfile(arg)):
            d['type'] = 'file'
    else:
            d['type'] = 'other'

    # Get owner and group
    statinfo = os.stat(arg)
    d['uid'] = statinfo.st_uid
    d['gid'] = statinfo.st_gid

    d['owner'] = getpwuid(statinfo.st_uid).pw_name
    d['group'] = getgrgid(statinfo.st_gid).gr_name

    # Get permissions
    d['permissions'] = oct(statinfo.st_mode)[-3:]

    # Return dictionary
    return(d)

# End _get_file_dir_info function 

#============================================#
# //         _ixp_debug function          // #
#============================================#

def _ixp_debug(dateyn, err_msg):
    ''' Debug function, writes logs to the daily logfile '''

    _function_call_location()

    (ttime, tdate, tstamp) = _ixp_timestamp()
    log_file = f'{log_dir}{tdate}-ixp.log'
    try:
        if (dateyn == 'y'):
            l_file = open(log_file, 'a')
            l_file.write (f'{tstamp}: {err_msg}\n')
        else:
            l_file = open(log_file, 'a')
            l_file.write (f'  {err_msg}\n')
    except:
        print(f'Cannot open the {log_file}')
        sys.exit(1)

    l_file.close()

    return

# End _ixp_debug function

#============================================#
# //         _ixp_schema_servers          // #
#============================================#

def _ixp_schema_servers():
    ''' Extract a list of servers from the IXP Schema '''

    _function_call_location()

    schema_yaml = _read_schema_yaml()
    e = list(schema_yaml[1].keys())
    return(e)

# End _ixp_schema_servers

#============================================#
# //           _validate_ip_net           // #
#============================================#

def _validate_ip_net(a):
    ''' Validate IP Networks and IP addresses to ensure YAML file is clean '''

    _function_call_location()
 
    _ixp_debug('y', f'INFO[validate_ip_net]: Validating: {a}')

    ip_dict = dict()
    
    # // Split away the mask and determine version //

    try:
        ver = ipaddress.ip_address(a.split('/')[0]).version
        ip = ipaddress.ip_interface(a)
        ip_dict['ver'] = ver
        ip_dict['addr'] = ip.with_prefixlen
        ip_dict['ip'] = str(ip.ip)
        ip_dict['prefix'] = a.split('/')[1]
        ip_dict['net'] = str(ip.network)
        _ixp_debug('y', f'INFO[validate_ip_net]: Address {a} validated') 

    except: 
        print(f'ERROR[validate_ip_net]: {a} is not a proper IP address format.\n')
        _ixp_debug('y', f'ERROR[validate_ip_net]: {a} is not a proper IP address format.')
        sys.exit(1)

    return(ip_dict)

# End validate_ip_net function

#============================================#
# //           _read_schema_yaml          // #
#============================================#

def _read_schema_yaml():
    ''' Function to read the YAML file of the IP Schema'''

    _function_call_location()

    # // Open configuration file //

    schema_yaml_path_file = f'{var_dir}{schema_yaml_file}'

    # Test if file exists and open
    exists = path.isfile(schema_yaml_path_file)
    if exists:
        file = open(schema_yaml_path_file, 'r')
    else:
        t_file = f'{var_dir}{schema_yaml_template}'
        subprocess.getstatusoutput(f'cp {t_file} {schema_yaml_path_file}')
        subprocess.getstatusoutput(f'chmod {file_perm} {schema_yaml_path_file}')
        file = open(schema_yaml_path_file, 'r')

    # // Load yaml file details //
    _ixp_debug('y', f'INFO[_read_schema_yaml]: Importing configuration from {schema_yaml_file}')
 
    yaml_schema_docs = list(yaml.load_all(file))

    # // The yaml file loads as two lists, the first contains the     //
    # // filename, the second is a dictionary where each IXP server   //
    # // is the key, each dictionary contains a list of dictionaries  //  
    # // with a key and associated IP addresses and masks.            //  
    # // i.e. print(yaml_schema_docs[1]['nms1'][1]['ipv6-peer'])      // 
    # //      2a99:9:9::205/48                                        //

    # // Return schema info //

    return(yaml_schema_docs) 

# End _read_schema_yaml function

#============================================#
# //         _read_site_info_yaml         // #
#============================================#

def _read_site_info_yaml():
    ''' Function to read the YAML file of the Site Info'''

    _function_call_location()

    site_info_yaml_path_file = f'{var_dir}{site_info_yaml_file}'

    # Test if file exists and open
    exists = path.isfile(site_info_yaml_path_file)
    if exists:
        file = open(site_info_yaml_path_file, 'r')
    else:
        t_file = f'{var_dir}{site_info_yaml_template}'
        subprocess.getstatusoutput(f'cp {t_file} {site_info_yaml_path_file}')
        file = open(site_info_yaml_path_file, 'r')

    # // Load yaml file details //
    _ixp_debug('y','INFO[read_site_info_yaml]: Importing configuration from {site_info_yaml_file}')
 
    yaml_site_info_docs = list(yaml.load_all(file))

    # // Return the site_info //
    return(yaml_site_info_docs) 

# End site_info_yaml function

#============================================#
# //        _string_search_replace        // #
#============================================#

def _string_search_replace(filein, searchstr, replacestr):
    ''' Function to open a file, search and replace and output to another file'''

    _function_call_location()

    # // Confirm supplied values are in fact strings //   
    searchstr = str(searchstr)
    replacestr = str(replacestr)

    tf = tempfile.NamedTemporaryFile()
    temp_file_name = tf.name
    tf.close()

    try:
        fi = open(filein, 'r')

    except:
        print(f'ERROR: Cannot open {filein}')
        _ixp_debug('y', f'ERROR[_string_search_replace]: Cannot open {filein}')
        sys.exit(1)

    try:
        fo = open(temp_file_name, 'w')

    except:
        print(f'ERROR: Cannot open {temp_file_name}')
        _ixp_debug('y', f'ERROR[_string_search_replace]: Cannot open {filein}')
        sys.exit(1)

    fo.write(fi.read().replace(searchstr, replacestr))

    _ixp_debug('y', f'INFO[_string_search_replace]: Replacing {searchstr} with {replacestr} in {filein}')

    fi.close()
    fo.close()

    subprocess.getstatusoutput(f'mv {temp_file_name} {filein}')
    subprocess.getstatusoutput(f'chmod {file_perm} {filein}')

# End _string_search_replace function

#============================================#
# //             _ip_link_list            // #
#============================================#

def _ip_link_list():
    ''' Function to list the available interfaces on the host computer '''

    _function_call_location()

    ip_links = dict()
    ipr = IPRoute()

    for link in ipr.get_links():
        ifname = link.get_attr('IFLA_IFNAME')
        linkinfo = link.get_attr('IFLA_LINKINFO')
        ip_links[ifname] = linkinfo

    #// Return list of dictionaries with the IFNAME and LINKINFO //

    return(ip_links)

# End _ip_link_list function

#============================================#
# //              _gai_build              // #
#============================================#

def _gai_build():
    ''' Function to Get Address Information, default Address Selection for IPv6 (rfc3484) '''

    _function_call_location()

    _ixp_debug('y','INFO[_ixp_host_build]: Network does not have an IPv6 public connection')

    gai_file_bu = f'{gai_file}.orig'

    # // Create temporary file for GAI //

    tf = tempfile.NamedTemporaryFile()
    temp_file_name = tf.name
    tf.close()
 
    try:
        gaifh = open(temp_file_name, 'w')
    except:
        print(f'ERROR[gai_build]: Cannot open {temp_file_name}')
        _ixp_debug('y', f'ERROR[gai_build]: Cannot open {temp_file_name}')
        sys.exit(1)

    gaifh.write('# Prioritise IPv4 over IPv6 for DNS requests\n')
    gaifh.write('# (No IPv6 public address for testbed lab)\n\n')
    gaifh.write('scopev4 ::ffff:169.254.0.0/112  2\n')
    gaifh.write('scopev4 ::ffff:127.0.0.0/104    2\n')
    gaifh.write('scopev4 ::ffff:0.0.0.0/96       14\n')
    gaifh.write('precedence ::ffff:0:0/96        100\n')
    gaifh.close()

    return(temp_file_name)

# End _gai_build function

##############################################
# //             IxpHelp class            // #
##############################################

class IxpHelp:
    ''' IXP Help class '''

    def __init__(self):
        ''' IxpHelp class constructor method '''
        pass

    #============================================#
    # //                ixp_help              // #
    #============================================#

    def ixp_help(self, err_code = 0):
        ''' ixp help method ''' 

        print('''Usage: ixp { help | ? } [OPTION] ... ...

        help, ?    - This help message.

        OPTION [list]:
        version   - Returns the code version of the ixp.py module.
        schema    - Builds an IP Schema for the testbed in a YAML file.
        host      - Builds configuration for the host.
        server    - Operations on the IXP container servers.
        software  - Installation and configuration of software on the 
                    IXP container servers.
        peer      - Management of IXP peers.
        route     - Review IXP container server routes.
        ''')
        _ixp_debug('y', '\'ixp help\' called.')
        # // REMOVE : Testing isp_debug() function //
        _ixp_debug('y','Debug test from ixp_help')
        (ttime, tdate, tstamp) = _ixp_timestamp()
        print(f'Time: ttime: {ttime} | tdate: {tdate} | tstamp: {tstamp}')

        sys.exit(err_code)

    ## End ixp_help 

    #============================================#
    # //            _ixp_schema_help          // #
    #============================================#

    def _ixp_schema_help(self, err_code = 0):
        ''' ixp schema help method '''

        print('''Usage: ixp schema [OPTION]

        ?, help  -  This help message.

        OPTION [list]:
        build          -  Build IP Scheme with default values.
        default        -  Default the IP Schema and site info.
        show           -  Show the IP Scheme that currently exists.
        ?, help        -  This help message.
        ''')
        _ixp_debug('y', '\'ixp schema help\' called.')
        sys.exit(err_code)

    ## End _ixp_schema_help 

    #============================================#
    # //        _ixp_schema_show_help         // #
    #============================================#

    def _ixp_schema_show_help(self, err_code = 0):
        ''' ixp schema show help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp schema show {{ help | ? }} [OPTION(s)] [OBJECT(s)]

        ?, help  -  This help message.

        OPTION [list]:
        all     -  Show all IXP Site and IXP Schema information.
        both    -  Same as 'all'.
        site    -  Get IXP site information.
        schema  -  Get IXP Schema information.

        OBJECT(s) [list]:
        {e[1]}  -  Show Name Server.
        {e[2]}  -  Show Route Collector.
        {e[3]}  -  Show Route Server.
        {e[4]}  -  Show AS112 Blackhole Server.
        ''')


        _ixp_debug('y', '\'ixp schema show help\' called.')
        sys.exit(err_code)

    ## End _ixp_schema_show_help 

    #============================================#
    # //       _ixp_schema_default_help       // #
    #============================================#

    def _ixp_schema_default_help(self, err_code = 0):
        ''' ixp schema default help method '''

        print('''Usage: ixp schema default [OPTION]

        ?, help  -  This help message.

        OPTION [list]:
        all    -  Default the both IP Schema and Site info.
        both   -  Same as 'all'.
        schema -  Default the IP Schema only.
        site   -  Default the Site info only.
        ?, help        -  This help message.
        ''')
        _ixp_debug('y', '\'ixp schema default help\' called.')
        sys.exit(err_code)

    ## End _ixp_schema_help 

    #============================================#
    # //      _ixp_schema_show_site_help      // #
    #============================================#

    def _ixp_schema_show_site_help(self, err_code = 0):
        ''' ixp schema show site help method '''

        # // Get site_info keys //
        s = list(_read_site_info_yaml()[1].keys())

        print(f'''Usage: ixp schema show site {{ help | ? }} [OBJECT(s)]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        {s[0]:<12}-  Show Country.
        {s[1]:<12}-  Show Town.
        {s[2]:<12}-  Show Elevation.
        {s[3]:<12}-  Show Organisation.
        {s[4]:<12}-  Show Location.
        ''')
        _ixp_debug('y', '\'ixp schema show site help\' called.')
        sys.exit(err_code)

    ## End _ixp_schema_show_help 

    #============================================#
    # //        _ixp_schema_build_help        // #
    #============================================#

    def _ixp_schema_build_help(self, err_code = 0):
        ''' ixp schema build help method '''

        print('''Usage: ixp schema build { help | ? } [ARGUMENT(s)] [VALUE]

        ?, help  -  This help message.

       ARGUMENT(s) [list]:
        -sn, --site-number  -  Site Number (i.e. 1-100).
        -o,  --organisation -  Organisation (i.e. netLabs!UG).
        -p4, --ipv4-peer    -  IPv4 Peering LAN (i.e. 199.9.9.0/24).
        -p6, --ipv6-peer    -  IPv6 Peering LAN (i.e. 2a99:9:9::/48).
        -m4, --ipv4-man     -  IPv4 Management LAN (i.e. 198.8.8.0/24).
        -m6, --ipv6-man     -  IPv6 Management LAN (i.e. 2a98:8:8::/48).
        -d,  --domain       -  IXP Domain name (i.e. netlabs.tst).
        -as, --as-number    -  IXP Autonomous System number (i.e. 5999).
        -c,  --country      -  Country IXP is located in (i.e. Uganda).
        -t,  --town         -  Town/city IXP is located in (i.e. Kampala).
        -e,  --elevation    -  IXP elevation (m) above sea level (i.e. 1072).
        -la, --latitude     -  Deg, min, sec, N|S (i.e. '00 20 51.3456 N').
        -lo, --longitude    -  Deg, min, sec, E|W (i.e. '32 34 57.0720 E').
        ''')
        _ixp_debug('y', '\'ixp schema build help\' called.')
        sys.exit(err_code)

    ## End _self.ixp_schema_builder_help 

    #============================================#
    # //             _ixp_host_help           // #
    #============================================#

    def _ixp_host_help(self, err_code = 0):
        ''' ixp host help method '''

        print('''Usage: ixp host { help | ? } [OPTIONS]

        ?, help                      -  This help message.

        OPTIONS [list]:
        list interfaces              -  Returns a list of interfaces.
        build interface <interface>  -  Build host based on <interface> gateway.
        ''')
        _ixp_debug('y', '\'ixp host help\' called.')
        sys.exit(err_code)

    ## End _ixp_host_help 

    #============================================#
    # //            _ixp_server_help          // #
    #============================================#

    def _ixp_server_help(self, err_code = 0):
        ''' ixp server help method '''

        print('''Usage: ixp server { help | ? } [OPTIONS]

        ?, help  -  This help message.

        OPTIONS [list]:
        build    -  Build IXP Servers.
        show     -  Show the state of an IXP Server.
        start    -  Bring an IXP Server to a state of 'Running'.
        stop     -  Bring an IXP Server to a state of 'Stopped'.
        delete   -  Erase an IXP Server.
        ''')
        _ixp_debug('y', '\'ixp server help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_help

    #============================================#
    # //        _ixp_server_build_help        // #
    #============================================#

    def _ixp_server_build_help(self, err_code = 0):
        ''' ixp server build help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp server build {{ help | ? }} [ARGUMENT] [OBJECT(s)]

        ?, help  -  This help message.
       
        ARGUMENT
        -y       -  Skip the yes/no questions.

        OBJECT(s) [list]:
        all      -  Build all IXP Servers.
        {e[1]}      -  Build Name Server.
        {e[2]}      -  Build Route Collector.
        {e[3]}      -  Build Route Server.
        {e[4]}      -  Build AS112 Blackhole Server.
        ''')
        _ixp_debug('y', '\'ixp server build help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_build_help 

    #============================================#
    # //        _ixp_server_show_help         // #
    #============================================#

    def _ixp_server_show_help(self, err_code = 0):
        ''' ixp server show help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp server show {{ help | ? }} [OBJECT(s)]

        OBJECT(s) [list]:
        all   -  Display the status of all IXP Servers.
        {e[1]}   -  Display the status of the Name Server.
        {e[2]}   -  Display the status of the Route Collector.
        {e[3]}   -  Display the status of the Route Server.
        {e[4]}   -  Display the status of the AS112 Blackhole Server.
        ''')
        _ixp_debug('y', '\'ixp server show help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_show_help

    #============================================#
    # //        _ixp_server_start_help        // #
    #============================================#

    def _ixp_server_start_help(self, err_code = 0):
        ''' ixp server start help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp server start {{ help | ? }} [OBJECT(s)]

        OBJECT(s) [list]:
        all   -  Start all IXP Servers.
        {e[1]}   -  Start the Name Server.
        {e[2]}   -  Start the Route Collector.
        {e[3]}   -  Start of the Route Server.
        {e[4]}   -  Start the AS112 Blackhole Server.
        ''')
        _ixp_debug('y', '\'ixp server start help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_start_help

    #============================================#
    # //        _ixp_server_stop_help         // #
    #============================================#

    def _ixp_server_stop_help(self, err_code = 0):
        ''' ixp server stop help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp server stop {{ help | ? }} [OBJECT(s)]

        OBJECT(s) [list]:
        all   -  Stop all IXP Servers.
        {e[1]}   -  Stop the Name Server.
        {e[2]}   -  Stop the Route Collector.
        {e[3]}   -  Stop of the Route Server.
        {e[4]}   -  Stop the AS112 Blackhole Server.
        ''')
        _ixp_debug('y', '\'ixp server stop help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_stop_help

    #============================================#
    # //       _ixp_server_delete_help        // #
    #============================================#

    def _ixp_server_delete_help(self, err_code = 0):
        ''' ixp server delete help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp server delete {{ help | ? }} [ARGUMENT] [OBJECT(s)]

        ARGUMENT
        -y       -  Skip the yes/no questions 
                    (Only necessary for 'delete' OPTION).

        OBJECT(s) [list]:
        all      -  Delete all IXP Servers.
        {e[1]}      -  Delete the Name Server.
        {e[2]}      -  Delete the Route Collector.
        {e[3]}      -  Delete the Route Server.
        {e[4]}      -  Delete the AS112 Blackhole Server.
        ''')
        _ixp_debug('y', '\'ixp server delete help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_delete_help

    #============================================#
    # //          _ixp_software_help          // #
    #============================================#

    def _ixp_software_help(self, err_code = 0):
        ''' ixp software help method '''

        print('''Usage: ixp software { help | ? } [OPTION]

        ?, help  -  This help message.

        OPTIONS [list]:
        install   -  Install required software on IXP Servers.
        configure -  Configure required software on IXP Servers.
        ''')
        _ixp_debug('y', '\'ixp software help\' called.')
        sys.exit(err_code)

    ## End _ixp_software_help

    #============================================#
    # //      _ixp_software_install_help      // #
    #============================================#

    def _ixp_software_install_help(self, err_code = 0):
        ''' ixp software install help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp software install {{ help | ? }} [OBJECT(s)]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all   -  Install software on all IXP Servers.
        {e[1]}   -  Install software on Name Server.
        {e[2]}   -  Install software on Route Collector.
        {e[3]}   -  Install software on Route Server.
        {e[4]}   -  Install software on AS 112 Server.
        ''')
        _ixp_debug('y', '\'ixp software install help\' called.')
        sys.exit(err_code)

    ## End _ixp_software_install_help

    #============================================#
    # //     _ixp_software_configure_help     // #
    #============================================#

    def _ixp_software_configure_help(self, err_code = 0):
        ''' ixp software configure help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp software config(ure) {{ help | ? }} [OBJECT(s)]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all   -  Configure software on all IXP Servers.
        {e[1]}   -  Configure software on the Name Server.
        {e[2]}   -  Configure software on the Route Server.
        {e[3]}   -  Configure software on the Route Collector.
        {e[4]}   -  Configure software on the AS 112 Server.
        ''')
        _ixp_debug('y', '\'ixp software configure help\' called.')
        sys.exit(err_code)

    ## End _ixp_software_configure_help

    #============================================#
    # //            _ixp_peer_help            // #
    #============================================#

    def _ixp_peer_help(self, err_code = 0):
        ''' ixp peer help method '''

        print('''Usage: ixp peer { help | ? } [OPTIONS]

        ?, help - This help message.

        OPTIONS [list]:
        add    -  Add peers to the IXP Servers.
        delete -  Delete peers to the IXP Servers. 
        list   -  List peers to the IXP Servers.
        route  -  List routes on the IXP Servers.
        ''')
        _ixp_debug('y', '\'ixp peer help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_help 

    #============================================#
    # //          _ixp_peer_add_help          // #
    #============================================#

    def _ixp_peer_add_help(self, err_code = 0):
        ''' ixp peer add help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp peer add {{ help | ? }} [OBJECT(s)] [VALUE]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all   -  Configure software on all IXP Servers.
        {e[2]}   -  Configure software on the Route Server.
        {e[3]}   -  Configure software on the Route Collector.
        {e[4]}   -  Configure software on the AS 112 Server.
        Note: Comma separated list permitted, i.e. : {e[2]}, {e[3]}

        ARGUMENT(s) [list]:
        -n, --name  - Remote Autonomous System Name. 
        -a' --asn   - Remote Autonomous System Number.
        -4, --ip    - Remote ISP peer IPv4 address.
        -6, --ipv6  - Remote ISP peer IPv6 address.
        ''')
        _ixp_debug('y', '\'ixp peer add help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_add_help

    #============================================#
    # //        _ixp_peer_delete_help         // #
    #============================================#

    def _ixp_peer_delete_help(self, err_code = 0):
        ''' ixp peer delete help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp peer delete {{ help | ? }} [OBJECT(s)] [VALUE]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all   -  Configure software on all IXP Servers.
        {e[2]}   -  Configure software on the Route Server.
        {e[3]}   -  Configure software on the Route Collector.
        {e[4]}   -  Configure software on the AS 112 Server.
        Note: Comma separated list permitted, i.e. : {e[2]}, {e[3]}

        ARGUMENT(s) [list]:
        -n, --name  - Remote Autonomous System Name. 
        -a' --asn   - Remote Autonomous System Number.
        ''')
        _ixp_debug('y', '\'ixp peer delete help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_delete_help

    #============================================#
    # //         _ixp_peer_list_help          // #
    #============================================#

    def _ixp_peer_list_help(self, err_code = 0):
        ''' ixp peer list help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp peer list {{ help | ? }} [OBJECT(s)]  [DETAIL]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all   -  Show protocols on RS, CS and BS IXP Servers.
        {e[2]}   -  Show protocols on the Route Server.
        {e[3]}   -  Show protocols on the Route Collector Server.
        {e[4]}   -  Show protocols on the AS112 Blackhole Server.
        Note: Comma separated list permitted, i.e. : {e[2]}, {e[3]}

        DETAIL [list]:
        terse   - Terse list with name, ASN and IP information.
        detail  - Detailed list including state and information.

        ''')
        _ixp_debug('y', '\'ixp peer list help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_list_help method

    #============================================#
    # //         _ixp_peer_route_help         // #
    #============================================#

    def _ixp_peer_route_help(self, err_code = 0):
        ''' ixp peer route help method '''

        e = _ixp_schema_servers()

        print(f'''Usage: ixp peer route {{ help | ? }} [ARGUMENT(s)] [VALUE]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all   -  Configure software on RS, CS and BS IXP Servers.
        {e[2]}   -  Configure software on the Route Server.
        {e[3]}   -  Configure software on the Route Collector Server.
        {e[4]}   -  Configure software on the AS112 Blackhole Server.
        Note: Comma separated list permitted, i.e. : {e[2]}, {e[3]}

        ARGUMENT(s) [list]:
        -4, --ip    - IPv4 route table.
        -6, --ipv6  - IPv6 route table.
        ''')
        _ixp_debug('y', '\'ixp peer route help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_route_help method

############ // End IxpHelp class // ############

#################################################
# //              _IxpLxc class              // #
#################################################

class _IxpLxc:
    ''' IXP LXC class '''

    def __init__(self):
        ''' IxpLxc class constructor method '''

        pass

    ## End of __init__ method

    #============================================#
    # //              _lxc_exec               // #
    #============================================#

    def _lxc_exec(self, c, command):
        ''' Method to access lxc exec '''

        cmd = f'lxc exec {c} -- {command}'

        _ixp_debug('y', f'INFO[_lxc_exec]: Executing: {cmd}')

        output = subprocess.getstatusoutput(cmd)

        if (output[0] == 255):
            print(f'Bad command: \'{cmd}\'')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Bad command: \'{cmd}\'')
            sys.exit(1)

        elif (output[0] == 1):
            _ixp_debug('y', f'ERROR[_lxc_exec]: Container error, cannot connect to: \'{c}\'')
            sys.exit(1)

        elif (output[0] in range(2,254)):
            print(f'Unknown lxc_exec error: {output}')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Unknown lxc_exec error: {output}')
            sys.exit(1)

        elif (not output[0] == 0):
            print(f'ERROR: Catch-all for unknown lxc_exec error: {output}')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Catch-all for unknown lxc_exec error: {output}')
            sys.exit(1)

        else:
            _ixp_debug('y', f'INFO[_lxc_exec]: \"{cmd}\" executed successfully')

        return (output)

    ## End _lxc_exec method

    #============================================#
    # //              _lxc_start              // #
    #============================================#

    def _lxc_start(self, c):
        ''' Method to start an LXC container '''

        subprocess.getstatusoutput(f'lxc start {c}')
        _IxpLxc._lxc_container_boot_delay(self)

        ixp_lxc_status, ixp_lxc_state = _IxpLxc._lxc_status(self, c)

        _ixp_debug('y', f'INFO[_lxc_start]: {c} container started')

        return (ixp_lxc_status)

    ## End _lxc_start method 

    #============================================#
    # //              _lxc_stop               // #
    #============================================#

    def _lxc_stop(self, c):
        ''' Method to stop an LXC container '''

        subprocess.getstatusoutput(f'lxc stop {c} --force')
        _IxpLxc._lxc_container_boot_delay(self)

        ixp_lxc_status, ixp_lxc_state = _IxpLxc._lxc_status(self, c)

        _ixp_debug('y', f'INFO[_lxc_stop]: {c} container stopped')

        return (ixp_lxc_status)

    ## End _lxc_stop method

    #============================================#
    # //             _lxc_status              // #
    #============================================#

    def _lxc_status(self, c):
        ''' Method to get the status of an LXC container'''

        lxd_status_check = subprocess.getstatusoutput(f'lxc info {c}')

        if (lxd_status_check[0] == 0):
            server_status = re.findall('\nStatus: (.*)\n', lxd_status_check[1])
            _ixp_debug('y', f'INFO[_lxc_status]: {c} container exists in a \'{server_status[0]}\' state')
            return (lxd_status_check[0], server_status[0])

        else: 
            _ixp_debug('y', f'INFO[_lxc_status]: {c} container does not exist')
            return (lxd_status_check[0], 'Nil')

        return(0)

    ## End _lxc_status method

    #============================================#
    # //            _lxc_status_up            // #
    #============================================#

    def _lxc_status_up(self, c): 
        ''' Method to get the status of an LXC container, if 'Stopped bring up '''

        ixp_lxc_start_status = ''
        ixp_lxc_status = ''

        lxd_status_check = subprocess.getstatusoutput(f'lxc info {c}')

        if (lxd_status_check[0] == 0):
            ixp_lxc_start_status = re.findall('\nStatus: (.*)\n', lxd_status_check[1])[0]
            ixp_lxc_status = ixp_lxc_start_status

            if not (ixp_lxc_status == 'Running'):
                _ixp_debug('y', f'INFO[_lxc_status_up]: {c} container exists in a \'{ixp_lxc_status}\' state, starting {c}')
                _IxpLxc._lxc_start(self, c)
                _IxpLxc._lxc_container_boot_delay(self)

                ixp_lxc_status = 'Running'
              
            return(ixp_lxc_start_status, ixp_lxc_status)

        else: 
            print(f'ERROR: {c} container does not exist, stopping\n')
            _ixp_debug('y', f'ERROR[_lxc_status_up]: {c} container does not exist, stopping')
            sys.exit(1)

            return(1,1)

    ## End _lxc_status_up method 

    #============================================#
    # //             _lxc_delete              // #
    #============================================#

    def _lxc_delete(self, skip_yes_no, c):
        ''' Method to delete an LXC container '''

        files_list = listdir(var_dir)

        # // Check if it is necessary to delete an non-existant container //
        if (_IxpLxc._lxc_status(self, c)[1] == 'Nil'):
            delete_flag = False

        # // Delete the file //
        if not (_IxpLxc._lxc_status(self, c)[1] == 'Nil'):

            if (skip_yes_no == False):
                cli_response = input(f'Deleting existing \'{c}\' container, are you sure ? (y|n) > ')
                cli_response = cli_response.lower()
                print('')
            else:
                cli_response = 'yes'

            # // Handle abbreviations //
            for x in ['yes', 'no']:
                if (cli_response == x[:len(cli_response)]): 
                    cli_response = x

            if (cli_response == 'no'):
                print('OK stopping lxc delete process\n')
                _ixp_debug('y','INFO[_lxc_delete]: OK stopping lxc delete process')
                sys.exit(0)

            elif (cli_response == 'yes'):
                _ixp_debug('y', f'INFO[_lxc_delete]: Stopping the {c} container before deleting')
                lxc_stop_check = _IxpLxc._lxc_stop(self, c)

                lxd_delete_check = subprocess.getstatusoutput(f'lxc delete {c} --force')

                # // Remove files associated with the container from /var/ixp //
                for f in files_list:
                    if (c in f):
                        remove(f'{var_dir}{f}')
                        _ixp_debug('y', f'INFO[_lxc_delete]: Deleting the file {var_dir}{f}')

                print(f'{c} container deleted\n')
                _ixp_debug('y', f'INFO[_lxc_delete]: {c} container deleted')

                return (lxd_delete_check[0])

            else:
                print('ERROR[_lxc_delete]: Not an acceptable input, exiting\n')
                _ixp_debug('y','ERROR[_lxc_delete]: Not an acceptable input, exiting')
                sys.exit(1)

        else:
            return (0)
    
    # End _lxc_delete method

    #============================================#
    # //              _lxc_upgrade            // #
    #============================================#
   
    def _lxc_upgrade(self, c):
        ''' Method to upgrade the software on the servers '''

        orig_yaml = f'{var_dir}ixp_{c}.yaml'
        temp_yaml = f'/tmp/ixp_{c}.yaml.tmp'

        # // Check server is running and identify original state //
        (start, current) = _IxpLxc._lxc_status_up(self, c)

        if (no_actual_download_test == False):
            # // Ensure the 'upstream_ns' is being used to prevent timeouts //
            _IxpLxc._lxc_pull(self, c, netplan_file, temp_yaml)
            _IxpLxc._lxc_push(self, c, orig_yaml, netplan_file)
            _IxpLxc._lxc_exec(self, c, 'netplan apply') 

            # // Update software repositories and distribution //
            _ixp_debug('y', f'INFO[_lxc_upgrade]: Updating and upgrading the container {c} from repositories')
            _IxpLxc._lxc_exec(self, c, 'apt-get -y update')
            _IxpLxc._lxc_exec(self, c, 'apt-get -y upgrade')

            # // Restore pre update_upgrade netplan //
            _IxpLxc._lxc_push(self, c, temp_yaml, netplan_file)
            _IxpLxc._lxc_exec(self, c, 'netplan apply') 
            subprocess.getstatusoutput(f'rm {temp_yaml}')
        else:
            print(f'no_actual_download_test is {no_actual_download_test}\n')

        # // Return server to original state //
        if (start == 'Running'):
            _ixp_debug('y', f'INFO[_lxc_upgrade]: Returning container {c} to original \'Running\' state')
        elif (start == 'Stopped'):
            _IxpLxc._lxc_stop(self, c)
            _ixp_debug('y', f'INFO[_lxc_upgrade]: Returning container {c} to original \'Stopped\' state')

        return(0)
    
    ## End of _lxc_upgrade method

    #============================================#
    # //            _lxc_file_test            // #
    #============================================#

    def _lxc_file_test(self, c, remote_file):
        ''' Method to check if lxc file exists '''

        file_test = _IxpLxc._lxc_exec(self, c, f'[ -f \'{remote_file}\' ]; echo $?')

        if (file_test[1] == '0'):
            _ixp_debug('y', f'INFO[_lxc_file_test]: The {c}: {remote_file} file exists')

            return('yes')

        else:
            _ixp_debug('y', f'INFO[_lxc_file_test]: There is no existing {remote_file} file on {c}')

            return('no')

    ## End _lxc_file_test method 

    #============================================#
    # //                _lxc_rm               // #
    #============================================#

    def _lxc_rm(self, c, remote_file):
        ''' lxc_rm function to remove files from container '''

        # // Delete file from container //

        file_test = _IxpLxc._lxc_file_test(self, c, remote_file)

        if (file_test == 'yes'):
            _ixp_debug('y', f'INFO[_lxc_rm]: Removing the {c}: {remote_file}')
            _IxpLxc._lxc_exec(self, c, f'rm {remote_file}')
        else:   
            _ixp_debug('y', f'INFO[_lxc_rm]: {c}: {remote_file} doesn\'t exist')

        return (0)

    ## End _lxc_rm method 

    #============================================#
    # //               _lxc_clone             // #
    #============================================#

    def _lxc_clone(self, skip_yes_no, template_c, new_c):
        ''' lxc_clone method copies the container template to the required container '''

        _ixp_debug('y', f'INFO[_lxc_clone]: Cloning the {new_c} container from {template_c}')
        _IxpLxc._lxc_delete(self, skip_yes_no, new_c)
        print(f'Please be patient as the {new_c} container is cloned from {template_c}\n')
        lxd_clone_check = subprocess.getstatusoutput(f'lxc copy {template_c} {new_c}')
        print(f'{new_c} container cloned from {template_c}\n')
        _ixp_debug('y', f'INFO[_lxc_clone]: {new_c} container cloned from {template_c}')

        return (lxd_clone_check[0])

    ## End _lxc_clone method 

    #============================================#
    # //               _lxc_pull              // #
    #============================================#

    def _lxc_pull(self, c, remote_file, local_file):
        ''' lxc_pull method to pull files from container '''

        # // Delete original file if it exists //        
        if (path.isfile(local_file)): 
            _ixp_debug('y', f'Replacing: {local_file}')
            subprocess.getstatusoutput(f'rm {local_file}')

        # // pull file from container to localhost //
        (m, n, o, p) = (c, remote_file, local_host, local_file)
        _ixp_debug('y', f'INFO[_lxc_pull]: lxc file pull\n  <-- {m}:{n}\n  --> {o}:{p}')

        output = subprocess.getstatusoutput(f'lxc file pull {c}{n} {p}')
        _ixp_debug('y', f'INFO[_lxc_pull]: LXC pull status: {output}')

        return (output[0])

    ## End _lxc_pull method 

    #============================================#
    # //               _lxc_push              // #
    #============================================#

    def _lxc_push(self, c, local_file, remote_file):
        ''' lxc_push method to pull files from container '''

        # // Delete original file from container first //
        _IxpLxc._lxc_rm(self, c, remote_file)

        # // Push new file to the container //
        (m, n, o, p) = (local_host, local_file, c, remote_file)
        _ixp_debug('y', f'INFO[_lxc_push]: lxc file push\n  <-- {m}:{n}\n  --> {o}:{p}\n')

        output = subprocess.getstatusoutput(f'lxc file push {n} {c}{p}')
        _ixp_debug('y', f'INFO[_lxc_push]: LXC push status: {output[1]}')

        return (output[0])

    ## End _lxc_push method 

    #============================================#
    # //      _lxc_container_boot_delay       // #
    #============================================#

    def _lxc_container_boot_delay(self):
        ''' Method to allow the container enough time to boot '''
        
        counter = 0

        for n in range(1,4):
            time.sleep(5)
            counter += 1

        return(0)

    ## End _lxc_container_boot_delay method

    #============================================#
    # //      _lxc_hypervisor_containers      // #
    #============================================#

    def _lxc_hypervisor_containers(self):
        ''' Method to get list of containers on hypervisor '''

        lxc_list = list()

        a = subprocess.getstatusoutput('lxc list -c ns --format csv')[1].split('\n')
        for x in a:
            b = x.split(',')
            lxc_list.append(b[0])

        return(lxc_list)

    ## End _lxc_hypervisor_containers method

    #============================================#
    # //          _lxc_internet_test          // #
    #============================================#

    def _lxc_internet_test(self,c):
        ''' Method to get check Internet connectivity '''

        dns_exists = ''
        url_exists = ''
        test_precentage = 0
        timeout=5
        try:
            dns_test = _IxpLxc._lxc_exec(self, c, f'ping -c1 {upstream_ns[0]}')
            if (dns_test[0] == 0):
                dns_test = True
        except: 
            print(f'The container {c} does not have Internet connectivity\n')
            sys.exit(1)

        # // Test some URLs //
        for x in test_websites:
            try:
                url_test = str(requests.get(x, timeout=timeout))
                if (url_test == '<Response [200]>'):
                    test_precentage += int(100/len(test_websites))
            except requests.ConnectionError:
                pass

        return(f'{test_precentage}')                       

    ## End _lxc_hypervisor_containers method

    #============================================#
    #  //            _bird_restart            // #
    #============================================#

    def _bird_restart(self, c):
        ''' Method to restart the bird server '''

        _IxpLxc._lxc_exec(self, c, 'systemctl restart bird')
        sys_ctl_bird = _IxpLxc._lxc_exec(self, c, 'systemctl --no-pager status bird')

        if (sys_ctl_bird[0] == 0):
            _ixp_debug('y', f'INFO[_bird_restart]: Restarting the bird daemon on {c}')
            _ixp_debug('y', f'{sys_ctl_bird[1]}')

        _IxpLxc._lxc_exec(self, c, 'systemctl restart bird6')
        sys_ctl_bird6 = _IxpLxc._lxc_exec(self, c, 'systemctl --no-pager status bird6')

        if (sys_ctl_bird6[0] == 0):
            _ixp_debug('y', f'INFO[_bird_restart]: Restarting the bird6 daemon on {c}')
            _ixp_debug('y', f'{sys_ctl_bird6[1]}')

        _ixp_debug('y', f'INFO[_bird_restart]: Restarting the bird daemons on {c}')

        return(0)

    ## End of _bird_restart method

    #============================================#
    #  //            _bind9_restart           // #
    #============================================#

    def _bind9_restart(self, c):
        ''' Method to restart the bind9 server '''

        _IxpLxc._lxc_exec(self, c, 'systemctl restart bind9')
        sys_ctl_bind9 = _IxpLxc._lxc_exec(self, c, 'systemctl --no-pager status bind9')

        if (sys_ctl_bind9[0] == 0):
            _ixp_debug('y', f'INFO[_bind9_restart]: Restarting the bind9 daemon on {c}')
            _ixp_debug('y', f'{sys_ctl_bind9[1]}')

        _ixp_debug('y', f'INFO[_bind9_restart]: Restarting the bind9 daemon on {c}')

        return(0)

    ## End of _bind9_restart method

############ // End _IxpLxc class // ############

#################################################
# //             IxpSchema class             // #
#################################################

class IxpSchema:
    ''' IXP Schema Class '''

    def __init__(self):
        ''' IxpSchema class constructor method '''

        self.ixp_schema_keys = ('ipv4-peer', 'ipv6-peer', 'ipv4-man', 
                                'ipv6-man', 'domain', 'as-number', 
                                'site-number') 
        self.ixp_site_info_keys = ('site-number','country', 'town', 
                                   'elevation', 'organisation', 
                                   'latitude', 'longitude')
        self.ixp_schema_temp = {'ipv4-peer':'199.9.9.0/24', 
                                'ipv6-peer':'2a99:9:9::/48',
                                'ipv4-man':'198.8.8.0/24', 
                                'ipv6-man':'2a98:8:8::/48', 
                                'domain':'netlabs.tst', 
                                'as-number':'5999', 
                                'site-number':'1',
                                'organisation':'netLabs!UG',
                                'country':'Uganda', 
                                'town':'Kampala', 
                                'elevation':1027, 
                                'latitude':'00 20 51.3456 N', 
                                'longitude':'32 34 57.0720 E'}

        self.build_dict = { '-sn':'--site-number',
                            '-o':'--organisation',
                            '-p4':'--ipv4-peer',
                            '-p6':'--ipv6-peer',
                            '-m4':'--ipv4-man',
                            '-m6':'--ipv6-man',
                            '-d' :'--domain',
                            '-as':'--as-number',
                            '-c':'--country',
                            '-t':'--town',
                            '-e':'--elevation',
                            '-la':'--latitude',
                            '-lo':'--longitude'}

        self.schema_yaml_template = 'ixp_schema_template.yaml'

    ## End of __init__ method

    #============================================#
    # //          _ixp_schema_build           // #
    #============================================#

    def _ixp_schema_build(self, arg):   
        ''' IXP Schema Build method, builds the IXP Schema and IXP Site info '''

        # // Extract dictionary from list //
        ixp_schema_final = arg[0]

        # // Deal with the IXP Schema file //

        # // Open IXP Schema file for writing //
        schema_yaml_path_file = f'{var_dir}{schema_yaml_file}'
        try:
            conf_file = open(schema_yaml_path_file, 'w')
        except:
            print(f'ERROR: Can\'t open {schema_yaml_file}')
            _ixp_debug('y',f'ERROR[ixp_schema_build]: Can\'t open {schema_yaml_file}')

        conf_file.write(f'{schema_yaml_file}\n---\n')
        _ixp_debug('y',f'INFO[ixp_schema_build]: Opened {schema_yaml_file} for update')

        # // Loop through the servers //
        for k, v in ixp_servers.items():
            conf_file.write(f"\n{k.lower()}{ixp_schema_final['site-number']}:\n")
            _ixp_debug('n',f"\n{k.lower()}{ixp_schema_final['site-number']}:")

            # // Loop through ixp schema //
            #for ik, iv in ixp_schema_final.items():
                #print(ik, iv)

            for x in self.ixp_schema_keys:
                (ik, iv) = (x, ixp_schema_final[x])

                ip_dict = dict()

                # // Add IPv4 addresses to the IXP Schema //
                if ('4' in ik):
                    ip_dict = _validate_ip_net(iv)                    
                    ipv4_net = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.)', ip_dict['ip'])[0]

                    # // Replace '202' for '1' for the peering LAN //
                    if (k == 'rs' and 'peer' in ik):
                        vd = 1
                    else:
                        vd = v   
                    conf_file.write(f"  - {ik}: {ipv4_net}{vd}/{ip_dict['prefix']}\n")
                    _ixp_debug('n', f"  - {ik}: {ipv4_net}{vd}/{ip_dict['prefix']}")

                # // Add IPv6 addresses to the IXP Schema //
                elif ('6' in ik):
                    #ip6_host = int(ixp_servers[c], 16)  # Convert hex # to decimal
                    ip_dict = _validate_ip_net(iv)

                    # // Replace '202' for '1' for the peering LAN //
                    if (k == 'rs' and 'peer' in ik):
                        vd = 1
                    else:
                        vd = v   

                    conf_file.write(f"  - {ik}: {ip_dict['ip']}{vd}/{ip_dict['prefix']}\n")
                    _ixp_debug('n', f"  - {ik}: {ip_dict['ip']}{vd}/{ip_dict['prefix']}")

                # // Add domain to the IXP Schema //
                elif (ik == 'domain'):
                    conf_file.write(f'  - {ik}: {iv}\n')
                    _ixp_debug('n', f'  - {ik}: {iv}')

                # // Add as-number to the IXP Schema //
                elif (ik == 'as-number'):
                    conf_file.write(f'  - {ik}: {iv}\n')
                    _ixp_debug('n', f'  - {ik}: {iv}')

        # // Close yaml file filehandle //
        conf_file.close()

        # // Open IXP Site Info file for writing //

        site_info_path_file = f'{var_dir}{site_info_yaml_file}'
        try:
            info_file = open(site_info_path_file, 'w')
        except:
            print(f'ERROR: Can\'t open {site_info_yaml_file}\n')
            _ixp_debug('y',f'ERROR[ixp_schema_build]: Can\'t open {site_info_yaml_file}')

        # // Write to the site_info.yaml file //
        info_file.write(f'{site_info_yaml_file}\n---\n\n')
        _ixp_debug('y',f'INFO[ixp_schema_build]: Opened {schema_yaml_file} for update')

        # // Write IXP site information to file //
        loc_a = loc_b = ''
        for k in self.ixp_site_info_keys:
            if (k == 'latitude'):
                loc_a = (ixp_schema_final[k])
            elif (k == 'longitude'):
                loc_b = (ixp_schema_final[k])
            else:
                info_file.write(f'{k}: {ixp_schema_final[k]}\n')
                _ixp_debug('n',f'{k}: {ixp_schema_final[k]}')
                pass
        info_file.write(f'location: {loc_a} {loc_b}\n')
        _ixp_debug('n',f'location: {loc_a} {loc_b}')

        # // Close yaml file filehandle //
        info_file.close()

        print('IXP Schema and IXP Site information updated\n')
        _ixp_debug('y','INFO[ixp_schema_build]: IXP Schema and IXP Site information updated')

        return(0)    

        ## End _ixp_schema_build method

    #============================================#
    # //          _ixp_schema_show            // #
    #============================================#

    def _ixp_schema_show(self, element):
        ''' ixp_schema_show method, displays contents of the IXP Schema '''

        _function_call_location()

        # // Read schema from IXP Schema YAML file //

        schema_yaml = _read_schema_yaml()

        # // Display element //

        print(f'\n{element}')
        print('-' * len(element))
        _ixp_debug('y',f'INFO[ixp_schema_show]: {element}\n')

        for d in schema_yaml[1][element]:

            for k in d:
                print(f'{k:<10} : {d[k]}')  
                _ixp_debug('n',f'{k:<10} {d[k]}')
        _ixp_debug('n',' ') 

        return(0)
         
    # End _ixp_schema_show method

    #============================================#
    # //       _ixp_schema_test_options       // #
    #============================================#

    def _ixp_schema_test_options(self, ixp_argv):
        ''' Test 'ixp_schema' cli options '''
        
        skip_yes_no = False
        first_options = ('build', 'default', 'show') 
        help_required = False 
        element_list = list()

        # // Lowercase first option //
        ixp_argv[0] = ixp_argv[0].lower()

        # // IXP Schema elements //
        e = _ixp_schema_servers()
        e.pop(0)

        # // Check if help called first //
        if (ixp_argv[0] in help_list): 
            IxpHelp._ixp_schema_help(self, 0)

        # // Handle abbreviations //
        if(ixp_argv[0] == '?'):
            ixp_argv[0] = 'help'
        else:
            for x in first_options:
                if (ixp_argv[0] == x[:len(ixp_argv[0])]):      
                    ixp_argv[0] = x

        # // Check first options are legitimate //
        if not (ixp_argv[0] in first_options):
            print(f'ERROR: ixp schema \'{ixp_argv[0]}\' not valid option\n')
            _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: ixp schema \'{ixp_argv[0]}\' not valid option')
            IxpHelp._ixp_schema_help(self, 1)

        # // Create element list from remaining elements //
        if (len(ixp_argv) > 1):
            element_list = ixp_argv[1:]
            element_list = list(set(element_list))  # Remove duplicates    

            # // Check if help required //
            help_required = (len(list(set(ixp_argv).intersection(help_list))))

        # // Handle 'show site' help //
        if (len(ixp_argv) > 2):
            if (ixp_argv[0] == 'show' and ixp_argv[1] == 'site' and help_required == True):
                IxpHelp._ixp_schema_show_site_help(self, 0) 
        else:
            pass

        # // Handle 'show' help //
        if (ixp_argv[0] == 'show' and help_required == True):
            IxpHelp._ixp_schema_show_help(self, 0) 

        # // Handle 'default' help //
        elif (ixp_argv[0] == 'default' and help_required == True):
            IxpHelp._ixp_schema_default_help(self, 0)     

        # // Handle 'build' help //
        elif (ixp_argv[0] == 'build' and help_required == True):
            IxpHelp._ixp_schema_build_help(self, 0) 

        else:
            pass

        # // Dealing with second elements //

        #### // Dealing with 'show' // ####  
        if (ixp_argv[0] == 'show'):

            # // Iterate over strings in list and lowercase them //
            element_list = [string.lower() for string in element_list]

            # // No elements //
            if (len(element_list) == 0):
                element_list = e
                print(f'No options given, assuming \'all\'\n')

            # // One element //
            elif (len(element_list) == 1):
                if (element_list[0] == 'all'): 
                    element_list = e
                    element_list.append('site')
                elif (element_list[0] == 'schema'): 
                    element_list = e
                elif (element_list[0] == 'site'):
                    pass
                elif not (element_list[0] in e):
                    print(f'ERROR: {element_list[0]} is not a valid option\n')
                    _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: {element_list[0]} is not a valid option')
                    IxpHelp._ixp_schema_show_help(self, 1)                

            # // check for more than one element //
            elif (len(element_list) >= 1):
           
                # // Handle an 'all' //
                if ('all' in element_list): 
                    element_list = e
                    element_list.append('site')

                # // Test elements in list, test for illegal elements //
                l = list()
                for x in element_list:
                    if (x in e or x in 'site'):
                        pass
                    else:
                        l.append(x)
                if (len(l) == 1):
                    print(f'ERROR: \'{l[0]}\' is not a valid option\n')
                    _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{l[0]}\' is not a valid option')
                    IxpHelp._ixp_schema_show_help(self, 0) 
                elif (len(l) > 1):
                    l_str = ", ".join(str(x) for x in l)
                    print(f'ERROR: \'{l_str}\' are not valid options\n')
                    _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{l_str}\' are not valid options')
                    IxpHelp._ixp_schema_show_help(self, 0) 
                else:
                    pass
                   
            else:
                print('ERROR: Some option I havent thought of yet\n')
                _ixp_debug('ERROR[_ixp_schema_test_options]: Some option I havent thought of yet\n')


        #### // Dealing with 'default' // ####
        if (ixp_argv[0] == 'default'):

            default_list = ['schema', 'site']

            # // Iterate over strings in list and lowercase them //
            element_list = [string.lower() for string in element_list]

            # // No elements //
            if (len(element_list) == 0):
                element_list = default_list
                print(f'No options given, assuming \'all\'\n')

            # // One element //
            elif (len(element_list) == 1):
                if ('both' in element_list or 'all' in element_list): 
                    element_list = default_list
                elif (element_list[0] == 'schema'):   
                    pass
                elif (element_list[0] == 'site'):
                    pass
                elif not (element_list[0] in e):
                    print(f'ERROR: {element_list[0]} is not a valid option\n')
                    _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: {element_list[0]} is not a valid option')
                    IxpHelp._ixp_schema_default_help(self, 1)                

            # // check for more than one element //
            elif (len(element_list) > 1):
           
                # // Handle 'both' or 'all' //
                if ('both' in element_list or 'all' in element_list): 
                    element_list = default_list 
   
                # // Test for both elements //
                if (set(element_list) == set(default_list)):

                    # // Test elements in list, test for illegal elements //
                    l = list()
                    for x in element_list:
                        if (x in default_list):
                            pass
                        else:
                            l.append(x)
                    if (len(l) == 1):
                        print(f'ERROR: \'{l[0]}\' is not a valid option\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{l[0]}\' is not a valid option')
                        IxpHelp._ixp_schema_default_help(self, 1) 
                    elif (len(l) > 1):
                        l_str = ", ".join(str(x) for x in l)
                        print(f'ERROR: \'{l_str}\' are not valid options\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{l_str}\' are not valid options')
                        IxpHelp._ixp_schema_default_help(self, 1) 
                    else:
                        pass
                       
                else:
                    l = list(set(element_list) - set(default_list))
                    l_str = ", ".join(str(x) for x in l)
                    if (len(l) == 1):
                        print(f'ERROR: \'{l[0]}\' is not a valid option\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{l[0]}\' is not a valid option')
                        IxpHelp._ixp_schema_default_help(self, 1) 
                    elif (len(l) > 1):
                        l_str = ", ".join(str(x) for x in l)
                        print(f'ERROR: \'{l_str}\' are not valid options\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{l_str}\' are not valid options')
                        IxpHelp._ixp_schema_default_help(self, 1)  


        #### // Dealing with 'build' // ####
        if (ixp_argv[0] == 'build'):

            ixp_schema_working = self.ixp_schema_temp
            element_list = ixp_argv[1:]
            element_dict = dict()

            # // Check for no elements //
            if (len(element_list) == 0):
                print('ERROR: No elements given\n')
                _ixp_debug('y','ERROR[_ixp_schema_test_options]: No elements given')
                IxpHelp._ixp_schema_build_help(self, 1)     

            # // Check for odd number of elements //
            if (len(element_list) % 2 != 0):
                print('ERROR: Odd number of elements\n')
                _ixp_debug('y','ERROR[_ixp_schema_test_options]: Odd number of elements')
                IxpHelp._ixp_schema_build_help(self, 1)         

            # // Create an element dictionary of pairs and a key list //
            while element_list:
                a = element_list.pop(0).lower()  # pop Key, lowercase
                b = element_list.pop(0)          # pop value
                element_dict[a] = b

            # // Change all keys to long format //
            for x in list(element_dict.keys()):
                if x in self.build_dict.keys():
                    element_dict[self.build_dict[x]] = element_dict[x]
                    del element_dict[x]

            # // Remove the double '--' from the keys //
            for x in (list(element_dict.keys())):
                element_dict[x[2:]] = element_dict[x]
                del element_dict[x]

            # // Add the given elements to the template //
            for k, v in element_dict.items():
                if k in list(ixp_schema_working.keys()):
                    ixp_schema_working[k] = v
                else:
                    print(f'ERROR: {k} is not a valid key\n')
                    _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: {k} is not a valid key')
                    IxpHelp._ixp_schema_build_help(self, 1)    

            # // Test that values are within parameters //
            for k,v in ixp_schema_working.items():

                # // Test IP addresses //
                if ('ipv' in k):
                    try:
                        _validate_ip_net(v)
                    except:
                        print(f'ERROR: \'{v}\' is not a valid format\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{v}\' is not a valid format')
                        IxpHelp._ixp_schema_build_help(self, 1)   
     
                # // Test domain //
                if ('domain' in k):
                    element_dict[k] = v.lower()
                    reply = validators.domain(v.lower())
                    if not (reply == True):
                        print(f'ERROR: \'{reply}\'\n')
                        _ixp_debug('y',f'\'{reply}\'')
                        IxpHelp._ixp_schema_build_help(self, 1)    

                # // Test AS Number //
                if ('as-number' in k):
                    if not ((v.isdigit()) and ((0 <= int(v) <= 4199999999) == True)):
                        print(f'ERROR: \'{v}\' is not a valid AS number\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{v}\' is not a valid AS number')
                        IxpHelp._ixp_schema_build_help(self, 1)  

                # // Test Site Number //
                if ('site-number' in k):
                    if not (v.isdigit() and (0 <= int(v) <= 100) == True):
                        print(f'ERROR: \'{v}\' is not a valid site number\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{v}\' is not a valid site number')
                        IxpHelp._ixp_schema_build_help(self, 1)  

                # // Test Organisation //
                if (k in ('organisation', 'town','country')):
                    if (len(v) > 25):
                        print(f'ERROR: \'{v}\' is too long for {k} name\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{v}\' is too long for {k} name')
                        IxpHelp._ixp_schema_build_help(self, 1) 
           
                # // Test Elevation //
                if ('elevation' in k):
                    if not (v.isdigit() and (0 <= int(v) <= 8848) == True):
                        print(f'ERROR: \'{v}\' must be digits between 0 - 8848\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{v}\' must be digits between 0 - 8848')
                        IxpHelp._ixp_schema_build_help(self, 1)  

                # // Test Latitude //
                lat_search = '^\d{1,2}\s\d{1,2}\s\d{1,2}\.\d{1,4}\s[NS]$'                
                if ('latitude' in k):
                    element_dict[k] = v.upper()    # Uppercase the N or S
                    v = v.upper()                  # Uppercase the N or S                             
                    if not re.search(lat_search, v, re.M):
                        print(f'ERROR: \'{v}\' is not a valid latitude\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{v}\' is not a valid latitude')
                        IxpHelp._ixp_schema_build_help(self, 1)  

                # // Test Longitude //
                long_search = '^\d{1,2}\s\d{1,2}\s\d{1,2}\.\d{1,4}\s[EW]$'                
                if ('longitude' in k):
                    element_dict[k] = v.upper()    # Uppercase the E or W
                    v = v.upper()                  # Uppercase the E or W                             
                    if not re.search(long_search, v, re.M):
                        print(f'ERROR: \'{v}\' is not a valid longitude\n')
                        _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: \'{v}\' is not a valid longitude')
                        IxpHelp._ixp_schema_build_help(self, 1)  

            element_list = [ixp_schema_working]

        return(ixp_argv[0], element_list)

    ## End _ixp_server_test_options method

    #============================================#
    # //               ixp_schema             // #
    #============================================#   

    def ixp_schema(self, ixp_argv):
        ''' 'ixp schema' commands method '''

        (command, element_list) = self._ixp_schema_test_options(ixp_argv)
        site_info = list()

        # // IXP Schema elements //
        e = _ixp_schema_servers()
        e.pop(0)

        # // Handle if 'ixp schema show' is called //
        if (command == 'show'):

            if ('site' in element_list):
                site_info = _read_site_info_yaml()

                print('IXP Site information')
                print('=' * 20, '\n')
                for k,v in site_info[1].items():
                    print(f'{k:<12} : {v}')
                element_list.remove('site') 
                print('')

            if (len(element_list) > 0) and (set(element_list).issubset(e)):
                print("IXP Schema information for", end = ' ')
                e_list = ", ".join(str(x) for x in element_list)
                print(e_list)
                print('=' * (27 + len(e_list)))
                if (set(element_list).issubset(e)):
                    for x in element_list:
                        self._ixp_schema_show(x)

        # // Handle if 'ixp schema default' is called //
        if (command == 'default'):
            for x in element_list:
                if (x in 'schema'):
                    a_file = f'{var_dir}{schema_yaml_template}'
                    b_file = f'{var_dir}{schema_yaml_file}'
                    subprocess.getstatusoutput(f'cp {a_file} {b_file}')
                    _string_search_replace(f'{b_file}', 
                                           f'{schema_yaml_template}', 
                                           f'{schema_yaml_file}')
                    print('IXP IP Schema defaulted\n')
                    _ixp_debug('y','INFO[ixp_schema]: IXP IP Schema')  
                elif(x in 'site'):
                    a_file = f'{var_dir}{site_info_yaml_template}'
                    b_file = f'{var_dir}{site_info_yaml_file}'
                    subprocess.getstatusoutput(f'cp {a_file} {b_file}')
                    _string_search_replace(f'{b_file}', 
                                           f'{site_info_yaml_template}',  
                                           f'{site_info_yaml_file}')
                    print('IXP site information defaulted.\n')
                    _ixp_debug('y','INFO[ixp_schema]: IXP site information defaulted.')  
                else:
                    print('ERROR: Not a valid element.\n')
                    _ixp_debug('y','ERROR[ixp_schema]: Not a valid element.')            
    
        # // handle 'ixp schema build' is called //
        elif (ixp_argv[0] == 'build'):

            self._ixp_schema_build(element_list)

        # // Exit //
        sys.exit(0)

    ## End to ixp_schema method

########### // End IxpSchema class // ###########

#################################################
# //              IxpHost class              // #
#################################################

class IxpHost:
    ''' IXP Host class '''

    def __init__(self):
        ''' IxpHost class constructor method '''

        self.host_yaml_template = 'ixp_host_template.yaml'
        self.host_yaml_file = 'ixp_host.yaml'

    ## End of __init__ method

    #============================================#
    # //           _ixp_host_build            // #
    #============================================#

    def _ixp_host_build(self, interface):
        ''' Create netplan YAML file and sudo bash script to execute changes '''

        host_yaml_path_template = f'{var_dir}{self.host_yaml_template}'
        host_yaml_path_file = f'{var_dir}{self.host_yaml_file}'
        interfaces = list()
        dhcp_list = ['no']
        dhcp_interface = False

        LXD = _ixp_schema_servers()[0]
        RS = _ixp_schema_servers()[3]

        # // Find the physical interfaces                                           //
        # // Finds interfaces where IFLA_LINKINFO = None and removes 'lo' interface //

        interface_dict = _ip_link_list()
        for k in interface_dict.keys():
            if (k != 'lo' and interface_dict[k] == None):
               interfaces.append(k)

        # // Sort interfaces //
        interfaces.sort()

        # // If there is more than 1 physical interface give an option for a DHCP interface //
        if (len(interfaces) > 1):
            print('Does the system require a separate interface running DHCP?')
            print('Select from the following?\n')
            for i in interfaces:
                if (i == interface):
                    pass
                else:
                    dhcp_list.append(i)
                    print(i, end = '\t')

            # // Check if a DHCP Interface is required or not //
            dhcp_interface = input('\n\n[ no | <<DHCP Interface>> ]: ')  
            dhcp_interface = dhcp_interface.lower()
            print('\n')

            if (dhcp_interface in dhcp_list):

                if (dhcp_interface != 'no'):
                    print(f'Configuring {interface} as the IXP interface and {dhcp_interface} as DHCP interface\n')
                    _ixp_debug('y',f'INFO[_ixp_host_build]: Configuring {interface} as the IXP interface and {dhcp_interface} as DHCP interface')

                else:
                    dhcp_interface = False
                    print(f'Configuring {interface} as the only interface\n')
                    _ixp_debug('y',f'INFO[_ixp_host_build]: Configuring {interface} as the only interface')                

            else:
                    print(f'ERROR: \'{dhcp_interface}\' is not a valid choice, exiting\n')
                    _ixp_debug('y',f'ERROR[_ixp_host_build]: \'{dhcp_interface}\' is not a valid choice, exiting')
                    sys.exit(1)

        # // Check the IXP host template YAML file exists //
        if (path.isfile(host_yaml_path_template)):
            subprocess.getstatusoutput(f'cp {host_yaml_path_template} {host_yaml_path_file}')
            print(f'Created IXP host skeleton\n')
            _ixp_debug('y',f'INFO[_ixp_host_build]:  Created the file {host_yaml_path_file}') 

        else:
            print(f'ERROR: {host_yaml_path_template} doesn\'t exist\n')
            _ixp_debug('y',f'ERROR[_ixp_host_build]: {host_yaml_path_template} doesn\'t exist')
            sys.exit(1)

        # // Read the IXP Schema YAML file //
        yaml_schema_docs = _read_schema_yaml()  # Passing 'False' to remote DEBUG

        # // Define the interface given as the interface to be configured //
        _string_search_replace(host_yaml_path_file, '<<interface>>', interface)

        # // Get the DNS Server IP address and deduce the management gateways //
        host_addresses = list()
        host_addresses.append(upstream_ns[0])     # Temporary nameresolver
        validated_ip_net = _validate_ip_net(yaml_schema_docs[1][RS][2]['ipv4-man'])
        host_addresses.append(ipaddress.IPv4Network(validated_ip_net['net'])[1])
        validated_ip_net = _validate_ip_net(yaml_schema_docs[1][RS][3]['ipv6-man'])
        host_addresses.append(ipaddress.IPv6Network(validated_ip_net['net'])[1])

        # // Replace the NS (nameserver) details //
        _string_search_replace(host_yaml_path_file, '<<ns-ipv4-peer>>' , host_addresses[0])
        _string_search_replace(host_yaml_path_file, '<<gateway4>>' , host_addresses[1])
        _string_search_replace(host_yaml_path_file, '<<gateway6>>' , host_addresses[2])

        # // Extract the lxd (host) elements from the IXP schema //
        host_schema = yaml_schema_docs[1][LXD]

        for d in host_schema:
            for k in d:
                keystr = '<<{}>>'.format(k)
                _string_search_replace(host_yaml_path_file, keystr, d[k])

        # // Add in the DHCP interface if it is required //
        if (dhcp_interface):
            _ixp_debug('y',f'INFO[_ixp_host_build]: Adding the \'{dhcp_interface}\' DHCP interface')
            dhcp_interface_block = (f'  ethernets:\n    {dhcp_interface}:\n'
                                    f'      dhcp4: true\n      dhcp6: true\n'
                                    f'      nameservers:\n' 
                                    f"        addresses: ['{upstream_ns[0]}']\n" 
                                   )                                        
            _string_search_replace(host_yaml_path_file, '  ethernets:', dhcp_interface_block)

        print(f'The IXP host configuration has been built\n')
        _ixp_debug('y',f'INFO[_ixp_host_build]: \'{host_yaml_path_file}\' has been built')

        # // Create temporary bash script to execute the changes with sudo //
        (ttime, tdate, tstamp) = _ixp_timestamp()

        tf = tempfile.NamedTemporaryFile()
        temp_file_name = tf.name
        tf.close()

        if (dhcp_interface):
            tf2 = tempfile.NamedTemporaryFile()
            temp_file_name2 = tf2.name
            tf2.close()
     
        try:
            fo = open(temp_file_name, 'w')
        except:
            print(f'ERROR: Cannot open {temp_file_name}')
            _ixp_debug('y',f'ERROR[_ixp_host_build]: Cannot open {temp_file_name}')
            sys.exit(1)

        bash_path = subprocess.getstatusoutput('which bash')[1]
        fo.write(f'#!{bash_path}\n')    
        fo.write('# // Install the ip_tables modules in the kernel //\n')
        fo.write('modinfo ip_tables > /dev/null 2>&1\n')
        fo.write('modinfo ip6_tables > /dev/null 2>&1\n')
        fo.write('# // Copy existing netplan YAML files for backup //\n')
        fo.write(f'mkdir -p {var_dir}netplan_bak\n')
        fo.write(f'mv {netplan_dir}* {var_dir}netplan_bak/\n')
        fo.write(f'chown -R root:ixp {var_dir}netplan_bak\n')
        fo.write(f'chmod -R {file_perm} {var_dir}netplan_bak\n\n')
        fo.write('# // Copy net netplan YAML file to netplan directory //\n')
        fo.write(f'cp {host_yaml_path_file} {netplan_dir}01-netplan.yaml\n')
        fo.write(f'chown root:root {netplan_dir}01-netplan.yaml\n')
        fo.write(f'chmod -R 644 {netplan_dir}01-netplan.yaml\n\n')
        fo.write('# // Apply the new netplan //\n')
        fo.write('netplan apply\n\n')

        if (public_ipv6_net == False):

            fo.write('# // Update gai.conf file if there is no public IPv6 network //\n')
            gai_file_bu = f'{gai_file}.orig'
            gai_temp_file = _gai_build()
            fo.write(f'cp {gai_file} {gai_file_bu}\n')  
            fo.write(f'mv {gai_temp_file} {gai_file}\n\n')    

        if (dhcp_interface):
            try:
                fo2 = open(temp_file_name2, 'w')
            except:
                print(f'ERROR: Cannot open {temp_file_name}')
                _ixp_debug('y',f'ERROR[_ixp_host_build]: Cannot open {temp_file_name}')
                sys.exit(1)       

            # // Second embedded script if DHCP Server interface //
            fo2.write(f'#!{bash_path}\n')
            fo2.write('# // Execute the main script //\n')
            fo2.write(f'{temp_file_name}\n\n')
            fo2.write('# // Change the default route //\n')
            fo2.write(f'ip route del default via {host_addresses[1]}\n')
            fo2.write(f'rm {temp_file_name}\n\n')
            fo2.write('echo "Destroying one use script"\n\n')     
            fo2.write('echo\n\n')
            fo2.write('# // Script self destroy after use //\n')
            fo2.write('rm -- \"$0\"\n')
            fo2.close()

        fo.write('# // Message to terminal //\n')
        fo.write('echo "New netplan has been applied"\n\n')     
        fo.write('echo\n\n')
        
        if not (dhcp_interface):
            fo.write('echo "Destroying one use script"\n\n')     
            fo.write('echo\n\n')
            fo.write('# // Script self destroy after use //\n')
            fo.write('rm -- \"$0\"\n')
        else:
            fo.write('sleep 3\n')        
        fo.close()

        print('Execute the following once off script to apply:\n')
        if(dhcp_interface):
           subprocess.getstatusoutput(f'chmod {file_perm} {temp_file_name}')
           subprocess.getstatusoutput(f'chmod {file_perm} {temp_file_name2}')
           print(f'$ sudo {temp_file_name2}\n')    
        else:
           subprocess.getstatusoutput(f'chmod {file_perm} {temp_file_name}')
           print(f'$ sudo {temp_file_name}\n')      

        return(0)

    ## End _ixp_host_build method

    #============================================#
    # //        _ixp_host_test_options        // #
    #============================================#

    def _ixp_host_test_options(self, ixp_argv):
        ''' Test 'ixp_host' cli options '''

        first_options = ['list','build']

        # // Iterate over strings in list and lowercase them //
        ixp_argv = [string.lower() for string in ixp_argv]

        # // 'ixp host' with no arguments given //
        if (len(ixp_argv) == 0):
            print('ERROR: No OPTION or ARGUMENT given\n')
            _ixp_debug('y','ERROR[_ixp_host_test_options]: No OPTION or ARGUMENT given')
            IxpHelp._ixp_host_help(self, 1)

        # // If 'help' or '?' in list of elements // 
        if (len(list(set(ixp_argv).intersection(help_list))) > 0):
            IxpHelp._ixp_host_help(self, 0)

        # // Handle abbreviations //
        if(ixp_argv[0] == '?'):
            ixp_argv[0] = 'help'
        else:
            for x in first_options:
                if (ixp_argv[0] == x[:len(ixp_argv[0])]):      
                    ixp_argv[0] = x

        # // Handle if 'ixp host list' is called //
        if (ixp_argv[0] == 'list'):

            # // Too many arguments given //
            if (len(ixp_argv) == 3):
                print('ERROR: Too many arguments\n')
                _ixp_debug('y','ERROR[_ixp_host_test_options]: Too many arguments')
                IxpHelp._ixp_host_help(self, 1)        

            # // No arguments given beyond 'list' //
            if (len(ixp_argv) == 1):
                print('ERROR: Try: $ ixp host list interfaces\n')
                _ixp_debug('y','ERROR[_ixp_host_test_options]: Try: $ ixp host list interfaces')
                IxpHelp._ixp_host_help(self, 1)           

            # // Make sure option is 'list interfaces' //
            else:

                if (ixp_argv[1] == 'interfaces'[:len(ixp_argv[1])]):      
                    element_list = ['interfaces']
                else:    
                    print('ERROR: Try: $ ixp host list interfaces\n')
                    _ixp_debug('y','ERROR[_ixp_host_test_options]: Try: $ ixp host list interfaces')
                    IxpHelp._ixp_host_help(self, 1)  

                # // Create list for return //
                #else:
                    #element_list = ixp_argv[1:]

        # // Handle if 'ixp host build' is called //        
        if (ixp_argv[0] == 'build'):

            # // No arguments given beyond 'build' //
            if (len(ixp_argv) == 1):
                print('ERROR: Try: $ ixp host build interface <interface>\n')
                _ixp_debug('y','ERROR[_ixp_host_test_options]: Try: $ ixp host build interface <interface>')
                IxpHelp._ixp_host_help(self, 1)  

            # // Accept both 'interface' and interfaces' //
            if (ixp_argv[1] == 'interfaces'[:len(ixp_argv[1])]):
                    element_list = ['interface', ixp_argv[2]]
    
            # // Too many arguments given //
            elif (len(ixp_argv) > 3):
                print('ERROR: Too many arguments given\n')
                _ixp_debug('y','ERROR[_ixp_host_test_options]: Too many arguments given')
                IxpHelp._ixp_host_help(self, 1) 

            # // No interface name given //
            elif (len(ixp_argv) == 2 and ixp_argv[1] == 'interface'):  
                print('ERROR: Interface name needed, try: $ ixp host build interface <interface>\n')
                _ixp_debug('y','ERROR[_ixp_host_test_options]: ERROR: Interface name needed, try: $ ixp host build interface <interface>')
                IxpHelp._ixp_host_help(self, 1)  

            # // Over long interface name //            
            elif (len(ixp_argv) == 3 and ixp_argv[1] == 'interface'):  
                if (len(ixp_argv[2]) > 10):
                    print('ERROR: Interface name too long\n')
                    _ixp_debug('y','ERROR[_ixp_host_test_options]: ERROR: Interface name too long')
                    IxpHelp._ixp_host_help(self, 1) 

                # // Create list for return //
                else:
                    element_list = ixp_argv[1:]    

        return (ixp_argv[0], element_list)
  
    ## End _ixp_host_test_options

    #============================================#
    # //               ixp_host               // #
    #============================================#

    def ixp_host(self, ixp_argv):
        ''' Root ixp_host method, handles 'ip host' command '''

        (command, element_list) = self._ixp_host_test_options(ixp_argv)
        print(command, element_list)

        # // Get list of interfaces on host computer //
        host_interfaces = _ip_link_list()
        interfaces = list()
        for k in host_interfaces:
            if (k != 'lo' and host_interfaces[k] == None):
                interfaces.append(k)

        #// Handle 'list' option //
        if (command == 'list'): 

            # // Output list of interfaces to terminal //
            print('\nAvailable interfaces on host server:\n')
            _ixp_debug('y','Available interfaces on host server:')

            # // Sort the interfaces //
            interfaces.sort()

            # // List the interfaces //
            for i in interfaces:
                print('{}'.format(i), end = '\t')
                _ixp_debug('n','{}'.format(i))
            print('\n')

        #// Handle 'build' option //
        if (command == 'build'):  

            if not (element_list[1] in interfaces):
                print(f'ERROR: {element_list[1]} does not exist on this platform\n')
                _ixp_debug('y',f'ERROR[_ixp_host]: {element_list[1]} does not exist on this platform')

                interface_str = ", ".join(str(x) for x in interfaces)  
                print('Use one of these interfaces:\n')
                _ixp_debug('n',f'Use one of these interfaces:')
                print(f'{interface_str}\n')  
                _ixp_debug('n',f'{interface_str}')

            else:
                # // Call the 'ixp_host_build' method //
                self._ixp_host_build(element_list[1])

        # // Exit //
        sys.exit(0)   

    ## End to ixp_host method

############ // End IxpHost class // ############

#################################################
# //             IxpServer class             // #
#################################################

class IxpServer:
    ''' IXP Server Class '''

    def __init__(self):
        '''IxpServer class constructor method'''
        
        pass

    ## End of __init__ method

    #============================================#
    # //      _non_schema_instances_test      // #
    #============================================#

    def _non_schema_instances_test(self, skip_yes_no):
        ''' Method to check if there are non schema instances '''

        # // Shouldn't happen but is possible if messing with site numbers //

        non_schema_list = list()

        # // Get list of IXP Schema servers
        e = _ixp_schema_servers()
        e.pop(0)    # POP off lxd

        #// Get list of container instances on hypervisor (hide templates) //
        lxc_list = _IxpLxc._lxc_hypervisor_containers(self)

        # // Remove phantom entry //
        if ('' in lxc_list):
            lxc_list.remove('')

        # // Remove template //        
        if (server_template[0] in lxc_list):
            lxc_list.remove(server_template[0])   

        # // Remove template backup //      
        if (server_template_bak in lxc_list):
            lxc_list.remove(server_template_bak)  

        # // Loop through hypervisor containers to check if they are in the schema //
        for c in lxc_list:
            if not c in e:  
                non_schema_list.append(c)

        # // Go through non schema container instances //
        if (len(non_schema_list) > 0):

            print('The following non IXP schema LXC instances exist\n')

            if (skip_yes_no == False):

                for e in non_schema_list:
                    print(e, end = '  ')
                ans = input('\n\nDelete these instances (y|n) > ')
                ans = ans.lower()
                print('')

                if ((ans == 'y' or ans == 'yes')):
                    files_list = listdir(var_dir)

                    for e in non_schema_list:
                        print(f'Deleting LXC instance {e}\n')
                        subprocess.getstatusoutput(f'lxc delete {e} --force')

                        for f in files_list:

                            if (e in f):
                                remove(f'{var_dir}{f}')
                                print(f'Deleting the file {var_dir}{f}\n')
                                _ixp_debug('y',f'INFO[non_schema_instances_test]: Deleting the file {var_dir}{f}')

                    print(f'LXC instance {e} deleted\n')

                elif ((ans == 'n' or ans == 'no')):
                    print('OK that is fine\n')

                else:
                    print('ERROR[non_schema_instances_test]: Only (y|n) please\n')
                    _ixp_debug('y','ERROR[non_schema_instances_test]: Only (y|n) please')
                    sys.exit(1)

           # // Deal with '-y' scenario //
            else:                

                files_list = listdir(var_dir)
                for e in non_schema_list:
                    print(f'Deleting LXC instance {e}\n')
                    subprocess.getstatusoutput(f'lxc delete {e} --force')

                    for f in files_list:
                        if (e in f):
                            remove(f'{var_dir}{f}')
                            print(f'Deleting the file {var_dir}{f}\n')
                            _ixp_debug('y',f'INFO[non_schema_instances_test]: Deleting the file {var_dir}{f}')

                    print(f'LXC instance {e} deleted\n')               
     
        return(0)

    ## End _non_schema_instances_test method

    #============================================#
    # //            _dualnic_profile          // #
    #============================================#

    def _dualnic_profile(self, c):
        ''' Test for and/or assign dualnic profile to container method '''

        # // Test if the dualnic profile exists //
        test_tuple = subprocess.getstatusoutput('lxc profile list')

        if ('dualnic' in test_tuple[1]):  
            _ixp_debug('y','INFO[dualnic_profile]: \'dualnic\' profile already exists')   
     
        else:
            print(f'No \'dualnic\' profile on {c}, creating LXC profile\n')
            _ixp_debug('y',f'INFO[dualnic_profile]: No \'dualnic\' profile on {c}, creating LXC profile')
            subprocess.getstatusoutput('lxc profile create dualnic')
            subprocess.getstatusoutput(f'lxc profile edit dualnic < {var_dir}{dualnic_template}')

        subprocess.getstatusoutput(f'lxc profile assign {c} dualnic') 
        _ixp_debug('y',f'INFO[dualnic_profile]: Assigned \'dualnic\' profile to the container template {c}')

        return(0)

    ## End _dualnic_profile method
 
    #============================================#
    # //        _container_netplan_yaml       // #
    #============================================#

    def _container_netplan_yaml(self, c):
        ''' Method to test for, create and apply netplan for containers '''

        container_file_orig = '/etc/netplan/50-cloud-init.yaml'
        container_file = '/etc/netplan/10-netplan.yaml'
        template_file = f'{var_dir}{container_yaml_template}'
        host_backup = f'{var_dir}netplan_bak/{c}-50-cloud-init.yaml'
        container_yaml_file = f'{var_dir}ixp_{c}.yaml'
        local_file = f'{var_dir}ixp_{c}.yaml'

        # // Test container status, if stopped start //
        _IxpLxc._lxc_status_up(self, c)[1]

        # // Check if the netplan is already on the container // 
        test = _IxpLxc._lxc_file_test(self, c, container_file)
        if (test == 'yes'):
            _ixp_debug('y', f'INFO[_container_netplan_yaml]: Existing {container_file} on {c}, deleting')
            _IxpLxc._lxc_rm(self, c, container_file) 

        ct_dict = dict()
        ct_dict['ns-ipv4-man'] = upstream_ns[0]    

        # // Generate YAML file for template container //
        if (c == server_template[0]): 
            for e in _read_schema_yaml()[1][_ixp_schema_servers()[1]]:
                for k, v in e.items():
                    if ('201/' in str(v)):
                        r = v.replace('201/', '250/')
                        ct_dict[k] = r  
                    else:
                        ct_dict[k] = v              

                    if ((k == 'ipv4-man') or (k == 'ipv6-man')):
                        n = re.findall(r'\w+(\d)-\w+', k)
                        r = re.sub('20\d/\d+', '1', v)
                        ct_dict[f'gateway{n[0]}'] = r

        # // Generate YAML file for containers in schema //       
        elif (c in _ixp_schema_servers()):
            svr_template = _read_schema_yaml()[1][c]
            for e in svr_template:
                for k, v in e.items():
                    ct_dict[k] = v 
                    if ((k == 'ipv4-man') or (k == 'ipv6-man')):
                        n = re.findall(r'\w+(\d)-\w+', k)
                        r = re.sub('20\d/\d+', '1', v)
                        ct_dict[f'gateway{n[0]}'] = r  

        else:
            print(f'ERROR: {c} is not a valid container\n')  
            _ixp_debug('y', f'ERROR[_container_netplan_yaml]: {c} is not a valid container') 

        _ixp_debug('y', f'INFO[_container_netplan_yaml]: Generating netplan YAML file for {c}')

        # // Check the IXP container template YAML file exists //        
        if (path.isfile(template_file)):
            subprocess.getstatusoutput(f'cp {template_file} {container_yaml_file}') 
            _ixp_debug('y',f'INFO[_container_netplan_yaml]:  Created the file {container_yaml_file}')

            # // Change file permissions //
            subprocess.getstatusoutput(f'chmod {file_perm} {container_yaml_file}')

            # // Print file characteristics //
            file_detail = _get_file_dir_info(container_yaml_file)
            _ixp_debug('n', f"  {file_detail['name']}")
            _ixp_debug('n', f"    - Owner       : {file_detail['owner']}")
            _ixp_debug('n', f"    - Group       : {file_detail['group']}")
            _ixp_debug('n', f"    - Permissions : {file_detail['permissions']}")
            _ixp_debug('n', '')

        else:
            print(f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y',f'ERROR[_container_netplan_yaml]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Generate new file and edit it //
        for e in ct_dict:
            _string_search_replace(container_yaml_file, f'<<{e}>>', ct_dict[e])
     
        # // Pull down default netplan file from container //        
        pull_file = _IxpLxc._lxc_pull(self, c, container_file_orig, host_backup)
        if not (pull_file == 0):

            _ixp_debug('y',f'ERROR[_container_netplan_yaml]: Problem pulling {container_file_orig} from {c}')

        else:   
            _ixp_debug('y',f'INFO[_container_netplan_yaml]: Pulling default netplan file from {c}')

            # // Change file permissions //
            subprocess.getstatusoutput(f'chmod {file_perm} {host_backup}')    

            # // Print file characteristics //
            file_detail = _get_file_dir_info(host_backup)
            _ixp_debug('n', f"  {file_detail['name']}")
            _ixp_debug('n', f"    - Owner       : {file_detail['owner']}")
            _ixp_debug('n', f"    - Group       : {file_detail['group']}")
            _ixp_debug('n', f"    - Permissions : {file_detail['permissions']}")
            _ixp_debug('n', '')
        
        # // Push new template to the container server //        
        push_file = _IxpLxc._lxc_push(self, c, local_file, container_file)
                
        if not (push_file == 0):
            _ixp_debug('y',f'ERROR[_container_netplan_yaml]: Problem pushing {container_file} to {c}')
            sys.exit(1)

        else:    
            _ixp_debug('y',f'INFO[_container_netplan_yaml]: Pushing new netplan file to {c}')
        
        # // Remove original netplan file from container //            
        _IxpLxc._lxc_rm(self, c, container_file_orig)    
 
        # // Applying new netplan file on container //                
        netplan_apply = _IxpLxc._lxc_exec(self, c, 'netplan apply')
        
        if not (netplan_apply[0] == 0):
            print(f'ERROR: Cannot apply netplan on {c}\n')
            _ixp_debug('y',f'ERROR[_container_netplan_yaml]: Cannot apply netplan on {c}')
            sys.exit(1)
        else:    
            _ixp_debug('y',f'INFO[_container_netplan_yaml]: New netplan applied on {c}')

        # // Reboot container //                
        _IxpLxc._lxc_exec(self, c, 'shutdown -r now')

        counter = 0
        # // Short sleep to allow container to come up //
        for n in range(1,4):
            time.sleep(5)
            counter += 1

        print('')

        return(0)
     
    ## End _container_netplan_yaml method

    #============================================#
    #  //        _lxc_container_image         // #
    #============================================#

    def _lxc_container_image(self):
        ''' Method to download a container template '''

        ixp_lxc_status, ixp_lxc_state = _IxpLxc._lxc_status(self, server_template[0])

        if (ixp_lxc_status == 1):
            print(f'Container {server_template[0]} template doesn\'t exist, checking for backup\n')
            _ixp_debug('y',f'INFO[_lxc_container_image]: Container {server_template[0]} template doesn\'t exist, checking for backup')
            ixp_lxc_bak_status, ixp_lxc_bak_state = _IxpLxc._lxc_status(self, server_template_bak)
            if (ixp_lxc_bak_status == 1):
                print(f'No backup template either, downloading, please be patient\n')
                _ixp_debug('y',f'INFO[_lxc_container_image]: No {server_template_bak} backup either, downloading, please be patient')  
                subprocess.getstatusoutput(f'lxc launch {server_template[1]} {server_template[0]}') 

                # // Assign dualnic profile to template //
                _ixp_debug('y',f'INFO[_lxc_container_image]: Assigning dualnic profile') 
                self._dualnic_profile(server_template[0])

                # // Generate and assign netplan YAML file to template //
                self._container_netplan_yaml(server_template[0])

                # // Update and upgrade the container template, then stop //
                print(f'Upgrading the {server_template[0]}, please be patient\n')
                _ixp_debug('y',f'INFO[lxc_get_container_image]: Upgrading the {server_template[0]}, please be patient')
                _IxpLxc._lxc_upgrade(self, server_template[0])
                _IxpLxc._lxc_stop(self, server_template[0])   
      
                # // Backup the template //
                print(f'Creating {server_template_bak} as backup for {server_template[0]}, please be patient\n')
                _ixp_debug('y',f'INFO[lxc_get_container_image]: Creating {server_template_bak} as backup for {server_template[0]}')
                subprocess.getstatusoutput(f'lxc copy {server_template[0]} {server_template_bak}')

        # // If container already exists //
        elif (ixp_lxc_status == 0):

            # // Test the current state of template container image //
            _IxpLxc._lxc_start(self, server_template[0])
            if (ixp_lxc_status == 0):
                _ixp_debug('y',f'INFO[_lxc_get_container_image]: {server_template[0]} already exists and is in the state {ixp_lxc_state}')
                if (ixp_lxc_state == 'Running'):
                    print(f'Upgrading the {server_template[0]}, please be patient\n')
                    _ixp_debug('y',f'INFO[lxc_get_container_image]: Upgrading the {server_template[0]}, please be patient')
                    _IxpLxc._lxc_upgrade(self, server_template[0])
                    _IxpLxc._lxc_stop(self, server_template[0])

                elif (ixp_lxc_state == 'Stopped'):
                    _IxpLxc._lxc_start(self, server_template[0])
                    print(f'Upgrading the {server_template[0]}, please be patient\n')
                    _ixp_debug('y',f'INFO[lxc_get_container_image]: Upgrading the {server_template[0]}, please be patient')
                    _IxpLxc._lxc_upgrade(self, server_template[0])

                    exit()
                    _IxpLxc._lxc_stop(self, server_template[0])

                else:
                    print(f'ERROR: {server_template[0]} in state {ixp_lxc_state}\n')
                    _ixp_debug('y',f'ERROR[lxc_get_container_image]: {server_template[0]} in state {ixp_lxc_state}')
                    sys.exit(1)

            # // Ooops what is this status ? //
            else:
                print(f'ERROR: {server_template[0]} in state {ixp_lxc_state}\n')
                _ixp_debug('y',f'ERROR[lxc_get_container_image]: {server_template[0]} in state {ixp_lxc_state}')
                sys.exit(1)

            # // Generate and assign netplan YAML file to template //
            self._container_netplan_yaml(server_template[0])

            # // Update and upgrade the container template, then stop //
            _IxpLxc._lxc_upgrade(self, server_template[0])
            _IxpLxc._lxc_stop(self, server_template[0]) 

        return(0)

    ## End _lxc_container_image method 

    #============================================#
    #  //         _ixp_server_build           // #
    #============================================#
    
    def _ixp_server_build(self, skip_yes_no, ixp_argv):
        ''' Method to build the server '''

        # // Check if there is a template image //
        print(f'Checking on, updating and upgrading the template {server_template[0]}\n')
        _ixp_debug('y', f'INFO[_ixp_server_build]: Checking on, updating and upgrading the template {server_template[0]}')
        self._lxc_container_image()

        # // Non-schema instance test //
        self._non_schema_instances_test(skip_yes_no)

        # // Loop through container server list and create from clone template //
        for c in ixp_argv:
    
            # // Clone servers //
            print(f'Cloning server {c} now\n')   
            _ixp_debug('y',f'INFO[_ixp_server_build]: Cloning container {c} now')  
            _IxpLxc._lxc_clone(self, skip_yes_no, server_template[0], c)
    
            # // Start the container servers //
            _IxpLxc._lxc_start(self, c)
    
            # // Prioritise IPv4 over IPv6 as if no public IPv6 network //
            # // # (No IPv6 public address for testbed lab) //
            if (public_ipv6_net == False):
                gai_temp_file = _gai_build()
                gai_file_bu = f'{gai_file}.orig'
    
                # // Add the new GAI file // 
                _IxpLxc._lxc_exec(self, c, f'cp {gai_file} {gai_file_bu}')
                _IxpLxc._lxc_push(self, c, gai_temp_file, '/etc/gai.conf')
                _ixp_debug('y','INFO[_ixp_server_build]: Network does not have an IPv6 public connection')

            # // Public IPv6 network exists //
            else:

                # // Message to confirm IPv6 public network exists //     
                _ixp_debug('y','INFO[_ixp_server_build]: Network has an IPv6 public connection')

            # // Set the server to autostart after boot //            
            _ixp_debug('y',f'INFO[_ixp_server_build]: Setting the {c} container to boot autostart on startup')
            subprocess.getstatusoutput(f'lxc config set {c} boot.autostart true') 

            # // Generate and assign netplan YAML file to each container server //
            self._container_netplan_yaml(c)       
         
        # // Output status of container //       
        print('Container state Summary\n-----------------------')
               
        for c in ixp_argv:
            pad = (max(len(x) for x in ixp_argv) + 3 )
            status = _IxpLxc._lxc_status(self, c)[1]
            pad = pad - len(c)
            print(f'{c:<{pad}}: {status}')
    
        return(0)
    
    ## End of _ixp_server_build method

    #============================================#
    # //       _ixp_server_test_options       // #
    #============================================#

    def _ixp_server_test_options(self, ixp_argv):
        ''' Test 'ixp_server' cli options '''
        
        skip_yes_no = False
        first_options = ('show', 'start', 'stop', 'delete', 'build')  
        help_required = False
        element_list = list()

        # // Iterate over strings in list and lowercase them //
        ixp_argv = [string.lower() for string in ixp_argv]

        # // IXP Schema elements //
        e = _ixp_schema_servers()
        e.pop(0)

        # // Check if help called first //
        if (ixp_argv[0] in help_list): 
            IxpHelp._ixp_server_help(self, 0)

        # // Handle Abbreviations //
        if (ixp_argv[0] == 's'):
            print('Choose between:  show  start  stop\n')
            ixp_help.ixp_help(1)
        if (ixp_argv[0] == 'st'):
            print('Choose between:  start  stop\n')
            ixp_help.ixp_help(1)
        elif(ixp_argv[0] == '?'):
            ixp_argv[0] = 'help'
        else:
            for x in first_options:
                if (ixp_argv[0] == x[:len(ixp_argv[0])]):      
                    ixp_argv[0] = x

        # // Check first options are legitimate //
        if not (ixp_argv[0] in first_options):
            print(f'ERROR: ixp server \'{ixp_argv[0]}\' not valid option\n')
            _ixp_debug('y',f'ERROR: ixp server \'{ixp_argv[0]}\' not valid option')
            IxpHelp._ixp_server_help(self, 1)

        # // Create element list from remaining elements //
        if (len(ixp_argv) > 1):
            element_list = ixp_argv[1:]
            element_list = list(set(element_list))  # Remove duplicates   

        # // Check if help required //
        if(len(list(set(element_list).intersection(help_list)))): 
            help_required = True

        # // Handle 'show' help //
        if (ixp_argv[0] == 'show' and help_required == True):
            IxpHelp._ixp_server_show_help(self, 0)   

        # // Handle 'start' help //
        elif (ixp_argv[0] == 'start' and help_required == True):
            IxpHelp._ixp_server_start_help(self, 0)     

        # // Handle 'stop' help //
        elif (ixp_argv[0] == 'stop' and help_required == True):
            IxpHelp._ixp_server_stop_help(self, 0)  

        # // Handle 'delete' help //
        elif (ixp_argv[0] == 'delete' and help_required == True):
            IxpHelp._ixp_server_delete_help(self, 0)                 

        # // Handle 'build' help //
        elif (ixp_argv[0] == 'build' and help_required == True):
            IxpHelp._ixp_server_build_help(self, 0)  

        else:
            pass

        # // Handle '-y' //
        if (len(element_list) >= 1 and element_list[0] == '-y'):
            skip_yes_no = True
            element_list.remove('-y')

        # // Handle elements //
        if (len(element_list) == 0):
            element_list = e
            print(f'No options given, assuming \'all\'\n')
            _ixp_debug('y',f'INFO: No options given, assuming \'all\'')
        elif (element_list[0] == 'all'):
            element_list = e
        elif (len(element_list) >= 1):
           
            # // Handle an 'all' //
            if ('all' in element_list): 
                element_list = e

            # // Remaining options, test for illegal options //
            l = list()
            for x in element_list:
                if (x in e):
                    pass
                else:
                    l.append(x)
            if (len(l) == 1):
                print(f'ERROR: \'{l[0]}\' is not a valid option\n')
                _ixp_debug('y',f'ERROR[_ixp_server_test_options]: \'{l[0]}\' is not a valid option')

            elif (len(l) > 1):
                l_str = ", ".join(str(x) for x in l)
                print(f'ERROR: \'{l_str}\' are not valid options\n')
                _ixp_debug('y',f'ERROR[_ixp_server_test_options]: \'{l_str}\' are not valid options')

            else:
                pass
               
        else:
            print('ERROR: Some option I havent thought of yet\n')
            _ixp_debug('ERROR[_ixp_server_test_options]: Some option I havent thought of yet\n')

        return(skip_yes_no, ixp_argv[0], element_list)

    ## End _ixp_server_test_options method

    #============================================#
    # //               ixp_server             // #
    #============================================#

    def ixp_server(self, ixp_argv):
        ''' Root 'ixp_server' method to handle for 'ip server' commands '''

        (skip_yes_no, command, element_list) = self._ixp_server_test_options(ixp_argv)

        # // Check for non-schema instances //
        self._non_schema_instances_test(skip_yes_no)

        # // Check if 'build' called //
        if (command == 'build'):

            # // Build out the server instances //
            self._ixp_server_build(skip_yes_no, element_list)

            # // Exit //
            sys.exit(0)

        # // If first argument is 'show' //
        elif (command == 'show'): 

            # // Process selected elements //
            for y in element_list:
                pad = (max(len(x) for x in element_list) + 3 )
                status = _IxpLxc._lxc_status(self, y)

                if (status[1] == 'Nil'):
                    status_label = f'Doesn\'t exist, suggest :: $ ixp server build {y}'

                else:
                    status_label = status[1]
                pad = pad - len(y)
                print(f'{y:<{pad}} : {status_label}', end = ' ')

                # // Carry out Internet conenctivity test //
                status = int(_IxpLxc._lxc_internet_test(self, y))  
                if (status == 100):
                    print('and has good Internet connectivity\n')     
                elif (status >= 50):
                    print('and  has poor Internet connectivity\n')   
                else: 
                    print(f'\nERROR: {y} has no Internet connectivity, exiting\n')   
                    _ixp_debug('y', f'ERROR[ixp_server]: {y} has poor Internet connectivity, exiting')   
                    sys.exit(1)  
    
        # // If second argument is 'start' //
        elif (command == 'start'):

            # // Process selected elements //
            for y in element_list:

                if (_IxpLxc._lxc_status(self, y)[1] == 'Nil'):
                    print(f'{y} doesn\'t exist;\n')
                    _ixp_debug('y',f'INFO[ixp_server]: {y} doesn\'t exist;')
                    print(f'suggest running: \n\n$ ixp server build {y}')
                    print(f'$ ixp server start {y}\n')

                elif (_IxpLxc._lxc_status(self, y)[1] == 'Running'):
                    print(f'{y} is already running\n')
                    _ixp_debug('y', f'INFO[ixp_server]: {y} is already running')

                elif (_IxpLxc._lxc_status(self, y)[1] == 'Stopped'):
                    print(f'Starting the {y} server\n')
                    _ixp_debug('y', f'INFO[ixp_server]: Starting the {y} server')
                    _IxpLxc._lxc_start(self, y)

                else:
                    print('ERROR[ixp_server_state]: Unrecognoised status received')
                    _ixp_debug('y', 'ERROR[ixp_server]: Unrecognoised status received')
                    sys.exit(1)

            # // Exit //
            sys.exit(0)

        # // If second argument is 'stop' //
        elif (command == 'stop'):

            # // Process selected elements //              
            for y in element_list:

                if (_IxpLxc._lxc_status(self, y)[1] == 'Nil'):
                    print(f'{y} doesn\'t exist;\n')
                    _ixp_debug('y',f'INFO[ixp_server]: {y} doesn\'t exist;')
                    print(f'suggest running: \n\n$ ixp server build {y}')

                elif (_IxpLxc._lxc_status(self, y)[1] == 'Stopped'):
                    print(f'{y} is already stopped\n')
                    _ixp_debug('y', f'INFO[ixp_server]: {y} is already stopped')

                elif (_IxpLxc._lxc_status(self, y)[1] == 'Running'):
                    print(f'Stopping the {y} server\n')
                    _ixp_debug('y', f'INFO[ixp_server]: Stopping the {y} server')
                    _IxpLxc._lxc_stop(self, y)

                else:
                    print('ERROR[ixp_server]: Unrecognoised status received\n')
                    _ixp_debug('y', 'ERROR[ixp_server]: Unrecognoised status received')

            # // Exit //
            sys.exit(0)

        # // If second argument is 'delete' //
        elif (command == 'delete'):

            # // Process selected elements //
            for y in element_list:

                if (_IxpLxc._lxc_status(self, y)[1] == 'Nil'):
                    print(f'{y} doesn\'t exist, so no need to delete\n')
                    _ixp_debug('y', f'INFO[ixp_server_state]: {y} doesn\'t exist, so no need to delete')

                elif ((_IxpLxc._lxc_status(self, y)[1] == 'Running') or (_IxpLxc._lxc_status(self, y)[1] == 'Stopped')):
                    _IxpLxc._lxc_delete(self, skip_yes_no, y)

                else:
                    print(_IxpLxc._lxc_status(self, y)[1])
                    print('ERROR[ixp_server]: Unrecognoised status received\n')
                    _ixp_debug('y', 'ERROR[ixp_server]: Unrecognoised status received')

            # // Exit //
            sys.exit(0)

        # // Catch-all //
        else:
            print('ERROR[ixp_server]: Illegal ARGUMENT given\n')
            _ixp_debug('y','ERROR[ixp_servere]: Illegal ARGUMENT given')
            IxpHelp._ixp_server_help(self, 1)

        # // Exit //
        sys.exit(0)

    ## End to ixp_server method

########### // End IxpServer class // ###########

#################################################
# //            IxpSoftware class            // #
#################################################

class IxpSoftware:
    ''' IXP Software Class '''

    def __init__(self):
        '''IxpSoftware class constructor method'''
        
        self.software = {'ns' : ('bind9','bind9utils'), 
                         'rs' : ('bird',), 
                         'cs' : ('bird',), 
                         'bs' : ('bird', 'bind9', 'bind9utils')
                        }
        self.birdseye_file = 'birdseye-v1.1.4.tar.bz2'
        self.birdseye_directory = 'birdseye-v1.1.4'
        self.lighttpd_dir = '/etc/lighttpd/'
        self.lighttpd_conf_template = 'lighttpd.conf_template'
        self.as112_addrs = ('192.175.48.1/24','192.175.48.6/24','192.175.48.42/24',
                       '192.31.196.1/24','2620:4f:8000::1/48','2620:4f:8000::6/48',
                       '2620:4f:8000::42/48','2001:4:112::1/48')
        self.as112_addrs_flag = 0
        self.container_file = '/etc/netplan/10-netplan.yaml'
        self.dummy_interface = '/etc/systemd/network/dummy1.netdev'
        self.bs_bird_template = ('bs_bird.conf_template', 
                                 'bs_bird6.conf_template'
                                )
        self.bs_named_templates = ('bs_named.conf.options_template', 
                                   'bs_named.conf.local_template'
                                  ) 
        self.bs_bird_zone_templates = ('bs_db.dr-empty_template', 
                                       'bs_db.dd-empty_template',
                                       'bs_db.hostname.as112.arpa_template',
                                       'bs_db.hostname.as112.net_template'
                                       )
        bird_root = '/etc/bird/'
        bird_file = 'bird.conf'
        bird6_file = 'bird6.conf'
        self.bird_template = ('bird.conf_template', 'bird6.conf_template')
        self.martian_template = ('martians-v4_template', 'martians-v6_template')
        self.named_log_dir = '/var/log/named/'
        self.named_log_file = 'bind9.log'
        self.bind9_root = '/etc/bind/'
        self.zones_root = '/etc/bind/zones/'
        self.www_data_user_template = '91-www-data-user_template'
        self.www_data_user = '91-www-data-user'
        self.ns_named_conf_options_template = 'ns_named.conf.options_template'
        self.ns_named_conf_local_template = 'ns_named.conf.local_template'
        self.ns_db_domain_template = 'ns_db.domain_template'
        self.ns_db_ipv4_template = 'ns_db.ipv4_template'
        self.ns_db_ipv6_template =  'ns_db.ipv6_template'
        [self.LXD, self.NS, self.CS, self.RS, self.BS] = _ixp_schema_servers()
        self.ixp_containers = _ixp_schema_servers()
        self.ixp_containers.pop(0)
        self.ixp_ns_ip = _read_schema_yaml()[1][self.NS][0]['ipv4-peer'].split('/')[0]
        self.ixp_servers_plus = dict()

        # // Build 'self.ixp_servers_plus' list //
        for k in _ixp_schema_servers():
            kless = re.findall('(\w+)\d+', k)
            self.ixp_servers_plus[k] = ixp_servers[kless[0]]

    ## End of __init__ method 

    #============================================#
    #  //        _replace_nameserver          // #
    #============================================#

    def _replace_nameserver(self, c, old_ns, new_ns):
        ''' Method to replace the upstream server with NS at the top of the list '''

        print(f'Replacing {old_ns} with {new_ns} on the {c} server\n')
        _ixp_debug('y', f'INFO[_replace_nameserver]: Replacing {old_ns} with {new_ns} on the {c} server')
        
        ns_sed = (f"sed -i 's/{old_ns}/{new_ns}/g' {netplan_file}")
        _IxpLxc._lxc_exec(self, c, ns_sed) 
       
        return(0)

    ## End _replace_nameserver method

    #============================================#
    #  //        _bind_structure_test         // #
    #============================================#

    def _bind_structure_test(self, c):
        ''' Method to check if BIND9 directory structure exists and backup if so '''

        # // Check the /var/log/named directory exists //
        dir_test = _IxpLxc._lxc_exec(self, c, f'test -d {self.named_log_dir}; echo $?')
        if (int(dir_test[1]) == 0):
            _ixp_debug('y', f'INFO[_bind_structure_test]: {self.named_log_dir} already exists on {c}')
                
        else:
            _ixp_debug('y', f'INFO[_bind_structure_test]: Creating the {self.zones_root} directory on {c}')
            _IxpLxc._lxc_exec(self, c, f'mkdir {self.named_log_dir}')
            
        file_test = _IxpLxc._lxc_exec(self, c, f'test -f {self.named_log_dir}{self.named_log_file}; echo $?')

        if (int(file_test[1]) == 0):
             _ixp_debug('y', f'INFO[_bind_structure_test]: {self.named_log_dir}{self.named_log_file} already exists on {c}') 
      
        else: 
            _IxpLxc._lxc_exec(self, c, f'touch {self.named_log_dir}{self.named_log_file}')

        _IxpLxc._lxc_exec(self, c, f'chown -R bind:bind {self.named_log_dir}')
        _IxpLxc._lxc_exec(self, c, f'chmod 755 {self.named_log_dir}')
        _IxpLxc._lxc_exec(self, c, f'chmod -R 644 {self.named_log_dir}{self.named_log_file}')

        # // Check the named.conf.local' named.conf.options exists and backup if it does //
        for f in ('named.conf.options', 'named.conf.local'):

            file_test = _IxpLxc._lxc_exec(self, c, f'test -f {self.bind9_root}{f}; echo $?')

            if (int(file_test[1]) == 0):
                _IxpLxc._lxc_exec(self, c, f'cp {self.bind9_root}{f} {self.bind9_root}{f}.bak')
                _IxpLxc._lxc_exec(self, c, f'chown -R root:bind {self.bind9_root}{f}.bak')
        else:
            _ixp_debug('y', f'INFO[_bind_structure_test]: {self.bind9_root}{f} doesn\'t exist on {c}')

        # // Create the zones directory //
        zones_test = _IxpLxc._lxc_exec(self, c, f'test -d {self.zones_root}; echo $?')

        if not (int(zones_test[1]) == 0):
            _IxpLxc._lxc_exec(self, c, f'mkdir {self.zones_root}')
            _IxpLxc._lxc_exec(self, c, f'chown -R root:bind {self.zones_root}')
            _IxpLxc._lxc_exec(self, c, f'chmod 755 {self.zones_root}')
            _IxpLxc._lxc_exec(self, c, f'chmod g+s {self.zones_root}')

        else: 
            _ixp_debug('y', f'ERROR[ns_software_configure_build]: {self.zones_root} directory doesn\'t exist on {c} so creating')

        print('BIND structure test completed successfully\n')

        return(0)

    ## End of _bind_structure_test method

    #============================================#
    #  //        _nameserver_configure        // #
    #============================================#

    def _nameserver_configure(self, c):
        ''' Configure nameserver '''

        print('Configuring and building the NS software\n')
        _ixp_debug('y','INFO[_nameserver_configure]: Configure and build the NS software')

        # // IXP Schema elements //
        e = _ixp_schema_servers()
        e.pop(0)
    
        # // Check the BIND9 structure exists and backup if it does //
        self._bind_structure_test(c)

        # // ###### Create the names.conf.options file ###### //
        print('Configuring and building the bind named.conf.options file\n')
        _ixp_debug('y','INFO[rs_software_configure_build]: Configuring and building the bind named.conf.options file')

        # Pointer to the NS self.ns_named_conf_options_template  template file //
        template_file = f'{var_dir}{self.ns_named_conf_options_template}'
        ns_named_conf_options = f'{var_dir}ns_named.conf.options'

        # // Check the NS named.conf.options template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput(f'cp {template_file} {ns_named_conf_options}') 
            print(f'Created the file {ns_named_conf_options}\n')
            _ixp_debug('y',f'INFO[_nameserver_configure]:  Created the file {ns_named_conf_options}')

        else:
            print(f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Insert forwarders in NS named.conf.options file //
        resolvers = str()

        for e in upstream_ns:

            if (upstream_ns.index(e) != (len(upstream_ns) - 1)):
                resolvers = resolvers + f'          {e};\n'

            else:
                resolvers = resolvers + f'          {e};'
        
        _string_search_replace(ns_named_conf_options, '      <<resolvers>>', resolvers)

        # // Nameserver details from schema //
        yaml_schema_docs = _read_schema_yaml()
        server_ip = yaml_schema_docs[1][c]
        server_ip.pop(-1)
        domain = server_ip.pop(-1)

        avps = dict()
        
        counter = 0
        for e in server_ip:

            for key in e:
                net_name = f'{key}-net'
                ip_name = f'{key}'
                stripped_net = re.findall(r'(.*[::|\.])(\d{1,3})\/(\d{1,2})', e[key])[0]

                if ('::' in stripped_net[0]):
                    new_net = f'{stripped_net[0]}/{stripped_net[2]}'
                    new_ip = f'{stripped_net[0]}{stripped_net[1]}'

                else:
                    new_net = f'{stripped_net[0]}0/{stripped_net[2]}'
                    new_ip = f'{stripped_net[0]}{stripped_net[1]}'

                avps[ip_name] = new_ip
                avps[f'{ip_name}-net'] = new_net  

        total_tab = 20

        # // Insert IP addresses and Networks from IXP Schema //
        for k in avps:
            s = (' ' * (20 - len(avps[k])))
            _string_search_replace(ns_named_conf_options, f'<<{k}>>;', f'{avps[k]};{s}')  
      
        # //Push the new file to the NS container //  
        _IxpLxc._lxc_push(self, c, ns_named_conf_options, '/etc/bind/named.conf.options') 
        _IxpLxc._lxc_exec(self, c, 'chown -R root:bind /etc/bind/named.conf.options')   

        # // ###### Create the query log file and give bind ownership ###### //
        print('Configuring and building the bind query log file\n')
        _ixp_debug('y','INFO[rs_software_configure_build]: Configuring and building the bind query log file')

        # // Check the NS named.conf.options template file exists //
        # // ###### Define the local forward zone    ###### //
        # // ###### Create the names.conf.local file ###### //
        print('Configuring and building the bind named.conf.local file\n')
        _ixp_debug('y','INFO[rs_software_configure_build]: Configuring and building the bind named.conf.local file')

        # // Extract network elements and in-addr.arpa and ip6.arpa //
        i = re.findall(r'(.*?.0)/.*', avps['ipv4-man-net'])
        ipv4_arpa = ipaddress.ip_address(i[0]).reverse_pointer
        ipv4_arpa_no0 = re.findall(r'[0.]*(.*)', ipv4_arpa)
        avps['ipv4-man-net-nomask'] = i[0]
        avps['ipv4-man-arpa'] = ipv4_arpa_no0[0]
        j = re.findall(r'(.*?).0', avps['ipv4-man-net-nomask'])[0]

        avps['ipv4-man-net-nomask-no0'] = j
        i6 = re.findall(r'(.*?::)/.*', avps['ipv6-man-net'])
        i6_nodcolon = re.findall(r'(.*?)::', i6[0])
        ipv6_arpa = ipaddress.IPv6Address(i6[0]).reverse_pointer
        ipv6_arpa_no0 = re.findall(r'[0.]*(.*)', ipv6_arpa)
        avps['ipv6-man-net-nomask'] = i6_nodcolon[0]
        avps['ipv6-man-arpa'] = ipv6_arpa_no0[0]

        # // Pointer to the self.ns_named_conf_local_template template file //
        template_file = f'{var_dir}{self.ns_named_conf_local_template}' 
        ns_named_conf_local = f'{var_dir}ns_named.conf.local'
        
        # // Check the NS named.conf.local template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput(f'cp {template_file} {ns_named_conf_local}') 
            print(f'Created the file {ns_named_conf_local}\n')
            _ixp_debug('y', f'INFO[_nameserver_configure]:  Created the file {ns_named_conf_local}')

        else:
            print(f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Make changes to NS named.conf.local file //
        _string_search_replace(ns_named_conf_local, '<<domain>>', domain['domain'])

        named_conf_local_elements = ('ipv4-man-net-nomask-no0', 'ipv4-man-arpa', 'ipv6-man-net-nomask', 'ipv6-man-arpa')

        for e in named_conf_local_elements:
            _string_search_replace(ns_named_conf_local, f'<<{e}>>', f'{avps[e]}')

        # // Push the new file to the NS container // 
        _IxpLxc._lxc_push(self, c, ns_named_conf_local, f'{self.bind9_root}named.conf.local') 
        _IxpLxc._lxc_exec(self, c, f'chown -R root:bind {self.bind9_root}named.conf.local') 
     
        # // ###### Make the zones directory if necessary ###### //
        # // ###### Create the db.domain file             ###### //
        print('Configuring and building the bind db.domain file\n')
        _ixp_debug('y','INFO[_nameserver_configure]: Configuring and building the bind db.domain file')

        # Pointer to the ns_db_domain_template template file //
        template_file = f'{var_dir}{self.ns_db_domain_template}'
        ns_db_domain = f'{var_dir}ns_db.domain'
        
        # // Check the ns_db_domain_template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput(f'cp {template_file} {ns_db_domain}') 
            print(f'Created the file {ns_db_domain}\n')
            _ixp_debug('y', f'INFO[_nameserver_configure]:  Created the file {ns_db_domain}')

        else:
            print(f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Make changes to NS ns_db_domain file //

        # // Replace the date time group under Serial //
        _string_search_replace(ns_db_domain, '<<yyyymmdd>>', _ixp_timestamp()[1])

        # // Replace the NS name //
        _string_search_replace(ns_db_domain, '<<ns>>', c)

        # // Replace the domain name //
        _string_search_replace(ns_db_domain, '<<domain>>', domain['domain'])

        # // Append the hosts to the file //
        try: 
             fh= open(ns_db_domain,'a')
        except:
             print(f'Cannot open {ns_db_domain}\n')
             _ixp_debug('y', f'ERROR[_nameserver_configure]: Cannot open {ns_db_domain}')

        fh.write('\n')

        for key, value in sorted(self.ixp_servers_plus.items(), key = itemgetter(1)):
            s = ' ' * (12 - len(key))
            in_a = 'IN   A      '
            fh.write(f"{key}{s}{in_a}{avps['ipv4-man-net-nomask-no0']}.{value}\n")
        fh.write('\n')

        for key, value in sorted(self.ixp_servers_plus.items(), key = itemgetter(1)):
            s = ' ' * (12 - len(key))
            in_a = 'IN   AAAA   '
            fh.write(f"{key}{s}{in_a}{avps['ipv6-man-net-nomask']}::{value}\n")
        fh.write('\n')

        # // Add CNAME entries in db.hostname for birdseye //
        ns_birdseye_cname = (f'{self.RS}-lan1-ipv4   IN    CNAME    {self.RS}.',
                             f'{self.RS}-lan1-ipv6   IN    CNAME    {self.RS}.',
                             f'{self.CS}-lan1-ipv4   IN    CNAME    {self.CS}.',
                             f'{self.CS}-lan1-ipv6   IN    CNAME    {self.CS}.',
                             f'{self.BS}-lan1-ipv4   IN    CNAME    {self.BS}.',
                             f'{self.BS}-lan1-ipv6   IN    CNAME    {self.BS}.'
                            )

        fh.write('; Added for Birdeye\n')
        fh.write('\n')

        for cname in ns_birdseye_cname:
            fh.write(f"{cname}{domain['domain']}.\n")

        fh.write('\n')

        # // Close filehandle //
        fh.close()

        # //Push the new file to the NS container // 
        _IxpLxc._lxc_push(self, c, ns_db_domain, f"{self.zones_root}db.{domain['domain']}")
        _IxpLxc._lxc_exec(self, c, f"chown -R root:bind {self.zones_root}db.{domain['domain']}")

        # // ###### Define the reverse lookup zones          ###### //
        # // ###### Create the IPv4 reverse lookup zone file ###### //

        print('Configuring and building the IPv4 reverse lookup zone\n')
        _ixp_debug('y','INFO[_nameserver_configure]: Configuring and building the IPv4 reverse lookup zone')

        # Pointer to the self.ns_db_ipv4_template template file //
        template_file = f'{var_dir}{self.ns_db_ipv4_template}'
        ns_db_ipv4 = f'{var_dir}ns_db.ipv4'
    
        # // Check the self.ns_db_ipv4_template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput(f'cp {template_file} {ns_db_ipv4}') 
            print(f'Created the file {ns_db_ipv4}\n')
            _ixp_debug('y', f'INFO[_nameserver_configure]:  Created the file {ns_db_ipv4}')
        else:
            print(f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Make changes to NS ns_db_ipv4 file //

        # // Replace the date time group under Serial //
        _string_search_replace(ns_db_ipv4, '<<yyyymmdd>>', _ixp_timestamp()[1])

        # // Replace the NS name //
        _string_search_replace(ns_db_ipv4, '<<ns>>', c)

        # // Replace the domain name //
        _string_search_replace(ns_db_ipv4, '<<domain>>', domain['domain'])

        # Replace the ipv4-man-net and ipv4-man-arpa //
        _string_search_replace(ns_db_ipv4, '<<ipv4-man-net>>', avps['ipv4-man-net'])
        _string_search_replace(ns_db_ipv4, '<<ipv4-man-arpa>>', avps['ipv4-man-arpa'])

        # // Append the hosts to the file //
        try: 
             fh= open(ns_db_ipv4,'a')
        except:
             print(f'Cannot open {ns_db_ipv4}\n')
             _ixp_debug('y', f'ERROR[_nameserver_configure]: Cannot open {ns_db_ipv4}')

        fh.write('\n')

        for key, value in sorted(self.ixp_servers_plus.items(), key = itemgetter(1)):
            s = ' ' * (8 - len(value))
            in_ptr = 'IN   PTR      '
            fh.write(f"{value}{s}{in_ptr}{key}.{domain['domain']}.\n")

        fh.write('\n')

        fh.close()

        # //Push the new file to the NS container // 
        _IxpLxc._lxc_push(self, c, ns_db_ipv4, f"{self.zones_root}db.{avps['ipv4-man-net-nomask-no0']}")
        _IxpLxc._lxc_exec(self, c, f"chown -R root:bind {self.zones_root}db.{avps['ipv4-man-net-nomask-no0']}")

        # // ###### Create the IPv6 reverse lookup zone file ###### //
        print('Configuring and building the IPv6 reverse lookup zone\n')
        _ixp_debug('y','INFO[_nameserver_configure]: Configuring and building the IPv6 reverse lookup zone')

        # Pointer to the self.ns_db_ipv6_template template file //
        template_file = f'{var_dir}{self.ns_db_ipv6_template}'
        ns_db_ipv6 = f'{var_dir}ns_db.ipv6'
    
        # // Check the self.ns_db_ipv6_template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput(f'cp {template_file} {ns_db_ipv6}') 
            print(f'Created the file {ns_db_ipv6}\n')
            _ixp_debug('y', f'INFO[_nameserver_configure]:  Created the file {ns_db_ipv6}')
        else:
            print(f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Make changes to NS ns_db_ipv6 file //

        # // Replace the date time group under Serial //
        _string_search_replace(ns_db_ipv6, '<<yyyymmdd>>', _ixp_timestamp()[1])

        # // Replace the NS name //
        _string_search_replace(ns_db_ipv6, '<<ns>>', c)

        # // Replace the domain name //
        _string_search_replace(ns_db_ipv6, '<<domain>>', domain['domain'])

        # Replace the ipv6-man-net and ipv6-man-arpa //
        _string_search_replace(ns_db_ipv6, '<<ipv6-man-net>>', avps['ipv6-man-net'])
        _string_search_replace(ns_db_ipv6, '<<ipv6-man-arpa>>', avps['ipv6-man-arpa'])

        # // Append the hosts to the file //
        try: 
             fh= open(ns_db_ipv6,'a')
        except:
             print(f'Cannot open {ns_db_ipv6}\n')
             _ixp_debug('y', f'ERROR[_nameserver_configure]: Cannot open {ns_db_ipv6}')

        fh.write('\n')

        # // PTR should have 32 - (length of ipv6-man-arpa) elements //
        len_ip6_arpa = len('ip6.arpa')
        len_ptr = 32 - (int((len(avps['ipv6-man-arpa']) - len_ip6_arpa)/2))

        for key, value in sorted(self.ixp_servers_plus.items(), key = itemgetter(1)):

            ptr_str = ''
            value = list(value) 
            value.reverse()
            len_value = len(value)

            # // Generate PRT entry
            for e in value:
                ptr_str = ptr_str + e +'.'

            for x in range(0, (len_ptr - len_value - 1)):
                ptr_str = ptr_str + '0.'

            ptr_str = ptr_str + '0'

            in_ptr = 'IN   PTR    '
            fh.write(f"{ptr_str}    {in_ptr}{key}.{domain['domain']}.\n")

        fh.write('\n')

        fh.close()

        # //Push the new file to the NS container // 
        _IxpLxc._lxc_push(self, c, ns_db_ipv6, f"{self.zones_root}db.{avps['ipv6-man-net-nomask']}")
        _IxpLxc._lxc_exec(self, c, f"chown -R root:bind {self.zones_root}db.{avps['ipv6-man-net-nomask']}")

        # // Restart the DNS Server //
        _IxpLxc._lxc_exec(self, c, 'systemctl enable bind9')
        _IxpLxc._bind9_restart(self, c)

        # // ###### Update nameserver NS in the other running containers ###### //

        for container in self.ixp_containers:
            status = _IxpLxc._lxc_status(self, container)

            if (status[1] == 'Running'):
                self._replace_nameserver(container, upstream_ns[0], self.ixp_ns_ip)
            else:
                print(f'The {container} server is not \'Running\' and will need to be updated\n')
                _ixp_debug('y', f'INFO[_nameserver_configure]: The {container} server is not \'Running\' and will need to be updated')

            _IxpLxc._lxc_exec(self, container, 'netplan apply')
        
        print(f'The nameserver {c} has been configured successfully\n')

        return(0)

    ## End to _nameserver_configure method

    #============================================#
    #  //           _bird_configure           // #
    #============================================#

    def _bird_configure(self, c):
        ''' Method to build the BIRD configuration on containers '''

        print(f'Configuring and building the {c} software\n')
        _ixp_debug('y', f'INFO[_bird_configure]: Configure and build the {c} software')

        # // Get information from schema relating to the server //
        yaml_schema_docs = _read_schema_yaml()
        yaml_schema = yaml_schema_docs[1][c]
        yaml_schema_dict = dict()

        for e in range(len(yaml_schema)):

            for key in yaml_schema[e]:

                if (key == 'ipv4-peer'): 
                    addr4 = re.findall(r'(.*)/\d{1,2}', yaml_schema[e][key])
                    yaml_schema_dict[key] = addr4[0]

                elif (key == 'ipv6-peer'):
                    addr6 = re.findall(r'(.*)/\d{1,2}', yaml_schema[e][key])
                    yaml_schema_dict[key] = addr6[0]

                elif (key == 'as-number'):
                    yaml_schema_dict[key] = str(yaml_schema[e][key])

        # // Loop through testing the bird and bird6 template files and create //
        for x in (0,1):

            # // Pointer to the bird template file //
            if (x == 0):
                template = f'{var_dir}{self.bird_template[0]}'
                bird_file = f'{var_dir}{c}_bird.conf'
                mtemplate = f'{var_dir}{self.martian_template[0]}'
                martian_file = f'{var_dir}{c}_import_policy_v4'
  
            else:
                template = f'{var_dir}{self.bird_template[1]}'
                bird_file = f'{var_dir}{c}_bird6.conf'
                mtemplate = f'{var_dir}{self.martian_template[1]}'
                martian_file = f'{var_dir}{c}_import_policy_v6'

            if (path.isfile(template)):
                subprocess.getstatusoutput(f'cp {template} {bird_file}') 
                _ixp_debug('y', f'INFO[_bird_configure]: Created the file {bird_file}')

            else:
                print(f'ERROR: {template} doesn\'t exist\n')
                _ixp_debug('y', f'ERROR[_bird_configure]: {template} doesn\'t exist')
                sys.exit(1)

            if (path.isfile(mtemplate)):
                subprocess.getstatusoutput(f'cp {mtemplate} {martian_file}') 
                _ixp_debug('y', f'INFO[_bird_configure]: Created the file {martian_file}')

            else:
                print(f'ERROR: {mtemplate} doesn\'t exist\n')
                _ixp_debug('y', f'ERROR[_bird_configure]: {mtemplate} doesn\'t exist')
                sys.exit(1)

            # // Make the changes to the files //
            for e in yaml_schema_dict:
                _string_search_replace(bird_file, f'<<{e}>>', yaml_schema_dict[e])

            # // If the server is CS then turn off export //
            if (c == self.CS):
                _string_search_replace(bird_file, 'export all;', '#export all;')

            # // Extract the remote file names //
            remote_file = re.findall(r'.*_(.*)', bird_file)
            remote_martian_file = re.findall(r'.*_(import_policy.*)', martian_file)

            # // Check the IXP bird configuration exists and backup if it does //
            file_test = _IxpLxc._lxc_exec(self, c, f'test -f {bird_root}{remote_file[0]}; echo $?')
            if (int(file_test[1]) == 1):
                print(f'{remote_file[0]} doesn\'t exist on {c} so nothing to backup\n')
                _ixp_debug('y', f'INFO[_bird_configure]: {remote_file[0]} doesn\'t exist on {c} so nothing to backup')            
            else:
                print(f'Backing up the {remote_file[0]} file on {c}\n')
                _ixp_debug('y', f'INFO[_bird_configure]: Backing up the {remote_file[0]} file on {c}')
                _IxpLxc._lxc_exec(self, c, f'mv {bird_root}{remote_file[0]} {bird_root}{remote_file[0]}.bak')       

            # // Push files to the server //
            _IxpLxc._lxc_push(self, c, bird_file, f'{bird_root}{remote_file[0]}')
            _IxpLxc._lxc_push(self, c, martian_file, f'{bird_root}{remote_martian_file[0]}')
            _IxpLxc._lxc_exec(self, c, f'chown -R bird:bird {bird_root}{remote_file[0]}') 
            _IxpLxc._lxc_exec(self, c, f'chown -R bird:bird {bird_root}{remote_martian_file[0]}')
            _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {bird_root}{remote_file[0]}')   
            _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {bird_root}{remote_martian_file[0]}')   

            # // Restart the bird Server //
            _IxpLxc._bird_restart(self, c)

        print(f'BIRD configuration on {c} completed successfully\n')

        return(0)

    ## End to _bird_configure method

    #============================================#
    #  //         _birdseye_configure         // #
    #============================================#
    
    def _birdseye_configure(self, c):
        ''' Method to configure Birdseye '''

        # // Configure the Birdseye // 
        birdeye_BIRDC = list()
        birdeye_BIRDC6 = list()
  
        # // Give user www-data permission to run birdc and birdc6 scripts //
        try:
            _IxpLxc._lxc_push(self, c, f'{var_dir}{self.www_data_user_template}', f'/etc/sudoers.d/{self.www_data_user}') 
 
        except:
            print(f'ERROR: Unable to upload the {self.www_data_user} file on {c}\n')
            _ixp_debug('y', f'ERROR[_birdseye_configure]: Unable to upload the {self.www_data_user} file on {c}')  
            sys.exit(1) 

        # // Change ownership to root & permissions to 440 on the 91-www-data-user file //
        _IxpLxc._lxc_exec(self, c, f'chown root: /etc/sudoers.d/{self.www_data_user}')
        _IxpLxc._lxc_exec(self, c, f'chmod 440 /etc/sudoers.d/{self.www_data_user}') 
        _ixp_debug('y', f'INFO[_birdseye_configure]: Changing ownership & permissions on the {self.www_data_user} file on {c}')
 
        # // Edit the birdseye environment file for IPv4 //
        _ixp_debug('y', f'INFO[_birdseye_configure]: Generating the /srv/birdseye/birdseye-{c}-lan1-ipv4.env file')
        _IxpLxc._lxc_exec(self, c, f'cp /srv/birdseye/.env.example /srv/birdseye/birdseye-{c}-lan1-ipv4.env')
        _IxpLxc._lxc_exec(self, c, f'sed -i.bak \'s/^BIRDC/#BIRDC/\' /srv/birdseye/birdseye-{c}-lan1-ipv4.env')

        birdeye_BIRDC.append('')
        birdeye_BIRDC.append('BIRDC=\\"/usr/bin/sudo /srv/birdseye/bin/birdc -4 -s /var/run/bird/bird.ctl\\"')
        birdeye_BIRDC.append('LOOKING_GLASS_ENABLED=true')

        for l in birdeye_BIRDC:
            _IxpLxc._lxc_exec(self, c, f'sh -c \'echo {l} >> /srv/birdseye/birdseye-{c}-lan1-ipv4.env\'')

        # // Edit the birdseye environment file for IPv6 //
        _ixp_debug('y', f'INFO[_birdseye_configure]: Generating the /srv/birdseye/birdseye-{c}-lan1-ipv6.env file') 
        _IxpLxc._lxc_exec(self, c, f'cp /srv/birdseye/.env.example /srv/birdseye/birdseye-{c}-lan1-ipv6.env')
        _IxpLxc._lxc_exec(self, c, f'sed -i.bak \'s/^BIRDC/#BIRDC/\' /srv/birdseye/birdseye-{c}-lan1-ipv6.env')

        birdeye_BIRDC6.append('')
        birdeye_BIRDC6.append('BIRDC=\\"/usr/bin/sudo /srv/birdseye/bin/birdc -6 -s /var/run/bird/bird6.ctl\\"')
        birdeye_BIRDC6.append('LOOKING_GLASS_ENABLED=true')

        for l in birdeye_BIRDC6:
            _IxpLxc._lxc_exec(self, c, f'sh -c \'echo {l} >> /srv/birdseye/birdseye-{c}-lan1-ipv6.env\'')

        # // Information back to the shell //
        print(f'Generating the following files on {c}')
        print(f'   - /etc/sudoers.d/{self.www_data_user}')
        print(f'   - /srv/birdseye/birdseye-{c}-lan1-ipv4.env file')
        print(f'   - /srv/birdseye/birdseye-{c}-lan1-ipv6.env file\n')
        _ixp_debug('y', f'INFO[_birdseye_configure]: Generating the following files on {c}')
        _ixp_debug('y', f'                 - /etc/sudoers.d/{self.www_data_user}')
        _ixp_debug('n', f'                 - /srv/birdseye/birdseye-{c}-lan1-ipv4.env file')
        _ixp_debug('n', f'                 - /srv/birdseye/birdseye-{c}-lan1-ipv6.env file')

        print(f'BIRDSEYE configuration on {c} completed successfully\n')
        
        return(0)
            
    ## End of _birdseye_configure method

    #============================================#
    #  //    _bs_software_configure_build     // #
    #============================================#
    
    def _bs_software_configure_build(self, c):
        ''' Method to configure the blackhole AS112 server '''

        ########### // Handle the netplan for BS // #############

        # // See if a dummy interface already exists //
        dummy_test = _IxpLxc._lxc_exec(self, c, f'[ -f \'{self.dummy_interface}\' ]; echo $?')

        if (dummy_test[1] == '0'):
            _ixp_debug('y', f'INFO[_bs_software_configure_build]: The {c}: {self.dummy_interface} file already exists')

        # // Build the dummy interface //
        else:
            print(f'There is no {self.dummy_interface} on {c}\n')
            _ixp_debug('y', f'INFO[_bs_software_configure_build]: There is no {self.dummy_interface} on {c}')

            # // Create a dummy file //
            dummy_ifce = ['[NetDev]']
            dummy_ifce.append('Name=dummy1')
            dummy_ifce.append('Kind=dummy')

            for d in dummy_ifce:
                print(c, f'sh -c \"echo \'{d}\' >> {self.dummy_interface}\"')
                _IxpLxc._lxc_exec(self, c, f'sh -c \"echo \'{d}\' >> {self.dummy_interface}\"')
 
            dummy_ifce_install = _IxpLxc._lxc_exec(self, c, f'sh -c \"cat {self.dummy_interface}\"')
            print (dummy_ifce_install[1])

            print(f'Configured and built the {c} netplan file\n')
            _ixp_debug('y', f'INFO[_bs_software_configure_build]: Configured and built the {c} netplan file')

        # // See if AS112 configuration already exists in BS netplan file //
        netplan_exist = _IxpLxc._lxc_exec(self, c, f'cat {self.container_file}')

        # // Loop through AS112 addresses //
        for a in self.as112_addrs:
 
            if (a in netplan_exist[1]):
                self.as112_addrs_flag += 1

        if (self.as112_addrs_flag == len(self.as112_addrs)):
                print (f'{c} netplan already configured\n')
                _ixp_debug('y', f'INFO[_bs_software_configure_build]: {c} netplan already configured')

        elif ((self.as112_addrs_flag < len(self.as112_addrs) and self.as112_addrs_flag > 0)):
                print (f'ERROR[_bs_software_configure_build]: {c} netplan incorrectly configured, fix manually, exiting\n')
                _ixp_debug('y', f'ERROR[_bs_software_configure_build]: {c} netplan incorrectly configured, fix manually, exiting')

        else:

            # // Create string to add to BS netplan file //
            netplan_br = ['\n    dummy1: {}']    
            netplan_br.append('\n  bridges:')
            netplan_br.append('    as112_br1:')
            netplan_br.append('      interfaces: [dummy1]')
            netplan_br.append('      addresses:')
            for a in self.as112_addrs:
                netplan_br.append(f'          - {a}')

            # // Add to c netplan file //  
            for b in netplan_br:
                _IxpLxc._lxc_exec(self, c, f'sh -c \"echo \'{b}\' >> {self.container_file}\"')
        
        # // Applying new netplan file on container //
        netplan_apply = _IxpLxc._lxc_exec(self, c, 'netplan apply')
   
        if not (netplan_apply[0] == 0):
            print(f'ERROR: Cannot apply netplan on {c}\n')
            _ixp_debug('y', f'ERROR[_bs_software_configure_build]: Cannot apply netplan on {c}')
            sys.exit(1)
        else:    
            print(f'New netplan applied on {c}\n')
            _ixp_debug('y', f'INFO[_bs_software_configure_build]: New netplan applied on {c}')

        ########### // Handle the BIRD files for BS // #############

        # // Configure and install the bird.conf and bird6.conf //    
        bs_bird_file = f'{c}_bird.conf'
        bs_bird6_file = f'{c}_bird6.conf'

        bs_bird_path_file = f'{var_dir}{bs_bird_file}'
        bs_bird6_path_file = f'{var_dir}{bs_bird6_file}'

        # // Create BS bird configuration files from templates //
        subprocess.getstatusoutput(f'cp {var_dir}{self.bs_bird_template[0]} {bs_bird_path_file}')
        subprocess.getstatusoutput(f'cp {var_dir}{self.bs_bird_template[1]} {bs_bird6_path_file}')

        # // Get the peer IPv4 address of the BS for the router ID // 
        yaml_schema_docs = _read_schema_yaml()
        bs_ip = _validate_ip_net(yaml_schema_docs[1][c][0]['ipv4-peer'])
        bs_ipv6 = _validate_ip_net(yaml_schema_docs[1][c][1]['ipv6-peer'])

        # // Search and replace the routerID field in the two bird files //
        for x in (bs_bird_path_file, bs_bird6_path_file):
            _string_search_replace(x, '<<ipv4-bs-peer>>' , bs_ip['ip'])
            _string_search_replace(x, '<<ipv6-bs-peer>>' , bs_ipv6['ip'])

        # // Backup the original bird configuration files //
        _IxpLxc._lxc_exec(self, c, f'cp {bird_root}{bird_file} {bird_root}{bird_file}.bak') 
        _IxpLxc._lxc_exec(self, c, f'cp {bird_root}{bird6_file} {bird_root}{bird6_file}.bak') 

        # // Copy new bird configuration files //
        _IxpLxc._lxc_push(self, c, f'{var_dir}{bs_bird_file}', f'{bird_root}{bird_file}') 
        _IxpLxc._lxc_push(self, c, f'{var_dir}{bs_bird6_file}', f'{bird_root}{bird6_file}') 
        _IxpLxc._lxc_exec(self, c, f'chown -R bird:bird {bird_root}{bird_file}') 
        _IxpLxc._lxc_exec(self, c, f'chown -R bird:bird {bird_root}{bird6_file}')
        _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {bird_root}{bird_file}')   
        _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {bird_root}{bird6_file}')  

        # // Print message to confirm //
        print(f'Created new {bird_file} and {bs_bird6_file} for {c} and uploaded them\n')
        _ixp_debug('y', f'INFO[_bs_software_configure_build]: Created new {bird_file} and {bs_bird6_file} for {c} and uploaded them')  

        # // Print message to confirm //
        _IxpLxc._bird_restart(self, c)

        # Configure birdseye for BS //
        self._birdseye_configure(c)

        ########### // Handle the BIND9 files for BS // #############

        # // Backup the original BIND9 configuration files //
        print('Configuring and building the BS bind9 software\n')
        _ixp_debug('y','INFO[ns_software_configure_build]: Configure and build the BS software')

        # // Check the BIND9 structure exists and backup if it does //
        self._bind_structure_test(c)

        # // Create the BIND9 named.conf files from templates //
        named_conf_files = list()

        for x in self.bs_named_templates:
            f = re.findall(r'bs(_named\.conf\.\w+)_template', x)
            m = f'{var_dir}{c}{f[0]}'
            named_conf_files.append(f'{m}') 
            subprocess.getstatusoutput(f'cp {var_dir}{x} {m}')

        # // Search and replace the listening-on field in the two bird files //
        _string_search_replace(named_conf_files[0], '<<ipv4-bs-peer>>' , bs_ip['ip'])
        _string_search_replace(named_conf_files[0], '<<ipv6-bs-peer>>' , bs_ipv6['ip'])

        # // Move files to the BS container //
        _IxpLxc._lxc_push(self, c, named_conf_files[0], f'{self.bind9_root}named.conf.options')
        _IxpLxc._lxc_push(self, c, named_conf_files[1], f'{self.bind9_root}named.conf.local')
        _IxpLxc._lxc_exec(self, c, f'chown -R bind:bind {self.bind9_root}named.conf.options') 
        _IxpLxc._lxc_exec(self, c, f'chown -R bind:bind {self.bind9_root}named.conf.local')
        _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {self.bind9_root}named.conf.options')   
        _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {self.bind9_root}named.conf.local')  
        
        # // Handle the zone files //
        domain = yaml_schema_docs[1][c][4]['domain']   

        named_zone_files = list()

        # // Create zone files for the BS //
        for x in self.bs_bird_zone_templates:
            
            f = re.findall(r'bs_(.*?)_template', x)
            named_zone_files.append(f'{var_dir}{c}_{f[0]}') 
            m = f'{var_dir}{c}_{f[0]}'
            subprocess.getstatusoutput(f'cp {var_dir}{x} {m}')

        # // Replace the timestamp, domain and nameserver name in the zone files //
        for x in named_zone_files:

            _string_search_replace(x, '<<yyyymmdd>>', _ixp_timestamp()[1]) 
            _string_search_replace(x, '<<ns>>', c)
            _string_search_replace(x, '<<domain>>', domain)
            f = re.findall(r'/var/ixp/bs\d_(.*)', x)
            _IxpLxc._lxc_push(self, c, x, f'{self.bind9_root}zones/{f[0]}')
            m = f'{self.bind9_root}zones/{f[0]}'
            _IxpLxc._lxc_exec(self, c, f"chown root:bind {m}")
            _IxpLxc._lxc_exec(self, c, f"chmod 644 {m}")

        # // Read site information //
        site_info_dict = dict()
        site_info_path_file = (f'{var_dir}{site_info_yaml_file}')

        try:
            sitefh = open(site_info_path_file, 'r')
        except:
            print(f'ERROR: Cannot open {site_info_yaml_file}')
            _ixp_debug('y', f'ERROR[_bs_software_configure_build]: Cannot open {site_info_yaml_file}')
            sys.exit(1)

        for l in sitefh:
            if (':' in l):
                site_info_dict[l.strip().split(':')[0]] = l.strip().split(':')[1].strip()

        # // Create list of TXT and LOC lines to append to hostname zone files //
        (m,n,o) = (site_info_dict['organisation'], site_info_dict['town'], site_info_dict['country'])
        txt_loc_list = [f""" "        TXT     \\"{m}\\" \\"{n}, {o}\\"  " """]
        txt_loc_list.append(""" "        TXT     \\"See http://www.as112.net/ for more information.\\" " """)
        txt_loc_list.append(f""" "        TXT     \\"Unique IP: {bs_ip['ip']}\\" \\"Unique IPv6: {bs_ipv6['ip']}\\" " """)
        txt_loc_list.append(""" \";\" """)
        (m,n) = (site_info_dict['location'], site_info_dict['elevation'])
        txt_loc_list.append(f""" "        LOC     {m} {n} 1m 10000m 10m " """)
 
        bind9_zone_files = _IxpLxc._lxc_exec(self, c, f'ls {self.zones_root}')[1].split('\n')

        for f in bind9_zone_files:
            if 'hostname' in f:
                for x in txt_loc_list:
                    _IxpLxc._lxc_exec(self, c, f"sh -c \'echo  {x}  >> {self.zones_root}{f} \' ") 

        # // Restart the DNS Server //
        _IxpLxc._bind9_restart(self, c)

        print(f'Configuration on {c} blackhole server completed successfully\n')

        return(0)

    ## End of _bs_software_configure_build method

    #============================================#
    #  //      _ixp_software_configure        // #
    #============================================#
    
    def _ixp_software_configure(self, ixp_argv):
        ''' Method to configure software on the servers '''

        # // IXP Schema elements //
        e = _ixp_schema_servers()
        e.pop(0)

        self.ixp_servers_plus = dict()

        for k in _ixp_schema_servers():
            kless = re.findall('(\w+)\d+', k)
            self.ixp_servers_plus[k] = ixp_servers[kless[0]]

        ixp_servers['gateway'] = '1'

        for c in ixp_argv:

            if (c == self.NS):
                self._nameserver_configure(c) 
     
            elif (c == self.RS):
                self._bird_configure(c)
                self._birdseye_configure(c)

            elif (c == self.CS):
                self._bird_configure(c)
                self._birdseye_configure(c)

            elif (c == self.BS):
                self._bs_software_configure_build(c) 

            else:
                print(f'ERROR: Shouldn\'t happen {c}, Illegal ARGUMENT given\n')
                _ixp_debug('y', f'ERROR[_ixp_software_configure]:  Shouldn\'t happen {c}, Illegal ARGUMENT given')

        return(0)

    ## End of _ixp_software_configure method

    #============================================#
    #  //       _ixp_software_install         // #
    #============================================#
    
    def _ixp_software_install(self, ixp_argv):
        ''' Method to install software on the servers '''

        php_software = list()

        # // Loop through the servers //
        for c in ixp_argv:

            # // Upgrade the servers //
            print(f'Upgrading server {c}, please be patient\n')
            upgrade_test = _IxpLxc._lxc_upgrade(self, c)

            if not (upgrade_test == 0):
                print(f'ERROR: Problem carrying out the repository update and distribution upgrade on {c}\n')
                _ixp_debug('y', f'ERROR[_ixp_software_install]: Problem carrying out the repository update and distribution upgrade in {c}')
                sys.exit(1) 

            # // Install software from list //
            for p in self.software[c[:2]]:  # // c[:2] is the first 2 letters of the server //
                
                _ixp_debug('y', f'INFO[_ixp_software_install]: Installing \'{p}\' on {c}')
                install_test = _IxpLxc._lxc_exec(self, c, f'apt-get -y install {p}')

                if (install_test[0] == 0):
                    dpkg_test = _IxpLxc._lxc_exec(self, c, f'dpkg -s {p} | grep \'Status:\'')

                    if (dpkg_test[0] == 0):
                        _ixp_debug('y', f'INFO[_ixp_software_install]: \'{p}\' on {c} - {dpkg_test[1]}')   

                    else:
                        print(f'ERROR: Problem Installing \'{p}\' on {c} - {dpkg_test[1]}')   
                        _ixp_debug('y', f'ERROR[_ixp_software_install]: Problem installing \'{p}\' on {c} - {dpkg_test[1]}')   
                        sys.exit(1)                  
           
                else:
                    print(f'ERROR: Problem Installing \'{p}\' on {c}\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_install]: Problem installing \'{p}\' on {c}')
                    sys.exit(1)  

                print(f'Installed {p} on {c}\n')

            # // Install Birdseye software on the servers with bird //

            if (c[:2] != 'ns'):

                # // Autoremove //
                _ixp_debug('y', 'INFO[_ixp_software_install]: apt-get autoremove')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y autoremove')

                # // Add software-properties-common (manages adding repositories) //
                _ixp_debug('y', 'INFO[_ixp_software_install]: Installing \'software-properties-common\'')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y install software-properties-common')
             
                # // Add repository //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Installing \'ppa:ondrej/php\' repository on {c}')
                _IxpLxc._lxc_exec(self, c, 'add-apt-repository -y ppa:ondrej/php')

                # // Update the repository //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Updating repository on {c}')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y update')

                # // Add PHP //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Installing PHP on {c}')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y install php7.0 php7.0-cgi php7.0-mbstring php7.0-xml unzip')

                # // Copy the birdseye archive to the /srv directory //
                if (path.isfile(f'/var/ixp/code/{self.birdseye_file}')):
                    _IxpLxc._lxc_push(self, c, f'/var/ixp/code/{self.birdseye_file}', '/srv/') 
                    print(f'Created the file /srv/{self.birdseye_file} on {c}\n') 
                    _ixp_debug('y', f'INFO[_ixp_software_install]: Created the file /srv/{self.birdseye_file} on {c}') 

                else:
                    print(f'/var/ixp/code/{self.birdseye_file} doesn\'t exist\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_install]: /var/ixp/code/{self.birdseye_file} doesn\'t exist')
                    sys.exit(1)  

                # // Unzip the birdseye archive //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Unzipping the file /srv/{self.birdseye_file} on {c}') 
                _IxpLxc._lxc_exec(self, c, f'tar -xjvf /srv/{self.birdseye_file} -C /srv/')    

                # // Move the birdseye directory //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Creating the /srv/birdseye directory on {c}') 
                birdseye_test = _IxpLxc._lxc_exec(self, c, '[ -d /srv/birdseye/ ]; echo $?')

                if (birdseye_test[1] == '0'):
                    _IxpLxc._lxc_exec(self, c, 'rm -r /srv/birdseye/') 
                try:    
                    _IxpLxc._lxc_exec(self, c, f'mv /srv/{self.birdseye_directory} /srv/birdseye/') 
                except:
                    print(f'ERROR: Failed to create the /srv/birdseye directory on {c}\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_install]: Failed to create the /srv/birdseye directory on {c}')
                    sys.exit(1)        

                # // Change owner of the /srv/birdseye/storage directory //
                _ixp_debug('y', f'INFO[_ixp_software_install]: change owner of /srv/birdseye/storage directory on {c}')
                _IxpLxc._lxc_exec(self, c, 'chown -R www-data: /srv/birdseye/storage')   

                # // Add lighttpd //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Installing lighttpd')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y install lighttpd') 


                print(f'Installed Lighthttpd software on {c}\n')

                # // Enable lighttpd modules //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Enabling lighttpd modules on {c}') 
                _IxpLxc._lxc_exec(self, c, 'lighty-enable-mod fastcgi') 
                _IxpLxc._lxc_exec(self, c, 'lighty-enable-mod fastcgi-php') 

                # // Copy the lighttpd.conf file to the /etc/lighttpd directory //
                if (path.isfile(f'{var_dir}{self.lighttpd_conf_template}')):
                    _IxpLxc._lxc_exec(self, c, f'cp {self.lighttpd_dir}lighttpd.conf {self.lighttpd_dir}lighttpd.conf.bak') 
                    _IxpLxc._lxc_push(self, c, f'{var_dir}{self.lighttpd_conf_template}', f'{self.lighttpd_dir}lighttpd.conf') 
                    _ixp_debug('y', f'INFO[_ixp_software_install]: Created the file {self.lighttpd_dir}lighttpd.conf on {c}') 

                # // Restart the lighttpd service //
                _IxpLxc._lxc_exec(self, c, 'systemctl restart lighttpd.service')
                _ixp_debug('y', f'INFO[_ixp_software_install]: Reloaded the lighttpd.service on {c}')

                print(f'Installed Birdseye on {c}\n')

        return(0)

    ## End of _ixp_software_build method

    #============================================#
    # //      _ixp_software_test_options      // #
    #============================================#     

    def _ixp_software_test_options(self, ixp_argv):
        ''' Test 'ixp_software' cli options '''

        first_options = ('install', 'configure', 'help')  
        element_list = list()

        # // Iterate over strings in list and lowercase them //
        ixp_argv = [string.lower() for string in ixp_argv]

        # // IXP Schema elements //
        e = _ixp_schema_servers()
        e.pop(0)

        # // Handle abbreviations //
        if(ixp_argv[0] == '?'):
            ixp_argv[0] = 'help'
        else:
            for x in first_options:
                if (ixp_argv[0] == x[:len(ixp_argv[0])]):      
                    ixp_argv[0] = x

        # // Check if help called first //
        if (ixp_argv[0] == 'help'):      
            IxpHelp._ixp_software_help(self, 0)

        # // Handle second level abbreviations //
        if (len(ixp_argv) >= 2):
            if(ixp_argv[1] == '?'):
                ixp_argv[1] = 'help'
            else:
                if (ixp_argv[1] == 'help'[:len(ixp_argv[1])]):      
                    ixp_argv[1] = 'help'

            # // Deal with help //
            if(ixp_argv[1] == 'help'):
                if (ixp_argv[0] == 'install'):
                    IxpHelp._ixp_software_install_help(self, 0)
                elif (ixp_argv[0] == 'configure'):
                    IxpHelp._ixp_software_configure_help(self, 0)

        # // Handle no options given //
        if (len(ixp_argv) == 1):
            print(f'No options given, assuming \'all\'\n')
            _ixp_debug('y', f'INFO[_ixp_software_test_options]: No options given, assuming \'all\'')
            element_list = e

        # // Handle 'all'
        elif (len(ixp_argv) == 2 and ixp_argv[1] == 'all'):
            element_list = e

        # // Handle 'all' mixed with other options //
        elif (len(ixp_argv) > 2 and 'all' in ixp_argv):
            print(f'ERROR: Cannot mix \'all\' with other options\n')
            _ixp_debug('y', f'ERROR[_ixp_software_test_options]: Cannot mix \'all\' with other options')
            IxpHelp._ixp_software_help(self, 1)

        # // Remove duplicates and test against server list //
        else:
            element_list = ixp_argv[1:]
            element_list = list(set(element_list))  # Remove duplicates  

            # // Test against server list //
            if not (set(element_list).issubset(e)):
                l = list()
                for x in element_list:
                    if not (x in e):
                        l.append(x)

                if (len(l) == 1):
                    print(f'ERROR: {l[0]} is an illegal option\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_test_options]: {l[0]} is an illegal option')
                    IxpHelp._ixp_software_help(self, 1)         
                elif (len(l) > 1):             
                    l_str = ", ".join(str(x) for x in l)
                    print(f'ERROR: {l_str} are illegal options.\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_test_options]: {l_str} are illegal options')
                    IxpHelp._ixp_software_help(self, 1)        
                else:  
                    print('Shouldn\'t be able to get here')
                    IxpHelp._ixp_software_help(self, 1)

        # // Test that the container servers are running //        
        for x in element_list:
            (start, status) = _IxpLxc._lxc_status_up(self, x)
            print(f'{x} status is {status}', end = ' ')

            # // Carry out Internet conenctivity test //
            status = int(_IxpLxc._lxc_internet_test(self, x))  
            if (status == 100):
                print('and has good Internet connectivity\n')     
            elif (status >= 50):
                print('and has poor Internet connectivity\n')   
            else: 
                print(f'ERROR: {x} has no Internet connectivity, exiting\n')   
                _ixp_debug('y', f'ERROR[_ixp_software_test_options]: {x} has poor Internet connectivity, exiting')   
                sys.exit(1)  

        return(ixp_argv[0], element_list)
              
    ## End _ixp_software_test_options method

    #============================================#
    # //             ixp_software             // #
    #============================================#

    def ixp_software(self, ixp_argv):
        ''' Root 'ixp_software' method to handle for 'ip server' commands '''

        (command, element_list) = self._ixp_software_test_options(ixp_argv)

        # // Check if 'install' called //
        if (command == 'install'):

            # // Install software in the servers //
            self._ixp_software_install(element_list)

        # // Check if 'configure' called //
        if (command == 'configure'):

            # // Configure software in the servers //
            self._ixp_software_configure(element_list)

        # // Exit //
        sys.exit(0)

    ## End to ixp_software method

########### // End IxpSoftware class // ###########

#################################################
# //              IxpPeer class              // #
#################################################

class IxpPeer:
    ''' IXP Peer Class '''

    def __init__(self):
        '''IxpPeer class constructor method'''
        
        self.build_dict = {'-n':'--name', 
                           '-a':'--asn',
                           '-4':'--ip', 
                           '-6':'--ipv6'
                          }

    ## End of __init__ method

    #============================================#
    # //          _ixp_get_peer_info          // #
    #============================================#

    def _ixp_get_peer_info(self, c, ipv):
        ''' Add a peer to a route server '''

        # // Define the bird file to be interrogated //
        if (ipv == 'ip'):
            bird_path_file = '/etc/bird/bird.conf'
        elif (ipv =='ipv6'):
            bird_path_file = '/etc/bird/bird6.conf'
        
        # // Get raw data from route server //
        try:
            proto = _IxpLxc._lxc_exec(self, c, f'cat {bird_path_file}')
        except:
            print(f'ERROR: Cannot access the {bird_path_file} file on {c}\n')
            _ixp_debug('y', f'ERROR[_ixp_get_peer_info]: Cannot access the {bird_path_file} file on {c}')

        # // Extract data //
        proto_list = list()
        l = proto[1].split('\n')
        for count, line in enumerate(l):
            if ('protocol bgp' in line):                
                m = re.findall(r'^protocol\sbgp\s(.*?)\sfrom\sPEERS',line ,re.M)[0]
                (n,o) = re.findall(r'neighbor\s(.*?)\sas\s(\d{1,10})', l[count + 2])[0]
                proto_list.append([count + 1, m,n,o])
        
        if (len(proto_list) == 0):
            proto_list.append([0,'','',0])

        return(proto_list)    

    ## End of _ixp_get_peer_info method

    #============================================#
    # //          _peer_help_handler          // #
    #============================================#

    # // Help handler function //
    def _peer_help_handler(self, command, err_code):
        ''' Peer help handler '''

        if (command == 'add'):
            IxpHelp._ixp_peer_add_help(self, err_code)
        elif (command == 'delete'):
            IxpHelp._ixp_peer_delete_help(self, err_code)
        elif (command == 'list'):
            IxpHelp._ixp_peer_list_help(self, err_code)
        elif (command == 'route'):
            IxpHelp._ixp_peer_route_help(self, err_code)

        ## End to _peer_help_handler function

    #============================================#
    # //            _peer_ip_test             // #
    #============================================#

    def _peer_ip_test(self, command, server, element_dict):
        ''' Function to test IP is valid for Schema '''

        schema_prefix = ''
        schema_net = ''
        ip_test_card = {'name': 0, 
                        'asn': 0,
                        'ip': 0,
                        'ipv6': 0
                       }

        ip_list = list()

        if ('name' in element_dict):
            name = element_dict['name']
        else:
            name = ''
                 
        if ('asn' in element_dict):
            asn = element_dict['asn']
        else:
            asn = ''

        if ('ip' in element_dict):
            ip = element_dict['ip']
            ip_list.append('ip')

        if ('ipv6' in element_dict):
            ipv6 = element_dict['ipv6']
            ip_list.append('ipv6')

        # // Test name //
        if (len(name) > 25):
            print(f'ERROR: \'{name}\' is too long for peer name\n')
            _ixp_debug('y',f'ERROR[_peer_ip_test]: \'{name}\' is too long for peer name')
            self._peer_help_handler(command, 1)   
        elif not (len(name) == 0):
            ip_test_card['name'] = 1  

        # // Test ASN //
        if (asn == ''):
            pass
        elif not ((asn.isdigit()) and ((0 < int(asn) <= 4199999999) == True)):
            print(f'ERROR: \'{asn}\' is not a valid ASN number\n')
            _ixp_debug('y', f'ERROR[_peer_ip_test]: \'{asn}\' is not a valid ASN number')
            self._peer_help_handler(command, 1)  
        elif not (len(asn) == 0):
            ip_test_card['asn'] = 1   

        # // Test IP //
        for x in ip_list:

            # // Get Server addresses from IP Schema //
            if (x == 'ip'):
                schema_entry = _read_schema_yaml()[1][server][0]['ipv4-peer'] 

            # // Get Server addresses from IP Schema //
            elif (x == 'ipv6'):           
                schema_entry = _read_schema_yaml()[1][server][1]['ipv6-peer'] 

            # // Extract the schema IP prefix and network //
            schema_prefix = _validate_ip_net(schema_entry)['prefix']
            schema_net = _validate_ip_net(schema_entry)['net']

            # // Create test IP with mask from schema and validate it //
            if (x == 'ip'):
                test_ip = f'{ip}/{schema_prefix}'
            elif (x == 'ipv6'):
                test_ip = f'{ipv6}/{schema_prefix}'

            test_net = _validate_ip_net(test_ip)['net']

            # // Compare networks //
            if not (schema_net == test_net):
                print(f'ERROR: IP {x} is not a valid IP address in the schema\n')
                _ixp_debug('y', f'ERROR[_peer_ip_test]: IP {x} is not a valid IP address in the schema')
                self._peer_help_handler(command, 1)
            else:
                if (x == 'ip'):
                    ip_test_card['ip'] = 1
                elif (x == 'ipv6'):
                    ip_test_card['ipv6'] = 1

        return(ip_test_card)

        ## End of _peer_ip_test function

    #============================================#
    # //            _ixp_peer_delete          // #
    #============================================#

    def _ixp_peer_delete(self, c, element_dict):
        ''' Delete a peer to a route server '''

        delete_flag = 0
        delete_list = list()
        element_list = list()
        bird_restart_flag = 0

        # // Test values given are valid //
        t = self._peer_ip_test('delete', c, element_dict)
        if (t['ip'] == 1):
            print ('Specifying an IP address is unnecessary for deleting peers\n')
        elif(t['ipv6'] == 1):
            print ('Specifying an IPv6 address is unnecessary for deleting peers\n')                
        if not (t['name'] == 1 or t['asn'] == 1):
            print('ERROR: Problem with elements given, either \'name\' is \'ASN\' is required\n')
            _ixp_debug('y', 'ERROR[ixp_peer]: Problem with elements given, either \'name\' is \'ASN\' is required')
            self._peer_help_handler('delete', 1)

        if (t['name'] == 1):
            element_list.append(element_dict['name'])
        if (t['asn'] == 1):
            element_list.append(element_dict['asn'])

        for x in ['ip','ipv6']:
            if ('6' in x):
                file = '/etc/bird/bird6.conf'
            else:
                file = '/etc/bird/bird.conf'    
            l = self._ixp_get_peer_info(c, x)

            for y in l:
                if (len(element_list) > 1):  
                    if (element_dict['name'] in y and element_dict['asn'] in y):
                        delete_list.append([c, y[0],file])                   
                        bird_restart_flag += 1
                elif (len(element_list) == 1): 
                    if (element_list[0] in y):
                        delete_list.append([c, y[0],file])
                        bird_restart_flag += 1
                else:
                    print('Error: Please select either a \'name\' or an \'ASN\'\n')
                    _ixp_debug('y', 'Error: Please select either a \'name\' or an \'ASN\'')
                    self._peer_help_handler('delete', 1)
                
        # // Handle the delete //
        if (len(delete_list) == 0):
            element_list_str = " or ".join(str(x) for x in element_list)
            print(f'There is no match for the {element_list_str} in {c}\n')
        else:
            for x in delete_list:
                if ('6' in x[2]):
                    ipv = 'IPv6'
                else:
                    ipv = 'IPv4'
                cli_response = input(f'Deleting \'{element_list[0]}\' from {ipv} table on {c}, are you sure ? (y|n) > ')
                cli_response = cli_response.lower()
                if (cli_response in ['y', 'yes']):
                    print(f'OK, deleting ...\n')
                    _ixp_debug('y', f'INFO[_ixp_peer_delete]: Deleting\'{element_list[0]}\' from {ipv} table on {c}')
                    delete_lines = f'{x[1]},{x[1] + 4}d'  
                    _IxpLxc._lxc_exec(self, x[0], f'sh -c \"sed -i -e {delete_lines} {x[2]}\"')                                
                elif (cli_response in ['n', 'no']):
                    print('Aborting delete\n')
                    sys.exit(0)
                else:
                    print('Not a valid response, aborting delete\n')
                    sys.exit(1)
                

        # // Restart bird daemons if elements deleted //
        #if (bird_restart_flag > 0):
            #print(f'Restarting BIRD Server on {c}\n')
            #_ixp_debug('y', f'INFO [ixp_peer_delete]: Restarting BIRD Server on {c}')
            #_IxpLxc._bird_restart(self, c)

    ## End of _ixp_peer_delete method

    #============================================#
    # //             _ixp_peer_add            // #
    #============================================#

    def _ixp_peer_add(self, c, element_dict):
        ''' Add a peer to a route server '''

        bird_restart_flag = 0
        test_flag = 0
        ip_list = list()
        schema_ipv4 = list()
        schema_ipv6 = list()
        schema_asn = ''

        # // Create list of IP address versions //
        if ('ip' in element_dict.keys()):
            ip_list.append('ip')
        if ('ipv6' in element_dict.keys()):
            ip_list.append('ipv6')

        # // Test values given are valid //
        t = self._peer_ip_test('add', c, element_dict)
        if not (t['name'] == 1 and t['asn'] == 1):
            print('ERROR: Problem with elements given\n')
            _ixp_debug('y', 'ERROR[_ixp_peer_add]: Problem with elements given')
            self._peer_help_handler('add', 1)
        if not (t['ip'] == 1 or t['ipv6'] == 1):
            print('ERROR: IP addresses required\n')
            _ixp_debug('y', 'ERROR[_ixp_peer_add]: IP addresses required')
            self._peer_help_handler('add', 1)

        test_card = [0,0,0,0] # Name, ASN, IP, IPv6

        # // Get ASN and IP addressing from IXP Schema and compare to given info//
        m = list(_read_schema_yaml()[1].values())[0][5]['as-number']
        if (element_dict['asn'] == str(m)):
            test_card[1] += 1

        for x in range (0, len(list(_read_schema_yaml()[1].values()))):
            n = _validate_ip_net(list(_read_schema_yaml()[1].values())[x][0]['ipv4-peer'])['ip']
            o = _validate_ip_net(list(_read_schema_yaml()[1].values())[x][1]['ipv6-peer'])['ip']
            if ('ip' in element_dict.keys()):
                if (element_dict['ip'] == n):
                    test_card[2] += 1
            if ('ipv6' in element_dict.keys()):
                if (element_dict['ipv6'] == o):
                    test_card[3] += 1

        # // Check results //
        if (test_card[1] > 0):
            test_flag += 1
            print (f"ERROR: Cannot use the IXP ASN {element_dict['asn']} for a peer\n")
            _ixp_debug('y', f"ERROR[_ixp_peer_add]: Cannot use the IXP ASN {element_dict['asn']} for a peer")
        if (test_card[2] > 0):
            test_flag += 1
            print (f"ERROR: {element_dict['ip']} is an IP address from the IXP Schema\n")
            _ixp_debug('y', f"ERROR[_ixp_peer_add]:{element_dict['ip']} is an IP address from the IXP Schema")
        if (test_card[3] > 0):
            test_flag += 1
            print (f"ERROR: {element_dict['ipv6']} is an IPv6 address from the IXP Schema\n")
            _ixp_debug('y', f"ERROR[_ixp_peer_add]:{element_dict['ipv6']} is an IPv6 address from the IXP Schema")

        if(test_flag > 0):
            sys.exit(1)

        # // Get existing peer info and compare to given info //

        for x in ip_list:
            for y in self._ixp_get_peer_info(c, x):
                if (y[1] == element_dict['name']):
                    test_card[0] += 1
                if (x == 'ip' and y[2] == element_dict[x]):
                    test_card[2] += 1
                if (x == 'ipv6' and y[2] == element_dict[x]):
                    test_card[3] += 1
                if (y[3] == element_dict['asn']):
                    test_card[1] += 1

        # // Check results //
        if (test_card[0] > 0):
            test_flag += 1
            print (f"ERROR: {element_dict['name']} already exists in the peer list\n")
            _ixp_debug('y', f"ERROR[_ixp_peer_add]: {element_dict['name']} already exists in the peer list")
        if (test_card[1] > 0):
            test_flag += 1
            print (f"ERROR: {element_dict['asn']} already exists in the peer list\n")
            _ixp_debug('y', f"ERROR[_ixp_peer_add]: {element_dict['asn']} already exists in the peer list")
        if (test_card[2] > 0):
            test_flag += 1
            print (f"ERROR: {element_dict['ip']} already exists in the peer list\n")
            _ixp_debug('y', f"ERROR[_ixp_peer_add]: {element_dict['ip']} already exists in the peer list")
        if (test_card[3] > 0):
            test_flag += 1
            print (f"ERROR: {element_dict['ipv6']} already exists in the peer list\n")
            _ixp_debug('y', f"ERROR[_ixp_peer_add]: {element_dict['ipv6']} already exists in the peer list")

        if(test_flag > 0):
            sys.exit(1)

        # // Get container server status //
        _IxpLxc._lxc_status_up(self, c)

        # // Add bird configuration to the servers //
        for x in ip_list:

            (m,n,o) = (element_dict['name'], 
                       element_dict['asn'], 
                       element_dict[x], 
                      )
            protocol = [f"protocol bgp {m} from PEERS {{"]
            protocol.append(f"        export all;")
            protocol.append(f"        neighbor {o} as {n};")
            protocol.append('}')
            protocol.append(' ')

            if (x == 'ip'):
                for y in protocol:
                    _IxpLxc._lxc_exec(self, c, f'''sh -c \"echo \'{y}\' >> {bird_root}{bird_file}\"''')
            elif (x == 'ipv6'):
                for y in protocol:
                    _IxpLxc._lxc_exec(self, c, f'''sh -c \"echo \'{y}\' >> {bird_root}{bird6_file}\"''')

            bird_restart_flag += 1

            print(f'Protocol entry {m} for ASN {n} with {x} address {o} added to {c}\n')

        if (bird_restart_flag > 0):
            print(f'Restarting BIRD Server on {c}\n')
            _ixp_debug('y', f'INFO [ixp_peer_add]: Restarting BIRD Server on {c}')
            _IxpLxc._bird_restart(self, c)

        return(0)

    ## End of _ixp_peer_add method

    #============================================#
    # //            _ixp_peer_list            // #
    #============================================#

    def _ixp_peer_list(self, c, ipv, detail):
        ''' Get protocol data from route server '''

        proto_list = list()
        line_list = list()
        name_list = list()
        addr_list = list()
        asn_list = list()
        pad = 18

        if (ipv == 'ip'):
            ip_label = 'IPv4'
        elif (ipv == 'ipv6'):
            ip_label = 'IPv6'

        if (detail == 'terse'):

            peer_data = self._ixp_get_peer_info(c, ipv)
            if (peer_data[0][0] == 0):
                print(f'There is no peer data on {c}\n')
                return(0)
            else:
                for x in peer_data:
                    name_list.append(x[1])
                    addr_list.append(x[2])
                    asn_list.append(x[3])

                name_pad = len(max(name_list, key=len)) + 2
                addr_pad = len(max(addr_list, key=len)) + 2
                asn_pad = len(max(asn_list, key=len)) + 2
                print('')

                # // Generate headings //
                title = f'{c} - [{ip_label} Table]'
                (heading_name, heading_asn, heading_addr) = ('Name', 'AS', 'IP address') 
                (bar_name, bar_asn, bar_addr) = ('----', '--', '----------') 
                heading = f'{heading_name:<{name_pad}}{heading_asn:<{asn_pad}}{heading_addr:<{addr_pad}}'
                bar = f'{bar_name:<{name_pad}}{bar_asn:<{asn_pad}}{bar_addr:<{addr_pad}}'
                title_pad = ('-' * (int((len(heading) - len(title) - 3) / 2)))

                # // Print tables //                   
                print(f'  {title_pad} {title} {title_pad}')
                print('')
                print(f'  {heading}')
                print(f'  {bar}')
                for x in range (0, len(name_list)):
                    print(f'  {name_list[x]:<{name_pad}}{asn_list[x]:<{asn_pad}}{addr_list[x]:<{addr_pad}}')        
                print()

                return(0)

        elif (detail == 'detail'):

            # // Go through servers for IPv4 //
            if (ipv == 'ip'):
                print('')
                print('-' * pad, f' {c} - [IPv4 Table]','-' * pad)
                proto_str = _IxpLxc._lxc_exec(self, c, 'birdc show protocols')[1]

            # // Go through servers for IPv6 //
            elif (ipv == 'ipv6'):
                print('')
                print('-' * pad, f' {c} - [IPv6 Table]','-' * pad)
                proto_str = _IxpLxc._lxc_exec(self, c, 'birdc6 show protocols')[1]

            proto_list = proto_str.split('\n')
            print(proto_list[1])

            for n in (0,1):
                proto_list.pop(0)

            for line in sorted(proto_list):
                if not (any(x in line for x in ('Direct', 'Device', 'Kernel', 'BIRD 1.6.3 ready.'))):
                    line_list.append(line)

            if (len(line_list) == 0):
                print ('There are no protocol entries\n')
            else:
                for x in line_list:
                    print(x)               

        return(0)

    ## End of _ixp_peer_list method

    #============================================#
    # //            _ixp_peer_route           // #
    #============================================#

    def _ixp_peer_route(self, c, ipv):
        ''' Get IP routes from route server '''

        route_list = list()

        # // Go through servers for IPv4 //
        if (ipv == 'ip'):
            print('')
            print(f'----------- {c} container - [IPv4 table] -----------')    
            route_str = _IxpLxc._lxc_exec(self, c, 'birdc show route primary')[1]
            route_list = route_str.split('\n')
            route_list.pop(0)
            if (len(route_list) == 0):     
                print('No routes in table\n')  
            else:    
                for r in route_list:                
                    print(r)
            _ixp_debug('y', f'INFO [_ixp_peer_route]: Executing \'birdc show routes primary\' on {c}')

        # // Go through servers for IPv6 //
        elif (ipv == 'ipv6'):   
            print('') 
            print(f'----------- {c} container - [IPv6 table] -----------') 
            route_str = _IxpLxc._lxc_exec(self, c, 'birdc6 show route primary')[1]
            route_list = route_str.split('\n')
            route_list.pop(0)
            if (len(route_list) == 0):     
                print('No routes in table\n')  
            else:    
                for r in route_list:                
                    print(r)
            _ixp_debug('y', f'INFO [_ixp_peer_route]: Executing \'birdc6 show routes primary\' on {c}')

        else:    
            print (f'ERROR: illegal version option {ipv}\n')
            _ixp_debug('y', f'ERROR [_ixp_peer_route]: illegal version option {ipv}')

        return(0)

    ## End of _ixp_peer_route method

    #============================================#
    # //        _ixp_peer_test_options        // #
    #============================================#

    def _ixp_peer_test_options(self, ixp_argv):
        ''' Test 'ixp_peer' cli options '''

        first_options = ('add', 'delete', 'list', 'route') 

        server_list = list()
        element_list = list()
        element_dict = dict()

        # // Lowercase first option //
        ixp_argv[0] = ixp_argv[0].lower()

        # // IXP Schema elements //
        e = _ixp_schema_servers()
        e = e[2:]  # Pop first two elements

        # // Check if help called first //
        if (ixp_argv[0] in help_list):      
            IxpHelp._ixp_peer_help(self, 0)

        # // Handle abbreviations //
        if(ixp_argv[0] == '?'):
            ixp_argv[0] = 'help'
        else:
            for x in first_options:
                if (ixp_argv[0] == x[:len(ixp_argv[0])]): 
                    ixp_argv[0] = x

        # // Check first options are legitimate //
        if not (ixp_argv[0] in first_options):
            print(f'ERROR: {ixp_argv[0]} is an illegal option.\n')
            _ixp_debug('y', f'ERROR[_ixp_software_test_options]: {ixp_argv[0]} is an illegal option.')
            IxpHelp._ixp_peer_help(self, 1)

        # // Handle no options given //
        if (len(ixp_argv) == 1):
            print(f'ERROR: No options given, exiting\n')
            _ixp_debug('y', f'ERROR[_ixp_peer_test_options]: No options given, exiting')
            self._peer_help_handler(ixp_argv[0], 1)        
        else:  
            ixp_argv[1] = ixp_argv[1].lower()

        # // Check if help called first //
        if (ixp_argv[1] in help_list):   
            self._peer_help_handler(ixp_argv[0], 0)

        # // Join elements to look for comma separated group //
        ixp_argv_str = " ".join(str(x) for x in ixp_argv[1:])
         
        # // Check for comma separated list and extract //
        if (',' in ixp_argv_str):
            l = list()
            ixp_argv_str = ixp_argv_str.replace(', ', ',')
            element_list = ixp_argv_str.split(' ')
            servers = element_list.pop(0).lower()
            server_list = list(set(servers.split(','))) # Remove duplicates

            for x in server_list:
                if not (x in e):
                    l.append(x.lower())

                # // Handle 'all' //
                if ('all' in l):
                    print('ERROR: \'all\' cannot be mixed with containers\n')
                    _ixp_debug('y', 'ERROR[_ixp_peer_test_options]: \'all\' cannot be mixed with containers')
                    self._peer_help_handler(ixp_argv[0], 1)

            # // Catch false containers //
            if (len(l) > 0):
                l_str = ", ".join(str(x) for x in l)

                # // One illegal option //
                if (len(l) == 1):
                    print(f'ERROR: {l[0]} is not a container option\n')
                    _ixp_debug('y', f'ERROR[_ixp_peer_test_options]: {l[0]} is not a container option')
                    self._peer_help_handler(ixp_argv[0], 1)

                # // Multiple illegal options //
                else:
                    print(f'ERROR: {l_str} are not container options\n')
                    _ixp_debug('y', f'ERROR[_ixp_peer_test_options]: {l_str} are not container options')
                    self._peer_help_handler(ixp_argv[0], 1)

        # // Handle 'all' //
        elif (ixp_argv[1] == 'all'):
            server_list = e
            element_list = ixp_argv[2:]

        # // Test against server list //
        elif (ixp_argv[1] in e):
            server_list.append(ixp_argv[1])   
            element_list = ixp_argv[2:]

        # // Handle error //
        else:
            print(f'ERROR: {ixp_argv[1]} is not a server, exiting\n')
            _ixp_debug('y', f'ERROR[_ixp_peer_test_options]: {ixp_argv[1]} is not a server, exiting')
            self._peer_help_handler(ixp_argv[0], 1)
     
        # // Test for equal numbers for add/delete //
        if (ixp_argv[0] in ['add','delete']):
            if not (len(element_list) % 2 == 0):
                print('ERROR: Uneven number of elements, exiting\n')
                _ixp_debug('y', 'ERROR[_ixp_peer_test_options]: ERROR: Uneven number of elements, exiting')
                self._peer_help_handler(ixp_argv[0], 1)

            # // Create an element dictionary of pairs and a key list //
            l = list()
            while element_list:
                a = element_list.pop(0).lower()  # pop Key, lowercase
                b = element_list.pop(0)          # pop value

                if (a in self.build_dict.keys()):
                    a_out = self.build_dict[a][2:]
                    element_dict[a_out] = b 
                elif (a in self.build_dict.values()):
                    a_out = a[2:]
                    element_dict[a_out] = b 
                else:
                    l.append(a)

            # // Catch false element switches //
            if (len(l) > 0):
                l_str = ", ".join(str(x) for x in l)
 
                # // One illegal option //
                if (len(l) == 1):
                    print(f'ERROR: {l[0]} is an illegal option\n')
                    _ixp_debug('y', f'ERROR[_ixp_peer_test_options]: {l[0]} is an illegal option')
                    self._peer_help_handler(ixp_argv[0], 1)        

                # // Multiple illegal options //
                else:
                    print(f'ERROR: {l_str} are illegal options.\n')
                    _ixp_debug('y', f'ERROR[_ixp_peer_test_options]: {l_str} are illegal options')
                    self._peer_help_handler(ixp_argv[0], 1)

            # // Return for add/delete options //
            return(ixp_argv[0], server_list, element_dict)
        
        ##### // If options is 'list' or 'route' // #####
        elif (ixp_argv[0] in ['route','list']):

            element_dict['option'] = list()
            peer_route_options = ['-4', '-6', '--ip', '--ipv6']
            peer_list_options = ['terse', 'detail']

            # // Check for help //
            if (len(list(set(element_list).intersection(help_list)))):
                self._peer_help_handler(ixp_argv[0], 1)

            # // Test for no options and assume 'all' and 'terse' //
            if (len(element_list) == 0):
                if ixp_argv[0] == 'list':
                    print('No options given, assuming \'both\' and \'terse\'\n') 
                    element_dict['option'] = ['ip', 'ipv6']
                    element_dict['detail'] = 'terse'
                if ixp_argv[0] == 'route':
                    print('No options given, assuming \'both\'\n') 
                    element_dict['option'] = ['ip', 'ipv6']

            # // Test options //
            elif (len(element_list) > 0):

                for x in peer_list_options:
                    for count, y in enumerate(element_list):
                        if (y == x[:len(y)]):
                             element_list[count] = x

                # // Handle argument switch //
                for x in element_list:
                    if (x in peer_route_options):
                        if (x == '-4'):
                            element_dict['option'].append(self.build_dict['-4'][2:])
                        elif (x == '-6'):
                            element_dict['option'].append(self.build_dict['-6'][2:])
                        else:
                            element_dict['option'].append(x[2:])  

                    elif (x in peer_list_options and ixp_argv[0] == 'list'):
                        if (x == 'terse'):
                            element_dict['detail'] = 'terse'
                        elif (x == 'detail'):
                            element_dict['detail'] = 'detail'
                        else:
                            element_dict['detail'] = 'terse'  
                    else:
                        print(f'ERROR: {x} is an illegal option\n')
                        _ixp_debug('y', f'ERROR[_ixp_peer_test_options]: {x} is an illegal option')
                        self._peer_help_handler(ixp_argv[0], 1) 

                # // Handle no IP options given //
                if (len(element_dict['option']) == 0):
                    print('No IP option given, assuming \'both\'\n')
                    element_dict['option'].append(self.build_dict['-4'][2:])
                    element_dict['option'].append(self.build_dict['-6'][2:])
 
                # // Handle no detail options given //
                if (ixp_argv[0] == 'list' and not 'detail' in element_dict.keys()):
                    print('No detail option given, assuming \'terse\'\n')
                    element_dict['detail'] = 'terse'               

                # // Remove duplicates //
                element_dict['option'] = list(set(element_dict['option']))

            # // Handle all other options //
            else:
                print('ERROR: Too many or illegal options given\n')
                _ixp_debug('y', f'ERROR[_ixp_peer_test_options]: Too many or illegal options given')
                self._peer_help_handler(ixp_argv[0], 1)   
        
            # // Return for route options //
            return(ixp_argv[0], server_list, element_dict)

        else:
            pass

        # // Exit //
        sys.exit(0)    
  
    ## End _ixp_peer_test_options

    #============================================#
    # //               ixp_peer               // #
    #============================================#

    def ixp_peer(self, ixp_argv):
        ''' Root ixp_peer method, handles 'ip peer' command '''

        (command, server_list, element_dict) = self._ixp_peer_test_options(ixp_argv)
      
        # // Handle 'add' //
        if (command == 'add'):       

            # // Iterate over servers and IP versions //
            for x in server_list:
                self._ixp_peer_add(x, element_dict)

        # // Handle 'delete' //
        if (command == 'delete'):

            # // Iterate over servers and IP versions //
            for x in server_list:
                self._ixp_peer_delete(x, element_dict)

        # // Handle 'list' //
        if (command == 'list'):

            # // Iterate over servers and IP versions //
            for x in server_list:
                for y in element_dict['option']:
                    self._ixp_peer_list(x, y, element_dict['detail'])

        # // Handle 'route' //
        if (command == 'route'):

            # // Iterate over servers and IP versions //
            for x in server_list:
                for y in element_dict['option']:
                    self._ixp_peer_route(x,y)

        # // Exit //
        sys.exit(0)

    ## End to ixp_peer method

############ // End IxpPeer class // ############

