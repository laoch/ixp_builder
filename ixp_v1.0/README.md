# IXPBuilder 

IXPBuilder phase 1.0 is a software application developed in Python version 3 (python3) to simplify the process of building an Internet eXchange Point (IXP). It provides a mechanism to build and manage the IXP Core Tier which is a containerised core incorporating the functionality of a Route Collector, Route Server, DNS Server and AS112 blackhole server. It consists of a function library ‘ixp.py’ that is accessed via a Command Line Interface (CLI) application ‘ixp’ and a set of template files that are used as part of the host and container server build and configure processes.  The Core Tier provides services to members in the Peer Tier via the IXP Switch in the Switching Tier.

Building the IXP consists of the following steps:

1. Create an IXP Schema
   * This step creates a domain and numbering framework upon which the IXP is built. This includes Domain, AS Number (ASN), Peering Local Area Network (LAN) and Management LAN Internet Protocol (IP) addressing.

2. Setup the IXP host
   * The Ubuntu 18.04 LTS is installed on the host server to provide the OS Layer. IXPBuilder lists the network interfaces available at the Hardware Layer and presents options to the IXP build engineer. 

3. Configure LXD on the host and install the containers 
   * The Service Layer is build using LXC containers, each to provide services, the DNS Server (ns<Site #>), the Route Collector (rc<Site #>), the Route Server (rs<Site #>) and the AS112 Blackhole (bs<Site #>) and connects them to the peering and management Virtual LANs (VLAN).

4. Install and configure the IXP Server software
   * There are three sub-steps in this process, firstly the container repositories are updated and the OS distribution is upgraded. Then the function specific software is installed in each container. These software services are configured in line with the IXP Schema to provide the services at the Service Layer.

5. Configure IXP peers in the Route Collector and Route Server 
   * IXP peers can be added to the Route Collector, Route Server and AS112 Blackhole Server via the CLI in the Application Layer. This allows peering members to establish Border Gateway Protocol (BGP) peer relationships with the IXP.
          
6. Configure the IXP Switch
   * The Switching Tier in phase 1 consists of an IXP Switch which is configured with two VLANs, one for the peering LAN and a second for the management LAN. Ports on the switch are then assigned to VLANs depending on their function.

At the heart of IXPBuilder the script installs the IXP Library ‘ixp.py’. This is a python3 module consisting of 11 functions as well as 7 classes which include 79 methods. These library classes facilitate:

* **IxpHelp** - IXP Help class. Provides methods to repond to help commands.
* **_IxpLxc** - IXP LXC class. Private class to provide methods for LXC container management.
* **IxpSchema** - IXP Schema Class. This class provides methods to develop the IXP site schema.
* **IxpHost** - IXP Host class. This class contains methods to prepare the core server for the role as the supplier of containerised functions.
* **IxpServer** - IXP Server Class. A class that creates the LXC containers defined in the IXP Schema.
* **IxpSoftware** – The IXP Software Class contains methods that configure the LXC container functions to provide the IXP services. 
* **IxpPeer** -  The IXP Peer Class facilitates the management of IXP peers and the monitoring of peers and routes.

## The IXP Builder version
The IXP Builder version can be viewed from the application as follows:
```
cIXP:~$ ixp version
IXPBuilder: Phase 1.0, 2.2
            Copyright 2019, C²S Consulting
            European Union Public Licence v1.2

```

## Create a Schema 
The schema is built with the ‘ixp schema build’ command. IP network addressing, domain and schema information is presented using appropriate switches.
```
-sn, --site-number    -  Site Number (i.e. 1-100)
-p4, --peer-ipv4      -  IPv4 Peering LAN (i.e. 199.9.9.0/24)
-p6, --peer-ipv6      -  IPv6 Peering LAN (i.e. 2a99:9:9::/48)
-m4, --man-ipv4       -  IPv4 Management LAN (i.e. 198.8.8.0/24)
-m6, --man-ipv6       -  IPv6 Management LAN (i.e. 2a98:8:8::/48)
-d,  --domain         -  IXP Domain name (i.e. netlabs.tst)
-as, --as-number  	  -  IXP Autonomous System number (i.e. 5999).
-c,  --country        -  Country IXP is located in (i.e. Uganda).
-t,  --town           -  Town/city IXP is located in (i.e. Kampala).
-e,  --elevation      -  IXP elevation (m) above sea level (i.e. 1072).
-la, --latitude       -  Deg, min, sec, N|S (i.e. '00 20 51.3456 N').
-lo, --longitude      -  Deg, min, sec, E|W (i.e. '32 34 57.0720 E').
```
The default IXP schema can be implemented using the following command.
```
ubuntu@ub-18-04:~$ ixp schema default all
IXP IP Schema defaulted
IXP site information defaulted.
```
## IXP Host
List the available interfaces on the server.
```
ubuntu@ub-18-04:~$ ixp host list interfaces

Available interfaces on host server:

enp0s3	enp0s4	enp0s5

```
## Build Host
```
ubuntu@ub-18-04:~$ ixp host interface enp0s3

Does the system require a separate interface running DHCP?
Select from the following?

enp0s4	enp0s5	

[ no | <<DHCP Interface>> ]: no	

The '/var/ixp/ixp_host.yaml' file has been built

Network does not have an IPv6 public connection

Execute the following once off script to apply:

$ sudo /tmp/tmpn6ruw918

ubuntu@ub-18-04:~$ sudo /tmp/tmpn6ruw918
[sudo] password for ubuntu: *****
```
## IXP Servers
The server build step confirms there is a template Ubuntu 18.04 LXC. If there not, then it retrieves one from the Internet and installs it.
```
ubuntu@ub-18-04:~$ ixp server build ?
Usage: ixp server build { help | ? } [ARGUMENT] [OBJECT(s)]

        ?, help  -  This help message.
       
        ARGUMENT
        -y       -  Skip the yes/no questions.

        OBJECT(s) [list]:
        all      -  Build all IXP Servers.
        ns1      -  Build Name Server.
        cs1      -  Build Route Collector.
        rs1      -  Build Route Server.
        bs1      -  Build AS112 Blackhole Server.
        
```
Build out all servers.
```
ubuntu@ub-18-04:~$ ixp server build -y all

Container state Summary
-----------------------
ns1:     Running and has good Internet connectivity
cs1:     Running and has good Internet connectivity
rs1:     Running and has good Internet connectivity
bs1:     Running and has good Internet connectivity

```
## IXP Software
### Install software
The IXP Software build process constructs the netplan for each container,  ‘ns1’, ‘rs1’, 'cs1' and ‘bs1’. 
```
ubuntu@ub-18-04:~$ ixp software install all

```

### Configure software
The installed software for each server container is configured in accordance with the IXP schema.
```
ubuntu@ub-18-04:~$ ixp software configure all

```
## IXP Peers
IXP peers can be added to the route servers as follows:
```
ubuntu@ub-18-04:~$ ixp peer add ?
Usage: ixp peer add { help | ? } [OBJECT(s)] [VALUE]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all   -  Configure software on all IXP Servers.
        cs1   -  Configure software on the Route Server.
        rs1   -  Configure software on the Route Collector.
        bs1   -  Configure software on the AS 112 Server.
        Note: Comma separated list permitted, i.e. : cs1, rs1

        ARGUMENT(s) [list]:
        -n, --name  - Remote Autonomous System Name. 
        -a' --asn   - Remote Autonomous System Number.
        -4, --ip    - Remote ISP peer IPv4 address.
        -6, --ipv6  - Remote ISP peer IPv6 address.

ubuntu@ub-18-04:~$ ixp peer add -n ISP_3 -a 5333 -4 199.9.9.33 -6 2a99:9:9::33

```
### Checking peer status
```
ubuntu@ub-18-04:~$ ixp peer rs1 list -4 -6 terse

  - rs1 - [IPv4 Table] -

  Name  AS    IP address
  ----  --    ----------
  isp1  5111  199.9.9.11
  isp3  5333  199.9.9.33
  isp2  5222  199.9.9.22


  -- rs1 - [IPv6 Table] --

  Name  AS    IP address
  ----  --    ----------
  isp1  5111  2a99:9:9::11
  isp3  5333  2a99:9:9::33
  isp2  5222  2a99:9:9::22

ubuntu@ub-18-04:~$ ixp peer list cs1, bs1 -4 -6 detail

----------------  cs1 - [IPv4 Table] -----------------
name   proto    table    state  since      info
isp1   BGP      master   up     15:25:49   Established
isp2   BGP      master   start  15:48:30   Idle        BGP Error: Hold timer expired
isp3   BGP      master   up     15:25:48   Established

-----------------  cs1 - [IPv6 Table] -----------------
name   proto    table    state  since      info
isp1   BGP      master   up     15:25:49   Established
isp2   BGP      master   start  15:48:41   Idle        BGP Error: Hold timer expired
isp3   BGP      master   up     15:25:49   Established

----------------  rs1 - [IPv4 Table] -----------------
name   proto    table    state  since      info
isp1   BGP      master   up     15:48:30   Established
isp2   BGP      master   start  15:48:25   Connect     Socket: No route to host
isp3   BGP      master   up     15:48:29   Established

----------------  rs1 - [IPv6 Table] -----------------
name   proto    table    state  since      info
isp1   BGP      master   up     15:48:32   Established
isp2   BGP      master   start  15:48:28   Connect
isp3   BGP      master   up     15:48:33   Established

----------------  bs1 - [IPv4 Table] -----------------
name   proto    table    state  since      info
isp1   BGP      master   up     15:35:10   Established
isp2   BGP      master   start  15:46:36   Active      Socket: No route to host
isp3   BGP      master   up     15:35:10   Established

----------------  bs1 - [IPv6 Table] -----------------
name   proto    table    state  since      info
isp1   BGP      master   up     15:35:11   Established   
isp2   BGP      master   start  15:46:42   Connect     BGP Error: Hold timer expired
isp3   BGP      master   up     15:35:12   Established  
   
```
### Checking routes

The routes table on each core IXP container can also be retrieved.

```
ubuntu@ub-18-04:~$ ixp peer route rs1 -4 -6 

----------- rs1 container - [IPv6 table] -----------
2a99:3:3::/48      via 2a99:9:9::33 on ens3 [isp3 10:14:26] * (100) [AS5333i]
2a99:2:2::/48      via 2a99:9:9::22 on ens3 [isp2 14:07:08] * (100) [AS5222i]
2a99:9:9::/48      via 2a99:9:9::11 on ens3 [isp1 10:14:34] ! (100) [AS5111i]

----------- rs1 container - [IPv4 table] -----------
192.175.48.0/24    via 199.9.9.204 on ens3 [isp1 14:07:05 from 199.9.9.11] * (100) [AS112i]
192.31.196.0/24    via 199.9.9.204 on ens3 [isp1 14:07:05 from 199.9.9.11] * (100) [AS112i]
199.1.1.0/24       via 199.9.9.11 on ens3 [isp1 14:07:05] * (100) [AS5111i]
199.3.3.0/24       via 199.9.9.33 on ens3 [isp3 14:06:42] * (100) [AS5333i]
199.2.2.0/24       via 199.9.9.22 on ens3 [isp2 14:07:18] * (100) [AS5222i]

```

# Switch configuration

## Cisco Catalyst
Default Username: [no username], Password: [no password]
```
 # // Configure the switch hostname //

Switch> enable
Switch# configure terminal 
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)# hostname IXP_SW

 # // Configure the peering VLANs 100 and 900. Add port 24 as a trunk port and port 1 as an untagged management port //

IXP_SW(config)# vlan 100
IXP_SW(config-vlan)# name VLAN100-Peering

IXP_SW(config-vlan)# vlan 900
IXP_SW(config-vlan)# name VLAN900-Management
IXP_SW(config-vlan)# exit 

 # // Add port 1 as a VLAN trunk, this interface faces the Core Server //

IXP_SW(config)# interface GigabitEthernet 1/0/1
IXP_SW(config-if)# description "VLAN Trunk interface to lxd1"
IXP_SW(config-if)# switchport trunk encapsulation dot1q
IXP_SW(config-if)# switchport mode trunk
IXP_SW(config-if)# switchport trunk allowed vlan add 100,900
IXP_SW(config-if)# no shutdown
IXP_SW(config-if)# exit

 # // Configure the management VLAN 900 and add an IP address to the VLAN for the purpose of management access. Place port 1 in VLAN 900 as an untagged interface //

IXP_SW(config)# interface vlan 900
IXP_SW(config-if)# ip address 198.8.8.210 255.255.255.0
IXP_SW(config-if)# no shutdown
IXP_SW(config-if)# exit
IXP_SW(config)# ip default-gateway 198.8.8.1

IXP_SW(config)# interface GigabitEthernet 1/0/2
IXP_SW(config-if)# description "Management interface in VLAN 900"
IXP_SW(config-if)# switchport mode access
IXP_SW(config-if)# switchport access vlan 900
IXP_SW(config-if)# no shutdown
IXP_SW(config-if)# exit

 # // Add the ports from 3 to 24 untagged to VLAN100. Only ports 3 and 24 are demonstrated here //

IXP_SW(config)# interface GigabitEthernet 1/0/3
IXP_SW(config-if)# description "<<ISP 1>> VLAN 100" 
IXP_SW(config-if)# switchport mode access
IXP_SW(config-if)# switchport access vlan 100
IXP_SW(config-if)# spanning-tree portfast
IXP_SW(config-if)# no shutdown
IXP_SW(config-if)# exit

... 

IXP_SW(config)# interface GigabitEthernet 1/0/48 
IXP_SW(config-if)# description "<<ISP 46>> VLAN 100"
IXP_SW(config-if)# switchport mode access
IXP_SW(config-if)# switchport access vlan 100
IXP_SW(config-if)# spanning-tree portfast
IXP_SW(config-if)# no shutdown
IXP_SW(config-if)# exit

 # // Save the configuration //

IXP_SW(config-if)# exit
IXP_SW(config)# exit
IXP_SW# copy running-config startup-config

```
## Netgear ProSAFE Switch

Default User: **admin**,  Password: [no password] 
```
 # // Configure the console to 9,600 baud (to match the other devices) //

(M4300-28G)> enable
(M4300-28G)> configure
(M4300-28G)(Config)# line console 
(M4300-28G)(Config-line)# serial baudrate 9600

 # // Configure the switch hostname.

(M4300-28G)# hostname IXP_SW 

 # // Configure the peering VLANs 100 and 900. Add port 1 as a trunk port and port 2 as an untagged management port //

(IXP_SW)# vlan database
(IXP_SW)(Vlan)# vlan 100
(IXP_SW)(Vlan)# vlan name 100 VLAN100-Peering
(IXP_SW)(Vlan)# vlan 900
(IXP_SW)(Vlan)# vlan name 900 VLAN900-Management

 # // Add port 24 as a VLAN trunk, this interface faces the Core Server //

(IXP_SW)# configure
(IXP_SW)(Config)# interface 1/0/1 
(IXP_SW)(Interface 1/0/1)# description "VLAN Trunk interface to lxd1"
(IXP_SW)(Interface 1/0/1)# switchport mode trunk
(IXP_SW)(Interface 1/0/1)# no shutdown
(IXP_SW)(Interface 1/0/1)# exit

 # // Configure the management VLAN 900 and add an IP address to the VLAN for the purpose of management access. Place port 2 in VLAN 900 as an untagged interface //

(IXP_SW)(Config)# ip management vlan 900 198.8.8.210 255.255.255.0
(IXP_SW)(Config)# ip default-gateway 198.8.8.1

(IXP_SW)(Config)# interface 1/0/2
(IXP_SW)(Interface 1/0/2)# description "Management interface in VLAN 900"
(IXP_SW)(Interface 1/0/2)# switchport mode access
(IXP_SW)(Interface 1/0/2)# switchport access vlan 900
(IXP_SW)(Interface 1/0/2)# no shutdown
(IXP_SW)(Interface 1/0/2)# exit

 # // Add the ports from 3 to 24 untagged to vlan 100. Only ports 2 and 23 are demonstrated here //

(IXP_SW)(Config)# interface 1/0/3
(IXP_SW)(Interface 1/0/3)# description "<<ISP 1>> VLAN 100"
(IXP_SW)(Interface 1/0/3)# switchport mode access
(IXP_SW)(Interface 1/0/3)# switchport access vlan 100
(IXP_SW)(Interface 1/0/3)# spanning-tree edgeport
(IXP_SW)(Interface 1/0/3)# no shutdown
(IXP_SW)(Interface 1/0/3)# exit

... 

(IXP_SW)(Config)# interface 1/0/24
(IXP_SW)(Interface 1/0/24)# description "<<ISP 22>> VLAN 100"
(IXP_SW)(Interface 1/0/24)# switchport mode access
(IXP_SW)(Interface 1/0/24)# switchport access vlan 100
(IXP_SW)(Interface 1/0/24)# spanning-tree edgeport
(IXP_SW)(Interface 1/0/24)# no shutdown
(IXP_SW)(Interface 1/0/24)# exit


 # // Save the configuration //

(IXP_SW)(Config)# exit
(IXP_SW)# save
This operation may take a few minutes.
Management interfaces will not be available during this time. 
Are you sure you want to save? (y/n) y 
Config file 'startup-config' created successfully.
Configuration Saved!

```
## Juniper EX series Switch

Default User: **root**,  Password: [no password] 

```
 # // Configure the switch hostname //

root@:RE:0% cli
root> configure
root# set system host-name IXP_SW

 # // Configure the peering VLANs 100 and 900. Add port 1 as a trunk port and port 2 as an untagged management port //

root@IXP_SW# set vlans vlan-100 vlan-id 100 description "Peering VLAN" 
root@IXP_SW# set vlans vlan-900 vlan-id 900 description "Management VLAN" 

 # // Add port 24 as a VLAN trunk, this interface faces the Core Server //

root@IXP_SW# set interfaces ge-0/0/0 unit 0 family ethernet-switching interface-mode trunk

 # // Configure the management VLAN 900 and add an IP address to the VLAN for the purpose of management access. Place port 2 (ge-0/0/1) in VLAN 900 as an untagged interface //

root@IXP_SW# set interfaces irb.900 family inet address 198.8.8.210/24
root@IXP_SW# set vlans vlan-900 l3-interface irb.900
root@IXP_SW# set routing-options static route 0.0.0.0/0 next-hop 198.8.8.1

 # // Add the second interface (ge-0/0/1) to VLAN 900 //

root@IXP_SW# delete interfaces ge-0/0/1 unit 0 family ethernet-switching vlan members default
root@IXP_SW# set interfaces ge-0/0/1 unit 0 family ethernet-switching vlan members vlan-900

 # // Add the ports from 2 to 23 untagged to VLAN 100. Only ports 3 (ge-0/0/2) and 24 (ge-0/0/23) are demonstrated here //

root@IXP_SW# delete interfaces ge-0/0/2 unit 0 family ethernet-switching vlan members default
root@IXP_SW# set interfaces ge-0/0/2 unit 0 family ethernet-switching vlan members vlan-100 

...

root@IXP_SW# delete interfaces ge-0/0/23 unit 0 family ethernet-switching vlan members default
root@IXP_SW# set interfaces ge-0/0/23 unit 0 family ethernet-switching vlan members vlan-100 


 # // Commit the configuration //

root@IXP_SW# commit 

```
## MikroTik Switch
Default Username: **admin**,  Password: [no password]
```
 # // Configure the switch hostname //

/system identity set name=IXP_SW

 # // Configure the peering VLANs 100 and 900. Add port 1 as a trunk port to each //

/interface vlan
add name=ether24-vlan900 vlan-id=900 interface=ether1
add name=ether24-vlan100 vlan-id=100 interface=ether1

 # // Create two bridges one for each VLAN //

/interface bridge
add name=bridge-vlan900
add name=bridge-vlan100

 # // Add the VLAN 900 to the bridge ‘bridge-vlan900’ and ether 2 which will be an untagged management port //

/interface bridge port
add bridge=bridge-vlan900 interface=ether24-vlan900
add bridge=bridge-vlan900 interface=ether2

 # // Add an IP address to the VLAN for the purpose of management access.

/ip address add address=198.8.8.210/24 interface=bridge-vlan900
/ip route add gateway=198.8.8.1

 # // Add the VLAN 100 to the bridge ‘bridge-vlan100’ and the remaining ports 3 - 23 which will be the untagged peering ports connecting peer ISPs //

/interface bridge port
add bridge=bridge-vlan100 interface=ether24-vlan100

add bridge=bridge-vlan100 interface=ether3 comment="<<ISP 1>> VLAN 100"

... 

add bridge=bridge-vlan100 interface=ether24 comment="<<ISP 22>> VLAN 100"
```
# IXP Peering members router configurations

## Cisco Router
Default Username: [no username], Password: [no password] 
```
 # // Add BGP instance and networks to be advertised //

ISP1_RTR(config)# router bgp 5111
ISP1_RTR(config-router)# bgp router-id 200.1.1.1
ISP1_RTR(config-router)# neighbor 199.9.9.1 remote-as 5999
ISP1_RTR(config-router)# neighbor 199.9.9.203 remote-as 5999
ISP1_RTR(config-router)# neighbor 2A99:9:9::204 remote-as 112 
ISP1_RTR(config-router)# neighbor 2a99:9:9::1 remote-as 5999
ISP1_RTR(config-router)# neighbor 2a99:9:9::203 remote-as 5999
ISP1_RTR(config-router)# neighbor 2a99:9:9::204 remote-as 112

 # // Configure IPv6 address families //

ISP1_RTR(config-router)# address-family ipv4
ISP1_RTR(config-router-af)# network 199.1.1.0 mask 255.255.255.0
ISP1_RTR(config-router-af)# neighbor 199.9.9.1 activate 
ISP1_RTR(config-router-af)# neighbor 199.9.9.203 activate 
ISP1_RTR(config-router-af)# neighbor 199.9.9.204 activate 
ISP1_RTR(config-router-af)# exit-address-family 

ISP1_RTR(config-router)# address-family ipv6
ISP1_RTR(config-router-af)# network 2a99:1:1::/48
ISP1_RTR(config-router-af)# neighbor 2a99:9:9::1 activate 
ISP1_RTR(config-router-af)# neighbor 2a99:9:9::203 activate 
ISP1_RTR(config-router-af)# neighbor 2a99:9:9::204 activate 
ISP1_RTR(config-router-af)# exit-address-family 

```
## Juniper MX Series Router
Default Login: **root**,  Password: [no password]
```
 # // Set the peer type to external BGP (eBGP) //

user@ISP_2# set protocols bgp group external-peers type external

 # // Set the local AS number //

user@ISP_2# set routing-options autonomous-system 5222

 # // Specify the AS number of the external AS //

user@ISP_2# set protocols bgp group external-peers peer-as 5999

 # // Create the BGP group, and add the external neighbour addresses for 'cs1' and 'rs1' //

user@ISP_2# set protocols bgp group external-peers neighbor 199.9.9.1
user@ISP_2# set protocols bgp group external-peers neighbor 199.9.9.203
user@ISP_2# set protocols bgp group external-peers neighbor 199.9.9.204
user@ISP_2# set protocols bgp group external-peers neighbor 2a99:9:9::1
user@ISP_2# set protocols bgp group external-peers neighbor 2a99:9:9::203
user@ISP_2# set protocols bgp group external-peers neighbor 2a99:9:9::204

```
## MikroTik Router
Default Username: **admin**, Password: [no password]
```
 # // Add BGP instance and networks to be advertised //

/routing bgp 
instance add name=ASN5111 as=5333 router-id=200.3.3.3
network add network=199.3.3.0/24 
network add network=2a99:3:3::/48

 # // Add BGP Peers for transit //

/routing bgp peer 
add name=ISP_3 instance=ASN5333 remote-as=5999 remote-address=199.9.9.1 
add name=ISP_3 instance=ASN5333 remote-as=5999 remote-address=199.9.9.203
add name=ISP_3 instance=ASN5333 remote-as=5999 remote-address=199.9.9.204 

add name=ISP_3 instance=ASN5333 remote-as=5999 address-families=ipv6 remote-address=2a99:9:9::1 
add name=ISP_3 instance=ASN5333 remote-as=5999 address-families=ipv6 remote-address=2a99:9:9::203
add name=ISP_3 instance=ASN5333 remote-as=5999 address-families=ipv6 remote-address=2a99:9:9::204
```
# Example configuration of basic IXP
The commands here executed on the core server builds a basic operational IXP testbed. These commands are responsible for over 2500 operations on the core server. 

## Install IXPBuilder
```
cIXP:~$ tar -xzvf ~/IXPBuilder-v3.0.tgz
cIXP:~$ ls ~
IXPBuilder-v3.0.tgz  ixp

cIXP:~$ cd ~/ixp/tools
cIXP:~/ixp/tools$ sudo ./ixp-install.sh

```
## Build IXP Testbed
```
cIXP:~$ ixp schema default
cIXP:~$ ixp host build 
  Configuring the IXP host for model: c

cIXP:~$ ixp server build
cIXP:~$ ixp software install
cIXP:~$ ixp software configure
cIXP:~$ ixp peer add all -n AS5111 -a 5111 -4 199.9.9.11 -6 2a99:9:9::11
cIXP:~$ ixp peer add all -n AS5222 -a 5222 -4 199.9.9.22 -6 2a99:9:9::22
cIXP:~$ ixp peer add all -n AS5333 -a 5333 -4 199.9.9.33 -6 2a99:9:9::33

```



