#!/bin/bash

### ixp-uninstall.sh: Installer script for IXP Builder. ###

# author     : "Diarmuid O'Briain"
# copyright  : "Copyright 2019, C²S Consulting"
# license    : "European Union Public Licence v1.2"
# version    : "Phase 2.0, version 4.0"

# // Declarations //
DIRECTORIES=('/opt/ixp/' '/var/ixp/' '/var/ixp/code/' '/var/log/ixp/')
APPLICATIONS=('python3' 'python3-pip' 'bird' 'dnsutils' 'fping' 'lxd' 
	          'mtr' 'snmp' 'vlan' 'sqlite3' 'python3-lxc' 
	          'openvswitch-switch' 'openvswitch-common')
PACKAGES=('pyroute2' 'python-apt' 'geocoder' 'validators')
DNS_RESOLVER='/etc/systemd/resolved.conf'
DNS_ADDR=('8.8.8.8' '8.8.8.4')

echo -e "\nUninstall script for IXP builder Phase 2.0, version 4.0 package"
printf "%0.s-" {1..63}; echo

# // Check if the script is being ran by root user //
USER=$(whoami)
 
if [ "$USER" != 'root' ]; then
    echo -e "\nThis script must be ran using 'sudo' not $USER\n\n$ sudo $0\n"
    exit 1
fi

# // Update repositories //
apt-get -y update

# // Establish DNS Resolver //
echo >>"$DNS_RESOLVER"
echo "DNS=${array[@]}" >> "$DNS_RESOLVER"

# // Restart systemd DNS resolver //
systemctl restart systemd-resolved

# // Delete IXP directories //
echo "Removing IXP Builder directories."
for dirs in "${DIRECTORIES[@]}"; do
   if [ -d "$dirs" ]; then
       rm -r "$dirs"
   fi
done

# // Remove the IXP Group //
groupdel ixp

# // Remove applications //
echo "Purging GNU/Linux applications."
for apps in "${APPLICATIONS[@]}"; do
    echo "  - Purging $apps."
    apt-get -y purge "$apps"
done

# // Remove python packages //
echo "Removing Python3 packages"
for packs in "${PACKAGES[@]}"; do
    echo "  - Removing $packs."
    sudo -H python3 -m pip uninstall "$packs"
done

# // Remove the ixp-logviewer //
rm '/usr/local/bin/ixp-logviewer'

# // Remove the ixp file softlink //
rm '/usr/local/bin/ixp'

# //  Python3 ixp.py package // #
py3_site=$(python3 -m site | grep '/usr/local/lib' | awk -v FS="('|')" '{print $2}')
echo "Removing ixp.py package from $py3_site."
rm "$py3_site/ixp.py"

# // Autoremove packages that are no longer required //
sudo apt-get -y autoremove

# // Final message //
echo "The IXP Builder package has been removed from $(hostname)."

# End
exit 0
