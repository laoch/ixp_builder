#!/bin/bash

### ixp-install.sh: Installer script for IXP Builder. ###

# author     : "Diarmuid O'Briain"
# copyright  : "Copyright 2019, C²S Consulting"
# license    : "European Union Public Licence v1.2"
# version    : "Phase 3.0, version 5.0"

# // Declarations //
DIRECTORIES=('/opt/ixp/' '/var/ixp/' '/var/ixp/code/' '/var/log/ixp/')
APPLICATIONS=('python3' 'dnsutils' 'fping' 'lxd' 'mtr'  
              'snmp' 'vlan' 'sqlite3' 'python3-lxc' 'openvswitch-switch' 
              'openvswitch-common' 'python3-pip')
PACKAGES=('pyroute2' 'python-apt' 'geocoder' 'validators' 'paramiko')
INTERFACES_FILE='/etc/network/interfaces'
DNS_RESOLVER='/etc/systemd/resolved.conf'
DNS_ADDR=('8.8.8.8' '8.8.8.4')

echo -e "\nSetup script for IXP builder Phase 3.0, version 5.0 package"
printf "%0.s-" {1..59}; echo

# // Check if the script is being ran by root user //
USER=$(whoami)
 
if [ "$USER" != 'root' ]; then
    echo -e "\nThis script must be ran using 'sudo' not $USER\n\n$ sudo $0\n"
    exit 1
fi

# // Update repositories //
apt-get -y update

# // Establish DNS Resolver //
echo >>"$DNS_RESOLVER"
echo "DNS=${array[@]}" >> "$DNS_RESOLVER"

# // Restart systemd DNS resolver //
systemctl restart systemd-resolved

###################### // TEMP WHILE DEVELOPING // ####################

# // Delete directories //

echo "Removing directories temporarily."
for dirs in "${DIRECTORIES[@]}"; do
   if [ -d "$dirs" ]; then
       rm -r "$dirs"
   fi
done

groupdel ixp

###################### // TEMP // #########################

### // Users, Groups, directories and ownership // ###

# // Create IXP group //
if [ $(getent group 'ixp') ]; then
    echo "IXP group 'ixp' already exists."
else
    echo "Creating group 'ixp'."
    groupadd ixp
fi

if [ $(cat '/etc/group' | grep ixp | grep $SUDO_USER) ]; then
    echo "$SUDO_USER is already part of the 'ixp' group."
else
    echo "Modifying user '$SUDO_USER' to add to the group 'ixp'."
    usermod -a -G ixp "$SUDO_USER"
fi

# // Make directories and change ownerships //
for dirs in "${DIRECTORIES[@]}"; do
   if [ -d "$dirs" ]; then
       echo "Directory $dirs already exists."
       ownership=$(stat -c "%U:%G" "$dirs")
       if [ "$ownership" != 'root:ixp' ];then
           echo "The ownership of the directory $dirs is $ownership, stopping install."
       fi
   else
       echo "Creating the directory $dirs."
       mkdir -p "$dirs"
       chown root:ixp "$dirs" 
       if [[ "$dirs" =~ ^/var/* ]]; then
           chmod 770 "$dirs" 
       else      
           chmod 550 "$dirs"    
       fi
   fi
done

#: <<'TEMPBLOCK'

# // Install applications //
echo "Installing GNU/Linux applications."
for apps in "${APPLICATIONS[@]}"; do
    echo "  - Installing $apps."
    apt-get -y install "$apps"
done

# // Update permissions on the user cache //
chown -R $USER /home/$USER/.cache/pip

# // Install pip and setuptools //
apt-get install -y python-dev
apt-get install -y python3-distutils
pip3 install --upgrade pip
pip3 install --upgrade setuptools wheel  

# // Install python packages //
echo "Installing Python3 packages"
for packs in "${PACKAGES[@]}"; do
    echo "  - Installing $packs."
    sudo -H python3 -m pip install "$packs"
done

#TEMPBLOCK

### //  Copy files to directories // ###

# // /opt/ixp/ files //
cp '../files/ixp' '/opt/ixp/'
chown root:ixp '/opt/ixp/ixp'
# //temp while developing
#chmod 750 '/opt/ixp/ixp'
chmod 770 '/opt/ixp/ixp'

# // /var/ixp/templates files //
cp ../files/templates/* '/var/ixp/'
chown -R root:ixp '/var/ixp/'
# //temp while developing
#chmod -R 750 '/var/ixp/'
chmod -R 770 '/var/ixp/'

# // /var/ixp/code files //
cp ../files/code/* '/var/ixp/code/'
chown -R root:ixp '/var/ixp/'
# //temp while developing
#chmod -R 750 '/var/ixp/code/'
chmod -R 770 '/var/ixp/code/'

# // Create IXP Logviewer //
echo "#!/bin/bash" > '/opt/ixp/ixp-logviewer'
echo "" >>  '/opt/ixp/ixp-logviewer'
echo "LOGLINK='/var/log/ixp/today'" >>  '/opt/ixp/ixp-logviewer'
echo "echo ''">>  '/opt/ixp/ixp-logviewer'
echo "echo 'IXP Logger enabled'" >>  '/opt/ixp/ixp-logviewer'
echo "echo '------------------'" >>  '/opt/ixp/ixp-logviewer'
echo "echo ''" >>  '/opt/ixp/ixp-logviewer'
echo 'tail -f $LOGLINK' >>  '/opt/ixp/ixp-logviewer'
echo "echo ''" >>  '/opt/ixp/ixp-logviewer'

# // link for IXP logviewer file //
if [ -L '/usr/local/bin/ixp-logviewer' ]; then
    echo "IXP Logviewer softlink already exists."
else
    echo "Creating IXP Logviewer softlink /usr/local/bin/ixp-logviewer."
    ln -s '/opt/ixp/ixp-logviewer' '/usr/local/bin/ixp-logviewer'
fi
chown root:ixp '/usr/local/bin/ixp-logviewer'
chmod 770 '/usr/local/bin/ixp-logviewer'

# // link for main file //
if [ -L '/usr/local/bin/ixp' ]; then
    echo "IXP softlink already exists."
else
    echo "Creating IXP softlink /usr/local/bin/ixp."
    ln -s '/opt/ixp/ixp' '/usr/local/bin/ixp'
fi
chown root:ixp '/usr/local/bin/ixp'
chmod 770 '/usr/local/bin/ixp'

### //  Python3 ixp.py package // ###
py3_site=$(python3 -m site | grep '/usr/local/lib' | awk -v FS="('|')" '{print $2}')
echo "Copying ixp.py package to $py3_site."
cp '../files/ixp.py' "$py3_site"
chown root:ixp "$py3_site/ixp.py"
# //temp while developing
#chmod 550 "$py3_site/ixp.py"
chmod 660 "$py3_site/ixp.py"

# // LXD init //

# // Preseed YAML file for LXD //
cat <<EOF | lxd init --preseed
config: {}
cluster: null
networks: []
storage_pools:
- config: {}
  description: ""
  name: default
  driver: dir
profiles:
- config: {}
  description: ""
  devices:
    root:
      path: /
      pool: default
      type: disk
  name: default

EOF

echo "LXD Hypervisor preceeded."

# // LXC profile //

# // Create Dual NIC profile for containers //
lxc profile create dualnic

cat <<EOF | lxc profile edit dualnic
# // Create default LXD profile //

### This is a yaml representation of the profile.
# Added for DualNIC configuration.

config: {}
description: LXD Dual NIC profile
devices:
  ens3:
    name: ens3
    nictype: bridged
    parent: br100
    type: nic
  ens4:
    name: ens4
    nictype: bridged
    parent: br900
    type: nic
  root:
    path: /
    pool: default
    type: disk
name: dualnic
used_by: []

EOF

echo "LXC Dualnic profile created."

# // Change ownership of the ~/.config/lxc directory //
chown -R $SUDO_USER: /home/$SUDO_USER/.config/

# // Autoremove packages that are no longer required //
apt-get -y autoremove

# // Final message //
echo "The IXP Builder package has been installed successfully on $(hostname)."

# End
exit 0
