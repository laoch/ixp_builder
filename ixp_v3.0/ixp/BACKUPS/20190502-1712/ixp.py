#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""ixp_Server builder: Builds IXP on LXD/LXC platform."""

__author__      = "Diarmuid O'Briain"
__copyright__   = "Copyright 2019, C²S Consulting"
__license__     = "European Union Public Licence v1.2"
__version__     = "Phase 3.0, Version 5.0"

#
# Copyright 2019 C²S Consulting
# 
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved 
# by the European Commission - subsequent versions of the EUPL (the "Licence");
#
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
# 
# https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
# 
# Unless required by applicable law or agreed to in writing, software distributed
# under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR 
# CONDITIONS OF ANY KIND, either express or implied.
# 
# See the Licence for the specific language governing permissions and limitations 
# under the Licence.
#

'''
Notes: Traditional and software-defined IXP site with virtualised backend.

'''

from grp import getgrgid
from operator import itemgetter
from os import listdir
from os import path
from os import remove
from pwd import getpwuid
from Crypto.PublicKey import RSA
from paramiko import SSHClient
from paramiko import AutoAddPolicy
import datetime
import getpass
import inspect
import ipaddress
import os
import re
import subprocess
import sys
import tempfile
import time
import validators
import sqlite3
from itertools import chain
import urllib.request
import urllib.error 
import urllib.parse
import json


# // Global variable declarations //
dev_test = True    ### DEFAULT: False, for dev only 
gai_file = '/etc/gai.conf'
help_list = {'?', 'help'}
ixp_servers = {'lxd':'230', 'ns':'231', 
               'rs':'232', 'cs':'233', 
               'bs':'234', 'sc':'235'
               }

ixp_schema_keys = ('ipv4_peer', 'ipv6_peer', 'ipv4_man', 
                   'ipv6_man', 'domain', 'as_number', 
                   'enabled'
                   )

ixp_site_info_keys = ('site_number','site_type', 
                      'switching_type','country',
                      'town','elevation',
                      'organisation', 
                      'latitude', 'longitude'
                      )

ixp_of_keys = ('switch_number', 'switch_name', 'switch_type', 
               'manufacturer','control_interface', 
               'data_interface', 'switch_interfaces', 
               'enabled_interfaces'
               )
local_host = subprocess.getstatusoutput ('hostname')[1] 
log_dir = '/var/log/ixp/'
netplan_dir = '/etc/netplan/'
server_template_bak = 'ub-18-04-bak'
netplan_file = '/etc/netplan/10-netplan.yaml'
ixp_db = '/var/ixp/ixp_db.sqlite'
int_speeds = ['100M', '1G', '10G', '40G']
public_ipv6_net = False
server_template = ('ub-18-04', 'ubuntu:18.04')
server_upgrade_flag = 0
test_websites = ['http://www.google.com',
                 'http://www.ubuntu.com'
                ]
bird_root = '/etc/bird/'
bird_file = 'bird.conf'
bird6_file = 'bird6.conf'
upstream_ns = ('8.8.8.8', '8.8.8.4')   # Quad9: PCH/IBM/GCA  Quad8: Google
var_dir = '/var/ixp/'
file_perm = 770
key_perm = 600
sudo_pass = ''
sdn_port = 6633
tun_port = 22
dpid_ip_filename = '/srv/ryu/.dpid_to_ip.map'
dpid_port_file = '.dpid_to_port.map'
dpid_port_dir = '/srv/ryu/'

# // Test if this file is being ran directly //

if (__name__ == '__main__'):
    print ('This module cannot be run directly')
    sys.exit(1)

##############################################
# //              FUNCTIONS               // #
##############################################

#============================================#
# //       _function_call_location        // #
#============================================#

def _function_call_location():
    ''' Function to check if a function is called via module or locally '''

    if (inspect.stack()[2][3] == '<module>'):
        print ('Sneaky attempt to access internal function.\n')
        exit(1)    

    # End _function_call_location

#============================================#
# //              _sudo_call              // #
#============================================#

def _sudo_call(language, code_list):
    ''' Function to allow sudo calls '''
    
    global sudo_pass

    # // Get language binary //
    lang = subprocess.getstatusoutput (f'which {language}')[1]
    _ixp_debug('y',f'INFO[_sudo_call]: Get the pathname for {language}')

    # // Get username and ask for password //
    # username = getpass.getuser()
    # if (sudo_pass == ''):
    #     sudo_pass = getpass.getpass(f'sudo password for {username} required: ')    
    #     _ixp_debug('y',f'INFO[_sudo_call]: Get the sudo user {username}\'s password')
    username = 'ubuntu'
    sudo_pass = 'ubuntu'

    # // Generate temporary filename //
    tf = tempfile.NamedTemporaryFile()
    temp_file_name = tf.name
    tf.close()

    _ixp_debug('y',f'INFO[_sudo_call]: Open the {temp_file_name} temporary file')
    with _ManagedFile(temp_file_name) as fo:  
        for x in code_list:
            fo.write(f'{x}\n')

    # // Execute the sudo script //
    _ixp_debug('y',f'INFO[_sudo_call]: Execute the {temp_file_name} file with {lang}')
    response = subprocess.getstatusoutput (f"echo {sudo_pass} | sudo -S -p '' {lang} {temp_file_name}")

    # // Delete the temporary file //
    _ixp_debug('y',f'INFO[_sudo_call]: Delete the temporary file {temp_file_name}')
    subprocess.getstatusoutput (f'rm {temp_file_name}')

    return (response)

    # End _sudo_call function

#============================================#
# //       _ixp_timestamp function        // #
#============================================#

def _ixp_timestamp():
    ''' Function presents current time, date and timestamp when requested '''

    _function_call_location()
 
    ttime = '{:%H:%M:%S}'.format(datetime.datetime.now())
    tdate = '{:%Y%m%d}'.format(datetime.datetime.now())
    tstamp = '{:%Y%m%d-%H%M%S}'.format(datetime.datetime.now())

    return (ttime, tdate, tstamp)

    # End _ixp_timestamp function

#============================================#
# //         _site_number function        // #
#============================================#

def _site_number():
    ''' Function to extract the site number and returns '''

    _function_call_location()

    str_ = str()
 
    str_ = list(_read_db('site').keys())[0]

    return (str_)

    # End _site_number function

#============================================#
# //       _switching_type function       // #
#============================================#

def _switching_type():
    ''' Function to extract the switching type and returns '''

    _function_call_location()

    str_ = str()
 
    str_ = _read_db('site')[_site_number()]['switching_type']

    return (str_)

    # End _switching_type function

#============================================#
# //          _site_type function         // #
#============================================#

def _site_type():
    ''' Function to extract the site type and returns '''

    _function_call_location()

    str_ = str()
 
    str_ = _read_db('site')[_site_number()]['site_type']

    return (str_)

    # End _site_type function

#============================================#
# //     _get_file_dir_info function      // #
#============================================#

def _get_file_dir_info(arg):
    ''' Function to get information from file or directory '''

    _function_call_location()

    dict_ = dict()

    # showing stat information for files
    dict_['name'] = arg

    # Get type
    if (os.path.isdir(arg)):
            dict_['type'] = 'directory'
    elif (os.path.isfile(arg)):
            dict_['type'] = 'file'
    else:
            dict_['type'] = 'not_exist'
            return (dict_)

    # Get owner and group
    statinfo = os.stat(arg)
    dict_['uid'] = statinfo.st_uid
    dict_['gid'] = statinfo.st_gid

    dict_['owner'] = getpwuid(statinfo.st_uid).pw_name
    dict_['group'] = getgrgid(statinfo.st_gid).gr_name

    # Get permissions
    dict_['permissions'] = oct(statinfo.st_mode)[-3:]

    return (dict_)

    # End _get_file_dir_info function 

#============================================#
# //         _is_empty function           // #
#============================================#

def _is_empty(args):
    ''' Function to test is a variable, list or dictionary is empty '''

    _function_call_location()

    if (args):
        return False
    else:
        return True

    # End _is_empty function 

#============================================#
# //         _ixp_debug function          // #
#============================================#

def _ixp_debug(dateyn, err_msg):
    ''' Function to debug function, writes logs to the daily logfile '''

    _function_call_location()

    (ttime, tdate, tstamp) = _ixp_timestamp()
    log_file = f'{log_dir}{tdate}-ixp.log'

    # // Check if logfile for 'today' already exists //
    if not (subprocess.getstatusoutput (f"readlink {log_dir}today")[1] == log_file):
        print (f'Generating daily logfile {log_file}\n')
        subprocess.getstatusoutput (f"rm {log_dir}today")
        subprocess.getstatusoutput (f"ln -s {log_dir}{tdate}-ixp.log {log_dir}today")

    # // Write logs to todays logfile //
    try:
        if (dateyn == 'y'):
            l_file = open(log_file, 'a')
            l_file.write (f'{tstamp}: {err_msg}\n')
        else:
            l_file = open(log_file, 'a')
            l_file.write (f'  {err_msg}\n')
    except:
        print (f'Cannot open the {log_file}')
        sys.exit(1)

    l_file.close()

    return

    return (0)

    # End _ixp_debug function

#============================================#
# //         _ixp_schema_servers          // #
#============================================#

def _ixp_schema_servers(str_ = 'containers'):
    ''' Function to extract a list of servers from the IXP Schema '''

    _function_call_location()

    dict_ = dict()

    if (len(list(_read_db('schema').keys())) == 0):
        _ixp_debug('y', 'INFO[_ixp_schema_servers]: There is no schema configured, suggest building one')
        return (0)

    if (str_ == 'all'):
        dict_ = list(_read_db('schema').keys())
    elif (str_ == 'enabled'):
        dict_ = list(_read_db('schema', 'enabled', 1).keys()) 
    elif (str_ == 'containers'):
        dict_ = list(_read_db('schema', 'enabled', 1).keys()) 
        dict_.pop(0)
    else:
        print (f'ERROR: Bad selection {str_}\n')       
        _ixp_debug('y', f'ERROR: Bad selection {str_}')     

    return (list(dict_))

    # End _ixp_schema_servers

#============================================#
# //           _validate_ip_net           // #
#============================================#

def _validate_ip_net(a):
    ''' Function to validate IP Networks and IP addresses to ensure DB is clean '''

    _function_call_location()
 
    _ixp_debug('y', f'INFO[_validate_ip_net]: Validating: {a}')

    list_ = list()
    ip_dict = dict()
    error_flag = 0
    
    # // Split away the mask and determine version //
    try:
        if ('/' in a):
            list_ = a.split('/')
        else:
            list_.append(a)
            list_.append(0)
        ver = ipaddress.ip_address(list_[0]).version
        ip = ipaddress.ip_interface(a)
        ip_dict['ver'] = ver
        ip_dict['addr'] = ip.with_prefixlen
        ip_dict['ip'] = str(ip.ip)
        ip_dict['prefix'] = int(list_[1])
        ip_dict['net'] = str(ip.network)
        _ixp_debug('y', f'INFO[_validate_ip_net]: Address {a} validated') 

    except: 
        print (f'ERROR[_validate_ip_net]: {a} is not a proper IP address format.\n')
        _ixp_debug('y', f'ERROR[_validate_ip_net]: {a} is not a proper IP address format.')
        sys.exit(1)

    # // Check for long IPv4 mask //
    if (ip_dict['ver'] == 4 and ip_dict['prefix'] > 24):
        print (f"The prefix {ip_dict['prefix']} is too long for IPv4, /24 is the longest\n")
        _ixp_debug('y', f"ERROR[_validate_ip_net]: The prefix {ip_dict['prefix']} is too long for IPv4, /24 is the longest")
        error_flag += 1
 
    # // Check for long IPv6 mask //       
    elif (ip_dict['ver'] == 6 and ip_dict['prefix'] > 48):
        print (f"The prefix {ip_dict['prefix']} is too long for IPv6, /48 is the longest\n")
        _ixp_debug('y', f"ERROR[_validate_ip_net]: The prefix {ip_dict['prefix']} is too long for IPv6, /48 is the longest")
        error_flag += 1

    if (error_flag == 0):
        return (ip_dict)
    else:
        sys.exit(1)

    # End validate_ip_net function

#============================================#
# //         _ixp_schema_network          // #   
#============================================#

def _ixp_schema_network(str_ = 'man'):
    ''' Function to extract the network from the IXP Schema ''' 

    _function_call_location()

    if not (str_ == 'man' or str_ == 'peer'):
        print (f'ERROR: Non valid variable supplied\n')
        _ixp_debug('y', 'ERROR[_ixp_schema_network]: Non valid variable supplied')
        sys.exit(1)

    name = 'Management LAN' if str_ == 'man' else 'Peering LAN'

    _ixp_debug('y', f'INFO[_ixp_schema_network]: Getting the: {name}')

    # // Get LXD Controller name //
    LXD = [x for x in _read_db('schema').keys() if x[:3] == 'lxd'][0]

    # // Get Host IP Address //
    v4 = _read_db('schema')[LXD][f'ipv4_{str_}']
    v6 = _read_db('schema')[LXD][f'ipv6_{str_}']

    # // Get the management networks from the host IP address //
    (v4_net, v4_prefix) = _validate_ip_net(v4)['net'].split('/')
    v4_net = v4_net[:-1]
    (v6_net, v6_prefix) = _validate_ip_net(v6)['net'].split('/')

    return (v4_net, v4_prefix, v6_net, v6_prefix)

    # End _ixp_schema_network function

#============================================#
# //         _non_ip_test function        // #
#============================================#

def _non_ip_test(test, value):
    ''' Function to test non-IP related values '''

    _function_call_location()    

    st_list = ['core','mini']
    sw_list = ['traditional','software-defined']
    lat_search = '^\d{1,2}\s\d{1,2}\s\d{1,2}\.\d{1,4}\s[NS]$'
    long_search = '^\d{1,2}\s\d{1,2}\s\d{1,2}\.\d{1,4}\s[EW]$' 

   # // Test domain //
    if (test == 'domain'):

        # // Test for uppercase characters //
        if not (value == value.lower()):
            print (f"ERROR: '{value} has uppercase characters'\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]:'{value}' has uppercase characters")   
            return (1)

        # // Test domain //
        else:
            reply = validators.domain(value)         
            if not (reply == True):
                print (f'ERROR: \'{value}\' is not a valid domain name\n')
                _ixp_debug('y',f'ERROR[_non_ip_test]: \'{value}\' is not a valid domain name')
                sys.exit(1)

    # // Test AS Number //
    elif (test == 'as_number'):
        try:
            value = int(value)
        except:
            print (f"ERROR: '{test}: {value}' must be an number\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{test}: {value}' must be an number")
            sys.exit(1)
        if not ((0 <= value < 4200000000) == True):
            print (f"ERROR: '{value}' is not a valid AS number\n")
            _ixp_debug('y',f'ERROR[_non_ip_test]: \'{value}\' is not a valid AS number')
            sys.exit(1)  

    # // Test Switch interfacess //
    elif (test == 'switch_interfaces'):
        try:
            value = int(value)
        except:
            print (f"ERROR: '{test.replace('_',' ')}: {value}' must be an number\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{test.replace('_',' ')}: {value}' must be an number")
            sys.exit(1)
        if not ((0 <= value <= 99) == True):
            print (f"ERROR: '{value}' Outside range of allowable interfaces (< 100)\n")
            _ixp_debug('y',f'ERROR[_non_ip_test]: \'{value}\' Outside range of allowable interfaces (< 100)')
            sys.exit(1)  

    # // Test Enabled interfaces //
    elif (test == 'enabled_interfaces'):
        if not (value == 'all'):
            value = max(_parse_range_list(value))
            if not ((0 <= int(value) <= 99) == True):
                print (f"ERROR: '{value}' Outside range of allowable interfaces (< 100)\n")
                _ixp_debug('y',f'ERROR[_non_ip_test]: \'{value}\' Outside range of allowable interfaces (< 100)')
                sys.exit(1)  

    # // Test Switch interface speed //
    elif (test == 'interface_speed'):
        if not (value.upper() in int_speeds): 
            str_ = ', '.join(int_speeds)
            print (f"ERROR: '{value}' is not a valid interface speed, try one from {str_}\n")
            _ixp_debug('y',f'ERROR[_non_ip_test]: \'{value}\' is not a valid interface speed, try one from {str_}')
            sys.exit(1)  

    # // Test Site Number //
    elif (test == 'site_number'):
        try:
            value = int(value)
        except:
            print (f"ERROR: '{test}: {value}' must be an number\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{test}: {value}' must be an number")
            sys.exit(1)
        if not ((0 <= value <= 100) == True):
            print (f"ERROR: '{value}' is not a valid site number\n")
            _ixp_debug('y',f'ERROR[_non_ip_test]: \'{value}\' is not a valid site number')
            sys.exit(1)   

    # // Test 10 character limit //
    elif (test in ('switch_name', 'switch_type','manufacturer')):
        if (len(value) > 10):
            print (f"ERROR: '{value}' is too long for {test} name\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}' is too long for {test} name")
            sys.exit(1) 

    # // Test 25 character limit //
    elif (test in ('organisation', 'town', 'country', 'site_name', 'location')):
        if (len(value) > 25):
            print (f"ERROR: '{value}' is too long for {test} name\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}' is too long for {test} name")
            sys.exit(1) 

    # // Test Peer name //
    elif (test == 'name'):
        if (len(value) > 25):
            print (f"ERROR: '{value}' is too long for {test} name\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}' is too long for {test} name")
            sys.exit(1) 

        if not (value[0].isalpha()):
            print (f"ERROR: The first letter of '{value}' must be an alpha (a-zA-Z)\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: The first letter of '{value}' must be an alpha (a-zA-Z)")
            sys.exit(1) 

    # // Test Site Type //
    elif (test == 'site_type'):
        value = [x for x in st_list if value == x[:len(value)]][0]
        if not (value in st_list):
            print (f"ERROR: '{value}' is not a valid site type\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}' is not a valid site type")
            sys.exit(1) 

    # // Test Switching Type //
    elif (test == 'switching_type'):
        value = [x for x in sw_list if value == x[:len(value)]][0]
        if not (value in sw_list):
            print (f"ERROR: '{value}' is not a valid switching type\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}' is not a valid switching type")
            sys.exit(1) 

    # // Test Elevation //
    elif (test == 'elevation'):
        try:
            value = int(value)
        except:
            print (f"ERROR: '{test}: {value}' must be an number\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{test}: {value}' must be an number")
            sys.exit(1)            
        if not ((0 <= value <= 8848) == True):
            print (f"ERROR: '{test}: {value}' must be digits between 0 - 8848\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{test}:{value}' must be digits between 0 - 8848")
            sys.exit(1)  

    # // Test Switch //
    elif (test == 'switch'):
        try:
            value = int(value)
        except:
            print (f"ERROR: '{test}: {value}' must be an number\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{test}: {value}' must be an number")
            sys.exit(1)            
        if not ((0 <= value <= 9) == True):
            print (f"ERROR: '{test}: {value}' must be digits between 0 - 9\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{test}:{value}' must be digits between 0 - 9")
            sys.exit(1)  

    # // Test Latitude //                
    elif (test == 'latitude'):
        value = value.upper()    # Uppercase the N or S                         
        if not re.search(lat_search, value, re.M):
            print (f"ERROR: '{value}' is not a valid latitude\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}' is not a valid latitude")
            sys.exit(1)   

    # // Test Longitude //               
    elif (test == 'longitude'):
        value = value.upper()                  # Uppercase the E or W                             
        if not re.search(long_search, value, re.M):
            print (f"ERROR: '{value}' is not a valid longitude\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}' is not a valid longitude")
            sys.exit(1) 

    # // Test IP type //               
    elif (test == 'ip_type'):
        if not (value in ['ipv4', 'ipv6']):
            print (f"ERROR: '{value}' is not a valid IP type\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}'  is not a valid IP type")
            sys.exit(1) 

    # // Test servers //               
    elif (test == 'route_server' or test == 'blackhole_server'):
        if not (value in ['yes', 'no']):
            print (f"ERROR: '{value}' is not a valid for {test.replace('_', ' ')}\n")
            _ixp_debug('y',f"ERROR[_non_ip_test]: '{value}' is not a valid for {test.replace('_', ' ')}")
            sys.exit(1) 

    else:
        print (f"ERROR: Not a valid test '{value}' for _non_ip_test function\n")
        _ixp_debug('y',f"ERROR[_non_ip_test]: Not a valid test '{value}' for _non_ip_test function")
        sys.exit(1)

    return (0)

    # End _non_ip_test function

#============================================#
# //          _ixp_database_drop          // #
#============================================#

def _ixp_database_drop(table):
    ''' Function to drop an ixp database '''

    _function_call_location()

    # // Connect to Database //
    conn = sqlite3.connect(ixp_db)
    cur = conn.cursor()

    # // Drop table if it currently exists in DB //
    query = f'DROP TABLE IF EXISTS {table}'
    cur.execute(query)
    _ixp_debug('y', f'INFO[_ixp_database_drop]: Dropping {table} from the database')
    _ixp_debug('n', query)     

    # // Commit to the database //
    conn.commit()

    # // Close link to the database //
    conn.close()
 
    return (0)

    # End _ixp_database_drop method

#============================================#
# //        _ixp_database_create          // #
#============================================#

def _ixp_database_create(table, labels_dict):
    ''' Function to create an ixp database, create DB table if it doesn't exist '''

    _function_call_location()

    # // Connect to Database //
    conn = sqlite3.connect(ixp_db)
    cur = conn.cursor()

    # // Create table if not exists //
    query_list = [f'CREATE TABLE IF NOT EXISTS {table} (']
    c = 1 
    for k,v in labels_dict.items():    
        query_list.append(f'{k} {v}')
        if (c != len(labels_dict.keys())):
            query_list.append(', ')            
        c += 1
    query_list.append(')')    

    query = "".join(str(x) for x in query_list)
    cur.execute(query)
    _ixp_debug('y', f"INFO[_ixp_database_create]: Creating database table {table} if it doesn't exist")
    _ixp_debug('n', query)    

    # // Commit to the database //
    conn.commit()

    # // Close link to the database //
    conn.close()
 
    return (0)

    # End _ixp_database_create method

#============================================#
# //        _ixp_database_rebuild         // #
#============================================#

def _ixp_database_rebuild(table, labels_dict):
    ''' Function to rebuild an ixp database, rebuilds DB tables '''

    _function_call_location()

    # // Connect to Database //
    conn = sqlite3.connect(ixp_db)
    cur = conn.cursor()

    # // Drop table if it currently exists in DB //
    query = f'DROP TABLE IF EXISTS {table}'
    cur.execute(query)
    _ixp_debug('y', f'INFO[_ixp_database_rebuild]: Dropping {table} from the database')
    _ixp_debug('n', query)     

    # // Create new query //
    query_list = [f'CREATE TABLE {table} (']
    c = 1 
    for k,v in labels_dict.items():    
        query_list.append(f'{k} {v}')
        if (c != len(labels_dict.keys())):
            query_list.append(', ')            
        c += 1
    query_list.append(')')    

    query = "".join(str(x) for x in query_list)
    cur.execute(query)
    _ixp_debug('y', f'INFO[_ixp_database_rebuild]: Creating fresh database table {table}')
    _ixp_debug('n', query)    

    # // Commit to the database //
    conn.commit()

    # // Close link to the database //
    conn.close()
 
    return (0)

    # End _ixp_database_rebuild method

#============================================#
# //         _ixp_database_insert         // #
#============================================#

def _ixp_database_insert(table, values_dict):
    ''' Function to insert into an ixp database '''

    _function_call_location()

    # // Connect to Database //
    conn = sqlite3.connect(ixp_db)
    cur = conn.cursor()

    keys_str = ", ".join(str(x) for x in values_dict.keys())
    values_str = ", ".join(str(x) for x in values_dict.values())

    query = f'INSERT INTO {table} ({keys_str}) VALUES ({values_str})'
    cur.execute(query)
    _ixp_debug('y', f'INFO[_ixp_database_insert]: Inserting data into database table {table}')
    _ixp_debug('n', query)

    # // Commit to the database //
    conn.commit()

    # // Close link to the database //
    conn.close()

    return (0)

    # End _ixp_database_insert method

#============================================#
# //         _ixp_database_delete         // #
#============================================#

def _ixp_database_delete(table, *args):
    ''' Function to delete an ixp database '''

    _function_call_location()

    # // Connect to Database //
    conn = sqlite3.connect(ixp_db)
    cur = conn.cursor()

    # // Detect no args //
    if (len(args) == 0):
        cur.execute(f"DELETE FROM '{table}'")
        conn.commit()
        conn.close()
        return (0)            

    # // Odd number of arguments //
    elif (len(args) % 2 != 0):
        args_str = ', '.join(x for x in args)
        print (f'ERROR: Uneven number of elements in {args_str}\n')   
        _ixp_debug('y',f'ERROR[_ixp_database_delete]: Uneven number of elements in {args_str}') 
        sys.exit(1)      

    # // Break arguments into pairs //
    else:
        args_list = list(args)
        pairs = list()

        while(args_list):
            a = args_list.pop(0); b = args_list.pop(0)
            pairs.append((a,b))  
            
    # // Go through pairs and build query //

    # // Delete primary key column name //
    query_list = [f"DELETE FROM {table}"] 
    for c, pair in enumerate(pairs):
        if (c == 0):
            query_list.append(f"WHERE {pair[0]} LIKE {pair[1]}")                
        else:
            query_list.append(f"AND {pair[0]} LIKE {pair[1]}") 

        query = ' '.join(x for x in query_list)  
        cur.execute(query)
        _ixp_debug('y', f"INFO[_ixp_database_delete]: Deleting data from the database table '{table}'")
        _ixp_debug('n', query)

    # // Commit to the database //
    conn.commit()

    # // Close link to the database //
    conn.close()

    return (0)

    # End _ixp_database_delete method

#============================================#
# //         _ixp_database_update         // #
#============================================#

def _ixp_database_update(table, row_key, values_dict, *args):
    ''' Function to update ixp database '''

    # // Two options: Find by Primary Key: row_key   //
    # //            : Find by arguments              //

    _function_call_location()

    # // Connect to Database //
    conn = sqlite3.connect(ixp_db)
    cur = conn.cursor()

    if (len(args) < 1):
        # // Extract primary key column name //
        cur.execute(f"PRAGMA table_info({table})")
        _ixp_debug('y',f'INFO[_read_db]: Extracting column names from {table}')

        # // Process table column names //
        key_id = cur.fetchall()[0][1]

        for k,v in values_dict.items():  
            if (v == None):
                query_list = [f"UPDATE {table} SET {k} = NULL"]        
            else:         
                query_list = [f"UPDATE {table} SET {k} = '{v}'"] 

            # // Assemble query //
            query_list.append(f"WHERE {key_id} LIKE {row_key}")

            query = ' '.join(x for x in query_list)
            cur.execute(query)
            _ixp_debug('y', f"INFO[_ixp_database_update]: Updating data for '{key_id}: {row_key}' in database table '{table}'")
            _ixp_debug('n', query)

    else:
        # // Odd number of arguments //
        if (len(args) % 2 != 0):
            args_str = ', '.join(x for x in args)
            print (f'ERROR: Uneven number of elements in {args_str}\n')   
            _ixp_debug('y',f'ERROR[_ixp_database_update]: Uneven number of elements in {args_str}') 
            sys.exit(1)      

        # // Break arguments into pairs //
        else:
            args_list = list(args)
            pairs = list()

            while(args_list):
                a = args_list.pop(0); b = args_list.pop(0)
                pairs.append((a,b))  
             
        # // Go through pairs and build query //

        # // Extract primary key column name //
        for k, v in values_dict.items():
            if (v == None):
                query_list = [f"UPDATE {table} SET {k} = NULL"]        
            else:         
                query_list = [f"UPDATE {table} SET {k} = '{v}'"] 

            for c, pair in enumerate(pairs):
                if (c == 0):
                    query_list.append(f"WHERE {pair[0]} LIKE {pair[1]}")                
                else:
                    query_list.append(f"AND {pair[0]} LIKE {pair[1]}") 

            query = ' '.join(x for x in query_list)  
            cur.execute(query)
            _ixp_debug('y', f"INFO[_ixp_database_update]: Updating data in the database table '{table}'")
            _ixp_debug('n', query)

    # // Commit to the database //
    conn.commit()

    # // Close link to the database //
    conn.close()

    return (0)

    # End _ixp_database_update method

#============================================#
# //              _read_db                // #
#============================================#

def _read_db(table, *args): 
    ''' Function to read the DB tables '''

    _function_call_location()

    dict_keys = list()
    dict_dict = dict()
    query_list = [f"SELECT * FROM {table}"]

    # // Check if a list of arguments was sent and convert to tuple //
    if (len(args) > 0 and type(args[0]) == list):
        args_ = tuple(args[0])
    else:
        args_ = args

    # // Open the database //
    try:
        conn = sqlite3.connect(ixp_db)
        cur = conn.cursor()
    except:
        print ('ERROR: No connection to database\n')
        _ixp_debug('y','ERROR[_read_db]: No connection to database')
        sys.exit(1)

    # // Extract table column names //
    query = f"PRAGMA table_info({table})"
    cur.execute(query)
    _ixp_debug('y',f'INFO[_read_db]: Extracting column names from {table}')
    _ixp_debug('n', query)

    # // Process table column names //
    raw = cur.fetchall()
    for x in raw:
        if (x[0] > 0):
            dict_keys.append(x[1])    # Row header data (except first)

    # // Import the schema from the database //
    if (len(args_) < 1):
        query = f"SELECT * FROM {table}"

    # // Odd number of arguments //
    elif (len(args_) % 2 != 0):
        args_str = ', '.join(x for x in args_)
        print (f'ERROR: Uneven number of elements in {args_str}\n')   
        _ixp_debug('y',f'ERROR[_read_db]: Uneven number of elements in {args_str}') 
        sys.exit(1)       

    # // Break arguments into pairs //
    else:
        args_list = list(args_)
        pairs = list()

        while(args_list):
            a = args_list.pop(0); b = args_list.pop(0)
            pairs.append((a,b))

        # // Go through pairs and build query //
        for c, pair in enumerate(pairs):
            if (c == 0):                
                query_list.append(f"WHERE {pair[0]} LIKE {pair[1]}")                
            else:
                query_list.append(f"AND {pair[0]} LIKE {pair[1]}") 

        query = ' '.join(x for x in query_list)

    # // Execute query //
    cur.execute(query)
    _ixp_debug('y',f'INFO[_read_db]: Extracting data from {table}')
    _ixp_debug('n', query)

    # // Process data and link with column names in dictionary //
    raw = cur.fetchall()
    for x in raw:
        temp_dict = dict()
        name = x[0]
        for y in range (0, len(x) - 1):
            temp_dict[dict_keys[y]] = x[y + 1] 
        dict_dict[name] = temp_dict

    # // Close the database connection //
    conn.close()

    # // site information from DB //
    _ixp_debug('y',f'INFO[_read_db]: Importing configuration from database')

    # // return list of dictionaries //
    return (dict_dict)

    # End _read_db function

#============================================#
# //          _parse_range_list           // #
#============================================#

def _parse_range_list(range_list):
    ''' Function to parse a comma-separated list of numbers and ranges '''

    _function_call_location()

    def _parse_range(number_range):
        if len(number_range) == 0:
            return []

        parts = number_range.split("-")
        if len(parts) > 2:
            _ixp_debug('y', f'ERROR[_parse_range_list]:Invalid range: {number_range}')

        range_pair = range(int(parts[0]), int(parts[-1]) + 1)
        (range1, range2) = (int(parts[0]), int(parts[-1]))
        _ixp_debug('y', f'INFO[_parse_range]: The range: {range1}, {range2}')

        # // Return range //
        return (range_pair)

    sorted_list = sorted(set(chain.from_iterable(map(_parse_range, range_list.split(",")))))
    sorted_list_str = ', '.join(str(x) for x in sorted_list) 
    _ixp_debug('y', f'INFO[_parse_range_list]: Sorted list: {sorted_list_str}')
 
    # // Return sort //
    return (sorted_list)

    # End _number_range_list function

#============================================#
# //             _rangeStr                // #
#============================================#

def _rangeStr(start, end):
    ''' Function to convert two integers into a range start-end, or a single value if they are the same ''' 

    _function_call_location()

    if (start == end): 
        return (str(start))
    else: 
        return (f'{start}-{end}')

    # End _rangeStr function

#============================================#
# //             _makeRange               // #
#============================================#

def _makeRange(seq):
    ''' Function to convert sequence of int to string with the ranges '''

    _function_call_location()

    # // Make sure that seq is an iterator //            
    seq = iter(seq)
    start = seq.__next__()
    current = start
    for val in seq:
        current += 1
        if (val != current):
            yield _rangeStr(start, current - 1)
            start = current = val

    # // Make sure the last range is included in the output //
    yield(_rangeStr(start, current))

    # End _makeRange function

#============================================#
# //          _stringifyRanges            // #
#============================================#

def _stringifyRanges(seq):
    ''' Function to join sequence into string '''

    _function_call_location()

    return (','.join(_makeRange(seq)))

    # End of _stringifyRanges function

#============================================#
# //        _string_search_replace        // #
#============================================#

def _string_search_replace(filein, searchstr, replacestr):
    ''' Function to open a file, search and replace and output to another file'''

    _function_call_location()

    # // Confirm supplied values are in fact strings //   
    searchstr = str(searchstr)
    replacestr = str(replacestr)

    tf = tempfile.NamedTemporaryFile()
    temp_file_name = tf.name
    tf.close()

    try:
        fi = open(filein, 'r')

    except:
        print (f'ERROR: Cannot open {filein}')
        _ixp_debug('y', f'ERROR[_string_search_replace]: Cannot open {filein}')
        sys.exit(1)

    with _ManagedFile(temp_file_name) as fo:
        fo.write(fi.read().replace(searchstr, replacestr))

        _ixp_debug('y', f'INFO[_string_search_replace]: Replacing {searchstr} with {replacestr} in {filein}')

    fi.close()

    subprocess.getstatusoutput (f'mv {temp_file_name} {filein}')
    subprocess.getstatusoutput (f'chmod {file_perm} {filein}')

    # End _string_search_replace function

#============================================#
# //             _ip_link_list            // #
#============================================#

def _ip_link_list():
    ''' Function to list the available interfaces on the host computer '''

    _function_call_location()

    list_ = list()
    list2_ = list()
    list3_ = list()

    # Get list of non-virtual interfaces (unsorted)
    cmd = "ls -l /sys/class/net | grep --invert-match virtual"
    temp_ = subprocess.getstatusoutput (cmd)[1].split('\n')

    for x in temp_:
        if ('total' in x):
            continue
        list_.append(x.split('/')[-1])

    # // Get a sorted list of all interfaces //
    cmd2 = "networkctl list --no-legend | awk '{print $2}'"
    list2_ = subprocess.getstatusoutput (cmd2)[1].split('\n')

    # // Compare lists only include interfaces that are in unsorted list //
    for x in list2_:
        if (x in list_):
            list3_.append(x)
        else:
            pass

    _ixp_debug('y',f'INFO[_ip_link_list]: Get list of physical interfaces on server')

    # // Return sortedf list of physical interfaces //
    return (list3_)    

    # End _ip_link_list function

#============================================#
# //              _gai_build              // #
#============================================#

def _gai_build():
    ''' Function to Get Address Information (GAI), default address selection for IPv6 (rfc3484) '''

    _function_call_location()

    _ixp_debug('y','INFO[_ixp_host_build_trad]: Network does not have an IPv6 public connection')

    # // Create temporary file for GAI //
    tf = tempfile.NamedTemporaryFile()
    temp_file_name = tf.name
    tf.close()
 
    with _ManagedFile(temp_file_name) as gaifh:
        gaifh.write('# Prioritise IPv4 over IPv6 for DNS requests\n')
        gaifh.write('# (No IPv6 public address for testbed lab)\n\n')
        gaifh.write('scopev4 ::ffff:169.254.0.0/112  2\n')
        gaifh.write('scopev4 ::ffff:127.0.0.0/104    2\n')
        gaifh.write('scopev4 ::ffff:0.0.0.0/96       14\n')
        gaifh.write('precedence ::ffff:0:0/96        100\n')

    return (temp_file_name)

    # End _gai_build function

#============================================#
# //           _validate_options          // #
#============================================#

def _validate_options(ref_dict_, dict_):
    ''' Function to validate commandline options '''

    _function_call_location()

    c = 0
    dict2_ = dict()
    dict3_ = dict()

    # // Main part of the function //        
    for k, v in dict_.items():
        k = k.lower()
        if (k in ref_dict_.keys()):
            dict2_[ref_dict_[k]] = v
            _ixp_debug('y', f"INFO[_validate_options]: Swapping {k} for {ref_dict_[k]}")
        elif (k in ref_dict_.values()):
            dict2_[k] = v
        else:
            print (f"INFO: {k} is not a valid switch, ignoring ...\n")
            _ixp_debug('y', f"INFO[_validate_options]:{k} is not a valid switch, ignoring ...")

    dict3_ = _remove_minus_from_options(dict2_)

    # // Validate the values //
    for k, v in dict3_.items():
        # // Remove trailing 0 from IPv6 if they exist //
        if ('ipv6' in k):
            try:
                matched = re.findall(r'(.*::)0+(/\d{1,2})', v)
                if (len(matched[0]) == 2):
                    dict3_[k] = v = f'{matched[0][0]}{matched[0][1]}'
            except:
                pass

        # // Validate IP network //
        if ('ipv' in k):
            if not (v == _validate_ip_net(v)['net']):
                print (f"ERROR: The {k.replace ('_', ' ')}: {v} is not valid, exiting...\n")
                _ixp_debug('y', f"ERROR[_validate_options]:The {k.replace ('_', ' ')}: {v} is not valid, exiting...")  
                sys.exit(1)    

        # // Validate IP host //
        elif ('host' in k):
            if (v == _validate_ip_net(v)['ip']):
                pass
            elif (v == _validate_ip_net(v)['addr']):
                dict3_[k] = _validate_ip_net(v)['ip']
            else:
                print (f"ERROR: The {k.replace ('_', ' ')}: {v} is not valid, exiting...\n")
                _ixp_debug('y', f"ERROR[_validate_options]:The {k.replace ('_', ' ')}: {v} is not valid, exiting...")  
                sys.exit(1)    

        # // Validate everything else //
        else:
            if not (_non_ip_test(k, v) == 0):
                print (f"ERROR: No '_non_ip_test' available for {k}: {v}\n")
                _ixp_debug('y', f"ERROR[_validate_options]: No '_non_ip_test' available for {k}: {v}")           
 
        c += 1

    # // Return out dict
    return (dict3_)          

    # End of _validate_options function

#============================================#
# //     _remove_minus_from_options       // #
#============================================#

def _remove_minus_from_options(dict_):
    ''' Function to replace - and remove -- '''

    _function_call_location()

    dict2_ = dict()

    for k, v in dict_.items():
        k = k.replace('--','')
        k = k.replace('-','_')
        dict2_[k] = v

    # // Return out list
    return (dict2_) 

    # End of _remove_minus_from_options function

#============================================#
# //             _list_to_dict            // #
#============================================#

def _list_to_dict(args_):
    ''' Function to convert list to dictionary '''

    _function_call_location()

    dict_ = dict()

    # // Confirm an even number of arguments //
    if not (len(args_) % 2 == 0):
        print ('ERROR: Uneven length of arguments, exiting ...')
        _ixp_debug('y', 'ERROR[_list_to_dict]:Uneven length of arguments, exiting ...')
        sys.exit(1)

    # // Create an element dictionary of pairs and a key list //
    while(args_):
        a = args_.pop(0); b = args_.pop(0)
        dict_[a] = b

    return (dict_)

    ## End _list_to_dict function

### End of functions ###

##############################################
# //           _ManagedFile class         // #
##############################################

class _ManagedFile:
    ''' _ManagedFile class '''

    def __init__(self, filename, rwa = 'w'):
        ''' _ManagedFile class constructor method '''

        self.rwa = rwa
        self.filename = filename
        self.functionality = 'Private class to manage writing to files'

        ## End of __init__ method

    def __str__(self):
        ''' _ManagedFile class __str__ method '''

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' _ManagedFile class __repr__ method '''

        return (f'{self.__class__.__name__} class: {self.functionality}') 

        ## End of __repr__ method

    def __enter__(self):
        ''' _ManagedFile class __enter__ method '''

        self.filehandle = open(self.filename, mode=f'{self.rwa}', encoding='utf-8') 
        return (self.filehandle)

        ## End of __enter__ method

    def __exit__(self, exc_type, exc_val, exc_tb):
        ''' _ManagedFile class __exit__ method '''

        if (self.filehandle): 
            self.filehandle.close()

        ## End of __exit__ method

    ## End _ManagedFile class

##############################################
# //             IxpHelp class            // #
##############################################

class IxpHelp:
    ''' IXP Help class '''

    def __init__(self):
        ''' IxpHelp class constructor method '''
        
        self.functionality = 'IXPBuilder help for public methods'

        ## End of __init__ method

    def __str__(self):
        ''' IxpHelp class __str__ method '''

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' IxpHelp class __repr__ method '''

        return (f'{self.__class__.__name__} class: {self.functionality}')    

        ## End of __repr__ method   

    #============================================#
    # //                ixp_help              // #
    #============================================#

    def ixp_help(self, err_code = 0):
        ''' Method to provide ixp help ''' 

        print ('''Usage: ixp { help | ? } [OPTION] ... ...

        help, ?    - This help message.

        OPTION [list]:
        version   - Returns the version of the IXPBuilder module.
        schema    - Builds an IP Schema for the testbed.
        host      - Builds configuration for the host.
        switch    - Configuration and monitoring of IXP switches.
        server    - Operations on the IXP container servers.
        software  - Installation and configuration of software on the 
                    IXP container servers.
        peer      - Management of IXP peers.
        route     - Review IXP container server routes.
        remote    - Access mIXP from the cIXP command shell.
        ''')
        _ixp_debug('y', '\'ixp help\' called.')  
        sys.exit(err_code)

    ## End ixp_help 

    #============================================#
    # //            _ixp_schema_help          // #
    #============================================#

    def _ixp_schema_help(self, err_code = 0):
        ''' Method to provide ixp schema help '''

        print ('''Usage: ixp schema [OPTION]

        ?, help  -  This help message.

        OPTION [list]:
        build          -  Build IP Scheme with default values.
        default        -  Default the IP Schema and site info.
        show           -  Show the IP Scheme that currently exists.
        ?, help        -  This help message.
        ''')
        _ixp_debug('y', '\'ixp schema help\' called.')
        sys.exit(err_code)

    ## End _ixp_schema_help 

    #============================================#
    # //        _ixp_schema_show_help         // #
    #============================================#

    def _ixp_schema_show_help(self, err_code = 0):
        ''' Method to provide ixp schema show help '''

        e = _ixp_schema_servers()

        print (f'''Usage: ixp schema show {{ help | ? }} [OPTION(s)] [OBJECT(s)]

        ?, help  -  This help message.

        OPTION [list]:
        all     -  Show all IXP Site and IXP Schema information.
        both    -  Same as 'all'.
        site    -  Get IXP site information.
        schema  -  Get IXP Schema information.

        OBJECT(s) [list]:
        {e[0]}  -  Show Name Server.
        {e[1]}  -  Show Route Collector.
        {e[2]}  -  Show Route Server.
        {e[3]}  -  Show AS112 Blackhole Server.
        ''', end = '')

        if (_switching_type() == 'software-defined'):
            print (f'{e[4]}  -  Show SDN Controller Server.\n')      
        else:
            print ('')

        _ixp_debug('y', '\'ixp schema show help\' called.')
        sys.exit(err_code)

    ## End _ixp_schema_show_help 

    #============================================#
    # //       _ixp_schema_default_help       // #
    #============================================#

    def _ixp_schema_default_help(self, err_code = 0):
        ''' Method to provide ixp schema default help '''

        print ('''Usage: ixp schema default [OPTION]

        ?, help  -  This help message.

        OPTION [list]:
        all      -  Default the both IP Schema and Site info.
        both     -  Same as 'all'.
        schema   -  Default the IP Schema only.
        site     -  Default the Site info only.
        ?, help  -  This help message.
        ''')
        _ixp_debug('y', '\'ixp schema default help\' called.')
        sys.exit(err_code)

    ## End _ixp_schema_help 

    #============================================#
    # //        _ixp_schema_build_help        // #
    #============================================#

    def _ixp_schema_build_help(self, err_code = 0):
        ''' Method to provide ixp schema build help '''

        print ('''Usage: ixp schema build { help | ? } [ARGUMENT(s)] [VALUE]

        ?, help  -  This help message.

       ARGUMENT(s) [list]:
        -sn, --site-number    -  Site Number (i.e. 1-100).
        -st, --site-type      -  Core IXP or Mini IXP (i.e. core|mini).
        -sw  --switching-type -  Switching (i.e. traditional|software-defined).
        -o,  --organisation   -  Organisation (i.e. netLabs!UG).
        -p4, --ipv4-peer      -  IPv4 Peering LAN (i.e. 199.9.9.0/24).
        -p6, --ipv6-peer      -  IPv6 Peering LAN (i.e. 2a99:9:9::/48).
        -m4, --ipv4-man       -  IPv4 Management LAN (i.e. 198.8.8.0/24).
        -m6, --ipv6-man       -  IPv6 Management LAN (i.e. 2a98:8:8::/48).
        -d,  --domain         -  IXP Domain name (i.e. netlabs.tst).
        -as, --as-number      -  IXP Autonomous System number (i.e. 5999).
        -c,  --country        -  Country IXP is located in (i.e. Uganda).
        -t,  --town           -  Town/city IXP is located in (i.e. Kampala).
        -e,  --elevation      -  IXP elevation (m) above sea level (i.e. 1072).
        -la, --latitude       -  Deg, min, sec, N|S (i.e. '00 20 51.3456 N').
        -lo, --longitude      -  Deg, min, sec, E|W (i.e. '32 34 57.0720 E').
        ''')
        _ixp_debug('y', '\'ixp schema build help\' called.')
        sys.exit(err_code)

    ## End _self.ixp_schema_builder_help 

    #============================================#
    # //             _ixp_host_help           // #
    #============================================#

    def _ixp_host_help(self, err_code = 0):
        ''' Method to provide ixp host help '''

        print ('''Usage: ixp host { help | ? } [OPTIONS]

        ?, help      - This help message.

        OPTIONS [list]:
        show, list   - Show host network interfaces.
        build        - Build host.
        ''')
        _ixp_debug('y', '\'ixp host help\' called.')
        sys.exit(err_code)

    ## End _ixp_host_help 

    #============================================#
    # //         _ixp_host_build_help         // #
    #============================================#

    def _ixp_host_build_help(self, err_code = 0):
        ''' Method to provide ixp host build help '''

        print ('''Usage: ixp host build { help | ? } [ARGUMENT] [OBJECT]

        ?, help           - This help message.

        ARGUMENT(s) [list]:
        -s, --switch  -  Number of switches (i.e. 1-10).
        ''', end = '')
        if (_switching_type() == 'traditional'):
            print ('-sa, --sim-model-a - Simulate model A')
            print ('        -sb, --sim-model-b - Simulate model B')
        else:
            print ('')
        _ixp_debug('y', '\'ixp host build help\' called.')
        sys.exit(err_code)

    ## End _ixp_host_build_help

    #============================================#
    # //            _ixp_switch_help          // # 
    #============================================#

    def _ixp_switch_help(self, err_code = 0):
        ''' Method to provide ixp switch help '''

        print ('''Usage: ixp switch { help | ? } [OPTION]

        ?, help - This help message.

        OPTION(s) [list]:
        list, show - Return list of switches and parameters.
        set        - Change the parameters for switches.
        delete     - Remove switch information from IP tables.
        ''')
        _ixp_debug('y', '\'ixp switch help\' called.')
        sys.exit(err_code)

    ## End _ixp_switch_help

    #============================================#
    # //        _ixp_switch_delete_help       // # 
    #============================================#

    def _ixp_switch_delete_help(self, err_code = 0):
        ''' Method to provide ixp switch delete help '''

        print ('''Usage: ixp switch delete { help | ? } [OPTION]

        ?, help - This help message.

        OPTION(s) [list]:
        -s, --switch-number - (M) Switch Number (i.e. 2 but not 0).

        (M) mandatory
        ''')
        _ixp_debug('y', '\'ixp switch help\' called.')
        sys.exit(err_code)

    ## End _ixp_switch_help

    #============================================#
    # //         _ixp_switch_set_help         // # 
    #============================================#

    def _ixp_switch_set_help(self, switching_type, err_code = 0):
        ''' Method to provide ixp switch set help '''

        print ('''Usage: ixp switch set { help | ? } [speed] --switch-number <SWITCH-NUMBER> [ARGUMENT] [OBJECT]

        ?, help                   - This help message.

        speed                     - Set interface speed (see. ixp switch set speed help)

        ARGUMENT(s) [list]:
        -s, --switch-number       - (M) Switch Number (i.e. 2 but not 0).
        -sn, --switch-name        - Switch Name (i.e. core_sw_1). 
        -st, --switch-type        - Switch Type (i.e. M4300-48G).
                                    Note: For software-defined 'OvS' must be specified. 
        -m, --manufacturer        - Manufacturer (i.e. Netgear).  
        -si, --switch-interfaces  - (M) Switch Interfaces, number 1 - 99 (i.e. 2). 

        (M) mandatory
        ''', end = '')
        if (_switching_type() == 'software-defined'):
            print ("-ei, --enabled-interfaces - Enabled Interfaces, list (i.e. 2,4,6-9,11,13-20 or 'all').")
        else:
            print ('')
        _ixp_debug('y', '\'ixp switch set help\' called.')
        sys.exit(err_code)

    ## End _ixp_switch_set_help

    #============================================#
    # //      _ixp_switch_set_speed_help      // # 
    #============================================#

    def _ixp_switch_set_speed_help(self, err_code = 0):
        ''' Method to provide ixp switch set speed help '''

        print ('''Usage: ixp switch set speed { help | ? } --switch-number <SWITCH-NUMBER> [ARGUMENT] [OBJECT]

        ?, help                   - This help message.

        ARGUMENT(s) [list]:
        -s, --switch-number       - (M) Switch Number (i.e. 2 but not 0). 
        -ei, --enabled-interfaces - Enabled Interfaces, list (i.e. 2,4,6-9,11,13-20 or 'all').
        -is, --interface-speed    - Interface speed from 100M, 1G, 10G or 40G, default 1G).

        (M) mandatory
        ''')
        _ixp_debug('y', '\'ixp switch set speed help\' called.')
        sys.exit(err_code)

    ## End _ixp_switch_set_speed_help

    #============================================#
    # //            _ixp_server_help          // #
    #============================================#

    def _ixp_server_help(self, err_code = 0):
        ''' Method to provide ixp server help '''

        print ('''Usage: ixp server { help | ? } [OPTIONS]

        ?, help  -  This help message.

        OPTIONS [list]:
        build    -  Build IXP Servers.
        show     -  Show the state of an IXP Server.
        start    -  Bring an IXP Server to a state of 'Running'.
        stop     -  Bring an IXP Server to a state of 'Stopped'.
        delete   -  Erase an IXP Server.
        ''')
        _ixp_debug('y', '\'ixp server help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_help

    #============================================#
    # //        _ixp_server_build_help        // #
    #============================================#

    def _ixp_server_build_help(self, err_code = 0):
        ''' Method to provide ixp server build help '''

        e = _ixp_schema_servers()

        print (f'''Usage: ixp server build {{ help | ? }} [ARGUMENT] [OBJECT(s)]

        ?, help  -  This help message.
       
        ARGUMENT
        -y       -  Skip the yes/no questions.

        OBJECT(s) [list]:
        all      -  Build all IXP Servers.
        {e[0]}      -  Build Name Server.
        {e[1]}      -  Build Route Collector.
        {e[2]}      -  Build Route Server.
        {e[3]}      -  Build AS112 Blackhole Server.
        ''', end = '')

        if (_switching_type() == 'software-defined'):
            print (f'{e[4]}      -  Build SDN Controller Server.\n')      
        else:
            print ('')

        _ixp_debug('y', '\'ixp server build help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_build_help 

    #============================================#
    # //        _ixp_server_show_help         // #
    #============================================#

    def _ixp_server_show_help(self, err_code = 0):
        ''' Method to provide ixp server show help '''

        e = _ixp_schema_servers()

        print (f'''Usage: ixp server show {{ help | ? }} [OBJECT(s)]

        ?, help  -  This help message.

        ARGUMENT
        -ct, --connectivity-test  -  Additional Internet connectivity test.

        OBJECT(s) [list]:
        all   -  Display the status of all IXP Servers.
        {e[0]}   -  Display the status of the Name Server.
        {e[1]}   -  Display the status of the Route Collector.
        {e[2]}   -  Display the status of the Route Server.
        {e[3]}   -  Display the status of the AS112 Blackhole Server.
        ''', end = '')

        if (_switching_type() == 'software-defined'):
            print (f'{e[4]}   -  Display the status of the SDN Controller Server.\n')      
        else:
            print ('')

        _ixp_debug('y', '\'ixp server show help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_show_help

    #============================================#
    # //        _ixp_server_start_help        // #
    #============================================#

    def _ixp_server_start_help(self, err_code = 0):
        ''' Method to provide ixp server start help '''

        e = _ixp_schema_servers()

        print (f'''Usage: ixp server start {{ help | ? }} [OBJECT(s)]

        OBJECT(s) [list]:
        all   -  Start all IXP Servers.
        {e[0]}   -  Start the Name Server.
        {e[1]}   -  Start the Route Collector.
        {e[2]}   -  Start of the Route Server.
        {e[3]}   -  Start the AS112 Blackhole Server.
        ''', end = '')

        if (_switching_type() == 'software-defined'):
            print (f'{e[4]}   -  Start the SDN Controller Server.\n')      
        else:
            print ('')

        _ixp_debug('y', '\'ixp server start help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_start_help

    #============================================#
    # //        _ixp_server_stop_help         // #
    #============================================#

    def _ixp_server_stop_help(self, err_code = 0):
        ''' Method to provide ixp server stop help '''

        e = _ixp_schema_servers()

        print (f'''Usage: ixp server stop {{ help | ? }} [OBJECT(s)]

        OBJECT(s) [list]:
        all   -  Stop all IXP Servers.
        {e[0]}   -  Stop the Name Server.
        {e[1]}   -  Stop the Route Collector.
        {e[2]}   -  Stop of the Route Server.
        {e[3]}   -  Stop the AS112 Blackhole Server.
        ''', end = '')

        if (_switching_type() == 'software-defined'):
            print (f'{e[4]}   -  Stop the SDN Controller Server.\n')      
        else:
            print ('')

        _ixp_debug('y', '\'ixp server stop help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_stop_help

    #============================================#
    # //       _ixp_server_delete_help        // #
    #============================================#

    def _ixp_server_delete_help(self, err_code = 0):
        ''' Method to provide ixp server delete help '''

        e = _ixp_schema_servers()

        print (f'''Usage: ixp server delete {{ help | ? }} [ARGUMENT] [OBJECT(s)]

        ?, help  -  This help message.

        ARGUMENT
        -y       -  Skip the yes/no questions 
                    (Only necessary for 'delete' OPTION).

        OBJECT(s) [list]:
        all      -  Delete all IXP Servers.
        {e[0]}      -  Delete the Name Server.
        {e[1]}      -  Delete the Route Collector.
        {e[2]}      -  Delete the Route Server.
        {e[3]}      -  Delete the AS112 Blackhole Server.
        ''', end = '')

        if (_switching_type() == 'software-defined'):
            print (f'{e[4]}      -  Delete the SDN Controller Server.\n')      
        else:
            print ('')

        _ixp_debug('y', '\'ixp server delete help\' called.')
        sys.exit(err_code)

    ## End _ixp_server_delete_help

    #============================================#
    # //          _ixp_software_help          // #
    #============================================#

    def _ixp_software_help(self, err_code = 0):
        ''' Method to provide ixp software help '''

        print ('''Usage: ixp software { help | ? } [OPTION]

        ?, help  -  This help message.

        OPTIONS [list]:
        install   -  Install required software on IXP Servers.
        configure -  Configure required software on IXP Servers.
        ''')
        _ixp_debug('y', '\'ixp software help\' called.')
        sys.exit(err_code)

    ## End _ixp_software_help

    #============================================#
    # //      _ixp_software_install_help      // #
    #============================================#

    def _ixp_software_install_help(self, err_code = 0):
        ''' Method to provide ixp software install help '''

        e = _ixp_schema_servers()

        print (f'''Usage: ixp software install {{ help | ? }} [OBJECT(s)]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all   -  Install software on all IXP Servers.
        {e[0]}   -  Install software on Name Server.
        {e[1]}   -  Install software on Route Collector.
        {e[2]}   -  Install software on Route Server.
        {e[3]}   -  Install software on AS 112 Server.
        ''', end = '')

        if (_switching_type() == 'software-defined'):
            print (f'{e[4]}   -  Install software on SDN Controller Server.\n')      
        else:
            print ('')

        _ixp_debug('y', '\'ixp software install help\' called.')
        sys.exit(err_code)

    ## End _ixp_software_install_help

    #============================================#
    # //     _ixp_software_configure_help     // #
    #============================================#

    def _ixp_software_configure_help(self, err_code = 0):
        ''' Method to provide ixp software configure help '''

        e = _ixp_schema_servers()

        print (f'''Usage: ixp software config(ure) {{ help | ? }} [OBJECT(s)]

        ?, help  -  This help message.

        OBJECT(s) [list]:
        all  -  Configure software on all IXP Servers.
        {e[0]}  -  Configure software on the Name Server.
        {e[1]}  -  Configure software on the Route Server.
        {e[2]}  -  Configure software on the Route Collector.
        {e[3]}  -  Configure software on the AS 112 Server.
        ''', end = '')

        if (_switching_type() == 'software-defined'):
            print (f'{e[4]}  -  Configure software on the SDN Controller Server.\n')      
        else:
            print ('')

        _ixp_debug('y', '\'ixp software configure help\' called.')
        sys.exit(err_code)

    ## End _ixp_software_configure_help

    #============================================#
    # //             _ixp_sdn_help            // #
    #============================================#

    def _ixp_sdn_help(self, err_code = 0):
        ''' Method to provide ixp sdn help '''

        print ('''Usage: ixp sdn { help | ? } [OPTIONS]

        ?, help      - This help message.

        OPTIONS [list]:
        list          - Get information from SDN Controller.
        ''')
        _ixp_debug('y', '\'ixp sdn help\' called.')
        sys.exit(err_code)

    ## End _ixp_sdn_help 

    #============================================#
    # //          _ixp_sdn_list_help          // #
    #============================================#

    def _ixp_sdn_list_help(self, err_code = 0):
        ''' Method to provide ixp sdn switch list help '''

        print ('''Usage: ixp sdn list { help | ? } [OPTIONS]

        ?, help      - This help message.

        OPTIONS [list]:
        switch       -  Get list of switches from SDN Controller perspective.
        flows        -  Get a list of flows applied to switches. 
        ''')
        _ixp_debug('y', '\'ixp sdn list help\' called.')
        sys.exit(err_code)

    ## End _ixp_sdn_list_help 

    #============================================#
    # //      _ixp_sdn_list_switch_help       // #
    #============================================#

    def _ixp_sdn_list_switch_help(self, err_code = 0):
        ''' Method to provide ixp sdn switch list help '''

        str_ = ', '.join(str(x) for x in list(_read_db('switch').keys()))

        print (f'''Usage: ixp sdn list switch {{ help | ? }} [ARGUMENT(s)] [VALUE]

        ?, help      - This help message.

        ARGUMENT(s) [list]:
        -s, --switch    - Switch number. i.e. {str_}. 
        ''')
        _ixp_debug('y', '\'ixp sdn list switch help\' called.')
        sys.exit(err_code)

    ## End _ixp_sdn_list_switch_help 

    #============================================#
    # //       _ixp_sdn_list_flows_help       // #
    #============================================#

    def _ixp_sdn_list_flows_help(self, err_code = 0):
        ''' Method to provide ixp sdn switch flows list help '''

        str_ = ', '.join(str(x) for x in list(_read_db('switch').keys()))

        print (f'''Usage: ixp sdn list flows {{ help | ? }} [ARGUMENT(s)] [VALUE]

        ?, help      - This help message.

        ARGUMENT(s) [list]:
        -s, --switch    - Switch number. i.e. {str_}. 
        ''')
        _ixp_debug('y', '\'ixp sdn list flows help\' called.')
        sys.exit(err_code)

    ## End _ixp_sdn_list_flows_help 

    #============================================#
    # //            _ixp_peer_help            // #
    #============================================#

    def _ixp_peer_help(self, err_code = 0):
        ''' Method to provide ixp peer help '''

        print ('''Usage: ixp peer { help | ? } [OPTIONS]

        ?, help      - This help message.

        OPTIONS [list]:
        add          -  Add peers to the IXP Servers.
        delete       -  Delete peers to the IXP Servers. 
        list, show   -  List peers to the IXP Servers.
        status       -  List the BGP state of the IXP Servers.
        route        -  List routes on the IXP Servers.
        ''')
        _ixp_debug('y', '\'ixp peer help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_help 

    #============================================#
    # //          _ixp_peer_add_help          // #
    #============================================#

    def _ixp_peer_add_help(self, err_code = 0):
        ''' Method to provide ixp peer add help '''

        print (f'''Usage: ixp peer add {{ help | ? }} [ARGUMENT(s)] [VALUE]

        ?, help                  -  This help message.

        ARGUMENT(s) [list]:
        -n, --name         (M)   - Remote Autonomous System Name. 
        -a, --as-number    (M)   - Remote Autonomous System Number.
        -d, --domain       (M)   - Peering member domain name.
        -rs, --route-server      - Include Route Server for this peer.
        -bs, --blackhole-server  - Include Blackhole Server for this peer.
        -is, --interface-speed   - Add interface speed required for peer.

        (M) Mandatory
        ''')
        _ixp_debug('y', '\'ixp peer add help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_add_help

    #============================================#
    # //        _ixp_peer_delete_help         // #
    #============================================#

    def _ixp_peer_delete_help(self, err_code = 0):
        ''' Method to provide ixp peer delete help'''

        print (f'''Usage: ixp peer delete {{ help | ? }} [ARGUMENT(s)] [VALUE]

        ?, help  -  This help message.

        ARGUMENT(s) [list]:
        -n, --name       - Remote Autonomous System Name. 
        -a' --as-number  - Remote Autonomous System Number.

        ''')
        _ixp_debug('y', '\'ixp peer delete help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_delete_help

    #============================================#
    # //         _ixp_peer_list_help          // #
    #============================================#

    def _ixp_peer_list_help(self, err_code = 0):
        ''' Method to provide ixp peer list help '''

        print (f'''Usage: ixp peer list {{ help | ? }} [ARGUMENT] [VALUE]

        ?, help         -  This help message.

        ARGUMENT:
        -ip, --ip-type  - List the configured peers (i.e. ipv4, ipv6 , both).    

        ''')
        _ixp_debug('y', '\'ixp peer list help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_list_help method

    #============================================#
    # //        _ixp_peer_status_help         // #
    #============================================#

    def _ixp_peer_status_help(self, err_code = 0):
        ''' Method to provide ixp peer status help '''

        print (f'''Usage: ixp peer status {{ help | ? }} [ARGUMENT] [VALUE]

        ?, help         -  This help message.

        ARGUMENT:
        -ip, --ip-type  - Show the BGP status of configured peers (i.e. ipv4, ipv6 , both).    

        ''')
        _ixp_debug('y', '\'ixp peer status help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_status_help method

    #============================================#
    # //         _ixp_peer_route_help         // #
    #============================================#

    def _ixp_peer_route_help(self, err_code = 0):
        ''' Method to provide ixp peer route help '''

        print (f'''Usage: ixp peer route {{ help | ? }} [ARGUMENT] [VALUE]

        ?, help         -  This help message.

        ARGUMENT:
        -ip, --ip-type  - Returns the route tables of the BIRD daemon services (i.e. ipv4, ipv6 , both).    

        ''')
        _ixp_debug('y', '\'ixp peer status help\' called.')
        sys.exit(err_code)

    ## End _ixp_peer_route_help method

    #============================================#
    # //           _ixp_remote_help           // #
    #============================================#

    def _ixp_remote_help(self, err_code = 0):
        ''' Method to provide ixp remote help '''

        print ('''Usage: ixp remote { help | ? } [OPTIONS]

        ?, help        - This help message.

        OPTIONS [list]:
        key            - Generate and Enter RSA keys.
        ''', end = '')

        if (_site_type() == 'core'):
            print (f'site           - Manage mIXPs in cIXP database.\n'
                  f"{' ' * 8}command, cmd   - Execute a command on an mIXP.\n") 
        else:
            print ('')
        _ixp_debug('y', '\'ixp remote help\' called.')
        sys.exit(err_code)

    ## End _ixp_remote_help method

    #============================================#
    # //        _ixp_remote_keys_help         // #
    #============================================#

    def _ixp_remote_keys_help(self, err_code = 0):
        ''' Method to provide ixp remote help '''

        print ('''Usage: ixp remote keys { help | ? } [OPTIONS]

        ?, help      - This help message.

        OPTIONS:
        ''', end = '')
               
        if (_site_type() == 'mini'):
            print (f'enter        - On a mIXP add a public RSA key generated by a cIXP.\n')   

        if (_site_type() == 'core'):
            print (f"generate     - On a cIXP generate a new RSA key pair.\n"
                  f"{' ' * 8}show         - Show the public RSA key.\n")   

        _ixp_debug('y', '\'ixp remote keys help\' called.')
        sys.exit(err_code)

    ## End _ixp_remote_keys_help method

    #============================================#
    # //        _ixp_remote_site_help         // #
    #============================================#

    def _ixp_remote_site_help(self, err_code = 0):
        ''' Method to provide ixp remote site help '''

        print ('''Usage: ixp remote site { help | ? } [OPTIONS]

        ?, help      - This help message.

        ARGUMENT:
        add          - Add a new remote site to the database
        delete       - Delete a site from the database
        show         - Show the remote sites in the database

        ''')        
        _ixp_debug('y', '\'ixp remote site help\' called.')
        sys.exit(err_code)

    ## End _ixp_remote_site_help method

    #============================================#
    # //      _ixp_remote_site_add_help       // #
    #============================================#

    def _ixp_remote_site_add_help(self, err_code = 0):
        ''' Method to provide ixp remote site add help '''

        print ('''Usage: ixp remote site add { help | ? } [ARGUMENTs]

        ?, help      - This help message.

        ARGUMENTs:
        -sn, --site-number (M)   - Add site number (i.e. 2).
        -se, --site-name         - Add site name (i.e. Gulu). 
        -l, --location           - Add site location (i.e. Northern Uganda).  
        -h4, --host-v4 (M)       - Add site IPv4 address (i.e. 176.6.6.230).
        -h6, --host-v6 (M)       - Add site IPv4 address (i.e. 2a76:6:6::230).

        (M) Mandatory.   Note: at least one of the IP addresses must be specified.

        ''')        
        _ixp_debug('y', '\'ixp remote site add help\' called.')
        sys.exit(err_code)

    ## End _ixp_remote_site_add_help method

    #============================================#
    # //    _ixp_remote_site_delete_help      // #
    #============================================#

    def _ixp_remote_site_delete_help(self, err_code = 0):
        ''' Method to provide ixp remote site delete help '''

        print ('''Usage: ixp remote site delete { help | ? } [ARGUMENT]

        ?, help      - This help message.

        ARGUMENT:
        -sn, --site-number (M)   - Add site number (i.e. 2).
        ''')        
        _ixp_debug('y', '\'ixp remote site delete help\' called.')
        sys.exit(err_code)

    ## End _ixp_remote_site_delete_help method

    #============================================#
    # //       _ixp_remote_command_help       // #
    #============================================#

    def _ixp_remote_command_help(self, err_code = 0):
        ''' Method to provide ixp remote command help '''

        print ('''Usage: ixp remote command <SITE #> { help | ? } -- [COMMAND]

        ?, help      - This help message.

        <SITE #>     - Site number of remote site.

        COMMAND:
        ixp schema show
        ixp host show
        ixp switch show
        ixp server show
        ixp sdn show switches
        ixp sdn show flows
        ixp peer add, delete, list, status, route
        ''')        
        _ixp_debug('y', '\'ixp remote site delete help\' called.')
        sys.exit(err_code)

    ## End _ixp_remote_site_delete_help method

############ // End IxpHelp class // ############

#################################################
# //              _IxpLxc class              // #
#################################################

class _IxpLxc:
    ''' IXP LXC class '''

    def __init__(self):
        ''' IxpLxc class constructor method '''

        self.functionality = 'Private class to manage interaction with LXC'

    ## End of __init__ method

    def __str__(self):
        ''' _IxpLxc class __str__ method '''        

        return (self.functionality)

    ## End of __str__ method
 
    def __repr__(self):
        ''' _IxpLxc class __repr__ method ''' 

        return (f'{self.__class__.__name__} class: {self.functionality}') 

    ## End of __repr__ method

    #============================================#
    # //              _lxc_exec               // #
    #============================================#

    def _lxc_exec(self, c, command):
        ''' Method to access lxc exec '''
    
        cmd = f'lxc exec {c} -- {command}'
        output = subprocess.getstatusoutput (cmd)

        _ixp_debug('y', f'INFO[_lxc_exec]: Executing: {cmd}')

        if (output[0] == 0):        
            _ixp_debug('y', f'INFO[_lxc_exec]: \"{cmd}\" executed successfully')
            return (output)  

        elif (output[0] == 255):
            print (f'Bad command: \'{cmd}\'')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Bad command: \'{cmd}\'')
            matched = output[1].split('\n')
            print (f'ERROR[_lxc_exec]: Bad command lxc_exec error info: {matched[0]}\n')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Bad command lxc_exec error info: {matched[0]}') 
            sys.exit(1)

        elif (output[0] == 1):
            _ixp_debug('y', f'ERROR[_lxc_exec]: Container error, cannot connect to: \'{c}\'')
            matched = output[1].split('\n')
            print (f'ERROR[_lxc_exec]: Container error lxc_exec error info: {matched[0]}\n')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Container error lxc_exec error info: {matched[0]}') 
            sys.exit(1)

        elif (output[0] in range(2,254)):
            print (f'ERRORUnknown lxc_exec error: {output[0]}')
            _ixp_debug('y', f'Unknown lxc_exec error: {output[0]}')
            matched = output[1].split('\n')
            print (f'ERROR[_lxc_exec]: Unknown lxc_exec error info: {matched[1]}\n')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Unknown lxc_exec error info: {matched[1]}')            
            sys.exit(1)

        elif (not output[0] == 0):
            print (f'ERROR: Catch-all for unknown lxc_exec error: {output}')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Catch-all for unknown lxc_exec error: {output}')
            matched = output[1].split('\n')
            print (f'ERROR[_lxc_exec]: Unknown lxc_exec error info: {matched[1]}\n')
            _ixp_debug('y', f'ERROR[_lxc_exec]: Unknown lxc_exec error info: {matched[1]}')     
            sys.exit(1)

    ## End _lxc_exec method

    #============================================#
    # //              _lxc_start              // #
    #============================================#

    def _lxc_start(self, c):
        ''' Method to start an LXC container '''

        subprocess.getstatusoutput (f'lxc start {c}')
        _IxpLxc._lxc_container_boot_delay(self)

        ixp_lxc_status, ixp_lxc_state = _IxpLxc._lxc_status(self, c)

        _ixp_debug('y', f'INFO[_lxc_start]: {c} container started')

        return (ixp_lxc_status)

    ## End _lxc_start method 

    #============================================#
    # //             _lxc_restart             // #
    #============================================#

    def _lxc_restart(self, c):
        ''' Method to restart an LXC container '''

        subprocess.getstatusoutput (f'lxc restart {c}')
        _IxpLxc._lxc_container_boot_delay(self)

        ixp_lxc_status, ixp_lxc_state = _IxpLxc._lxc_status(self, c)

        _ixp_debug('y', f'INFO[_lxc_restart]: {c} container re-started')

        return (ixp_lxc_status)

    ## End _lxc_restart method 

    #============================================#
    # //              _lxc_stop               // #
    #============================================#

    def _lxc_stop(self, c):
        ''' Method to stop an LXC container '''

        subprocess.getstatusoutput (f'lxc stop {c} --force')
        _IxpLxc._lxc_container_boot_delay(self)

        ixp_lxc_status, ixp_lxc_state = _IxpLxc._lxc_status(self, c)

        _ixp_debug('y', f'INFO[_lxc_stop]: {c} container stopped')

        return (ixp_lxc_status)

    ## End _lxc_stop method

    #============================================#
    # //             _lxc_status              // #
    #============================================#

    def _lxc_status(self, c):
        ''' Method to get the status of an LXC container'''

        lxd_status_check = subprocess.getstatusoutput (f'lxc info {c}')

        if (lxd_status_check[0] == 0):
            server_status = re.findall('\nStatus: (.*)\n', lxd_status_check[1])
            _ixp_debug('y', f'INFO[_lxc_status]: {c} container exists in a \'{server_status[0]}\' state')
            return (lxd_status_check[0], server_status[0])

        else: 
            _ixp_debug('y', f'INFO[_lxc_status]: {c} container does not exist')
            return (lxd_status_check[0], 'Nil')

        return (0)

    ## End _lxc_status method

    #============================================#
    # //            _lxc_status_up            // #
    #============================================#

    def _lxc_status_up(self, c): 
        ''' Method to get the status of an LXC container, if 'Stopped' bring up '''

        ixp_lxc_start_status = ''
        ixp_lxc_status = ''

        lxd_status_check = subprocess.getstatusoutput (f'lxc info {c}')

        if (lxd_status_check[0] == 0):
            ixp_lxc_start_status = re.findall('\nStatus: (.*)\n', lxd_status_check[1])[0]
            ixp_lxc_status = ixp_lxc_start_status

            if not (ixp_lxc_status == 'Running'):
                _ixp_debug('y', f'INFO[_lxc_status_up]: {c} container exists in a \'{ixp_lxc_status}\' state, starting {c}')
                _IxpLxc._lxc_start(self, c)
                _IxpLxc._lxc_container_boot_delay(self)

                ixp_lxc_status = 'Running'
              
            return (ixp_lxc_start_status, ixp_lxc_status)

        else: 
            print (f'ERROR: {c} container does not exist, stopping\n')
            _ixp_debug('y', f'ERROR[_lxc_status_up]: {c} container does not exist, stopping')
            sys.exit(1)

            return (1,1)

    ## End _lxc_status_up method 

    #============================================#
    # //             _lxc_delete              // #
    #============================================#

    def _lxc_delete(self, skip_yes_no, c):
        ''' Method to delete an LXC container '''

        files_list = listdir(var_dir)

        # // Delete the file //
        if not (_IxpLxc._lxc_status(self, c)[1] == 'Nil'):

            if (skip_yes_no == False):
                cli_response = input(f'Deleting existing \'{c}\' container, are you sure ? (y|n) > ')
                cli_response = cli_response.lower()
                print ('')
            else:
                cli_response = 'yes'

            # // Handle abbreviations //
            for x in ['yes', 'no']:
                if (cli_response == x[:len(cli_response)]): 
                    cli_response = x

            if (cli_response == 'no'):
                print ('OK stopping lxc delete process\n')
                _ixp_debug('y','INFO[_lxc_delete]: OK stopping lxc delete process')
                sys.exit(0)

            elif (cli_response == 'yes'):
                _ixp_debug('y', f'INFO[_lxc_delete]: Stopping the {c} container before deleting')
                lxd_delete_check = subprocess.getstatusoutput (f'lxc delete {c} --force')

                # // Remove files associated with the container from /var/ixp //
                for f in files_list:
                    if (c in f):
                        remove(f'{var_dir}{f}')
                        _ixp_debug('y', f'INFO[_lxc_delete]: Deleting the file {var_dir}{f}')

                print (f'{c} container deleted\n')
                _ixp_debug('y', f'INFO[_lxc_delete]: {c} container deleted')

                return (lxd_delete_check[0])

            else:
                print ('ERROR[_lxc_delete]: Not an acceptable input, exiting\n')
                _ixp_debug('y','ERROR[_lxc_delete]: Not an acceptable input, exiting')
                sys.exit(1)

        else:
            return (0)
    
    # End _lxc_delete method

    #============================================#
    # //              _lxc_upgrade            // #
    #============================================#
   
    def _lxc_upgrade(self, c):
        ''' Method to upgrade the software on the servers '''

        orig_yaml = f'{var_dir}ixp_{c}.yaml'
        temp_yaml = f'/tmp/ixp_{c}.yaml.tmp'
        global server_upgrade_flag

        if (server_upgrade_flag == 0):
            # // Check server is running and identify original state //
            (start, current) = _IxpLxc._lxc_status_up(self, c)
 
            # // Ensure the 'upstream_ns' is being used to prevent timeouts //
            _IxpLxc._lxc_pull(self, c, netplan_file, temp_yaml)
            _IxpLxc._lxc_push(self, c, orig_yaml, netplan_file)
            _IxpLxc._lxc_exec(self, c, 'netplan apply') 
            
            # // Update software repositories and distribution //            
            _ixp_debug('y', f'INFO[_lxc_upgrade]: Updating and upgrading the container {c} from repositories')
            if (dev_test == False):
                _IxpLxc._lxc_exec(self, c, 'apt-get -y update')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y upgrade')

            # // Restore pre update_upgrade netplan //
            _IxpLxc._lxc_push(self, c, temp_yaml, netplan_file)
            _IxpLxc._lxc_exec(self, c, 'netplan apply')
            subprocess.getstatusoutput (f'rm {temp_yaml}')

            # // Return server to original state //
            if (start == 'Running'):
                _ixp_debug('y', f'INFO[_lxc_upgrade]: Returning container {c} to original \'Running\' state')
            elif (start == 'Stopped'):
                _IxpLxc._lxc_stop(self, c)
                _ixp_debug('y', f'INFO[_lxc_upgrade]: Returning container {c} to original \'Stopped\' state')

        else:
            _ixp_debug('y', f'INFO[_lxc_upgrade]: No need to upgrade {c} it was already upgraded')  

        # // Increment server upgrade flag //        
        server_upgrade_flag += 1

        return (0)
    
    ## End of _lxc_upgrade method

    #============================================#
    # //            _lxc_file_test            // #
    #============================================#

    def _lxc_file_test(self, c, remote_file):
        ''' Method to check if lxc file exists '''

        file_test = _IxpLxc._lxc_exec(self, c, f'[ -f \'{remote_file}\' ]; echo $?')

        if (file_test[1] == '0'):
            _ixp_debug('y', f'INFO[_lxc_file_test]: The {c}: {remote_file} file exists')

            return ('yes')

        else:
            _ixp_debug('y', f'INFO[_lxc_file_test]: There is no existing {remote_file} file on {c}')

            return ('no')

    ## End _lxc_file_test method 

    #============================================#
    # //                _lxc_rm               // #
    #============================================#

    def _lxc_rm(self, c, remote_file):
        ''' Method to remove files from container '''

        # // Delete file from container //

        file_test = _IxpLxc._lxc_file_test(self, c, remote_file)

        if (file_test == 'yes'):
            _ixp_debug('y', f'INFO[_lxc_rm]: Removing the {c}: {remote_file}')
            _IxpLxc._lxc_exec(self, c, f'rm {remote_file}')
        else:   
            _ixp_debug('y', f'INFO[_lxc_rm]: {c}: {remote_file} doesn\'t exist')

        return (0)

    ## End _lxc_rm method 

    #============================================#
    # //               _lxc_clone             // #
    #============================================#

    def _lxc_clone(self, skip_yes_no, template_c, new_c):
        ''' Method to copy the container template to the required container '''

        _ixp_debug('y', f'INFO[_lxc_clone]: Cloning the {new_c} container from {template_c}')
        _IxpLxc._lxc_delete(self, skip_yes_no, new_c)
        print (f'Please be patient as the {new_c} container is cloned from {template_c}\n')
        lxd_clone_check = subprocess.getstatusoutput (f'lxc copy {template_c} {new_c}')
        print (f'{new_c} container cloned from {template_c}\n')
        _ixp_debug('y', f'INFO[_lxc_clone]: {new_c} container cloned from {template_c}')

        return (lxd_clone_check[0])

    ## End _lxc_clone method 

    #============================================#
    # //               _lxc_pull              // #
    #============================================#

    def _lxc_pull(self, c, remote_file, local_file):
        ''' Method to pull files from container '''

        # // Delete original file if it exists //        
        if (path.isfile(local_file)): 
            _ixp_debug('y', f'Replacing: {local_file}')
            subprocess.getstatusoutput (f'rm {local_file}')

        # // pull file from container to localhost //
        (m, n, o, p) = (c, remote_file, local_host, local_file)
        _ixp_debug('y', f'INFO[_lxc_pull]: lxc file pull\n  <-- {m}:{n}\n  --> {o}:{p}')

        output = subprocess.getstatusoutput (f'lxc file pull {c}{n} {p}')
        _ixp_debug('y', f'INFO[_lxc_pull]: LXC pull status: {output[0]}')

        return (output[0])

    ## End _lxc_pull method 

    #============================================#
    # //               _lxc_push              // #
    #============================================#

    def _lxc_push(self, c, local_file, remote_file):
        ''' Method to pull files from container '''

        # // Delete original file from container first //
        _IxpLxc._lxc_rm(self, c, remote_file)

        # // Push new file to the container //
        (m, n, o, p) = (local_host, local_file, c, remote_file)
        _ixp_debug('y', f'INFO[_lxc_push]: lxc file push\n  <-- {m}:{n}\n  --> {o}:{p}')

        output = subprocess.getstatusoutput (f'lxc file push {n} {c}{p}')
        _ixp_debug('y', f'INFO[_lxc_push]: LXC push status: {output[1]}')

        return (output[0])

    ## End _lxc_push method 

    #============================================#
    # //      _lxc_container_boot_delay       // #
    #============================================#

    def _lxc_container_boot_delay(self):
        ''' Method to allow the container enough time to boot '''
        
        counter = 0

        for n in range(1,4):
            time.sleep(5)
            counter += 1

        return (0)

    ## End _lxc_container_boot_delay method

    #============================================#
    # //      _lxc_hypervisor_containers      // #
    #============================================#

    def _lxc_hypervisor_containers(self):
        ''' Method to get list of containers on hypervisor '''

        lxc_list = list()

        a = subprocess.getstatusoutput ('lxc list -c ns --format csv')[1].split('\n')
        for x in a:
            b = x.split(',')
            lxc_list.append(b[0])

        return (lxc_list)

    ## End _lxc_hypervisor_containers method

    #============================================#
    # //          _lxc_internet_test          // #
    #============================================#

    def _lxc_internet_test(self,c):
        ''' Method to check Internet connectivity '''

        test_precentage = 0

        try:
            dns_test = _IxpLxc._lxc_exec(self, c, f'ping -c1 {upstream_ns[0]}')
            if (dns_test[0] == 0):
                dns_test = True
        except: 
            print (f'The container {c} does not have Internet connectivity\n')
            sys.exit(1)  

        # // Test some URLs //
        for x in test_websites:
            try:
                url_test = urllib.request.urlopen(f"{x}").getcode()

                if (url_test == 200):
                    test_precentage += int(100/len(test_websites))

            except urllib.error.HTTPError as e:
                print (e.code)
                print (e.read())


        return (test_precentage)                      

    ## End _lxc_internet_test method

    #============================================#
    #  //            _bird_restart            // #
    #============================================#

    def _bird_restart(self, c):
        ''' Method to restart the bird server '''

        _IxpLxc._lxc_exec(self, c, 'systemctl restart bird.service')
        sys_ctl_bird = _IxpLxc._lxc_exec(self, c, 'systemctl --no-pager status bird.service')

        if (sys_ctl_bird[0] == 0):
            _ixp_debug('y', f'INFO[_bird_restart]: Restarting the bird daemon on {c}')
            _ixp_debug('y', f'{sys_ctl_bird[1]}')

        _IxpLxc._lxc_exec(self, c, 'systemctl restart bird6')
        sys_ctl_bird6 = _IxpLxc._lxc_exec(self, c, 'systemctl --no-pager status bird6.service')

        if (sys_ctl_bird6[0] == 0):
            _ixp_debug('y', f'INFO[_bird_restart]: Restarting the bird6 daemon on {c}')
            _ixp_debug('y', f'{sys_ctl_bird6[1]}')

        _ixp_debug('y', f'INFO[_bird_restart]: Restarting the bird daemons on {c}')

        return (0)

    ## End of _bird_restart method

    #============================================#
    #  //            _bind9_restart           // #
    #============================================#

    def _bind9_restart(self, c):
        ''' Method to restart the bind9 server '''

        _IxpLxc._lxc_exec(self, c, 'systemctl restart bind9')
        sys_ctl_bind9 = _IxpLxc._lxc_exec(self, c, 'systemctl --no-pager status bind9')

        if (sys_ctl_bind9[0] == 0):
            _ixp_debug('y', f'INFO[_bind9_restart]: Restarting the bind9 daemon on {c}')
            _ixp_debug('y', f'{sys_ctl_bind9[1]}')

        _ixp_debug('y', f'INFO[_bind9_restart]: Restarting the bind9 daemon on {c}')

        return (0)

    ## End of _bind9_restart method

############ // End _IxpLxc class // ############

#################################################
# //             IxpSchema class             // #
#################################################

class IxpSchema:
    ''' IXP Schema Class '''

    def __init__(self):
        ''' IxpSchema class constructor method '''

        self.functionality = 'Build the IXP Schema, the foundation of the IXP'

        self.ixp_schema_temp = {'ipv4_peer':'199.9.9.0/24', 
                                'ipv6_peer':'2a99:9:9::/48',
                                'ipv4_man':'198.8.8.0/24', 
                                'ipv6_man':'2a98:8:8::/48', 
                                'domain':'netlabs.tst', 
                                'as_number':'5999', 
                                'enabled':1, 
                                'site_number':'1',
                                'site_type':'core',
                                'switching_type':'traditional',                          
                                'organisation':'netLabs!UG',
                                'country':'Uganda', 
                                'town':'Kampala', 
                                'elevation':1027, 
                                'latitude':'00 20 51.3456 N', 
                                'longitude':'32 34 57.0720 E'}

        self.build_dict = { '-sn':'--site-number',
                            '-st':'--site-type',
                            '-sw':'--switching-type',
                            '-o':'--organisation',
                            '-p4':'--ipv4-peer',
                            '-p6':'--ipv6-peer',
                            '-m4':'--ipv4-man',
                            '-m6':'--ipv6-man',
                            '-d' :'--domain',
                            '-as':'--as-number',
                            '-c':'--country',
                            '-t':'--town',
                            '-e':'--elevation',
                            '-la':'--latitude',
                            '-lo':'--longitude'}

        conn = sqlite3.connect(ixp_db)
        cur = conn.cursor()
        try: 
            cur.execute('SELECT * from schema')
            conn.close()

        except:
            print (f'\nERROR: No default database, creating one\n'
                   f'       This creates an initial default database\n'
                   f'       Re-run this command if you want to make\n'
                   f'       a schema that is not the default\n')
            _ixp_debug('y', 'ERROR[IxpSchema.__init__]: No default database, creating one')
            self._ixp_schema_build({'cmd':'build','ins':'both', 'elm':self.ixp_schema_temp}, 'quiet')
            sys.exit(0)

        ## End of __init__ method

    def __str__(self):
        ''' IxpSchema class __str__ method ''' 

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' IxpSchema class __repr__ method ''' 

        return (f'{self.__class__.__name__} class: {self.functionality}') 

        ## End of __repr__ method

    #============================================#
    # //       _ixp_schema_help_handler       // #
    #============================================#

    # // Help handler function //
    def _ixp_schema_help_handler(self, command, err_code):
        ''' Schema help handler '''

        if (command == 'help'):
            IxpHelp._ixp_schema_help(self, err_code)
        elif (command == 'show'):
            IxpHelp._ixp_schema_show_help(self, err_code)
        elif (command == 'default'):
            IxpHelp._ixp_schema_default_help(self, err_code)
        elif (command == 'build'):
            IxpHelp._ixp_schema_build_help(self, err_code)
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        ## End to _ixp_schema_help_handler function

    #============================================#
    # //          _ixp_schema_build           // #
    #============================================#

    def _ixp_schema_build(self, kargs, mode='noisy'):   
        ''' Method to build the IXP Schema and IXP Site info '''

        dict_ = dict()

        # // Extract dictionary from list //
        ixp_schema = kargs['elm']
        as_number = f"{ixp_schema['as_number']}"
        domain = f"{ixp_schema['domain']}"

        # // Temporary schema dictionary //
        schema_temp_dict = {'lxc':'TEXT PRIMARY KEY',
                            'ipv4_peer':'TEXT',
                            'ipv6_peer':'TEXT',
                            'ipv4_man':'TEXT',
                            'ipv6_man':'TEXT',
                            'domain':'TEXT',
                            'as_number':'INTEGER',
                            'enabled':'INTEGER'
                           }

        site_temp_dict = {'site_number':'INTEGER',
                          'site_type':'TEXT', 
                          'switching_type':'TEXT',
                          'country':'TEXT',
                          'town':'TEXT',
                          'elevation':'TEXT',
                          'organisation':'TEXT', 
                          'latitude':'TEXT',
                          'longitude':'TEXT'
                         }

        ip_temp_dict = {'addr':'TEXT PRIMARY KEY',
                        'switch_number': 'INTEGER',
                        'port_number':'INTEGER', 
                        'name':'TEXT', 
                        'assigned':'TEXT', 
                        'as_number':'TEXT', 
                        'domain':'TEXT',
                        'interface_speed':'TEXT',
                        'route_server': 'TEXT',
                        'blackhole_server': 'TEXT',
                        'function':'TEXT',
                        'enabled':'INTEGER',
                        'reserved':'INTEGER' 
                         }

        sw_temp_dict = {'switch_number': 'INTEGER PRIMARY KEY',
                        'switch_name': 'TEXT',
                        'switch_type': 'TEXT',         
                        'manufacturer': 'TEXT',              
                        'control_interface': 'TEXT',
                        'data_interface': 'TEXT',
                        'switch_interfaces': 'INTEGER',
                        'enabled_interfaces': 'TEXT',
                        'ipv4_man': 'TEXT',
                        'ipv6_man': 'TEXT'
                       }

        temp_dict = dict()
        schema_pass_dict = dict()    
        site_pass_dict = dict()          
        site_list = list()

        # // Call help //
        if (kargs['ins'] == 'help'):
            self._ixp_schema_help_handler('build', 1)

        # // Check if this is really required //
        if (mode == 'noisy'):
            # // Check the installer really wants to do this //
            print (f"\n  {'-' * 55}")
            print ('  WARNING: This command erases the database configuration')
            print (f"  {'-' * 55}")
            schema_destroy = input('\n  Are you certain you want to proceed with this step? (y/n): ')

            if (schema_destroy.lower() == 'yes'[:len(schema_destroy)]):
                print ('  Building IXP schema ...\n')
            else:
                print ('  Exiting ...\n')
                sys.exit(1)            

        # // Drop and rebuild database tables //
        _ixp_database_rebuild('schema', schema_temp_dict)
        _ixp_database_rebuild('site', site_temp_dict)
        _ixp_database_rebuild('ipv4_peer', ip_temp_dict)
        _ixp_database_rebuild('ipv6_peer', ip_temp_dict)
        _ixp_database_rebuild('ipv4_man', ip_temp_dict)   
        _ixp_database_rebuild('ipv6_man', ip_temp_dict)
        _ixp_database_rebuild('switch', sw_temp_dict)
        _ixp_debug('y','INFO[_ixp_schema_buil]: Created the IXP database tables')

        # // Build the IP and IPv6 tables //
        ipv4_peer_net = (_validate_ip_net(ixp_schema['ipv4_peer'])['ip'])
        ipv6_peer_net = (_validate_ip_net(ixp_schema['ipv6_peer'])['ip'])
        ipv4_peer_net_prefix = (_validate_ip_net(ixp_schema['ipv4_peer'])['prefix'])
        ipv6_peer_net_prefix = (_validate_ip_net(ixp_schema['ipv6_peer'])['prefix'])

        ipv4_man_net = (_validate_ip_net(ixp_schema['ipv4_man'])['ip'])
        ipv6_man_net = (_validate_ip_net(ixp_schema['ipv6_man'])['ip'])
        ipv4_man_net_prefix = (_validate_ip_net(ixp_schema['ipv4_man'])['prefix'])
        ipv6_man_net_prefix = (_validate_ip_net(ixp_schema['ipv6_man'])['prefix'])

        _ixp_debug('y', f"INFO[_ixp_schema_build]: Adding IPv4 table information to the database") 
        _ixp_debug('y', f"INFO[_ixp_schema_build]: Adding IPv6 table information to the database") 

        # // Loop through the available peer addresses from schema //
        for a in range(1, 255):
            resv = 0 if a < 236 else 1
            tmp4 = f'{str(ipaddress.ip_address(ipv4_peer_net) + a)}/{ipv4_peer_net_prefix}'
            tmp6 = f'{str(ipaddress.ip_address(ipv6_peer_net) + int(str(a), 16))}/{ipv6_peer_net_prefix}'

            # // Send to database //
            _ixp_database_insert('ipv4_peer', {'addr': f"'{tmp4}'", 'function':"'peer'", 'reserved':f'{resv}'})         
            _ixp_database_insert('ipv6_peer', {'addr': f"'{tmp6}'", 'function':"'peer'", 'reserved':f'{resv}'})  

        # // Loop through the available management addresses from schema //
        for a in range(1, 255):
            resv = 0 if a < 236 else 1
            tmp4 = f'{str(ipaddress.ip_address(ipv4_man_net) + a)}/{ipv4_man_net_prefix}'
            tmp6 = f'{str(ipaddress.ip_address(ipv6_man_net) + int(str(a), 16))}/{ipv6_man_net_prefix}'

            # // Send to database //
            _ixp_database_insert('ipv4_man', {'addr': f"'{tmp4}'", 'function':"'core'", 'reserved':f'{resv}'})         
            _ixp_database_insert('ipv6_man', {'addr': f"'{tmp6}'", 'function':"'core'", 'reserved':f'{resv}'})  

        # // Configure the IXP Schema IP Information     
        if (kargs['ins'] == 'both' or kargs['ins'] == 'schema'):

            temp_ip_list = list()

            # // Update the IXP Schema in the database //
 
            # // Loop through the servers //
            for k, v in ixp_servers.items():

                container = f"{k}{ixp_schema['site_number']}"                

                # // Loop through ixp schema //
                for x in ixp_schema_keys:      

                    ixp_dict = dict()                                 
                    (ik, iv) = (x, ixp_schema[x])

                    # // Add IPv4 addresses to the IXP Schema //
                    if ('4' in ik):
                        ixp_dict = _validate_ip_net(iv)    

                        # // Replace '202' for '1' for the peering LAN //
                        if (k == 'rs' and 'peer' in ik):
                            vd = 1
                        else:
                            vd = v

                        ip = f"{str(ipaddress.ip_address(ixp_dict['ip']) + int(v))}/{ixp_dict['prefix']}" 
                        temp_ip_list.append((f'{ik}',f"'{ip}'", '', '', '', 'core', 0))

                        ip = f"{str(ipaddress.ip_address(ixp_dict['ip']) + int(vd))}/{ixp_dict['prefix']}"                         
                        temp_dict[ik] = f"'{ip}'"                   
                        temp_ip_list.append((f'{ik}',f"'{ip}'", f'{container}', as_number, domain, 'core', 1))

                    # // Add IPv6 addresses to the IXP Schema //
                    elif ('6' in ik):                    
                        ixp_dict = _validate_ip_net(iv)

                        # // Replace '202' for '1' for the peering LAN //
                        if (k == 'rs' and 'peer' in ik):
                            vd = 1
                        else:
                            vd = v  

                        ip = f"{str(ipaddress.ip_address(ixp_dict['ip']))}{v}/{ixp_dict['prefix']}"
                        temp_ip_list.append((f'{ik}',f"'{ip}'", '', '', '', 'core', 0))

                        ip = f"{str(ipaddress.ip_address(ixp_dict['ip']))}{vd}/{ixp_dict['prefix']}"
                        temp_dict[ik] = f"'{ip}'"         
                        temp_ip_list.append((f'{ik}',f"'{ip}'", f'{container}', as_number, domain, 'core', 1))

                    # // Add domain to the IXP Schema //
                    elif (ik == 'domain'):
                        temp_dict[ik] = f"'{iv}'" 

                    # // Add as-number to the IXP Schema //
                    elif (ik == 'as_number'):
                        temp_dict[ik] = f"{iv}" 

                    # // Add enabled to the IXP Schema //
                    elif (ik == 'enabled'):
                        if (ixp_schema['switching_type'] == 'traditional'):
                            if (k == 'sc'):
                                temp_dict[ik] = 0 
                            else:
                                temp_dict[ik] = 1
                        else:
                            temp_dict[ik] = 1                                  

                # // Add IXP schema data to the site table //
                values_list = [f"'{container}'"]
                values_list.extend(list(temp_dict.values()))

                for c, k in enumerate(schema_temp_dict.keys()):
                    schema_pass_dict[k] = values_list[c]

                # // Send to database //
                _ixp_database_insert('schema', schema_pass_dict) 

            # // Add Switch no., ASN and Domain to the IPv4 and IPv6 containers //
            for (a, b, c, d, e, f, g) in temp_ip_list:
                dict_ = {'switch_number':100,
                           'name': f'{c}',  
                           'assigned': f'{c}',    
                           'as_number': f'{d}', 
                           'domain': f'{e}', 
                           'function': f'{f}',
                           'enabled': f'{g}',
                           'reserved':1
                       }

                if ('ipv4_peer' in a):
                    _ixp_database_update('ipv4_peer', b, dict_)
                elif ('ipv6_peer' in a):
                    _ixp_database_update('ipv6_peer', b, dict_)
                elif ('ipv4_man' in a):
                    _ixp_database_update('ipv4_man', b, dict_)
                elif ('ipv6_man' in a):
                    _ixp_database_update('ipv6_man', b, dict_)   
                else:
                    print ('ERROR: Not a valid IP type in the schema\n')
                    _ixp_debug('y','ERROR[_ixp_schema_build]: Not a valid IP type in the schema')

            # // Disable 'sc' if traditional switching //
            if (ixp_schema['switching_type'] == 'traditional'):
                for x in ['ipv4_peer','ipv6_peer', 'ipv4_man','ipv6_man']:
                    _ixp_database_update(x, '', {'enabled': 0}, 'assigned', "'sc_'")

            if (mode == 'noisy'):
                print ("IXP Schema inserted in the schema, IPv4 and IPv6 database tables\n")         
            _ixp_debug('y', "INFO[_ixp_schema_build]: IXP Schema inserted in the schema, IPv4 and IPv6 database tables")

        # // Configure the IXP Schema Site Information
        if (kargs['ins'] == 'both' or kargs['ins'] == 'site'):

            # // Update the IXP Site information in the database //
            for k in ixp_site_info_keys:   
                if (k == 'site_number'): 
                    site_list.append(ixp_schema[k])
                else:
                    site_list.append(f"'{ixp_schema[k]}'")

            for c, k in enumerate(site_temp_dict.keys()):
                site_pass_dict[k] = site_list[c]

            # // Send to database //
            _ixp_database_insert('site', site_pass_dict)   

            if (mode == 'noisy'):
                print ("IXP Site information inserted in the site database table\n")     
            _ixp_debug('y', "INFO[_ixp_schema_build]: IXP Site information inserted in the site database table")

        # // Return //
        return (0)    

        ## End _ixp_schema_build method

    #============================================#
    # //          _ixp_schema_show            // #
    #============================================#

    def _ixp_schema_show(self, argv):
        ''' Method to display contents of the IXP Schema '''

        # // Read schema from IXP Schema database //
        dict_ = _read_db('schema')

        pad = list() 
        str_ = str()

        # // Create pads //
        for k, v in dict_[argv].items():
            x = max([len(str(k)), len(str(v))])
            pad.append(x + 2)
 
        # // Print title //
        str_ = f'{argv} schema'
        str_pad = sum(pad) + 23 - len(str_)

        print (f'{str_:^{str_pad}}', end = '')
        print ('')

        # // Print column headers //
        for x in pad:
            print (f"+{'-' * (x + 1)}", end = '')
        print ('+')

        for c, k in enumerate(dict_[argv].keys()):
            print (f'| {k:^{pad[c]}}', end = '')
        print ('|')

        # // Print header bottom line //
        print (f"+{'-' * (sum(pad) + 13)}+")

        # // Print data //
        for c, v in enumerate(dict_[argv].values()):
            print (f'| {v:^{pad[c]}}', end = '')
        print ('|')

        # // Print bottom line //
        print (f"+{'-' * (sum(pad) + 13)}+")

        print ('')

        return (0)
         
    # End _ixp_schema_show method

    #============================================#
    # //       _ixp_schema_test_options       // #
    #============================================#

    def _ixp_schema_test_options(self, args):
        ''' Method to Test 'ixp_schema' cli options '''

        list_ = list()
        dict_ = dict()
        kargs = dict()
        cmd2 = str()

        # // Split command from instructions or elements //
        if (len(args) == 1):
            cmd = args[0]; args = list()
        else:
            cmd = args[0]; args = args[1:]           

        # // Get list of containers from the database if they exist //
        if (_ixp_schema_servers() == 0 and cmd == 'show'):
            print (f"INFO: There are no containers in the database currently, suggest'ixp schema build' \n")
            _ixp_debug('y',f"INFO[_ixp_schema_test_options]: There are no containers in the database currently")
            sys.exit(1)

        list_.extend(['site', 'schema' , 'all', 'both', 'help'])
        first_options = ('build', 'default', 'show', 'help') 
        second_options = {'build':['site', 'schema' , 'all', 'both', 'help'], 
                          'show':list_,
                          'default':list_,
                          'help':'',
                         }

        st_list = ['core','mini']
        sw_list = ['traditional','software-defined']

        working_schema = self.ixp_schema_temp

        # // Handle abbreviations in command //
        for x in first_options:
            if (cmd.lower() == x[:len(cmd.lower())]):   
                cmd = x
        if not (cmd in first_options):
            print (f'ERROR: Not a legimate option {cmd}\n')
            _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: Not a legimate option {cmd}')  
            self._ixp_schema_help_handler('help', 1)                      

        #// Handle second options //
        if (len(args) == 1):
            for x in second_options[cmd]:
                if (args[0].lower() == x[:len(args[0])]):   
                    cmd2 = x
            if not (cmd2):
                print (f'ERROR: Not a legimate option {args[0]}\n')
                _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: Not a legimate option {args[0]}')                      
                self._ixp_schema_help_handler(cmd, 1)

        else:
            # // Convert 'args' list to 'kargs' dictionary and validate keys //
            kargs = _list_to_dict(args)
            kargs = _validate_options(self.build_dict, kargs)

            # // Update the IXP schema from default //
            for k, v in kargs.items():
                working_schema[k] = v

            # // Fix abbreviation for site and switching types //  
            working_schema['site_type'] = [x for x in st_list if working_schema['site_type'] == x[:len(working_schema['site_type'])]][0]
            working_schema['switching_type'] = [x for x in sw_list if working_schema['switching_type'] == x[:len(working_schema['switching_type'])]][0]
 
        # // Dealing with help //
        if (cmd == 'help'):           
            self._ixp_schema_help_handler(cmd, 1)

        # // Dealing with 'default' // #
        if (cmd == 'default'):   
            cmd = 'build'

        # // Assemble dictionary to pass back //
        dict_['cmd'] = cmd
        dict_['ins'] = cmd2

        if (cmd == 'build' and cmd2 == 'help'):
            dict_['elm'] = working_schema
        elif (cmd == 'build'):
            dict_['ins'] = 'both'  
            dict_['elm'] = working_schema         
        else:
            dict_['elm'] = ''

        if (cmd == 'show'):
            if (dict_['ins'] == ''):
                dict_['ins'] = 'both'
            elif (dict_['ins'] == 'all'):
                dict_['ins'] = 'both'
            elif (dict_['ins'] == 'both'):
                pass
            elif (dict_['ins'] == 'schema'):
                pass
            elif (dict_['ins'] == 'site'):
                pass
            elif (dict_['ins'] == 'help'):
                self._ixp_schema_help_handler(cmd, 1)
            else:
                assert False, 'ERROR: Not sure how we arrived here'
 
        return (dict_)        

    ## End _ixp_schema_test_options method

    #============================================#
    # //               ixp_schema             // #
    #============================================#   

    def ixp_schema(self, args):
        ''' Method to handle ixp schema' commands '''

        str_ = str()
        list_ = list()
        dict_ = {'schema':0, 'site':0}

        schema_options = self._ixp_schema_test_options(args)

        # // Handle if 'ixp schema show' is called //
        if (schema_options['cmd'] == 'show'):

            # // Call help //
            if (schema_options['ins'] == 'help'):
                self._ixp_schema_help_handler('show', 1)

            # // Determine what to show //
            if (schema_options['ins'] == 'both'): 
                dict_['schema'] = 1
                dict_['site'] = 1            
            elif (schema_options['ins'] == 'schema'): 
                dict_['schema'] = 1
            elif (schema_options['ins'] == 'site'): 
                dict_['site'] = 1     
            elif (schema_options['ins'] == 'help'): 
                dict_['site'] = 1   
            else:
                print ('ERROR: Illegal option\n')
                _ixp_debug('y', 'ERROR[ixp_schema]: Illegal option')       
                     
            # // IXP Schema elements //
            try:
                e = _ixp_schema_servers()
       
            except:
                print ('ERROR: No IXP Schema in the database, one needs to be configured\n')
                _ixp_debug('y','ERROR[ixp_schema]: No IXP Schema in the database, one needs to be configured')
                sys.exit(1) 

            # // Show site //
            if (dict_['site'] > 0):

                list_ = _read_db('site')[_site_number()]
            
                print ('\nIXP Site information')
                print ('=' * 20, '\n')
                for k,v in list_.items():
                    print (f'{k:<15} : {v}')
                print ('')

            # // Show schema //
            if (dict_['schema'] > 0):

                print ("\nIXP Schema information for", end = ' ')
                str_ = ", ".join(str(x) for x in e)
                print (f"{str_}\n{'=' * (27 + len(str_))}")
                print ('')

                for x in e:
                    self._ixp_schema_show(x)        
                print ('')

        # // Handle if 'ixp schema build' is called //
        elif (schema_options['cmd'] == 'build'):
            self._ixp_schema_build(schema_options)

        sys.exit(0)

    ## End to ixp_schema method

########### // End IxpSchema class // ###########

#################################################
# //              IxpHost class              // #
#################################################

class IxpHost:
    ''' IXP Host class '''

    def __init__(self):
        ''' IxpHost class constructor method '''

        self.functionality = 'Building the host based on the IXP Schema'

        self.ovs_start_script = "ovs-config_start.sh"
        self.ovs_stop_script = "ovs-config_stop.sh"
        self.ovs_config_service = "ovs-config.service"
        self.lxd_service = "ixp-lxd.service"
        self.lxd_start_script = "ixp-lxd-start.sh"
        self.lxd_stop_script = "ixp-lxd-stop.sh"
        self.lxd_service_log = "ixp-lxd-service.log"
        self.sw_pass_dict = dict()
        self.host_temp_dict = {'interface': 'TEXT PRIMARY KEY',
                               'model': 'TEXT',
                               'function': 'TEXT',
                               'notes': 'TEXT'
                              }
        self.sw_int_dict = {'switch_number': '0',
                            'switch_name': "'int'",
                            'switch_type': "'OvS'",
                            'manufacturer': "'LF'",
                            'control_interface': "'int'",
                            'data_interface': "'int'",
                            'switch_interfaces': '0',
                            'enabled_interfaces': '0',
                            'ipv4_man': "'N/A'",
                            'ipv6_man': "'N/A'"   
                           }                       

        ## End of __init__ method
        
    def __str__(self):
        ''' IxpHost class __str__ method ''' 

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' IxpHost class __repr__ method ''' 

        return (f'{self.__class__.__name__} class: {self.functionality}')    

        ## End of __repr__ method  

    #============================================#
    # //        _ixp_host_help_handler        // #
    #============================================#

    # // Help handler function //
    def _ixp_host_help_handler(self, command, err_code):
        ''' host help handler '''

        if (command == 'help'):
            IxpHelp._ixp_host_help(self, err_code)
        elif (command == 'show'):
            IxpHelp._ixp_host_help(self, err_code)
        elif (command == 'build'):
            IxpHelp._ixp_host_build_help(self, err_code)
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        ## End to _ixp_host_help_handler method

    #============================================#
    # //        _ixp_host_build_core          // #
    #============================================#

    def _ixp_host_build_core(self, kargs):
        ''' Method to build core servers '''

        address = list()
        list_ = list()
        lxd = _ixp_schema_servers('all')[0]
        interfaces = _ip_link_list()

        core_br = ['br100', 'br900']
        svr_br = ['ns0', 'rs0', 'cs0', 'bs0']

        # // Get IP addresses for management LAN //
        for x in ['ipv4_man','ipv6_man']:
            address.append(_read_db('schema', 'lxc', f"'{lxd}'")[f'{lxd}'][x])

        # // Clear any existing bridges if they exist //
        list_.append(f'ovs-vsctl --if-exists del-br br0')
        for br in core_br:
            list_.append(f'ovs-vsctl --if-exists del-br {br}')

        for br in svr_br:
            list_.append(f'ovs-vsctl --if-exists del-br {br}')

        # // Add bridges
        for br in core_br:
            list_.append(f'ovs-vsctl add-br {br}')
            list_.append(f'ip link set dev {br} up')

        # // Add peers to peering LAN //
        if 'peers' in kargs.keys():
            for peer in kargs['peers']:
                peer_port = interfaces.index(peer) + 1
                list_.append(f'ovs-vsctl add-port br100 {peer} '
                             f'-- set interface {peer} ofport={1000 + peer_port}')
                list_.append(f'ip link set dev {peer} up')

        for c, br in enumerate(svr_br):
            list_.append(f'ovs-vsctl add-br {br}')
            list_.append(f'ovs-vsctl add-port {br} {br}-2 '
                         f'-- set interface {br}-2 type=patch options:peer={br}-1 ofport={3000 + c + 1}')
            list_.append(f'ip link set dev {br} up')

        # // Setup management LAN //
        if 'man' in kargs.keys():
            man_port = interfaces.index(kargs['man'][0]) + 1
            list_.append(f"ovs-vsctl add-port br900 {kargs['man'][0]} "
                         f"-- set interface {kargs['man'][0]} ofport={7000 + man_port}")
            list_.append(f"ip link set dev {kargs['man'][0]} up")

        # // Add IP addresses to management interfaces //
        for c, ip_int in enumerate(['ip900-4', 'ip900-6']):  
            list_.append(f'ovs-vsctl add-port br900 {ip_int} '
                         f'-- set interface {ip_int} type=internal ofport={8000 + c + 1}')
            list_.append(f'ip link set dev {ip_int} up')

            (net, unused_) = _validate_ip_net(address[c])['net'].split('/')
            if ('4' in ip_int):
                ip_route = f"{net[:-1]}1"
                list_.append(f'ip address add {address[c]} dev {ip_int}')
                #list_.append(f'ip route add default via {ip_route}')
            else:
                ip_route = f"{net}1"
                list_.append(f'ip address add {address[c]} dev {ip_int}')
                #list_.append(f'ip -6 route add default via {ip_route}')

        # // Add patch connections to peering LAN for servers // 
        for c, br in enumerate(svr_br): 
            list_.append(f'ovs-vsctl add-port br100 {br}-1 '
                         f'-- set interface {br}-1 type=patch options:peer={br}-2 ofport={2000 + c + 1}')

        return (list_)

        ## End of ixp_host_build_core method

    #============================================#
    # //    _ovs_lxd_config_file_manager      // #
    #============================================#

    def _ovs_lxd_config_file_manager(self, username, list_):
        ''' Method to manage OvS configuration file '''

        config_list = list()

        # // Get list of IXP Schema servers
        e = _ixp_schema_servers()
        container_list = f"""'{"' '".join(str(y) for y in e)}'"""

        # // Create OvS configuration start file //
        with _ManagedFile(f'/tmp/{self.ovs_start_script}') as ovsh:
            ovsh.write('#!/bin/bash\n\n')
            ovsh.write('# -- OvS start script -- #\n\n')
            for x in list_:             
                ovsh.write(f'{x}\n')

        # // Create OvS configuration stop file //
        with _ManagedFile(f'/tmp/{self.ovs_stop_script}') as ovsh:
            ovsh.write('#!/bin/bash\n\n')
            ovsh.write('# -- OvS stop script -- #\n\n')
            for x in ['br0','br100','br900','ns0','rs0','cs0','bs0']:             
                ovsh.write(f'ovs-vsctl --if-exists del-br {x}\n')

        # // Create OvS start service //
        _ixp_debug('y','INFO[_ovs_config_file_manager]: Building OvS restart service')
        with _ManagedFile(f'/tmp/{self.ovs_config_service}') as ovsh:

            ovsh.write('[Unit]')
            ovsh.write('\nDescription=OvS Switch Configuration')
            ovsh.write('\nAfter=systemd-user-sessions.service')
            ovsh.write('\n\n[Service]')
            ovsh.write('\nType=forking')
            ovsh.write(f'\nExecStart=/srv/ovs/{self.ovs_start_script}')
            ovsh.write(f'\nExecStop=/srv/ovs/{self.ovs_stop_script}')
            ovsh.write('\nRemainAfterExit=yes')
            ovsh.write('\n\n[Install]') 
            ovsh.write('\nWantedBy=multi-user.target') 

        # // Create LXD start service //
        _ixp_debug('y','INFO[_ixp_host_build_soft]: Building LXD restart service')
        with _ManagedFile(f'/tmp/{self.lxd_service}') as lxdh:
            lxdh.write('[Unit]') 
            lxdh.write('\nDescription=IXP LXD Service') 
            lxdh.write('\nRequires=network-online.target') 
            lxdh.write('\nAfter=network-online.target') 
            lxdh.write('\n\n[Service]') 
            lxdh.write('\nType=forking')
            lxdh.write(f'\nExecStart=/srv/lxd/{self.lxd_start_script}') 
            lxdh.write(f'\nExecStop=/srv/lxd/{self.lxd_stop_script}') 
            lxdh.write('\nRemainAfterExit=yes')
            lxdh.write('\n\n[Install]') 
            lxdh.write('\nWantedBy=multi-user.target') 

        with _ManagedFile(f'/tmp/{self.lxd_start_script}') as lxdh:
            lxdh.write('#!/bin/sh')
            lxdh.write(f'\n# Filename: /srv/lxd/{self.lxd_start_script}')
            lxdh.write('\n')
            lxdh.write(f'\nDATE=$(date +%Y%m%d-%H%M%S)')
            lxdh.write('\n')
            lxdh.write(f'\necho "LXC start log - $DATE" >  /var/log/ixp/"${{DATE}}-{self.lxd_service_log}"')            
            lxdh.write(f'\nfor CONTAINER in {container_list}')
            lxdh.write('\ndo')
            lxdh.write('\n')
            lxdh.write(f'\n    lxc start "$CONTAINER"')
            lxdh.write(f'\n    echo "Starting $CONTAINER" >> /var/log/ixp/"${{DATE}}-{self.lxd_service_log}"')
            lxdh.write('\n')
            lxdh.write('\ndone\n')
            lxdh.write(f'\necho "Started the containers {container_list}" >> /var/log/ixp/"${{DATE}}-{self.lxd_service_log}"')
            lxdh.write(f'\n')
            lxdh.write(f'\nchown {username}: /var/log/ixp/"${{DATE}}-{self.lxd_service_log}"')

        with _ManagedFile(f'/tmp/{self.lxd_stop_script}') as lxdh:
            lxdh.write('#!/bin/sh')
            lxdh.write(f'\n# Filename: /srv/lxd/{self.lxd_stop_script}')
            lxdh.write('\n')
            lxdh.write(f'\nDATE=$(date +%Y%m%d-%H%M%S)')
            lxdh.write('\n')
            lxdh.write(f'\necho "LXC stop log - $DATE" >  /var/log/ixp/"${{DATE}}-{self.lxd_service_log}"')            
            lxdh.write(f'\nfor CONTAINER in {container_list}')
            lxdh.write('\ndo')
            lxdh.write('\n')
            lxdh.write(f'\n    lxc stop "$CONTAINER" --force')
            lxdh.write(f'\n    echo "Stopping $CONTAINER" >> /var/log/ixp/"${{DATE}}-{self.lxd_service_log}"')
            lxdh.write('\n')
            lxdh.write('\ndone\n')
            lxdh.write(f'\necho "Stopped the containers {container_list}" >> /var/log/ixp/"${{DATE}}-{self.lxd_service_log}"')
            lxdh.write(f'\n')
            lxdh.write(f'\nchown {username}: /var/log/ixp/"${{DATE}}-{self.lxd_service_log}"')

        # // Handle GAI file to prioritise IPv4 over IPv6 //
        if (public_ipv6_net == False):
            _ixp_debug('y','INFO[_ixp_host_build_soft]: Prioritising IPv4 over IPv6')
            gai_file_bu = f'{gai_file}.orig'
            gai_temp_file = _gai_build()

        # // Build commands requiring sudo execution //
        config_list.append('mkdir -p /srv/ovs')
        config_list.append('mkdir -p /srv/lxd')
        config_list.append('mkdir -p /srv/keys')
        config_list.append(f'mv /tmp/{self.ovs_start_script} /srv/ovs/{self.ovs_start_script}')        
        config_list.append(f'mv /tmp/{self.ovs_stop_script} /srv/ovs/{self.ovs_stop_script}')
        config_list.append(f'mv /tmp/{self.ovs_config_service} /etc/systemd/system/{self.ovs_config_service}')
        config_list.append(f'chmod +x /etc/systemd/system/{self.ovs_config_service}')
        config_list.append(f'chown -R {username}: /srv/ovs')
        config_list.append(f'chmod +x /srv/ovs/*')
        config_list.append(f'mv /tmp/{self.lxd_service} /etc/systemd/system/')
        config_list.append(f'chown root: /etc/systemd/system/{self.lxd_service}')
        config_list.append(f'chmod {file_perm} /etc/systemd/system/{self.lxd_service}')    
        config_list.append(f'mv /tmp/{self.lxd_start_script} /srv/lxd/{self.lxd_start_script}')
        config_list.append(f'mv /tmp/{self.lxd_stop_script} /srv/lxd/{self.lxd_stop_script}')
        config_list.append(f'chown -R root: /srv/lxd/')
        config_list.append(f'chmod {file_perm} /srv/lxd/*')
        config_list.append(f'chown -R {username}: /srv/lxd')
        config_list.append(f'chmod +x /srv/lxd/*')
        config_list.append('systemctl daemon-reload')
        config_list.append(f'systemctl restart {self.ovs_config_service}')
        config_list.append(f'systemctl enable {self.ovs_config_service}')
        config_list.append(f'systemctl restart {self.lxd_service}')
        config_list.append(f'systemctl enable {self.lxd_service}')
        config_list.append(f'chown -R {username}: /srv/keys')
        config_list.append(f'chmod 700 /srv/keys') 
        config_list.append(f'cp {gai_file} {gai_file_bu}')  
        config_list.append(f'mv {gai_temp_file} {gai_file}')

        # // Send the files to the host // 
        response = _sudo_call('bash', config_list)
        if (response[0] == 0):
            _ixp_debug('y',f'INFO[_ovs_config_file_manager]: Executed the OvS configuration on host')
        else:
            _ixp_debug('y',f'ERROR[_ovs_config_file_manager]: Failed to execute the OvS configuration host') 
            sys.exit(1)

        counter = 0
        # // Short sleep to allow containers to come up //
        for n in range(1,4):
            time.sleep(5)
            counter += 1        

        return (0)

        ## End of _ovs_lxd_config_file_manager

    #============================================#
    # //        _ixp_host_build_soft          // #
    #============================================#

    def _ixp_host_build_soft(self, kargs):
        ''' Method to build host software-defined model '''

        str_ = str()
        str2_ = str()
        list_ = list()
        dict_ = dict()
        model = kargs['mod']
        switches = int(kargs['switch'])
        interfaces = kargs['int']
        ovs_config = list()
        int_type_dict = dict()

        (unused_, unused_, tstamp) = _ixp_timestamp()

        # // Get username for regular user //
        username = getpass.getuser()

        # // Get SDN Controller name //
        SC = [x for x in _read_db('schema').keys() if x[:2] == 'sc'][0]

        # // Get SDN Controller IP Address //
        v4 = _read_db('schema')[SC]['ipv4_man']
        v6 = _read_db('schema')[SC]['ipv6_man']
        (sdn_controller_4, unused_) = v4.split('/')
        (sdn_controller_6, unused_) = v6.split('/')

        # // Get the management networks //
        (v4_net, v4_prefix, v6_net, v6_prefix) = _ixp_schema_network()

        print (f'\nConfiguring the IXP host for software-defined model: {model}\n')
        _ixp_debug('y',f'INFO[_ixp_host_build_soft]: Configuring the IXP host for software-defined model: {model}')
        
        # // Go through the models //
        if (model == 'S'):

            int_type_dict['oob'] = [interfaces[0]]
            int_type_dict['man'] = [interfaces[1]]
            int_type_dict['sw_pairs'] = []
            int_type_dict['peers'] = interfaces[2:]       

        elif (model == 'T'):

            int_type_dict['oob'] = [interfaces[0]]
            int_type_dict['man'] = [interfaces[1]]

            # // Loop through switches //
            int_type_dict['sw_pairs'] = list()
            count = 2
            for x in range(1, switches + 1, 1):
                int_type_dict['sw_pairs'].append([interfaces[x * 2],interfaces[x * 2 + 1]])
                count += 2
            int_type_dict['peers'] = interfaces[count:]
 
        elif (model == 'U'):
            int_type_dict['oob'] = [interfaces[0]]
            int_type_dict['man'] = [interfaces[1]]

            # // Loop through switches //
            int_type_dict['sw_pairs'] = list()
            for x in range(1, switches + 1, 1):
                int_type_dict['sw_pairs'].append([interfaces[x * 2],interfaces[x * 2 + 1]])
            int_type_dict['peers'] = []

        else:
            print (f'ERROR: \'{model}\' is an illegal software-defined model\n')
            _ixp_debug('y',f'ERROR[_ixp_host_build_soft]: \'{model}\' is an illegal software-defined model')
            sys.exit(1)

        # // Note to installer //
        if (model == 'S'):
            print (f'  ----------------------------------',
                  f'\n  INSTALLER NOTE',
                  f'\n  \"Software-defined switching\"',
                  f'\n  Model: {model} is ideal for mIXP and can', 
                  f"\n  support {len(int_type_dict['peers'])} unmanaged traditional", 
                  f"\n  Ethernet switches", 
                  f'\n  ----------------------------------', 
                  f'\n', 
                 ) 
        elif (model == 'T'):
            print (f'  -------------------------------------', 
                  f'\n  INSTALLER NOTE',
                  f'\n  \"Software-defined switching\"',
                  f"\n  Model: {model} supports a mix of {len(int_type_dict['sw_pairs'])} OpenFlow", 
                  f"\n  switches as well as {len(int_type_dict['peers'])} peers", 
                  f'\n  -------------------------------------', 
                  f'\n', 
                 ) 
        elif (model == 'U'):
            print (f'  -------------------------------------', 
                  f'\n  INSTALLER NOTE',
                  f'\n  \"Software-defined switching\"',
                  f"\n  Model: {model} supports {len(int_type_dict['sw_pairs'])} OpenFlow switches", 
                  f'\n  -------------------------------------', 
                  f'\n', 
                 ) 

        # // Get core server and bridge configuration //
        ovs_config = self._ixp_host_build_core(int_type_dict)

        # // Add switch details //
        if ('sw_pairs' in int_type_dict):
            if (len(int_type_dict['sw_pairs']) > 0):
                for c, pair in enumerate(int_type_dict['sw_pairs']):
                    switch_no = (c + 1) * 100
                    ovs_config.append(f'ovs-vsctl add-port br900 {pair[0]} '
                                      f'-- set interface {pair[0]} ofport={7000 + switch_no + interfaces.index(pair[0]) + 1}')  
                    ovs_config.append(f'ovs-vsctl add-port br100 {pair[1]} '
                                      f'-- set interface {pair[1]} ofport={4000 + switch_no + interfaces.index(pair[1]) + 1}')   
                    ovs_config.append(f'ip link set dev {pair[0]} up')
                    ovs_config.append(f'ip link set dev {pair[1]} up')

        # // Add SDN Controller //
            ovs_config.append(f'ovs-vsctl set-controller br100 tcp:{sdn_controller_4}:{sdn_port}') 
            ovs_config.append(f'ovs-vsctl set-fail-mode br100 standalone') 

        # // Build OvS config and lxd restart service //
        self._ovs_lxd_config_file_manager(username, ovs_config)
      
        # // Get OOB IP Addresses //
        oob_show_ip = subprocess.getstatusoutput (f"ip address show dev {int_type_dict['oob'][0]}")[1]
        matched = re.findall(r'inet6? (.*?)\s', oob_show_ip)
        oob_ip_list = list()
        for x in matched:
            if not ('fe80::' in x):
                oob_ip_list.append(x)
            
        # // Get IP addresses for peer ports //
        list4_ = list(_read_db('ipv4_peer', 'function', "'peer'", 'reserved', '0').keys())[:len(int_type_dict['peers'])]
        list6_ = list(_read_db('ipv6_peer', 'function', "'peer'", 'reserved', '0').keys())[:len(int_type_dict['peers'])]

        # // Interface table information //
        _ixp_database_insert('host', {'interface': f"'{int_type_dict['oob'][0]}'", 
                             'model':f"'{model}'", 
                             'function':"'OOB'", 
                             'notes':"''"})        
        _ixp_database_insert('host', {'interface': f"'{int_type_dict['man'][0]}'", 
                             'model':f"'{model}'", 
                             'function':"'Management'", 
                             'notes':"''"})  
        for c, i in enumerate(interfaces[2:]):
            cnt = 0
            for ii in int_type_dict['sw_pairs']:
                cnt += 1
                if i in ii[0]:
                    _ixp_database_insert('host', {'interface': f"'{int_type_dict['sw_pairs'][cnt - 1][0]}'", 
                                         'model':f"'{model}'", 
                                         'function':"'Ctrl (SDN)'", 
                                         'notes':f"'Switch {cnt} OOB'"})  
                    _ixp_database_insert('host', {'interface': f"'{int_type_dict['sw_pairs'][cnt - 1][1]}'", 
                                         'model':f"'{model}'", 
                                         'function':"'Peer (Data)'", 
                                         'notes':f"'Switch {cnt} Port 1'"}) 

        for c, peer in enumerate(int_type_dict['peers']):
            _ixp_database_insert('host', {'interface': f"'{peer}'", 
                                 'model':f"'{model}'", 
                                 'function':"'Local peer'", 
                                 'notes':f"'{list4_[c]}, {list6_[c]}'"}) 

            # // Add OvS peer ports to the database //  
            dict_ = {'switch_number':'0', 'port_number':f'{interfaces.index(peer) + 1}', 
                     'interface_speed': '1G','function':'peer', 'enabled': 1,'reserved': 1}
            _ixp_database_update('ipv4_peer', f"'{list4_[c]}'", dict_)
            _ixp_database_update('ipv6_peer', f"'{list6_[c]}'", dict_)

        # // Add internal switch to the 'switch' table in the database //
        str_ = len(int_type_dict['peers'])
        for x in int_type_dict['peers']:
            list_.append(interfaces.index(x) + 1)
        str2_ = _rangeStr(list_[0], list_[-1])
        self.sw_int_dict['switch_interfaces'] = str_
        self.sw_int_dict['enabled_interfaces'] = f"'{str2_}'"

        # // Insert internal switch into the database //
        _ixp_database_insert('switch', self.sw_int_dict)

        # // Return the picture to the terminal //
        self._ixp_host_show()

        # // Add switch data to the 'switch' table in the database //
        for c, i in enumerate(int_type_dict['sw_pairs']):
            sw_ip_v4 = f'{v4_net}24{c + 1}/{v4_prefix}'
            sw_ip_v6 = f'{v6_net}24{c + 1}/{v6_prefix}'
            dict_ = {'switch_number':f'{c + 1}','function':'core', 'enabled': 1,'reserved': 1}
            _ixp_database_update('ipv4_man', f"'{sw_ip_v4}'", dict_)
            _ixp_database_update('ipv6_man', f"'{sw_ip_v6}'", dict_)

            self.sw_pass_dict['switch_number'] = c + 1
            self.sw_pass_dict['switch_name'] = f"'sw_{c + 1}'"
            self.sw_pass_dict['switch_type'] = f"'Bare-metal'"            
            self.sw_pass_dict['manufacturer'] = f"'Bare-metal'"
            self.sw_pass_dict['control_interface'] = f"'{i[0]}'"
            self.sw_pass_dict['data_interface'] = f"'{i[1]}'"
            self.sw_pass_dict['switch_interfaces'] = '0'
            self.sw_pass_dict['enabled_interfaces'] = '0'
            self.sw_pass_dict['ipv4_man'] = f"'{sw_ip_v4}'"
            self.sw_pass_dict['ipv6_man'] = f"'{sw_ip_v6}'"

            try:
                _ixp_database_insert('switch', self.sw_pass_dict)
            except:
                print (f"INFO: Switch '{c + 1}' already exists in the database")
                _ixp_debug('y', f"INFO[_ixp_host_build_soft]: Switch '{c + 1}' already exists in the database")        

        _ixp_debug('y','INFO[_ixp_host_build_soft]: Adding switch data to the IXP switches table in the database')

        print ('Completed configuration of IXP host\n')
        _ixp_debug('y', 'Completed configuration of IXP host')

        return (0)

    ## End of _ixp_host_build_soft method

    #============================================#
    # //        _ixp_host_build_trad          // #
    #============================================#

    def _ixp_host_build_trad(self, kargs):
        ''' Method to build traditional model '''

        str_ = str()
        str2_ = str()
        list_ = list()
        dict_ = dict()
        model = kargs['mod']
        switches = int(kargs['switch'])
        interfaces = kargs['int']
        ovs_config = list()
        int_type_dict = dict()

        (unused_, unused_, tstamp) = _ixp_timestamp()

        # // Get username for regular user //
        username = getpass.getuser()

        # // Get management LAN details //
        (v4_net, v4_prefix, v6_net, v6_prefix) = _ixp_schema_network()

        print (f'\nConfiguring the IXP host for traditional switching model: {model}\n')
        _ixp_debug('y',f'INFO[_ixp_host_build_trad]: Configuring the IXP host for traditional switching model: {model}')
        
        # // Go through the models //
        if (model == 'A'):
            int_type_dict['trunks'] = [interfaces[0]]

        elif (model == 'B'):
            int_type_dict['oob'] = interfaces[0]
            int_type_dict['trunks'] = [interfaces[1]]

        elif (model == 'C'):
            int_type_dict['oob'] = interfaces[0]
            int_type_dict['man'] = [interfaces[1]]
            int_type_dict['peers'] = interfaces[2:]

        elif (model == 'D'):
            int_type_dict['oob'] = interfaces[0]
            int_type_dict['man'] = [interfaces[1]]
            int_type_dict['trunks'] = interfaces[2:switches + 2]
            int_type_dict['peers'] = interfaces[switches + 2:]

            if (len(int_type_dict['peers']) < 1):
                print ('Model E, there are no peer interfaces\n')
                _ixp_debug('y','Model E, there are no peer interfaces')
                model = 'E'

        elif (model == 'E'):
            int_type_dict['oob'] = interfaces[0]
            int_type_dict['man'] = [interfaces[1]]

            int_type_dict['trunks'] = interfaces[2:]

        else:
            print (f'ERROR: \'{model}\' is an illegal traditional model\n')
            _ixp_debug('y',f'ERROR[_ixp_host_build_trad]: \'{model}\' is an illegal traditional model')
            sys.exit(1)

        # // Note to installer //
        if (model in ('A','B')):
            print (f'  -----------------------------------------', 
                  f'\n  INSTALLER NOTE',
                  f'\n  \"Traditional switching\"',
                  f'\n  Model: {model} is not ideal as an IXP,', 
                  f'\n  switches require VLANs 100 and 900', 
                  f'\n  configured for peering and management',
                  f'\n  -----------------------------------------', 
                  f'\n', 
                 ) 
        elif (model == 'C'):
            print (f'  --------------------------------------',
                  f'\n  INSTALLER NOTE',
                  f'\n  \"Traditional switching\"',
                  f'\n  Model: {model} is ideal as an IXP, switches', 
                  f'\n  do not require VLANs to be configured', 
                  f'\n  -------------------------------------', 
                  f'\n', 
                 ) 
        elif (model == 'D'):
            print (f'  --------------------------------------', 
                  f'\n  INSTALLER NOTE',
                  f'\n  \"Traditional switching\"',
                  f'\n  Model: {model} is ideal as an IXP, switches', 
                  f'\n  require VLANs 100 and 900 configured', 
                  f'\n   for peering and management',
                  f'\n  --------------------------------------', 
                  f'\n', 
                 ) 
        elif (model == 'E'):
            print (f'  --------------------------------------------', 
                  f'\n  INSTALLER NOTE',
                  f'\n  \"Traditional switching\"',
                  f'\n  Model: {model} is ideal as an IXP, all interfaces', 
                  f'\n  are trunked except the first two. Switches',
                  f'\n  require VLANs 100 and 900 configured for',
                  f'\n  peering and management',
                  f'\n  -------------------------------------------', 
                  f'\n', 
                 ) 

        # // Get core server and bridge configuration //
        ovs_config = self._ixp_host_build_core(int_type_dict)

        # // Add switch details //
        if ('trunks' in int_type_dict.keys()):
            if (len(int_type_dict['trunks']) > 0):
                ovs_config.append('ovs-vsctl add-br br0') 
                ovs_config.append(f'ovs-vsctl add-port br0 pat100-1 tag=100 '
                                  f'-- set interface pat100-1 type=patch '
                                  f'options:peer=pat100-2')
                ovs_config.append(f'ovs-vsctl add-port br100 pat100-2 '
                                  f'-- set interface pat100-2 type=patch '
                                  f'options:peer=pat100-1')
                ovs_config.append(f'ovs-vsctl add-port br0 pat900-1 tag=900 '
                                  f'-- set interface pat900-1 type=patch '
                                  f'options:peer=pat900-2')
                ovs_config.append(f'ovs-vsctl add-port br900 pat900-2 '
                                  f'-- set interface pat900-2 type=patch '
                                  f'options:peer=pat900-1')
                ovs_config.append('ip link set dev br0 up')

                for trunk in int_type_dict['trunks']:                     
                    ovs_config.append(f'ovs-vsctl add-port br0 {trunk} trunks=100,900')     
                    ovs_config.append(f'ip link set dev {trunk} up')

        # // Add a default route for model 'A' only as no OOB interface exists //
        if (model == 'A'):
            (v4_net, unused_, v6_net, unused_) = _ixp_schema_network()   
            ovs_config.append(f'ip route add 0.0.0.0/0 via {v4_net}1') 
            ovs_config.append(f'ip -6 route add ::/0 via {v6_net}1')   
            ovs_config.append(f'rm /etc/netplan/*')
            ovs_config.append(f'netplan apply')

            # // As there is no OOB in model 'A', set the upstream_ns //
            # // Read the resolved.conf file //
            subprocess.getstatusoutput ('cp /etc/systemd/resolved.conf /tmp/resolved.conf')
            f = open('/tmp/resolved.conf',"r")
            lines = f.readlines()
            f.close()           

            # // Rewrite the resolved.conf file //
            f = open('/tmp/resolved.conf',"w")
            for line in lines:
                matched = re.findall(r'(^DNS=).*', line)
                if (len(matched) == 0):
                    f.write(line)
            f.close()

            # // Update resolved.conf //
            ovs_config.append(f"mv /tmp/resolved.conf /etc/systemd/resolved.conf")
            ovs_config.append(f"echo '\nDNS={upstream_ns[0]} {upstream_ns[1]}' >> /etc/systemd/resolved.conf")
            ovs_config.append('systemctl restart systemd-resolved')

            _ixp_debug('y',f'INFO[_ixp_host_build_trad]: Adding default routes via {v4_net} and {v6_net}')

        # // Build OvS config and lxd restart service //
        self._ovs_lxd_config_file_manager(username, ovs_config)
        
        if ('oob' in int_type_dict.keys()): 
            # // Get OOB IP Addresses //
            oob_show_ip = subprocess.getstatusoutput (f"ip address show dev {int_type_dict['oob'][0]}")[1]
            matched = re.findall(r'inet6? (.*?)\s', oob_show_ip)
            oob_ip_list = list()
            for x in matched:
                if not ('fe80::' in x):
                    oob_ip_list.append(x)

        if ('peers' in int_type_dict.keys()): 
            # // Get IP addresses for peer ports //
            list4_ = list(_read_db('ipv4_peer', 'function', "'peer'", 'reserved', '0').keys())[:len(int_type_dict['peers'])]
            list6_ = list(_read_db('ipv6_peer', 'function', "'peer'", 'reserved', '0').keys())[:len(int_type_dict['peers'])]

        # // Interface table information //
        if ('oob' in int_type_dict.keys()):
            _ixp_database_insert('host', {'interface': f"'{int_type_dict['oob']}'", 
                                          'model':f"'{model}'", 
                                          'function':"'OOB'", 
                                          'notes':"''"})

        if ('man' in int_type_dict.keys()):  
            _ixp_database_insert('host', {'interface': f"'{int_type_dict['man'][0]}'", 
                                          'model':f"'{model}'", 
                                          'function':"'Management'", 
                                          'notes':"''"}) 
 
        if ('trunks' in int_type_dict.keys()):  
            for trunk in int_type_dict['trunks']:
                _ixp_database_insert('host', {'interface': f"'{trunk}'", 
                                              'model':f"'{model}'", 
                                              'function':"'Trunk'", 
                                              'notes':"''"})

        if ('peers' in int_type_dict.keys()): 
            for c, peer in enumerate(int_type_dict['peers']):     
                _ixp_database_insert('host', {'interface': f"'{peer}'",
                                              'model':f"'{model}'",
                                              'function':"'Local peer'",
                                              'notes':f"'{list4_[c]}, {list6_[c]}'"}) 

                dict_ = {'switch_number':'0', 'port_number':f'{interfaces.index(peer) + 1}', 
                         'interface_speed': '1G', 'function':'peer', 'enabled': 1,'reserved': 1}
                _ixp_database_update('ipv4_peer', f"'{list4_[c]}'", dict_)  
                _ixp_database_update('ipv6_peer', f"'{list6_[c]}'", dict_)

        # // Add internal switch to the 'switch' table in the database //
        str_ = len(int_type_dict['peers'])
        for x in int_type_dict['peers']:
            list_.append(interfaces.index(x) + 1)
        str2_ = _rangeStr(list_[0], list_[-1])    
        self.sw_int_dict['switch_interfaces'] = str_
        self.sw_int_dict['enabled_interfaces'] = f"'{str2_}'"

        # // Insert internal switch into the database //
        _ixp_database_insert('switch', self.sw_int_dict)

        # // Return the picture to the terminal //
        self._ixp_host_show()

        # // Add switch data to the 'switch' table in the database //
        if ('trunks' in int_type_dict.keys()): 
            for c, i in enumerate(int_type_dict['trunks']):
                sw_ip_v4 = f'{v4_net}24{c + 1}/{v4_prefix}'
                sw_ip_v6 = f'{v6_net}24{c + 1}/{v6_prefix}'
                dict_ = {'switch_number':f'{c + 1}','function':'core', 'enabled': 1,'reserved': 1}
                _ixp_database_update('ipv4_man', f"'{sw_ip_v4}'", dict_)
                _ixp_database_update('ipv6_man', f"'{sw_ip_v6}'", dict_)

                self.sw_pass_dict['switch_number'] = c + 1
                self.sw_pass_dict['switch_name'] = f"'sw_{c + 1}'"
                self.sw_pass_dict['switch_type'] = f"'Traditional Ethernet'"            
                self.sw_pass_dict['manufacturer'] = f"'Traditional Ethernet'"
                self.sw_pass_dict['control_interface'] = f"'{int_type_dict['trunks'][c]}'"
                self.sw_pass_dict['data_interface'] = f"'{int_type_dict['trunks'][c]}'"
                self.sw_pass_dict['switch_interfaces'] = '0'
                self.sw_pass_dict['ipv4_man'] = f"'{sw_ip_v4}'"
                self.sw_pass_dict['ipv6_man'] = f"'{sw_ip_v6}'"

                try:
                    _ixp_database_insert('switch', self.sw_pass_dict)

                except:
                    print (f"INFO: Switch '{c + 1}' already exists in the database")
                    _ixp_debug('y', f"INFO[_ixp_host_build_trad]: Switch '{c + 1}' already exists in the database")        

        _ixp_debug('y','INFO[_ixp_host_build_trad]: Adding switch data to the IXP switches table in the database')

        print ('Completed configuration of IXP host\n')
        _ixp_debug('y', 'Completed configuration of IXP host')

        return (0)

    ## End _ixp_host_build_trad method

    #============================================#
    # //             _ixp_host_show           // #
    #============================================#

    def _ixp_host_show(self):
        ''' Method to show 'ixp_host' interfaces '''

        pad = 13
        dict_ = _read_db('host')

        print (f"  IXP Host interfaces - Model '{dict_[_ip_link_list()[0]]['model']}'")

        print (f" + {'-' * (pad * 2 + 3)} +")
        print (f" | {'Host interfaces':^{pad * 2 + 4}}|")
        print (f" + {'-' * pad} + {'-' * pad} +")
        print (f" | {'Interface':^{pad}} | {'Function':^{pad}} |")
        print (f" + {'-' * pad} + {'-' * pad} +")
        for k, v in dict_.items():
            if (v['function'] == 'OOB'):
                oob_show_ip = subprocess.getstatusoutput (f"ip address show dev {k}")[1]
                matched = re.findall(r'inet6? (.*?)\s', oob_show_ip)
                oob_ip_list = list()
                for x in matched:
                    if not ('fe80::' in x):
                        oob_ip_list.append(x)
                oob_ip = ", ".join(str(y) for y in oob_ip_list)

                print (f" | {k:<{pad}} | {v['function']:<{pad}} | {oob_ip:<{pad}}")
            else:
                print (f" | {k:<{pad}} | {v['function']:<{pad}} | {v['notes']:<{pad}}")
        print (f" + {'-' * pad} + {'-' * pad} +")

        return (0)

    ## End _ixp_host_show method

    #============================================#
    # //        _ixp_host_test_options        // #
    #============================================#

    def _ixp_host_test_options(self, args, simulator):
        ''' Method to test 'ixp_host' cli options '''

        str_ = str()
        cmd2 = str()
        kargs = dict()
        ports = str()

        # // Replace any instance of 'list' with 'show' //
        args = ['show' if x == 'list' else x for x in args]

        # // Split command from instructions or elements //
        if (len(args) == 1):
            cmd = args[0]; args = list()
        else:
            cmd = args[0]; args = args[1:]           

        first_options = ['build', 'show', 'help']
        second_options = {'build':['help'], 
                          'show':['help'],
                          'help':'',
                         }

        # // Handle abbreviations in command //
        for x in first_options:
            if (cmd.lower() == x[:len(cmd.lower())]):   
                cmd = x

        if not (cmd in first_options):
            print (f'ERROR: Not a legimate option {cmd}\n')
            _ixp_debug('y',f'ERROR[_ixp_host_test_options]: Not a legimate option {cmd}')  
            self._ixp_host_help_handler('help', 1) 

        #// Handle second options //
        if (len(args) == 1):
            for x in second_options[cmd]:
                if (args[0].lower() == x[:len(args[0])]):   
                    cmd2 = x
            if not (cmd2):
                print (f'ERROR: Not a legimate option {args[0]}\n')
                _ixp_debug('y',f'ERROR[_ixp_host_test_options]: Not a legimate option {args[0]}')                      
                self._ixp_host_help_handler(cmd, 1)

        # // Handle show //
        if (cmd == 'show' and cmd2 == 'help'):
            self._ixp_host_help_handler(cmd, 1)
        elif (cmd == 'show'):
            return ('show', {})

        # // Handle switch numbers from build //
        elif (cmd == 'build'):
            if (len(args) == 0):
                kargs = {'switch': 0}
            elif not (args[0] == '--switch' or args[0] == '-s'):
                print (f'ERROR: Not a legimate switch option {args[0]}\n')
                _ixp_debug('y',f'ERROR[_ixp_host_test_options]: Not a legimate switch option {args[0]}') 
                self._ixp_host_help_handler(cmd, 1)                         

            else:
                kargs = {'switch':int(args[1])} if [_non_ip_test('switch', args[1]) == 0] else sys.exit(1)   

        elif (cmd == 'help'):
            self._ixp_host_help_handler('help', 0)

        else:
            print (f'ERROR: Not a legimate option {args[0]}\n')
            _ixp_debug('y',f'ERROR[_ixp_host_test_options]: Not a legimate option {args[0]}')            
            self._ixp_host_help_handler(cmd, 1)

        # // Get available number of interfaces on server //
        _ixp_debug('y', f'INFO[_ixp_host_test_options]: Get physical interfaces on server')
        interfaces = _ip_link_list()
        ports = len(interfaces)
       
        # // Handle based on switching type //
        if (_switching_type() == 'traditional'):

            # // Check if simulator set, if so fool system //
            if (simulator == 1):
                kargs['switch'] = 1
                ports = 1
            elif (simulator == 2):
                kargs['switch'] = 1
                ports = 2
            else:
                pass

            # // For traditional switching find model // 
            if (ports == 1):
                kargs['mod'] = 'A'       
                if not (kargs['switch'] == 1):
                    print (f"ERROR: Model 'A' can only work with one switch and must have one switch\n") 
                    _ixp_debug('y', f"ERROR[_ixp_host_test_options]: Model 'A' can only work with one switch and must have one switch") 
                    sys.exit(1)
            elif (ports == 2):
                kargs['mod'] = 'B'
                if not (kargs['switch'] == 1):
                    print (f"ERROR: Model 'B' can only work with one switch and must have one switch\n") 
                    _ixp_debug('y', f"ERROR[_ixp_host_test_options]: Model 'B' can only work with one switch and must have one switch") 
                    sys.exit(1)
            elif (int(kargs['switch']) == 0):
                kargs['mod'] = 'C'
            elif (int(kargs['switch']) < int(ports - 1)):
                print ('\nTraditional Switching Models')
                print (f"{'-' * 28}\n")
                print ('Model C: OOB, Management, and remaining \'Peer\' interfaces')
                print ('Model D: OOB, Management, one \'Trunk\' interface and remaining \'Peer\' interfaces')
                print ('Model E: OOB, Management, and remaining \'Trunk\' interfaces\n')
                str_= input('  Choice of model, \'C\', \'D\' or \'E\': ')
                kargs['mod'] = str_.upper()
                if not (kargs['mod'] in ('C', 'D', 'E')):
                    print ('\nERROR: Not a valid selection, \'C\', \'D\' or \'E\' please\n') 
                    _ixp_debug('y', 'ERROR[_ixp_host_test_options]: Not a valid selection, \'C\', \'D\' or \'E\' please') 
                    sys.exit()
            else:
                kargs['mod'] = 'E'

        # // For software-defined switching //
        elif (_switching_type() == 'software-defined'):

            # // Error is there are not a minimum of 4 interfaces on the server //
            if (ports < 3):
                print ('\nERROR: Software-defined models require a minimum of 4 Ethernet interfaces\n') 
                _ixp_debug('y', 'ERROR[_ixp_host_test_options]: Software-defined models require a minimum of 4 Ethernet interfaces')

                str_ = ", ".join(str(x) for x in interfaces)
                print (f'Interfaces: {str_ }\n')
                sys.exit(1)

            # // Handle 3 ports //
            elif (ports == 3):
                print ('\nINFO: Pretty pointless, 1 peer interface, except for small testing applications\n') 
                _ixp_debug('y', 'INFO[_ixp_host_test_options]: Pretty pointless, 1 peer interface, except for small testing applications')
                print ('model S - ALL PEER PORTS\n')
                kargs['mod'] = 'S'

            # // Handle 0 switches, mIXP with peer interfaces //
            if (kargs['switch'] == 0):
                kargs['mod'] = 'S'

            # // Handle number of switches given in CLI //
            elif (kargs['switch'] > 0):
    
                # // Test switches against available interfaces //
                if (int(kargs['switch']) > int((ports - 2) / 2)):   
                    print (f"\nERROR: With {ports} Ethernet interfaces there are not enough to to handle {kargs['switch']} OpenFlow switches\n") 
                    _ixp_debug('y', f"ERROR[_ixp_host_test_options]: With {ports} Ethernet interfaces there are not enough to to handle {kargs['switch']} OpenFlow switches")
                    print (f"Port {'Interface':<10}: Function")
                    print ('---- ---------   --------')
                    print (f' 1:  {interfaces[0]:<10}: OOB')
                    print (f' 2:  {interfaces[1]:<10}: Management')
                    s = 0.5
                    for c, x in enumerate(interfaces[2:]):
                        if (c % 2):
                            print (f' {c + 3}:  {x:<10}: Switch {int(s)} Data plane') 
                        else:                           
                            print (f' {c + 3}:  {x:<10}: Switch {int(s + 0.5)} Control plane ')                               
                        s += 0.5
 
                    if (int(s + 0.5) == kargs['switch']):
                        print (f"\nNo Ethernet interfaces remaining for switch {kargs['switch']}")
                        print (f"\nThis server can handle a maximum of {int(s - 0.5)} switches\n")
                    elif (int(s + 0.5) < kargs['switch']):
                        print (f"\nNo Ethernet interfaces remaining for switches {int(s + 0.5)} - {kargs['switch']}")
                        print (f"\nThis server can handle a maximum of {int(s - 0.5)} switches\n")

                    sys.exit(1)

                # // If some spare interfaces, model T //
                elif (int(kargs['switch']) < int((ports - 2) / 2)):
                    kargs['mod'] = 'T'

                # // If no spare interfaces, model U //
                elif (int(kargs['switch']) == int((ports - 2) / 2)):
                    kargs['mod'] = 'U'

                else:
                    pass

        else:
            assert False, 'ERROR: Not sure how we arrived here'

        # // Add mod and interfaces to kargs //
        kargs['ports'] = ports
        kargs['int'] = interfaces
 
        _ixp_debug('y', f"INFO[_ixp_host_test_options]: Model:{kargs['mod']}, Interfaces:{ports}, Switches:{kargs['switch']}")

        return (cmd, kargs)
 
    ## End _ixp_host_test_options method

    #============================================#
    # //               ixp_host               // #
    #============================================#

    def ixp_host(self, args):
        ''' Method to handle the 'ixp host' command '''

        cmd = str()
        dict_ = dict()
        dict2_ = dict()

        # // Simulator function for models A and B for servers with > 2 interfaces //
        simulator = 0
        sim_dict = {'-sa':1,'--sim-model-a':1,'-sb':2,'--sim-model-b':2}

        for x in args:
            if (x in sim_dict.keys() and simulator == 0):
                simulator = sim_dict[x]
                args.remove(x)                     

        # // Check the IXP Host options //
        cmd, dict_ = self._ixp_host_test_options(args, simulator)

        if (cmd == 'build'):

            # // Clear peer interfaces to the database //  
            dict2_ = {'switch_number': None, 'port_number': None, 'name': None,
                      'assigned': None, 'as_number': None, 'domain': None,
                      'interface_speed': None, 'route_server': None, 
                      'blackhole_server': None, 'enabled':None, 'reserved':0, 
                      'switch_number':None, 'function':'peer'
                     }
            for x in ['ipv4_peer', 'ipv6_peer']:
                _ixp_database_update(x, '', dict2_, 'function', "'peer'", 'reserved', '1') 

            # // Clear switches from the database //
            (v4_net, v4_prefix, v6_net, v6_prefix) = _ixp_schema_network()
            dict2_ = {'switch_number': None, 'port_number': None, 'name': None,
                      'assigned': None, 'as_number': None, 'domain': None,
                      'interface_speed': None, 'route_server': None, 
                      'blackhole_server': None, 'enabled':None, 'reserved':1, 
                      'switch_number':None, 'function':'peer'
                     }
            _ixp_database_delete('switch')
            for n in range(1, 10):
                _ixp_database_update("'ipv4_man'", f"'{v4_net}24{n}/{v4_prefix}'", dict2_) 
                _ixp_database_update("'ipv6_man'", f"'{v6_net}24{n}/{v6_prefix}'", dict2_)    

            # // Call the IXP host build method //
            if (_switching_type() == 'traditional'):

                # // Drop and rebuild database tables //
                _ixp_database_rebuild('host', self.host_temp_dict)  

                # // Mask the real no. of interfaces in simulator mode //          
                if (simulator == 1):
                    response = self._ixp_host_build_trad({'mod':'A','ports': 1, 'switch': 1, 'int':[_ip_link_list()[0]]})                    
                elif (simulator == 2):
                    response = self._ixp_host_build_trad({'mod':'B', 'ports': 2, 'switch': 1, 'int':[_ip_link_list()[0], _ip_link_list()[1]]})
                else:
                    response = self._ixp_host_build_trad(dict_)

            elif (_switching_type() == 'software-defined'):
                if (simulator > 0):
                    print (f"ERROR: Cannot simulate in '{_switching_type()}' mode\n")

                # // Drop and rebuild database tables //
                _ixp_database_rebuild('host', self.host_temp_dict)          
                response = self._ixp_host_build_soft(dict_)

            else:
                pass

            if (response == 0):  
                _ixp_debug('y', f'INFO[ixp_host]: Completed configuration for {_switching_type()} IXP host')

        # // Show host interfaces //
        elif (cmd == 'show'):
            self._ixp_host_show()

        sys.exit(0)

    ## End to ixp_host method

############ // End IxpHost class // ############

#################################################
# //             IxpSwitch class             // #
#################################################

class IxpSwitch:
    ''' IXP Switch Class '''

    def __init__(self):
        ''' IxpSwitch class constructor method '''
        
        self.functionality = 'Manage switches defined when building the host'

        self.sw_cli_dict = {'-s':'--switch', 
                            '-sn':'--switch-name', 
                            '-st':'--switch-type', 
                            '-m':'--manufacturer', 
                            '-si':'--switch-interfaces', 
                            '-ei':'--enabled-interfaces',
                            '-is':'--interface-speed'
                           }       

        ## End of __init__ method
        
    def __str__(self):
        ''' IxpSwitch class __str__ method ''' 

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' IxpSwitch class __repr__ method ''' 

        return (f'{self.__class__.__name__} class: {self.functionality}')    

        ## End of __repr__ method  

    #============================================#
    # //       _ixp_switch_help_handler       // #
    #============================================#

    # // Help handler function //
    def _ixp_switch_help_handler(self, command, err_code):
        ''' switch help handler '''

        if (command == 'help'):
            IxpHelp._ixp_switch_help(self, err_code)
        elif (command == 'list'):
            IxpHelp._ixp_switch_help(self, err_code)
        elif (command == 'delete'):
            IxpHelp._ixp_switch_delete_help(self, err_code)
        elif (command == 'set'):
            IxpHelp._ixp_switch_set_help(self, err_code)
        elif (command == 'set_speed'):
            IxpHelp._ixp_switch_set_speed_help(self, err_code)
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        ## End to _ixp_switch_help_handler function

    #============================================#
    # //       _ixp_switch_test_options       // #
    #============================================#

    def _ixp_switch_test_options(self, args):
        ''' Method to handle the 'ixp switch' command '''

        str_ = str()
        dict_ = dict()
        first_options = ['list', 'set', 'delete', 'help']
 
        # // Replace any instance of 'show' with 'list' //
        args = ['list' if x == 'show' else x for x in args]

        # // Handle abbreviations //
        for x in first_options:
            if (x[:len(args[0])] == args[0]):
                args[0] = x

        # // Assign args to the dictionary //
        if (args[0] in first_options):
            dict_['cmd'] = args[0]
        else:
            str_ = ', '.join(x for x in first_options[:-1])
            print (f"ERROR: Not a valid choice, select '{str_} or {first_options[-1]}'\n")
            _ixp_debug('y',f"ERROR[ixp_switch]: Not a valid choice, select '{str_} or {first_options[-1]}'")                
            self._ixp_switch_help_handler('help', 1)            

        # // Handle one element //
        if (len(args) == 1):
            dict_['ins'] = ''
            dict_['elm'] = {}

        # // Handle two elements //
        elif (len(args) == 2):
            if (args[1] in ['list', 'help']):
                dict_['ins'] = ''
                dict_['elm'] = {}

            # // Handle help //
            if (args[1] == 'help'):
                self._ixp_switch_help_handler(dict_['cmd'], 0)  

            else:
                print (f"ERROR: Not a valid choice '{args[1]}'\n")
                _ixp_debug('y',f"ERROR[ixp_switch]: Not a valid choice '{args[1]}'")                
                self._ixp_switch_help_handler(dict_['cmd'], 1)

        # // Handle more that 2 elements //
        elif (len(args) > 2):

            # // Check for set speed //
            if (args[0] == 'set'[:len(args[1])] and args[1] == 'speed'[:len(args[1])]):
                dict_['cmd'] = 'set_speed'
                if ('help' in args[2:]):
                    self._ixp_switch_help_handler(dict_['cmd'], 0)   
                dict_['elm'] = _list_to_dict(args[2:])

            else:
                dict_['elm'] = args[1:] 
                if ('help' in args[1:]):
                    self._ixp_switch_help_handler(dict_['cmd'], 0)  
                dict_['elm'] = _list_to_dict(args[1:])

            # // Verify elements //
            dict_['elm'] = _validate_options(self.sw_cli_dict, dict_['elm'])

        # //Handle 'help' //
        if (dict_['cmd'] == 'help'):
            self._ixp_switch_help_handler('help', 0)

        # // Return dictionary //
        return(dict_)

    ## End to _ixp_switch_test_options method

    #============================================#
    # //              ixp_switch              // #
    #============================================#

    def ixp_switch(self, args):
        ''' Method to handle the 'ixp switch' command '''

        list_ = list()
        list4_ = list()
        list6_ = list()
        dict_ = dict()
        dict2_ = dict()
        dict3_ = dict()  
        speed_dict4 = {'100M': [], '1G': [], '10G': [], '40G': []}
        speed_dict6 = {'100M': [], '1G': [], '10G': [], '40G': []}  
        start_port = 2 

        dict_ = self._ixp_switch_test_options(args)

        # //Handle 'list' //
        if (dict_['cmd'] == 'list'):

            dict2_ = _read_db('switch')

            # // Handle no entries //
            if (dict2_ == {}):
                print (f'INFO: There are no switches set yet\n')
                _ixp_debug('y',f'INFO[ixp_switch]: There are no switches set yet')
                sys.exit(1)

            # // Loop through switches //
            for k,v in dict2_.items():
                print (f'Switch No. {k}')
                print (f"-----------{'-'* len(str(k))}\n")
                for m,n in v.items():
                    if (m == 'switch_interfaces'):                     
                        list4_ = list(_read_db('ipv4_peer', 'switch_number', k, 'function', "'peer'", 'reserved', '1').values())
                        list6_ = list(_read_db('ipv4_peer', 'switch_number', k, 'function', "'peer'", 'reserved', '1').values())
                        for d in list4_:
                            speed_dict4[d['interface_speed']].append(d['port_number'])
                        for d in list6_:
                            speed_dict6[d['interface_speed']].append(d['port_number'])
                        if not (speed_dict4 == speed_dict6):
                            print('ERROR: Database tables out of sync\n')
                            ixp_debug('y', 'ERROR[ixp_switch]: Database tables out of sync\n')
                        m = m.replace('_',' ')
                        m = m.title()
                        print (f"{m:<18} : {n}")
                        for y,z in speed_dict4.items():
                            if not (_is_empty(z) == True):
                                s = _stringifyRanges(z)
                                print(f"{'':<21}{y:<4} {s}")
                        continue
                    if ('4' in m):
                        m = 'OOB IPv4 address'
                    elif ('6' in m):
                        m = 'OOB IPv6 address'
                    else:
                        pass
                    m = m.replace('_',' ')
                    m = m.title()
                    print (f"{m:<18} : {n}")
                print ('')

            _ixp_debug('y',f"INFO[ixp_switch]: Calling 'ixp switch list")
            sys.exit(0)

        # // Determine the switch // 
        if ('switch' in dict_['elm']):
            dict_['ins'] = dict_['elm']['switch']
            del dict_['elm']['switch']
        else:
            print (f'ERROR: A switch is not specified (-s, --switch)\n')
            _ixp_debug('y',f'ERROR[ixp_switch]: A switch is not specified (-s, --switch)')
            self._ixp_switch_help_handler(dict_['cmd'], 0) 

        # //Handle 'delete' //
        if (dict_['cmd'] == 'delete'):

            str_ = str()

            # // Prevent deleting information from internal interface //
            if (dict_['ins'] == '0'):
                print (f"ERROR: Deleting information from the internal switch '{dict_['ins']}' is not permitted\n")
                _ixp_debug('y', f"ERROR[ixp_switch]: Deleting information from the internal switch '{dict_['ins']}' is not permitted")  
                sys.exit(1)  

            # // Clear flows from the switch via the SDN Controller //
            if (_switching_type() == 'software-defined'):
                IxpSdn._clear_flows(self, dict_['ins'])

            # // Message about surplus options //
            if not (dict_['elm'] == {}):
                str_ = ', '.join(list(dict_['elm'].keys()))
                print (f"The options '{str_}' are unnecessary for the delete command\n")
                _ixp_debug('y', f"INFO[ixp_switch]: The options '{str_}' are unnecessary for the delete command")

            # // Default information in IPv4 and IPv6 tables relating to this switch(s) //
            temp_dict = {'port_number':None, 'name':None, 'assigned':None, 
                          'as_number':None, 'domain':None, 'interface_speed': None,
                          'route_server': None, 'blackhole_server': None,
                          'enabled':None, 'reserved':0, 'switch_number':None,
                         }

            # // Apply to both IPv4 and IPv6 tables // 
            for y in ['ipv4_peer','ipv6_peer']:
                _ixp_database_update(y, '', temp_dict, 'switch_number', dict_['ins'])
                y = y.replace('_', ' ')
                print (f"Deleted switch '{dict_['ins']}' data from the {y} table in the database\n")
                _ixp_debug('y', f"INFO[ixp_switch]: Deleted switch '{dict_['ins']}' from the {y} table in the database")

            # // Default the interfaces in the switch table //
            _ixp_database_update('switch', dict_['ins'], {'switch_interfaces': '0', 'enabled_interfaces': '0'}) 
            print (f"Removed interfaces from switch '{dict_['ins']}' data in the 'switch' table in the database\n")
            _ixp_debug('y', f"INFO[ixp_switch]: Removed interfaces from switch '{dict_['ins']}' data in the 'switch' table in the database")

        # // Handle 'set' //
        elif (dict_['cmd'] == 'set'):

            list_ = list()
            list2_ = list()

            # // Prevent deleting information from internal interface //
            if (dict_['ins'] == '0'):
                print (f"ERROR: Setting information on the internal switch '{dict_['ins']}' is not permitted\n")
                _ixp_debug('y', f"ERROR[ixp_switch]: Setting information on the internal switch '{dict_['ins']}' is not permitted")  
                sys.exit(1)  

            # // Handle traditional switching //
            if (_switching_type() == 'traditional'):

                # // If no enabled interfaces set //
                if not ('enabled_interfaces' in dict_['elm']):
                    dict_['elm']['enabled_interfaces'] = 'all'

                # // Traditional models reserve //
                     # - port 1 for trunk 
                     # - port 2 for man VLAN
                     # - remainder for peer VLAN 

                start_port = 3 

                # // Test for 'all' and replace //                 
                if not (dict_['elm']['enabled_interfaces'] == 'all'):
                    print (f"INFO: In the traditional model all interfaces are enabled\n")
                    _ixp_debug('y',f"INFO[ixp_switch]: In the traditional model all interfaces are enabled")
                    dict_['elm']['enabled_interfaces'] = _rangeStr(start_port, int(dict_['elm']['switch_interfaces']))

                elif not ('switch_interfaces' in dict_['elm'].keys()):
                    print (f"ERROR: In the traditional model switch interfaces (-si, --switch-interfaces) must be given\n")
                    _ixp_debug('y',f"ERROR[ixp_switch]: In the traditional model switch ports (-si, --switch-interfaces) must be given")
                    self._ixp_switch_help_handler('help', 0)

            # // Handle 'all' and options around interfaces for 'software-defined' model//
            elif (_switching_type() == 'software-defined'):

                # // If no enabled interfaces set //
                if not ('enabled_interfaces' in dict_['elm']):
                    dict_['elm']['enabled_interfaces'] = 'all'

                # // Switch type necessary as OvS doesn't have an OOB interface //
                if not ('switch_type' in dict_['elm'].keys()):
                    print (f'ERROR: Switch type is necessary in software-defined models, i.e. ovs, hp, netgear\n')
                    _ixp_debug('y',f'ERROR[ixp_switch]: Switch type is necessary in software-defined models, i.e. ovs, hp, netgear')
                    self._ixp_switch_help_handler('set', 1)  

                # // The OvS switch doesn't have an OOB interface so an additional interfaces needs to be reserved //
                elif (dict_['elm']['switch_type'].lower() == 'ovs'):
                    start_port = 3 

                if ('switch_interfaces' in dict_['elm'].keys() and 'enabled_interfaces' not in dict_['elm'].keys()):
                    list_ = _rangeStr(start_port, int(dict_['elm']['switch_interfaces'])) 
                    dict_['elm']['enabled_interfaces'] = list_

                elif ('enabled_interfaces' in dict_['elm'].keys() and 'switch_interfaces' not in dict_['elm'].keys()):
                    print (f'ERROR: Cannot enable interfaces when no switch interfaces given\n')
                    _ixp_debug('y',f'ERROR[ixp_switch]: Cannot enable interfaces when no switch interfaces given')
                    self._ixp_switch_help_handler('help', 0)
            else:
                assert False, 'ERROR: Not sure how we arrived here'

            # // Test for 'all' and replace //
            if (dict_['elm']['enabled_interfaces'] == 'all'):
                dict_['elm']['enabled_interfaces'] = _rangeStr(start_port, int(dict_['elm']['switch_interfaces']))

            # // Test for more enabled ports than switched ports //
            if (len(dict_['elm']['enabled_interfaces']) > int(dict_['elm']['switch_interfaces'])):
                print (f'ERROR: Cannot enable more interfaces than there are switch interfaces\n')
                _ixp_debug('y',f'ERROR[ixp_switch]: Cannot enable more interfaces than there are switch interfaces')
                self._ixp_switch_help_handler('help', 0)

            elif (int(max(dict_['elm']['enabled_interfaces'])) > int(dict_['elm']['switch_interfaces'])):
                print (f"ERROR: Cannot have an enabled interface with a greater number than {dict_['elm']['switch_interfaces']}\n")
                _ixp_debug('y',f"ERROR[ixp_switch]: Cannot have an enabled interface with a greater number than {dict_['elm']['switch_interfaces']}")
                self._ixp_switch_help_handler('help', 0)

            else:
                pass

            # // Update the 'switch' table in the database //
            for k, v in dict_['elm'].items():
                _ixp_database_update('switch', dict_['ins'], {f'{k}':f'{v}'}) 
                _ixp_debug('y', f"INFO[ixp_switch]: Uploading switch information for the switch '{dict_['ins']}")      

            # // Check if this switch has been assigned in the IPv4 and IPv6 peer tables in the database already //
            flag = 0
            for x in ['ipv4_peer','ipv6_peer']:                
                list2_ = _read_db(x, 'switch_number', int(f"{dict_['ins']}"))

                if (_is_empty(list2_) == False):
                    y = x.replace('_', ' ')
                    print (f"ERROR: Switch {dict_['ins']} is already configured for {y}\n")
                    _ixp_debug('y', f"ERROR[ixp_switch]: Switch {dict_['ins']} is already configured for {y}")
                    flag += 1

            if (flag == 0):
                # // Get Peer IP addresses where interface number is unassigned and assign interfaces dynamically //
                for x in ['ipv4_peer','ipv6_peer']:
                    dict2_ = _read_db(x, 'function', "'peer'", 'reserved', '0')
                    port_list = list(range (start_port, int(dict_['elm']['switch_interfaces']) + 1))
                    for n in port_list:
                        dict3_ = {'switch_number':int(dict_['ins']),
                                     'port_number': n,
                                     'interface_speed': '1G',                                     
                                     'enabled': 0,
                                     'reserved': 1
                                    }
                        _ixp_database_update(x, f"'{list(dict2_.keys())[n - start_port]}'", dict3_) 
                        _ixp_debug('y', f"INFO[ixp_switch]: Adding switch information to IP peer tables") 

            # // Enable interfaces (all in the case of traditional and selective in software-defined) // 
            for x in ['ipv4_peer','ipv6_peer']:
                for y in _parse_range_list(dict_['elm']['enabled_interfaces']): 
                    _ixp_database_update(x, '', {'enabled': 1, 'reserved': 1}, 'switch_number', int(f"{dict_['ins']}"), 'port_number', y)

            print (f"Switch {dict_['ins']} configuration complete")
            _ixp_debug('y', f"INFO[ixp_switch]: Switch {dict_['ins']} configuration complete")  
            sys.exit(0)  

        # //Handle 'set_speed' //
        elif (dict_['cmd'] == 'set_speed'):

            str_ = str()
            list_ = list()
            list2_ = list()

            if not ('enabled_interfaces' in dict_['elm'].keys()):
                print('ERROR: Enabled interfaces must be given\n')
                _ixp_debug('y', 'ERROR[ixp_switch]: Enabled interfaces must be given')
                sys.exit()
            elif not ('interface_speed' in dict_['elm'].keys()):
                print('ERROR: Interface speed must be given for specified switch interfaces\n')
                _ixp_debug('y', 'ERROR[ixp_switch]: Interface speed must be given for specified switch interfaces')
                sys.exit()

            # // Get switch enabled interfaces list //
            str_ = _read_db('switch')[int(dict_['ins'])]['enabled_interfaces']
            if (str_ == None):
                print(f"There are no enabled interfaces on switch: {dict_['ins']} yet\n")
                _ixp_debug('y', f"There are no enabled interfaces on switch: {dict_['ins']} yet")
                sys.exit(1)
            list_ = _parse_range_list(_read_db('switch')[int(dict_['ins'])]['enabled_interfaces'])

            #// Get the list of interfaces to be configured //
            list2_ = _parse_range_list(dict_['elm']['enabled_interfaces'])

            # // Check if given interfaces are actually enabled //
            if not (set(list2_).issubset(set(list_))): 
                print(f"Given interfaces are not enabled on switch: {dict_['ins']} yet\n")
                _ixp_debug('y', f"Given interfaces are not enabled on switch: {dict_['ins']} yet")
                sys.exit(1)                

            # // Update the database //
            for x in ['ipv4_peer', 'ipv6_peer']:
                for y in list2_:
                    speed = dict_['elm']['interface_speed'].upper()
                    _ixp_database_update(x, '', {'interface_speed': f"{speed}"}, 'switch_number', int(f"{dict_['ins']}"), 'port_number', y)

            print (f"Switch {dict_['ins']} port speed configuration complete")
            _ixp_debug('y', f"INFO[ixp_switch]: Switch {dict_['ins']} configuration complete")  
            sys.exit(0)  

        sys.exit(0)

    ## End to ixp_switch method

########## // End IxpSwitch class // ##########

#################################################
# //             IxpServer class             // #
#################################################

class IxpServer:
    ''' IXP Server Class '''

    def __init__(self):
        '''IxpServer class constructor method'''
 
        self.functionality = 'Install LXC container servers to provide IXP Core functions'

        self.dualnic_template = 'dualnic_template.yaml'
        self.br900nic_template = 'br900nic_template.yaml'
        self.container_yaml_template = 'ixp_container_template.yaml'
        self.sc_container_yaml_template = 'ixp_sc_container_template.yaml'

        ## End of __init__ method
        
    def __str__(self):
        ''' IxpServer class __str__ method ''' 

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' IxpServer class __str__ method ''' 

        return (f'{self.__class__.__name__} class: {self.functionality}')    

        ## End of __repr__ method  

    #============================================#
    # //       _ixp_server_help_handler       // #
    #============================================#

    # // Help handler function //
    def _ixp_server_help_handler(self, command, err_code):
        ''' server help handler '''

        if (command == 'help'):
            IxpHelp._ixp_server_help(self, err_code)
        elif (command == 'show'):
            IxpHelp._ixp_server_show_help(self, err_code)
        elif (command == 'start'):
            IxpHelp._ixp_server_start_help(self, err_code)
        elif (command == 'stop'):
            IxpHelp._ixp_server_stop_help(self, err_code)
        elif (command == 'delete'):
            IxpHelp._ixp_server_delete_help(self, err_code)
        elif (command == 'build'):
            IxpHelp._ixp_server_build_help(self, err_code)

        ## End to _ixp_server_help_handler function

    #============================================#
    # //      _non_schema_instances_test      // #
    #============================================#

    def _non_schema_instances_test(self, skip_yes_no):
        ''' Method to check if there are non schema instances '''

        # // Shouldn't happen but is possible if messing with site numbers //

        non_schema_list = list()

        # // Get list of IXP Schema servers
        e = _ixp_schema_servers()

        #// Get list of container instances on hypervisor (hide templates) //
        lxc_list = _IxpLxc._lxc_hypervisor_containers(self)  

        # // Remove phantom entry //
        if ('' in lxc_list):
            lxc_list.remove('')

        # // Remove template //        
        if (server_template[0] in lxc_list):
            lxc_list.remove(server_template[0])   

        # // Remove template backup //      
        if (server_template_bak in lxc_list):
            lxc_list.remove(server_template_bak)  

        # // Loop through hypervisor containers to check if they are in the schema //
        for c in lxc_list:
            if not c in e:  
                non_schema_list.append(c)

        # // Go through non schema container instances //
        if (len(non_schema_list) > 0):

            print ('The following non IXP schema LXC instances exist\n')

            if (skip_yes_no == False):

                for e in non_schema_list:
                    print (e, end = '  ')
                ans = input('\n\nDelete these instances (y|n) > ')
                ans = ans.lower()
                print ('')

                if (ans == 'yes'[:len(ans)]):
                    files_list = listdir(var_dir)

                    for e in non_schema_list:
                        print (f'Deleting LXC instance {e}\n')
                        subprocess.getstatusoutput (f'lxc delete {e} --force')

                        for f in files_list:

                            if (e in f):
                                remove(f'{var_dir}{f}')
                                _ixp_debug('y',f'INFO[non_schema_instances_test]: Deleting the file {var_dir}{f}')

                    print (f'LXC instance {e} deleted\n')

                elif (ans == 'no'[:len(ans)]):
                    print ('OK that is fine\n')

                else:
                    print ('ERROR[non_schema_instances_test]: Only (y|n) please\n')
                    _ixp_debug('y','ERROR[non_schema_instances_test]: Only (y|n) please')
                    sys.exit(1)

           # // Deal with '-y' scenario //
            else:                

                files_list = listdir(var_dir)
                for e in non_schema_list:
                    print (f'Deleting LXC instance {e}\n')
                    subprocess.getstatusoutput (f'lxc delete {e} --force')

                    for f in files_list:
                        if (e in f):
                            remove(f'{var_dir}{f}')
                            _ixp_debug('y',f'INFO[non_schema_instances_test]: Deleting the file {var_dir}{f}')

                    print (f'LXC instance {e} deleted\n')               
     
        return (0)

    ## End _non_schema_instances_test method

    #============================================#
    # //              _nic_profile            // #
    #============================================#

    def _nic_profile(self, c, nicprofile):
        ''' Method to test for and/or assign dualnic profile to container method '''

        # // Test if the nic profile exists //
        test_tuple = subprocess.getstatusoutput ('lxc profile list')

        if (nicprofile in test_tuple[1]):  
            _ixp_debug('y',f'INFO[_nic_profile]: {nicprofile} profile already exists')   
     
        else:
            print (f'No {nicprofile} profile on {c}, creating LXC profile\n')
            _ixp_debug('y',f'INFO[_nic_profile]: No {nicprofile} profile on {c}, creating LXC profile')
            subprocess.getstatusoutput (f'lxc profile create {nicprofile}')

            # // Create required profile //
            if (nicprofile == 'dualnic'):
                subprocess.getstatusoutput (f'lxc profile edit {nicprofile} < {var_dir}{self.dualnic_template}')
            elif (nicprofile == 'br900nic'):
                subprocess.getstatusoutput (f'lxc profile edit {nicprofile} < {var_dir}{self.br900nic_template}')
            else: 
                subprocess.getstatusoutput (f'cp {var_dir}{self.dualnic_template} /tmp/{c[:2]}0nic')
                _string_search_replace(f'/tmp/{c[:2]}0nic', 'br100', f'{c[:2]}0')
                subprocess.getstatusoutput (f'lxc profile edit {nicprofile} < /tmp/{c[:2]}0nic')
                subprocess.getstatusoutput (f'rm /tmp/{c[:2]}0nic')  

        # // Assign profile to container server //
        subprocess.getstatusoutput (f'lxc profile assign {c} {nicprofile}') 
        _ixp_debug('y',f'INFO[_nic_profile]: Assigned {nicprofile} profile to the container template {c}')

        return (0)

    ## End _nic_profile method
 
    #============================================#
    # //        _container_netplan_yaml       // #
    #============================================#

    def _container_netplan_yaml(self, c):
        ''' Method to test for, create and apply netplan for containers '''

        container_file_orig = '/etc/netplan/50-cloud-init.yaml'
        container_file = '/etc/netplan/10-netplan.yaml'
        host_backup = f'{var_dir}netplan_bak/{c}-50-cloud-init.yaml'
        container_yaml_file = f'{var_dir}ixp_{c}.yaml'
        local_file = f'{var_dir}ixp_{c}.yaml'

        # // Test container status, if stopped start //
        _IxpLxc._lxc_status_up(self, c)[1]

        # // Test if this is a SDN Controller //
        if (_switching_type() == 'software-defined' and 'sc' in c):
            # // Assign br900nic profile to template //
            _ixp_debug('y',f'INFO[_container_netplan_yaml]: Assigning br900nic profile') 
            self._nic_profile(c, 'br900nic')
            template_file = f'{var_dir}{self.sc_container_yaml_template}'

        elif not (c == server_template[0]): 
            self._nic_profile(c, f'{c[:2]}0nic')
            template_file = f'{var_dir}{self.container_yaml_template}'

        else:
            # // Assign dualnic profile to template //
            self._nic_profile(c, 'dualnic')
            template_file = f'{var_dir}{self.container_yaml_template}'

        # // Check if the netplan is already on the container // 
        test = _IxpLxc._lxc_file_test(self, c, container_file)
        if (test == 'yes'):
            _ixp_debug('y', f'INFO[_container_netplan_yaml]: Existing {container_file} on {c}, deleting')
            _IxpLxc._lxc_rm(self, c, container_file) 

        ct_dict = dict()
        ct_dict['ns_ipv4_man'] = upstream_ns[0]    

        # // Generate YAML file for template container //
        if (c == server_template[0]): 

            # // Get Nameserver name //
            NC = [x for x in _read_db('schema').keys() if x[:2] == 'ns'][0]

            # // Swap 231 for 250 in IP addresses //
            ct = _read_db('schema')[NC]

            for k, v in ct.items():
                if ('231/' in str(v)):
                    r = v.replace('231/', '250/')
                    ct_dict[k] = r
                if ((k == 'ipv4_man') or (k == 'ipv6_man')):                
                    n = re.findall(r'\w+(\d)_\w+', k)
                    r = re.sub('23\d/\d+', '1', v)
                    ct_dict[f'gateway{n[0]}'] = r
                if (k == 'domain'):
                    ct_dict[k] = v

        # // Generate YAML file for containers in schema //       
        elif (c in _ixp_schema_servers('all')):
            svr_template = _read_db('schema')[c]
            for k, v in svr_template.items():
                ct_dict[k] = v 
                if ((k == 'ipv4_man') or (k == 'ipv6_man')):
                    n = re.findall(r'\w+(\d)_\w+', k)
                    r = re.sub('23\d/\d+', '1', v)
                    ct_dict[f'gateway{n[0]}'] = r 
                if (k == 'domain'):
                    ct_dict[k] = v
        else:
            print (f'ERROR: {c} is not a valid container\n')  
            _ixp_debug('y', f'ERROR[_container_netplan_yaml]: {c} is not a valid container') 

        _ixp_debug('y', f'INFO[_container_netplan_yaml]: Generating netplan YAML file for {c}')

        # // Check the IXP container template YAML file exists //        
        if (path.isfile(template_file)):
            subprocess.getstatusoutput (f'cp {template_file} {container_yaml_file}') 
            _ixp_debug('y',f'INFO[_container_netplan_yaml]:  Created the file {container_yaml_file}')

            # // Change file permissions //
            subprocess.getstatusoutput (f'chmod {file_perm} {container_yaml_file}')

            # // Print file characteristics //
            file_detail = _get_file_dir_info(container_yaml_file)
            _ixp_debug('n', f"  {file_detail['name']}")
            _ixp_debug('n', f"    - Owner       : {file_detail['owner']}")
            _ixp_debug('n', f"    - Group       : {file_detail['group']}")
            _ixp_debug('n', f"    - Permissions : {file_detail['permissions']}")
            _ixp_debug('n', '')

        else:
            print (f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y',f'ERROR[_container_netplan_yaml]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Generate new file and edit it //
        for e in ct_dict:
            _string_search_replace(container_yaml_file, f'<<{e}>>', ct_dict[e])

        # // Pull down default netplan file from container //        
        pull_file = _IxpLxc._lxc_pull(self, c, container_file_orig, host_backup)

        if not (pull_file == 0):
            _ixp_debug('y',f'ERROR[_container_netplan_yaml]: Problem pulling {container_file_orig} from {c}')

        else:   
            _ixp_debug('y',f'INFO[_container_netplan_yaml]: Pulling default netplan file from {c}')

            # // Change file permissions //
            subprocess.getstatusoutput (f'chmod {file_perm} {host_backup}')    

            # // Print file characteristics //
            file_detail = _get_file_dir_info(host_backup)
            _ixp_debug('n', f"  {file_detail['name']}")
            _ixp_debug('n', f"    - Owner       : {file_detail['owner']}")
            _ixp_debug('n', f"    - Group       : {file_detail['group']}")
            _ixp_debug('n', f"    - Permissions : {file_detail['permissions']}")
            _ixp_debug('n', '')
        
        # // Push new template to the container server //        
        push_file = _IxpLxc._lxc_push(self, c, local_file, container_file)
                
        if not (push_file == 0):
            _ixp_debug('y',f'ERROR[_container_netplan_yaml]: Problem pushing {container_file} to {c}')
            sys.exit(1)

        else:    
            _ixp_debug('y',f'INFO[_container_netplan_yaml]: Pushing new netplan file to {c}')
        
        # // Remove original netplan file from container //            
        _IxpLxc._lxc_rm(self, c, container_file_orig)     

        # // Applying new netplan file on container //                
        netplan_apply = _IxpLxc._lxc_exec(self, c, 'netplan apply')
        
        if not (netplan_apply[0] == 0):
            print (f'ERROR: Cannot apply netplan on {c}\n')
            _ixp_debug('y',f'ERROR[_container_netplan_yaml]: Cannot apply netplan on {c}')
            sys.exit(1)
        else:    
            _ixp_debug('y',f'INFO[_container_netplan_yaml]: New netplan applied on {c}')

        # // Reboot container //                
        _IxpLxc._lxc_exec(self, c, 'shutdown -r now')

        counter = 0
        # // Short sleep to allow container to come up //
        for n in range(1,4):
            time.sleep(5)
            counter += 1

        return (0)
     
    ## End _container_netplan_yaml method

    #============================================#
    #  //        _lxc_container_image         // #
    #============================================#

    def _lxc_container_image(self):
        ''' Method to download a container template '''

        ixp_lxc_status, ixp_lxc_state = _IxpLxc._lxc_status(self, server_template[0])

        if (ixp_lxc_status == 1):
            print (f'Container {server_template[0]} template doesn\'t exist, checking for backup\n')
            _ixp_debug('y',f'INFO[_lxc_container_image]: Container {server_template[0]} template doesn\'t exist, checking for backup')
            ixp_lxc_bak_status, ixp_lxc_bak_state = _IxpLxc._lxc_status(self, server_template_bak)

            if (ixp_lxc_bak_status == 1):
                print (f'No backup template either, downloading, please be patient\n')
                _ixp_debug('y',f'INFO[_lxc_container_image]: No {server_template_bak} backup either, downloading, please be patient')  
                subprocess.getstatusoutput (f'lxc launch {server_template[1]} {server_template[0]}') 

                # // Assign dualnic profile to template //
                _ixp_debug('y',f'INFO[_lxc_container_image]: Assigning dualnic profile') 
                self._nic_profile(server_template[0], 'dualnic')
                # // Generate and assign netplan YAML file to template //
                self._container_netplan_yaml(server_template[0])
                # // Update and upgrade the container template, then stop //
                print (f'Upgrading the {server_template[0]}, please be patient\n')
                _ixp_debug('y',f'INFO[lxc_get_container_image]: Upgrading the {server_template[0]}, please be patient')
                _IxpLxc._lxc_upgrade(self, server_template[0]) 
                _IxpLxc._lxc_stop(self, server_template[0])  
      
                # // Backup the template //
                print (f'Creating {server_template_bak} as backup for {server_template[0]}, please be patient\n')
                _ixp_debug('y',f'INFO[lxc_get_container_image]: Creating {server_template_bak} as backup for {server_template[0]}')
                subprocess.getstatusoutput (f'lxc copy {server_template[0]} {server_template_bak}')

            else:
                # // Copy from the backup template //
                print (f'Cloning {server_template[0]} from {server_template_bak}, please be patient\n')
                _ixp_debug('y',f'INFO[lxc_get_container_image]: Cloning {server_template[0]} from {server_template_bak}, please be patient')
                subprocess.getstatusoutput (f'lxc copy {server_template_bak} {server_template[0]}')        

        # // If container already exists //
        elif (ixp_lxc_status == 0):

            # // Test the current state of template container image //
            _IxpLxc._lxc_start(self, server_template[0])
            if (ixp_lxc_status == 0):
                _ixp_debug('y',f'INFO[_lxc_get_container_image]: {server_template[0]} already exists and is in the state {ixp_lxc_state}')
                if (ixp_lxc_state == 'Running'):
                    print (f'Upgrading the {server_template[0]}, please be patient\n')
                    _ixp_debug('y',f'INFO[lxc_get_container_image]: Upgrading the {server_template[0]}, please be patient')
                    _IxpLxc._lxc_upgrade(self, server_template[0])
                    _IxpLxc._lxc_stop(self, server_template[0])
                elif (ixp_lxc_state == 'Stopped'):
                    _IxpLxc._lxc_start(self, server_template[0])
                    print (f'Upgrading the {server_template[0]}, please be patient\n')
                    _ixp_debug('y',f'INFO[lxc_get_container_image]: Upgrading the {server_template[0]}, please be patient')                   
                    _IxpLxc._lxc_upgrade(self, server_template[0])
                    _IxpLxc._lxc_stop(self, server_template[0])

                else:
                    print (f'ERROR: {server_template[0]} in state {ixp_lxc_state}\n')
                    _ixp_debug('y',f'ERROR[lxc_get_container_image]: {server_template[0]} in state {ixp_lxc_state}')
                    sys.exit(1)

            # // Ooops what is this status ? //
            else:
                print (f'ERROR: {server_template[0]} in state {ixp_lxc_state}\n')
                _ixp_debug('y',f'ERROR[lxc_get_container_image]: {server_template[0]} in state {ixp_lxc_state}')
                sys.exit(1)

            # // Generate and assign netplan YAML file to template //
            self._container_netplan_yaml(server_template[0])

            # // Update and upgrade the container template, then stop //
            _IxpLxc._lxc_upgrade(self, server_template[0])
            _IxpLxc._lxc_stop(self, server_template[0]) 

        return (0)

    ## End _lxc_container_image method 

    #============================================#
    #  //         _ixp_server_build           // #
    #============================================#
    
    def _ixp_server_build(self, skip_yes_no, args):
        ''' Method to build the server '''

        # // Active IXP Schema elements //
        e = _ixp_schema_servers()

        # // Test against server list //
        if not (set(args).issubset(e)):
            for x in args:             
                if ('sc' in x and _switching_type() == 'traditional'):
                    print ('An SDN Controller is unnecessary in a traditional model\n')
                    _ixp_debug('y', 'INFO[_ixp_server_build]: An SDN Controller is unnecessary in a traditional model')

                    # // Remove SDN Controller from the args list //
                    args.remove(x)

                    # // Test if LXC container already exists //
                    if not (_IxpLxc._lxc_status(self, x)[1] == 'Nil'):
                        _ixp_debug('y', 'INFO[_ixp_server_build]: Deleting the container {x}')
                        _IxpLxc._lxc_delete(self, True, x)

        # // Check if there is a template image //
        print (f'Checking on, updating and upgrading the template {server_template[0]}\n')
        _ixp_debug('y', f'INFO[_ixp_server_build]: Checking on, updating and upgrading the template {server_template[0]}')
        self._lxc_container_image()

        # // Non-schema instance test //
        self._non_schema_instances_test(skip_yes_no)

        # // Loop through container server list and create from clone template //
        for c in args:
    
            # // Clone servers //
            print (f'Cloning server {c} now\n')   
            _ixp_debug('y',f'INFO[_ixp_server_build]: Cloning container {c} now')  
            _IxpLxc._lxc_clone(self, skip_yes_no, server_template[0], c)
    
            # // Start the container servers //
            _IxpLxc._lxc_start(self, c)
    
            # // Prioritise IPv4 over IPv6 as if no public IPv6 network //
            # // # (No IPv6 public address for testbed lab) //
            if (public_ipv6_net == False):
                gai_temp_file = _gai_build()
                gai_file_bu = f'{gai_file}.orig'
    
                # // Add the new GAI file // 
                _IxpLxc._lxc_exec(self, c, f'cp {gai_file} {gai_file_bu}')
                _IxpLxc._lxc_push(self, c, gai_temp_file, '/etc/gai.conf')
                _ixp_debug('y','INFO[_ixp_server_build]: Network does not have an IPv6 public connection')

            # // Public IPv6 network exists //
            else:

                # // Message to confirm IPv6 public network exists //     
                _ixp_debug('y','INFO[_ixp_server_build]: Network has an IPv6 public connection')

            # // Set the server to autostart after boot //            
            _ixp_debug('y',f'INFO[_ixp_server_build]: Setting the {c} container to boot autostart on startup')
            subprocess.getstatusoutput (f'lxc config set {c} boot.autostart true') 

            # // Generate and assign netplan YAML file to each container server //
            self._container_netplan_yaml(c)       

        # // Restart host networking service //
        restart_net = [f'systemctl restart ovs-config.service']
        print ('Restarting the IXP host ovs-config service\n')

        response = _sudo_call('bash', restart_net)
        if (response[0] == 0):
            _ixp_debug('y',f'INFO[_ixp_server_build]: Restarted the host ovs-config service')
        else:
            _ixp_debug('y',f'ERROR[_ixp_server_build]: Failed to restarted the host ovs-config service') 
            sys.exit(1)

        # // Output status of container //       
        print ('\nContainer state summary\n-----------------------')
               
        for c in args:
            _IxpLxc._lxc_restart(self, c)
            pad = (max(len(x) for x in args) + 3 )
            status = _IxpLxc._lxc_status(self, c)[1]
            pad = pad - len(c)
            print (f'{c:<{pad}}: {status}')
    
        print ('')

        return (0)
    
    ## End of _ixp_server_build method

    #============================================#
    # //       _ixp_server_test_options       // #
    #============================================#

    def _ixp_server_test_options(self, args):
        ''' Method to test 'ixp_server' cli options '''

        str_ = str()        
        list_ = list()
        dict_ = dict()
        cmd = str()        
        ct_flag = False
        skip_yes_no = False
        first_options = ('show', 'start', 'stop', 'delete', 'build')  

        # // Iterate over strings in list and lowercase them //
        args = [string.lower() for string in args]

        # // IXP Schema elements //
        e = _ixp_schema_servers()

        # // Lowercase any instance of 'help' and swap 'help' for '?'//
        args = ['help' if x == '?' else x for x in args]
        args = ['help' if x.lower() == 'help'[:len(x)] else x for x in args]

        #// Define command //
        cmd = args[0]

        # // Check if help called first //
        if (cmd in help_list): 
            self._ixp_server_help_handler('help', 1)

        # // Handle Abbreviations //
        if (cmd == 's'):
            print ('Choose between:  show  start  stop\n')
            self._ixp_server_help_handler('help', 1)
        if (cmd == 'st'):
            print ('Choose between:  start  stop\n')
            self._ixp_server_help_handler('help', 1)
        else:
            for x in first_options:
                if (cmd == x[:len(cmd)]):      
                    cmd = x

        # // Check first options are legitimate //
        if not (cmd in first_options):
            print (f'ERROR: ixp server \'{cmd}\' not valid option\n')
            _ixp_debug('y',f'ERROR: ixp server \'{cmd}\' not valid option')
            self._ixp_server_help_handler('help', 1)

        # // Create element list from remaining elements //
        if (len(args) > 1):
            list_ = args[1:]
            list_ = list(set(list_))  # Remove duplicates   

        # // Check if help required //
        if ('help' in args): 
            self._ixp_server_help_handler(cmd, 0)
        else:
            pass

        # // Handle '-y' //
        if (len(list_) >= 1 and '-y' in list_):
            skip_yes_no = True
            list_.remove('-y')

        # // Handle connectivity option //
        if (cmd == 'show'):
            if ('-ct' in list_):
                ct_flag = True
                list_.remove('-ct')
            if ('--connectivity-test' in list_):
                ct_flag = True
                list_.remove('--connectivity-test')

        # // Handle elements //
        if (len(list_) == 0):
            list_ = e
            print (f'No container options given, assuming \'all\'\n')
            _ixp_debug('y',f'INFO[_ixp_server_test_options]: No container options given, assuming \'all\'')
        elif (list_[0] == 'all'):
            list_ = e
        elif (len(list_) >= 1):
           
            # // Handle an 'all' //
            if ('all' in list_): 
                list_ = e

            # // Remaining options, test for illegal options //
            list2_ = list()
            for x in list_:
                if (x in e):
                    pass
                else:
                    list2_.append(x)
            if (len(list2_) == 1):
                print (f'ERROR: \'{list2_[0]}\' is not a valid option\n')
                _ixp_debug('y',f'ERROR[_ixp_server_test_options]: \'{list2_[0]}\' is not a valid option')
                sys.exit(1)

            elif (len(list2_) > 1):
                str_ = ", ".join(str(x) for x in list2_)
                print (f'ERROR: \'{str_}\' are not valid options\n')
                _ixp_debug('y',f'ERROR[_ixp_server_test_options]: \'{str_}\' are not valid options')
                sys.exit(1)

            else:
                pass
               
        else:
            print ('ERROR: Some option I havent thought of yet\n')
            _ixp_debug('ERROR[_ixp_server_test_options]: Some option I havent thought of yet')

        # // Create return dictionary //
        dict_['cmd'] = cmd
        dict_['ins'] = list_

        # // At ct_flag if set //
        dict_['ct'] = ct_flag 

        return (skip_yes_no, dict_)

    ## End _ixp_server_test_options method

    #============================================#
    # //               ixp_server             // #
    #============================================#

    def ixp_server(self, args):
        ''' Method to handle 'ixp server' dict_['cmd']s '''

        dict_ = dict()
        list_ = list()

        (skip_yes_no, dict_) = self._ixp_server_test_options(args)

        # // Check for non-schema instances //
        self._non_schema_instances_test(skip_yes_no)

        # // Check if 'build' called //
        if (dict_['cmd'] == 'build'):

            # // Build out the server instances //
            self._ixp_server_build(skip_yes_no, dict_['ins'])

            sys.exit(0)

        # // If first argument is 'show' //
        elif (dict_['cmd'] == 'show'): 

            # // Test container status //
            for x in dict_['ins']:
                status = _IxpLxc._lxc_status(self, x)

                if (status[1] == 'Nil'):
                    print (f'Doesn\'t exist, suggest :: $ ixp server build {x}')

                else:
                    status2 = subprocess.getstatusoutput (f"lxc list --columns n46s {x}")
                    print (status2[1])
                    list_.append(x)
            print ('')

            # // Process selected elements //
            if (dict_['ct'] == True):
                for x in list_:

                    # // Carry out Internet conenctivity test //
                    status = int(_IxpLxc._lxc_internet_test(self, x))  
                    if (status == 100):
                        print (f'  {x} has good Internet connectivity\n')     
                    elif (status >= 50):
                        print (f'  {x} has poor Internet connectivity\n')   
                    else: 
                        print (f'\nERROR: {x} has no Internet connectivity, exiting\n')   
                        _ixp_debug('y', f'ERROR[ixp_server]: {x} has poor Internet connectivity, exiting')   
                        sys.exit(1)  
    
        # // If second argument is 'start' //
        elif (dict_['cmd'] == 'start'):

            # // Process selected elements //
            for y in dict_['ins']:

                if (_IxpLxc._lxc_status(self, y)[1] == 'Nil'):
                    print (f'{y} doesn\'t exist;\n')
                    _ixp_debug('y',f'INFO[ixp_server]: {y} doesn\'t exist;')
                    print (f'suggest running: \n\n$ ixp server build {y}')
                    print (f'$ ixp server start {y}\n')

                elif (_IxpLxc._lxc_status(self, y)[1] == 'Running'):
                    print (f'{y} is already running\n')
                    _ixp_debug('y', f'INFO[ixp_server]: {y} is already running')

                elif (_IxpLxc._lxc_status(self, y)[1] == 'Stopped'):
                    print (f'Starting the {y} server\n')
                    _ixp_debug('y', f'INFO[ixp_server]: Starting the {y} server')
                    _IxpLxc._lxc_start(self, y)

                else:
                    print ('ERROR[ixp_server_state]: Unrecognoised status received')
                    _ixp_debug('y', 'ERROR[ixp_server]: Unrecognoised status received')
                    sys.exit(1)

            sys.exit(0)

        # // If second argument is 'stop' //
        elif (dict_['cmd'] == 'stop'):

            # // Process selected elements //              
            for y in dict_['ins']:

                if (_IxpLxc._lxc_status(self, y)[1] == 'Nil'):
                    print (f'{y} doesn\'t exist;\n')
                    _ixp_debug('y',f'INFO[ixp_server]: {y} doesn\'t exist;')
                    print (f'suggest running: \n\n$ ixp server build {y}')

                elif (_IxpLxc._lxc_status(self, y)[1] == 'Stopped'):
                    print (f'{y} is already stopped\n')
                    _ixp_debug('y', f'INFO[ixp_server]: {y} is already stopped')

                elif (_IxpLxc._lxc_status(self, y)[1] == 'Running'):
                    print (f'Stopping the {y} server\n')
                    _ixp_debug('y', f'INFO[ixp_server]: Stopping the {y} server')
                    _IxpLxc._lxc_stop(self, y)

                else:
                    print ('ERROR[ixp_server]: Unrecognoised status received\n')
                    _ixp_debug('y', 'ERROR[ixp_server]: Unrecognoised status received')

            sys.exit(0)

        # // If second argument is 'delete' //
        elif (dict_['cmd'] == 'delete'):

            # // Process selected elements //
            for y in dict_['ins']:

                if (_IxpLxc._lxc_status(self, y)[1] == 'Nil'):
                    print (f'{y} doesn\'t exist, so no need to delete\n')
                    _ixp_debug('y', f'INFO[ixp_server_state]: {y} doesn\'t exist, so no need to delete')

                elif ((_IxpLxc._lxc_status(self, y)[1] == 'Running') or (_IxpLxc._lxc_status(self, y)[1] == 'Stopped')):
                    _IxpLxc._lxc_delete(self, skip_yes_no, y)

                else:
                    print (_IxpLxc._lxc_status(self, y)[1])
                    print ('ERROR[ixp_server]: Unrecognoised status received\n')
                    _ixp_debug('y', 'ERROR[ixp_server]: Unrecognoised status received')

            sys.exit(0)

        # // Catch-all //
        else:
            print ('ERROR[ixp_server]: Illegal ARGUMENT given\n')
            _ixp_debug('y','ERROR[ixp_servere]: Illegal ARGUMENT given')
            self._ixp_server_help_handler('help', 1)

        sys.exit(0)

    ## End to ixp_server method

########### // End IxpServer class // ###########

#################################################
# //            IxpSoftware class            // #
#################################################

class IxpSoftware:
    ''' IXP Software Class '''

    def __init__(self):
        ''' IxpSoftware class constructor method '''

        self.functionality = 'Install and configure software on LXC containers providing IXP services'
        
        self.software = {'ns' : ('bind9','bind9utils'), 
                         'rs' : ('bird',), 
                         'cs' : ('bird',), 
                         'bs' : ('bird', 'bind9', 'bind9utils'),
                         'sc' : ('python3-ryu',)
                        }
        self.birdseye_file = 'birdseye-v1.1.4.tar.bz2'
        self.birdseye_directory = 'birdseye-v1.1.4'
        self.lighttpd_dir = '/etc/lighttpd/'
        self.lighttpd_conf_template = 'lighttpd.conf_template'
        self.as112_addrs = ('192.175.48.1/24','192.175.48.6/24','192.175.48.42/24',
                       '192.31.196.1/24','2620:4f:8000::1/48','2620:4f:8000::6/48',
                       '2620:4f:8000::42/48','2001:4:112::1/48')
        self.as112_addrs_flag = 0
        self.netplan_dir = '/etc/netplan/'
        self.netplan_file = '10-netplan.yaml'
        self.dummy_interface = '/etc/systemd/network/dummy1.netdev'
        self.bs_bird_template = ('bs_bird.conf_template', 
                                 'bs_bird6.conf_template'
                                )
        self.bs_named_templates = ('bs_named.conf.options_template', 
                                   'bs_named.conf.local_template'
                                  ) 
        self.bs_bird_zone_templates = ('bs_db.dr-empty_template', 
                                       'bs_db.dd-empty_template',
                                       'bs_db.hostname.as112.arpa_template',
                                       'bs_db.hostname.as112.net_template'
                                       )
        self.bird_template = ('bird.conf_template', 'bird6.conf_template')
        self.martian_template = ('martians-v4_template', 'martians-v6_template')
        self.named_log_dir = '/var/log/named/'
        self.named_log_file = 'bind9.log'
        self.bind9_root = '/etc/bind/'
        self.zones_root = '/etc/bind/zones/'
        self.www_data_user_template = '91-www-data-user_template'
        self.www_data_user = '91-www-data-user'
        self.ns_named_conf_options_template = 'ns_named.conf.options_template'
        self.ns_named_conf_local_template = 'ns_named.conf.local_template'
        self.ns_db_domain_template = 'ns_db.domain_template'
        self.ns_db_ipv4_template = 'ns_db.ipv4_template'
        self.ns_db_ipv6_template =  'ns_db.ipv6_template'
        self.srv_ryu_dir = '/srv/ryu/'
        self.ryu_log_dir = '/var/log/ryu/'
        self.ryu_sub_class = './ixp_switch_13_template.py' 
        self.ryu_start_script = 'ryu-start.sh'   
        self.ryu_stop_script = 'ryu-stop.sh'  
        self.ryu_service_script = 'ryu-service'     # For /usr/local/bin  
        self.ryu_systemd_service = 'ryu.service'    # For /etc/systemd/system
        self.ryu_service = 'ryu.service'
        self.ryu_log_rot_file = 'ryu'
        self.ryu_log = 'ryu.log'
        [self.LXD, self.NS, self.RS, self.CS, self.BS, self.SC] = _ixp_schema_servers('all')
        self.ixp_ns_peer_ip = _read_db('schema')[self.NS]['ipv4_peer'].split('/')[0]
        self.ixp_ns_man_ip = _read_db('schema')[self.NS]['ipv4_man'].split('/')[0]
        self.ixp_bs_man_ipv4 = _read_db('schema')[self.BS]['ipv4_man'].split('/')[0]
        self.ixp_bs_man_ipv6 = _read_db('schema')[self.BS]['ipv6_man'].split('/')[0]
        self.ixp_servers_plus = dict()

        # // Build 'self.ixp_servers_plus' list //
        for x in _ixp_schema_servers():
            self.ixp_servers_plus[x] = ixp_servers[x[:-1]]

        ## End of __init__ method
        
    def __str__(self):
        ''' IxpSoftware class __str__ method ''' 

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' IxpSoftware class __repr__ method ''' 

        return (f'{self.__class__.__name__} class: {self.functionality}')    

        ## End of __repr__ method 

    #============================================#
    # //      _ixp_software_help_handler      // #
    #============================================#

    # // Help handler function //
    def _ixp_software_help_handler(self, command, err_code):
        ''' software help handler '''

        if (command == 'help'):
            IxpHelp._ixp_software_help(self, err_code)
        elif (command == 'install'):
            IxpHelp._ixp_software_install_help(self, err_code)
        elif (command == 'configure'):
            IxpHelp._ixp_software_configure_help(self, err_code)

        ## End to _ixp_software_help_handler function

    #============================================#
    #  //        _replace_nameserver          // #
    #============================================#

    def _replace_nameserver(self, c, new_man_ns, *args):
        ''' Method to replace the upstream server with NS addresses '''

        # // Check if new_peer_ns given, if not use the new_man_ns for both //
        if (len(args) == 0):
            new_peer_ns = new_man_ns
        else: 
            new_peer_ns = args[0]

        # // Get existing file from container server //
        _IxpLxc._lxc_pull(self, c, netplan_file, f'/tmp/{c}-netplan-file')

        # // Open file, create list of lines and list of positions of NS addresses //
        fh = open(f'/tmp/{c}-netplan-file', "r")
        file_list = list()
        file_pos = list()
        for cnt, x in enumerate(fh):
            file_list.append(x)
            if ('nameservers:' in x):
                file_pos.append(cnt + 2)

        # // Only one interface (case SC) //
        if (len(file_pos) == 1):
            file_list[file_pos[0]] = f"{' ' * 8}addresses: ['{new_man_ns}']\n"
            file_ = ''.join(file_list)
            _ixp_debug('y', f'INFO[_replace_nameserver]: Changed DNS to Man: {new_man_ns} on the {c} server')

        # // Replace first instance with new_peer_ns and second with new_man_ns //
        else:            
            flag = 0
            for x in file_pos:
                if (flag == 0):
                    file_list[x] = f"{' ' * 8}addresses: ['{new_peer_ns}']\n"
                else:
                    file_list[x] = f"{' ' * 8}addresses: ['{new_man_ns}']\n"
                flag += 1
            file_ = ''.join(file_list)
            _ixp_debug('y', f'INFO[_replace_nameserver]: Changed DNS to Peer: {new_peer_ns} and Man: {new_man_ns} on the {c} server')

        # // Return updated file //
        with open(f'/tmp/{c}-netplan-file', mode='w', encoding='utf-8') as fh2:
            fh2.write(file_)
        _IxpLxc._lxc_push(self, c, f'/tmp/{c}-netplan-file', netplan_file) 
        _IxpLxc._lxc_push(self, c, f'/tmp/{c}-netplan-file', netplan_file)
        subprocess.getstatusoutput (f'rm /tmp/{c}-netplan-file')
            
        return (0)

    ## End _replace_nameserver method

    #============================================#
    #  //        _bind_structure_test         // #
    #============================================#

    def _bind_structure_test(self, c):
        ''' Method to check if BIND9 directory structure exists and backup if so '''

        # // Check the /var/log/named directory exists //
        dir_test = _IxpLxc._lxc_exec(self, c, f'test -d {self.named_log_dir}; echo $?')
        if (int(dir_test[1]) == 0):
            _ixp_debug('y', f'INFO[_bind_structure_test]: {self.named_log_dir} already exists on {c}')
                
        else:
            _ixp_debug('y', f'INFO[_bind_structure_test]: Creating the {self.zones_root} directory on {c}')
            _IxpLxc._lxc_exec(self, c, f'mkdir {self.named_log_dir}')
            
        file_test = _IxpLxc._lxc_exec(self, c, f'test -f {self.named_log_dir}{self.named_log_file}; echo $?')

        if (int(file_test[1]) == 0):
             _ixp_debug('y', f'INFO[_bind_structure_test]: {self.named_log_dir}{self.named_log_file} already exists on {c}') 
      
        else: 
            _IxpLxc._lxc_exec(self, c, f'touch {self.named_log_dir}{self.named_log_file}')

        _IxpLxc._lxc_exec(self, c, f'chown -R bind:bind {self.named_log_dir}')
        _IxpLxc._lxc_exec(self, c, f'chmod 755 {self.named_log_dir}')
        _IxpLxc._lxc_exec(self, c, f'chmod -R 644 {self.named_log_dir}{self.named_log_file}')

        # // Check the named.conf.local' named.conf.options exists and backup if it does //
        for f in ('named.conf.options', 'named.conf.local'):

            file_test = _IxpLxc._lxc_exec(self, c, f'test -f {self.bind9_root}{f}; echo $?')

            if (int(file_test[1]) == 0):
                _IxpLxc._lxc_exec(self, c, f'cp {self.bind9_root}{f} {self.bind9_root}{f}.bak')
                _IxpLxc._lxc_exec(self, c, f'chown -R root:bind {self.bind9_root}{f}.bak')
        else:
            _ixp_debug('y', f'INFO[_bind_structure_test]: {self.bind9_root}{f} doesn\'t exist on {c}')

        # // Create the zones directory //
        zones_test = _IxpLxc._lxc_exec(self, c, f'test -d {self.zones_root}; echo $?')

        if not (int(zones_test[1]) == 0):
            _IxpLxc._lxc_exec(self, c, f'mkdir {self.zones_root}')
            _IxpLxc._lxc_exec(self, c, f'chown -R root:bind {self.zones_root}')
            _IxpLxc._lxc_exec(self, c, f'chmod 755 {self.zones_root}')
            _IxpLxc._lxc_exec(self, c, f'chmod g+s {self.zones_root}')

        else: 
            _ixp_debug('y', f'ERROR[ns_software_configure_build]: {self.zones_root} directory doesn\'t exist on {c} so creating')

        print ('BIND9 structure test completed successfully\n')

        return (0)

    ## End of _bind_structure_test method

    #============================================#
    #  //        _nameserver_configure        // #
    #============================================#

    def _nameserver_configure(self, c):
        ''' Method to configure nameserver '''

        dict_ = dict()

        print ('Configuring and building the BIND9 nameserver software\n')
        _ixp_debug('y','INFO[_nameserver_configure]: Configure and build the BIND9 nameserver software')

        # // IXP Schema elements //
        e = _ixp_schema_servers()
    
        # // Check the BIND9 structure exists and backup if it does //
        self._bind_structure_test(c)

        # // ###### Create the names.conf.options file ###### //
        _ixp_debug('y','INFO[rs_software_configure_build]: Configuring and building the bind named.conf.options file')

        # Pointer to the NS self.ns_named_conf_options_template  template file //
        template_file = f'{var_dir}{self.ns_named_conf_options_template}'
        ns_named_conf_options = f'{var_dir}ns_named.conf.options'

        # // Check the NS named.conf.options template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput (f'cp {template_file} {ns_named_conf_options}') 
            _ixp_debug('y',f'INFO[_nameserver_configure]:  Created the file {ns_named_conf_options}')

        else:
            print (f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Insert forwarders in NS named.conf.options file //
        resolvers = str()

        for e in upstream_ns:

            if (upstream_ns.index(e) != (len(upstream_ns) - 1)):
                resolvers = resolvers + f'          {e};\n'

            else:
                resolvers = resolvers + f'          {e};'
        
        _string_search_replace(ns_named_conf_options, '      <<resolvers>>', resolvers)

        # // Nameserver details from schema //
        yaml_schema_docs = _read_db('schema')
        server_ip = yaml_schema_docs[c]

        domain = server_ip['domain']

        # // Loop through networks //
        for key in ('ipv4_peer', 'ipv6_peer', 'ipv4_man', 'ipv6_man'):
            ip_name = f'{key}'
            stripped_net = re.findall(r'(.*[::|\.])(\d{1,3})\/(\d{1,2})', server_ip[key])[0]

            if ('::' in stripped_net[0]):
                new_net = f'{stripped_net[0]}/{stripped_net[2]}'
                new_ip = f'{stripped_net[0]}{stripped_net[1]}'

            else:
                new_net = f'{stripped_net[0]}0/{stripped_net[2]}'
                new_ip = f'{stripped_net[0]}{stripped_net[1]}'

            dict_[ip_name] = new_ip
            dict_[f'{ip_name}_net'] = new_net  

        # // Insert IP addresses and Networks from IXP Schema //
        for k in dict_:
            s = (' ' * (20 - len(dict_[k])))
            _string_search_replace(ns_named_conf_options, f'<<{k}>>;', f'{dict_[k]};{s}')  
      
        # //Push the new file to the NS container //  
        _IxpLxc._lxc_push(self, c, ns_named_conf_options, '/etc/bind/named.conf.options') 
        _IxpLxc._lxc_exec(self, c, 'chown -R root:bind /etc/bind/named.conf.options')   

        # // ###### Create the query log file and give bind ownership ###### //
        _ixp_debug('y','INFO[rs_software_configure_build]: Configuring and building the bind query log file')

        # // Check the NS named.conf.options template file exists //
        # // ###### Define the local forward zone    ###### //
        # // ###### Create the names.conf.local file ###### //
        _ixp_debug('y','INFO[rs_software_configure_build]: Configuring and building the bind named.conf.local file')

        # // Extract network elements and in-addr.arpa and ip6.arpa //
        i = re.findall(r'(.*?.0)/.*', dict_['ipv4_man_net'])
        ipv4_arpa = ipaddress.ip_address(i[0]).reverse_pointer
        ipv4_arpa_no0 = re.findall(r'[0.]*(.*)', ipv4_arpa)
        dict_['ipv4_man_net_nomask'] = i[0]
        dict_['ipv4_man_arpa'] = ipv4_arpa_no0[0]
        j = re.findall(r'(.*?).0', dict_['ipv4_man_net_nomask'])[0]

        dict_['ipv4_man_net_nomask_no0'] = j
        i6 = re.findall(r'(.*?::)/.*', dict_['ipv6_man_net'])
        i6_nodcolon = re.findall(r'(.*?)::', i6[0])
        ipv6_arpa = ipaddress.IPv6Address(i6[0]).reverse_pointer
        ipv6_arpa_no0 = re.findall(r'[0.]*(.*)', ipv6_arpa)
        dict_['ipv6_man_net_nomask'] = i6_nodcolon[0]
        dict_['ipv6_man_arpa'] = ipv6_arpa_no0[0]

        # // Pointer to the self.ns_named_conf_local_template template file //
        template_file = f'{var_dir}{self.ns_named_conf_local_template}' 
        ns_named_conf_local = f'{var_dir}ns_named.conf.local'
        
        # // Check the NS named.conf.local template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput (f'cp {template_file} {ns_named_conf_local}') 
            _ixp_debug('y', f'INFO[_nameserver_configure]:  Created the file {ns_named_conf_local}')

        else:
            print (f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Make changes to NS named.conf.local file //
        _string_search_replace(ns_named_conf_local, '<<domain>>', domain)

        named_conf_local_elements = ('ipv4_man_net_nomask_no0', 'ipv4_man_arpa', 'ipv6_man_net_nomask', 'ipv6_man_arpa')

        for e in named_conf_local_elements:
            _string_search_replace(ns_named_conf_local, f'<<{e}>>', f'{dict_[e]}')

        # // Push the new file to the NS container // 
        _IxpLxc._lxc_push(self, c, ns_named_conf_local, f'{self.bind9_root}named.conf.local') 
        _IxpLxc._lxc_exec(self, c, f'chown -R root:bind {self.bind9_root}named.conf.local') 
     
        # // ###### Make the zones directory if necessary ###### //
        # // ###### Create the db.domain file             ###### //
        _ixp_debug('y','INFO[_nameserver_configure]: Configuring and building the bind db.domain file')

        # Pointer to the ns_db_domain_template template file //
        template_file = f'{var_dir}{self.ns_db_domain_template}'
        ns_db_domain = f'{var_dir}ns_db.domain'
        
        # // Check the ns_db_domain_template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput (f'cp {template_file} {ns_db_domain}') 
            _ixp_debug('y', f'INFO[_nameserver_configure]:  Created the file {ns_db_domain}')

        else:
            print (f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Make changes to NS ns_db_domain file //

        # // Replace the date time group under Serial //
        _string_search_replace(ns_db_domain, '<<yyyymmdd>>', _ixp_timestamp()[1])

        # // Replace the NS name //
        _string_search_replace(ns_db_domain, '<<ns>>', c)

        # // Replace the domain name //
        _string_search_replace(ns_db_domain, '<<domain>>', domain)

        # // Append the hosts to the file //
        try: 
             fh = open(ns_db_domain,'a')
        except:
             print (f'ERROR: Cannot open {ns_db_domain}\n')
             _ixp_debug('y', f'ERROR[_nameserver_configure]: Cannot open {ns_db_domain}')

        fh.write('\n')

        for key, value in sorted(self.ixp_servers_plus.items(), key = itemgetter(1)):
            s = ' ' * (12 - len(key))
            in_a = 'IN   A      '
            fh.write(f"{key}{s}{in_a}{dict_['ipv4_man_net_nomask_no0']}.{value}\n")
        fh.write('\n')

        for key, value in sorted(self.ixp_servers_plus.items(), key = itemgetter(1)):
            s = ' ' * (12 - len(key))
            in_a = 'IN   AAAA   '
            fh.write(f"{key}{s}{in_a}{dict_['ipv6_man_net_nomask']}::{value}\n")
        fh.write('\n')

        # // Add CNAME entries in db.hostname for birdseye //
        ns_birdseye_cname = (f'{self.RS}-ipv4   IN    CNAME    {self.RS}.',
                             f'{self.RS}-ipv6   IN    CNAME    {self.RS}.',
                             f'{self.CS}-ipv4   IN    CNAME    {self.CS}.',
                             f'{self.CS}-ipv6   IN    CNAME    {self.CS}.',
                             f'{self.BS}-ipv4   IN    CNAME    {self.BS}.',
                             f'{self.BS}-ipv6   IN    CNAME    {self.BS}.'
                            )

        fh.write('; Added for Birdeye\n')
        fh.write('\n')

        for cname in ns_birdseye_cname:
            fh.write(f"{cname}{domain}.\n")

        fh.write('\n')

        # // Close filehandle //
        fh.close()

        # //Push the new file to the NS container // 
        _IxpLxc._lxc_push(self, c, ns_db_domain, f"{self.zones_root}db.{domain}")
        _IxpLxc._lxc_exec(self, c, f"chown -R root:bind {self.zones_root}db.{domain}")

        # // ###### Define the reverse lookup zones          ###### //
        # // ###### Create the IPv4 reverse lookup zone file ###### //

        _ixp_debug('y','INFO[_nameserver_configure]: Configuring and building the IPv4 reverse lookup zone')

        # Pointer to the self.ns_db_ipv4_template template file //
        template_file = f'{var_dir}{self.ns_db_ipv4_template}'
        ns_db_ipv4 = f'{var_dir}ns_db.ipv4'
    
        # // Check the self.ns_db_ipv4_template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput (f'cp {template_file} {ns_db_ipv4}') 
            _ixp_debug('y', f'INFO[_nameserver_configure]:  Created the file {ns_db_ipv4}')
        else:
            print (f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Make changes to NS ns_db_ipv4 file //

        # // Replace the date time group under Serial //
        _string_search_replace(ns_db_ipv4, '<<yyyymmdd>>', _ixp_timestamp()[1])

        # // Replace the NS name //
        _string_search_replace(ns_db_ipv4, '<<ns>>', c)

        # // Replace the domain name //
        _string_search_replace(ns_db_ipv4, '<<domain>>', domain)

        # Replace the ipv4-man-net and ipv4-man-arpa //
        _string_search_replace(ns_db_ipv4, '<<ipv4_man_net>>', dict_['ipv4_man_net'])
        _string_search_replace(ns_db_ipv4, '<<ipv4_man_arpa>>', dict_['ipv4_man_arpa'])

        # // Append the hosts to the file //
        try: 
             fh= open(ns_db_ipv4,'a')
        except:
             print (f'ERROR: Cannot open {ns_db_ipv4}\n')
             _ixp_debug('y', f'ERROR[_nameserver_configure]: Cannot open {ns_db_ipv4}')

        fh.write('\n')

        for key, value in sorted(self.ixp_servers_plus.items(), key = itemgetter(1)):
            s = ' ' * (8 - len(value))
            in_ptr = 'IN   PTR      '
            fh.write(f"{value}{s}{in_ptr}{key}.{domain}.\n")

        fh.write('\n')

        fh.close()

        # //Push the new file to the NS container // 
        _IxpLxc._lxc_push(self, c, ns_db_ipv4, f"{self.zones_root}db.{dict_['ipv4_man_net_nomask_no0']}")
        _IxpLxc._lxc_exec(self, c, f"chown -R root:bind {self.zones_root}db.{dict_['ipv4_man_net_nomask_no0']}")

        # // ###### Create the IPv6 reverse lookup zone file ###### //
        _ixp_debug('y','INFO[_nameserver_configure]: Configuring and building the IPv6 reverse lookup zone')

        # Pointer to the self.ns_db_ipv6_template template file //
        template_file = f'{var_dir}{self.ns_db_ipv6_template}'
        ns_db_ipv6 = f'{var_dir}ns_db.ipv6'
    
        # // Check the self.ns_db_ipv6_template file exists //
        if (path.isfile(template_file)):
            subprocess.getstatusoutput (f'cp {template_file} {ns_db_ipv6}') 
            _ixp_debug('y', f'INFO[_nameserver_configure]:  Created the file {ns_db_ipv6}')
        else:
            print (f'ERROR: {template_file} doesn\'t exist\n')
            _ixp_debug('y', f'ERROR[_nameserver_configure]: {template_file} doesn\'t exist')
            sys.exit(1)

        # // Make changes to NS ns_db_ipv6 file //

        # // Replace the date time group under Serial //
        _string_search_replace(ns_db_ipv6, '<<yyyymmdd>>', _ixp_timestamp()[1])

        # // Replace the NS name //
        _string_search_replace(ns_db_ipv6, '<<ns>>', c)

        # // Replace the domain name //
        _string_search_replace(ns_db_ipv6, '<<domain>>', domain)

        # Replace the ipv6-man-net and ipv6-man-arpa //
        _string_search_replace(ns_db_ipv6, '<<ipv6_man_net>>', dict_['ipv6_man_net'])
        _string_search_replace(ns_db_ipv6, '<<ipv6_man_arpa>>', dict_['ipv6_man_arpa'])

        # // Append the hosts to the file //
        try: 
             fh = open(ns_db_ipv6,'a')
        except:
             print (f'ERROR: Cannot open {ns_db_ipv6}\n')
             _ixp_debug('y', f'ERROR[_nameserver_configure]: Cannot open {ns_db_ipv6}')

        fh.write('\n')

        # // PTR should have 32 - (length of ipv6-man-arpa) elements //
        len_ip6_arpa = len('ip6.arpa')
        len_ptr = 32 - (int((len(dict_['ipv6_man_arpa']) - len_ip6_arpa)/2))

        for key, value in sorted(self.ixp_servers_plus.items(), key = itemgetter(1)):

            ptr_str = ''
            value = list(value) 
            value.reverse()
            len_value = len(value)

            # // Generate PRT entry
            for e in value:
                ptr_str = ptr_str + e +'.'

            for x in range(0, (len_ptr - len_value - 1)):
                ptr_str = ptr_str + '0.'

            ptr_str = ptr_str + '0'

            in_ptr = 'IN   PTR    '
            fh.write(f"{ptr_str}    {in_ptr}{key}.{domain}.\n")

        fh.write('\n')

        fh.close()

        # // Push the new file to the NS container // 
        _IxpLxc._lxc_push(self, c, ns_db_ipv6, f"{self.zones_root}db.{dict_['ipv6_man_net_nomask']}")
        _IxpLxc._lxc_exec(self, c, f"chown -R root:bind {self.zones_root}db.{dict_['ipv6_man_net_nomask']}")

        # // Restart the DNS Server //
        _IxpLxc._lxc_exec(self, c, 'systemctl enable bind9')
        _IxpLxc._bind9_restart(self, c)

        print (f'The nameserver {c} has been configured successfully\n')
        _ixp_debug('y','INFO[_nameserver_configure]: The nameserver {c} has been configured successfully')

        return (0)

    ## End to _nameserver_configure method

    #============================================#
    #  //           _bird_configure           // #
    #============================================#

    def _bird_configure(self, c):
        ''' Method to build the BIRD configuration on containers '''

        print (f'Configuring and building BIRD software on {c}\n')
        _ixp_debug('y', f'INFO[_bird_configure]: Configuring and building BIRD software on {c}')

        # // Get information from schema relating to the server //
        yaml_schema_docs = _read_db('schema')
        yaml_schema = yaml_schema_docs[c]
        yaml_schema_dict = dict()

        for key in yaml_schema:

            if (key == 'ipv4_peer'): 
                addr4 = re.findall(r'(.*)/\d{1,2}', yaml_schema[key])
                yaml_schema_dict[key] = addr4[0]

            elif (key == 'ipv6_peer'):
                addr6 = re.findall(r'(.*)/\d{1,2}', yaml_schema[key])
                yaml_schema_dict[key] = addr6[0]

            elif (key == 'as_number'):
                yaml_schema_dict[key] = str(yaml_schema[key])

        # // Loop through testing the bird and bird6 template files and create //
        for x in (0,1):

            # // Pointer to the bird template file //
            if (x == 0):
                template = f'{var_dir}{self.bird_template[0]}'
                bird_file = f'{var_dir}{c}_bird.conf'
                mtemplate = f'{var_dir}{self.martian_template[0]}'
                martian_file = f'{var_dir}{c}_import_policy_v4'
  
            else:
                template = f'{var_dir}{self.bird_template[1]}'
                bird_file = f'{var_dir}{c}_bird6.conf'
                mtemplate = f'{var_dir}{self.martian_template[1]}'
                martian_file = f'{var_dir}{c}_import_policy_v6'

            if (path.isfile(template)):
                subprocess.getstatusoutput (f'cp {template} {bird_file}') 
                _ixp_debug('y', f'INFO[_bird_configure]: Created the file {bird_file}')

            else:
                print (f'ERROR: {template} doesn\'t exist\n')
                _ixp_debug('y', f'ERROR[_bird_configure]: {template} doesn\'t exist')
                sys.exit(1)

            if (path.isfile(mtemplate)):
                subprocess.getstatusoutput (f'cp {mtemplate} {martian_file}') 
                _ixp_debug('y', f'INFO[_bird_configure]: Created the file {martian_file}')

            else:
                print (f'ERROR: {mtemplate} doesn\'t exist\n')
                _ixp_debug('y', f'ERROR[_bird_configure]: {mtemplate} doesn\'t exist')
                sys.exit(1)

            # // Make the changes to the files //
            for e in yaml_schema_dict:
                _string_search_replace(bird_file, f'<<{e}>>', yaml_schema_dict[e])

            # // If the server is CS then turn off export //
            if (c == self.CS):
                _string_search_replace(bird_file, 'export all;', '#export all;')

            # // Extract the remote file names //
            remote_file = re.findall(r'.*_(.*)', bird_file)
            remote_martian_file = re.findall(r'.*_(import_policy.*)', martian_file)

            # // Check the IXP bird configuration exists and backup if it does //
            file_test = _IxpLxc._lxc_exec(self, c, f'test -f {bird_root}{remote_file[0]}; echo $?')
            if (int(file_test[1]) == 1):
                _ixp_debug('y', f'INFO[_bird_configure]: {remote_file[0]} doesn\'t exist on {c} so nothing to backup')            
            else:
                _ixp_debug('y', f'INFO[_bird_configure]: Backing up the {remote_file[0]} file on {c}')
                _IxpLxc._lxc_exec(self, c, f'mv {bird_root}{remote_file[0]} {bird_root}{remote_file[0]}.bak')       

            # // Push files to the server //
            _IxpLxc._lxc_push(self, c, bird_file, f'{bird_root}{remote_file[0]}')
            _IxpLxc._lxc_push(self, c, martian_file, f'{bird_root}{remote_martian_file[0]}')
            _IxpLxc._lxc_exec(self, c, f'chown -R bird:bird {bird_root}{remote_file[0]}') 
            _IxpLxc._lxc_exec(self, c, f'chown -R bird:bird {bird_root}{remote_martian_file[0]}')
            _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {bird_root}{remote_file[0]}')   
            _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {bird_root}{remote_martian_file[0]}')   

            # // Restart the bird Server //
            _IxpLxc._bird_restart(self, c)

        print (f'BIRD configuration on {c} completed successfully\n')

        return (0)

    ## End to _bird_configure method

    #============================================#
    #  //         _birdseye_configure         // #
    #============================================#
    
    def _birdseye_configure(self, c):
        ''' Method to configure Birdseye '''

        # // Configure the Birdseye // 
        birdeye_BIRDC = list()
        birdeye_BIRDC6 = list()

        print (f'Configuring Birdseye on {c}\n')
        _ixp_debug('y', f'INFO[_birdseye_configure]: Configuring Birdseye on {c}')
  
        # // Give user www-data permission to run birdc and birdc6 scripts //
        try:
            _IxpLxc._lxc_push(self, c, f'{var_dir}{self.www_data_user_template}', f'/etc/sudoers.d/{self.www_data_user}') 
 
        except:
            print (f'ERROR: Unable to upload the {self.www_data_user} file on {c}\n')
            _ixp_debug('y', f'ERROR[_birdseye_configure]: Unable to upload the {self.www_data_user} file on {c}')  
            sys.exit(1) 

        # // Change ownership to root & permissions to 440 on the 91-www-data-user file //
        _IxpLxc._lxc_exec(self, c, f'chown root: /etc/sudoers.d/{self.www_data_user}')
        _IxpLxc._lxc_exec(self, c, f'chmod 440 /etc/sudoers.d/{self.www_data_user}') 
        _ixp_debug('y', f'INFO[_birdseye_configure]: Changing ownership & permissions on the {self.www_data_user} file on {c}')
 
        # // Edit the birdseye environment file for IPv4 //
        _ixp_debug('y', f'INFO[_birdseye_configure]: Generating the /srv/birdseye/birdseye-{c}-ipv4.env file')
        _IxpLxc._lxc_exec(self, c, f'cp /srv/birdseye/.env.example /srv/birdseye/birdseye-{c}-ipv4.env')
        _IxpLxc._lxc_exec(self, c, f'sed -i.bak \'s/^BIRDC/#BIRDC/\' /srv/birdseye/birdseye-{c}-ipv4.env')

        birdeye_BIRDC.append('')
        birdeye_BIRDC.append('BIRDC=\\"/usr/bin/sudo /srv/birdseye/bin/birdc -4 -s /var/run/bird/bird.ctl\\"')

        for l in birdeye_BIRDC:
            _IxpLxc._lxc_exec(self, c, f'sh -c \'echo {l} >> /srv/birdseye/birdseye-{c}-ipv4.env\'')

        # // Edit the birdseye environment file for IPv6 //
        _ixp_debug('y', f'INFO[_birdseye_configure]: Generating the /srv/birdseye/birdseye-{c}-ipv6.env file') 
        _IxpLxc._lxc_exec(self, c, f'cp /srv/birdseye/.env.example /srv/birdseye/birdseye-{c}-ipv6.env')
        _IxpLxc._lxc_exec(self, c, f'sed -i.bak \'s/^BIRDC/#BIRDC/\' /srv/birdseye/birdseye-{c}-ipv6.env')

        birdeye_BIRDC6.append('')
        birdeye_BIRDC6.append('BIRDC=\\"/usr/bin/sudo /srv/birdseye/bin/birdc -6 -s /var/run/bird/bird6.ctl\\"')

        for l in birdeye_BIRDC6:
            _IxpLxc._lxc_exec(self, c, f'sh -c \'echo {l} >> /srv/birdseye/birdseye-{c}-ipv6.env\'')

        # // Information back to the shell //
        _ixp_debug('y', f'INFO[_birdseye_configure]: Generating the following files on {c}')
        _ixp_debug('y', f'                 - /etc/sudoers.d/{self.www_data_user}')
        _ixp_debug('n', f'                 - /srv/birdseye/birdseye-{c}-ipv4.env file')
        _ixp_debug('n', f'                 - /srv/birdseye/birdseye-{c}-ipv6.env file')

        print (f'Birdseye configuration on {c} completed successfully\n')
        _ixp_debug('y', f'INFO[_birdseye_configure]: BIRDSEYE configuration on {c} completed successfully')
        
        return (0)
            
    ## End of _birdseye_configure method

    #============================================#
    #  //    _bs_software_configure_build     // #
    #============================================#
    
    def _bs_software_configure_build(self, c):
        ''' Method to configure the blackhole AS112 server '''

        # ########### // Handle the netplan for BS // #############

        list_ = list()
        as112_prefix = ['192.175.48.0/24', '2620:4f:8000::/48']

        # // See if a dummy interface already exists //
        dummy_test = _IxpLxc._lxc_exec(self, c, f'[ -f \'{self.dummy_interface}\' ]; echo $?')

        if (dummy_test[1] == '0'):
            _ixp_debug('y', f'INFO[_bs_software_configure_build]: The {c}: {self.dummy_interface} file already exists')

        # // Build the dummy interface //
        else:
            print (f'There is no {self.dummy_interface} on {c}\n')
            _ixp_debug('y', f'INFO[_bs_software_configure_build]: There is no {self.dummy_interface} on {c}')

            # // Create a dummy file //
            dummy_ifce = ['[NetDev]']
            dummy_ifce.append('Name=dummy1')
            dummy_ifce.append('Kind=dummy')

            for d in dummy_ifce:
                _IxpLxc._lxc_exec(self, c, f'sh -c \"echo \'{d}\' >> {self.dummy_interface}\"')
 
            print (f'Configured and built the {c} netplan file\n')
            _ixp_debug('y', f'INFO[_bs_software_configure_build]: Configured and built the {c} netplan file')

        # // See if AS112 configuration already exists in BS netplan file //
        netplan_exist = _IxpLxc._lxc_exec(self, c, f'cat {self.netplan_dir}{self.netplan_file}')

        # // Loop through AS112 addresses //
        for a in self.as112_addrs:
 
            if (a in netplan_exist[1]):
                self.as112_addrs_flag += 1

        if (self.as112_addrs_flag == len(self.as112_addrs)):
                print (f'{c} netplan already configured\n')
                _ixp_debug('y', f'INFO[_bs_software_configure_build]: {c} netplan already configured')

        elif ((self.as112_addrs_flag < len(self.as112_addrs) and self.as112_addrs_flag > 0)):
                print (f'ERROR[_bs_software_configure_build]: {c} netplan incorrectly configured, fix manually, exiting\n')
                _ixp_debug('y', f'ERROR[_bs_software_configure_build]: {c} netplan incorrectly configured, fix manually, exiting')

        else:

            # // Create string to add to BS netplan file //
            netplan_br = ['\n    dummy1: {}']    
            netplan_br.append('\n  bridges:')
            netplan_br.append('    as112_br1:')
            netplan_br.append('      interfaces: [dummy1]')
            netplan_br.append('      addresses:')
            for a in self.as112_addrs:
                netplan_br.append(f'          - {a}')

            # // Add to c netplan file //  
            for b in netplan_br:
                _IxpLxc._lxc_exec(self, c, f'sh -c \"echo \'{b}\' >> {self.netplan_dir}{self.netplan_file}\"')
        
        # // Applying new netplan file on container //
        netplan_apply = _IxpLxc._lxc_exec(self, c, 'netplan apply')
   
        if not (netplan_apply[0] == 0):
            print (f'ERROR: Cannot apply netplan on {c}\n')
            _ixp_debug('y', f'ERROR[_bs_software_configure_build]: Cannot apply netplan on {c}')
            sys.exit(1)
        else:    
            print (f'New netplan applied on {c}\n')
            _ixp_debug('y', f'INFO[_bs_software_configure_build]: New netplan applied on {c}')

        ########### // Handle the BIRD files for BS // #############

        # // Configure and install the bird.conf and bird6.conf //    
        bs_bird_file = f'{c}_bird.conf'
        bs_bird6_file = f'{c}_bird6.conf'

        bs_bird_path_file = f'{var_dir}{bs_bird_file}'
        bs_bird6_path_file = f'{var_dir}{bs_bird6_file}'

        # // Create BS bird configuration files from templates //
        subprocess.getstatusoutput (f'cp {var_dir}{self.bs_bird_template[0]} {bs_bird_path_file}')
        subprocess.getstatusoutput (f'cp {var_dir}{self.bs_bird_template[1]} {bs_bird6_path_file}')

        # // Get the peer IPv4 address of the BS for the router ID // 
        yaml_schema_docs = _read_db('schema')
        bs_ip = _validate_ip_net(yaml_schema_docs[c]['ipv4_peer'])
        bs_ipv6 = _validate_ip_net(yaml_schema_docs[c]['ipv6_peer'])

        # // Search and replace the routerID field in the two bird files //
        for x in (bs_bird_path_file, bs_bird6_path_file):
            _string_search_replace(x, '<<ipv4_bs_peer>>' , bs_ip['ip'])
            _string_search_replace(x, '<<ipv6_bs_peer>>' , bs_ipv6['ip'])

        # // Backup the original bird configuration files //
        _IxpLxc._lxc_exec(self, c, f'cp {bird_root}{bird_file} {bird_root}{bird_file}.bak') 
        _IxpLxc._lxc_exec(self, c, f'cp {bird_root}{bird6_file} {bird_root}{bird6_file}.bak') 

        # // Copy new bird configuration files //
        _IxpLxc._lxc_push(self, c, f'{var_dir}{bs_bird_file}', f'{bird_root}{bird_file}') 
        _IxpLxc._lxc_push(self, c, f'{var_dir}{bs_bird6_file}', f'{bird_root}{bird6_file}') 
        _IxpLxc._lxc_exec(self, c, f'chown -R bird:bird {bird_root}{bird_file}') 
        _IxpLxc._lxc_exec(self, c, f'chown -R bird:bird {bird_root}{bird6_file}')
        _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {bird_root}{bird_file}')   
        _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {bird_root}{bird6_file}')  

        # // Log entry //
        _ixp_debug('y', f'INFO[_bs_software_configure_build]: Created new {bird_file} and {bs_bird6_file} for {c} and uploaded them')  

        # // Restart BIRD //
        _IxpLxc._bird_restart(self, c)

        # Configure birdseye for BS //
        self._birdseye_configure(c)

        ########### // Handle the BIND9 files for BS // #############

        # // Backup the original BIND9 configuration files //
        print ('Configuring and building the BS bind9 software\n')
        _ixp_debug('y','INFO[ns_software_configure_build]: Configure and build the BS software')

        # // Check the BIND9 structure exists and backup if it does //
        self._bind_structure_test(c)

        # // Create the BIND9 named.conf files from templates //
        named_conf_files = list()

        for x in self.bs_named_templates:
            f = re.findall(r'bs(_named\.conf\.\w+)_template', x)
            m = f'{var_dir}{c}{f[0]}'
            named_conf_files.append(f'{m}') 
            subprocess.getstatusoutput (f'cp {var_dir}{x} {m}')

        # // Search and replace the listening-on field in the two bird files //
        _string_search_replace(named_conf_files[0], '<<ipv4_bs_peer>>' , bs_ip['ip'])
        _string_search_replace(named_conf_files[0], '<<ipv6_bs_peer>>' , bs_ipv6['ip'])

        # // Move files to the BS container //
        _IxpLxc._lxc_push(self, c, named_conf_files[0], f'{self.bind9_root}named.conf.options')
        _IxpLxc._lxc_push(self, c, named_conf_files[1], f'{self.bind9_root}named.conf.local')
        _IxpLxc._lxc_exec(self, c, f'chown -R bind:bind {self.bind9_root}named.conf.options') 
        _IxpLxc._lxc_exec(self, c, f'chown -R bind:bind {self.bind9_root}named.conf.local')
        _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {self.bind9_root}named.conf.options')   
        _IxpLxc._lxc_exec(self, c, f'chmod -R 640 {self.bind9_root}named.conf.local')  
        
        # // Handle the zone files //
        domain = yaml_schema_docs[c]['domain']   

        named_zone_files = list()

        # // Create zone files for the BS //
        for x in self.bs_bird_zone_templates:
            
            f = re.findall(r'bs_(.*?)_template', x)
            named_zone_files.append(f'{var_dir}{c}_{f[0]}') 
            m = f'{var_dir}{c}_{f[0]}'
            subprocess.getstatusoutput (f'cp {var_dir}{x} {m}')

        # // Replace the timestamp, domain and nameserver name in the zone files //
        for x in named_zone_files:

            _string_search_replace(x, '<<yyyymmdd>>', _ixp_timestamp()[1]) 
            _string_search_replace(x, '<<ns>>', c)
            _string_search_replace(x, '<<domain>>', domain)
            f = re.findall(r'/var/ixp/bs\d_(.*)', x)
            _IxpLxc._lxc_push(self, c, x, f'{self.bind9_root}zones/{f[0]}')
            m = f'{self.bind9_root}zones/{f[0]}'
            _IxpLxc._lxc_exec(self, c, f"chown root:bind {m}")
            _IxpLxc._lxc_exec(self, c, f"chmod 644 {m}")

        # // Read site information // 

        site_info_dict = _read_db('site')[_site_number()]

        location_join = f"{site_info_dict['latitude']} {site_info_dict['longitude']}"

        # // Create list of TXT and LOC lines to append to hostname zone files //
        (m,n,o) = (site_info_dict['organisation'], site_info_dict['town'], site_info_dict['country'])
        txt_loc_list = [f""" "        TXT     \\"{m}\\" \\"{n}, {o}\\"  " """]
        txt_loc_list.append(""" "        TXT     \\"See http://www.as112.net/ for more information.\\" " """)
        txt_loc_list.append(f""" "        TXT     \\"Unique IP: {bs_ip['ip']}\\" \\"Unique IPv6: {bs_ipv6['ip']}\\" " """)
        txt_loc_list.append(""" \";\" """)
        (m,n) = (location_join, site_info_dict['elevation'])
        txt_loc_list.append(f""" "        LOC     {m} {n} 1m 10000m 10m " """)
 
        bind9_zone_files = _IxpLxc._lxc_exec(self, c, f'ls {self.zones_root}')[1].split('\n')

        for f in bind9_zone_files:
            if 'hostname' in f:
                for x in txt_loc_list:
                    _IxpLxc._lxc_exec(self, c, f"sh -c \'echo  {x}  >> {self.zones_root}{f} \' ") 

        # // Restart the DNS Server //
        _IxpLxc._bind9_restart(self, c)

        # // Add routes to NS for private addresses on BS //
        list_.append(f'      routes:')
        list_.append(f"        - to: '{as112_prefix[0]}'")
        list_.append(f"          via: '{self.ixp_bs_man_ipv4}'")
        list_.append(f"        - to: '{as112_prefix[1]}'")
        list_.append(f"          via: '{self.ixp_bs_man_ipv6}'")

        # // Grab NS template and add routes to BS //
        subprocess.getstatusoutput (f'cp /var/ixp/ixp_{self.NS}.yaml /tmp/10-netplan.yaml')
 
        for x in list_:
            subprocess.getstatusoutput (f"echo '{x}' >> /tmp/10-netplan.yaml")

        _IxpLxc._lxc_push(self, self.NS, '/tmp/10-netplan.yaml', f'{self.netplan_dir}{self.netplan_file}')
        subprocess.getstatusoutput (f"rm /tmp/10-netplan.yaml")

        # // Applying new netplan file on the NS container //
        _IxpLxc._lxc_exec(self, self.NS, 'ip route flush proto static')
        _IxpLxc._lxc_exec(self, self.NS, 'netplan apply')

        print (f'Configuration on {c} blackhole server completed successfully\n')
        _ixp_debug('y', f'INFO[_bs_software_configure_build]: Configuration on {c} blackhole server completed successfully')

        return (0)

    ## End of _bs_software_configure_build method

    #============================================#
    #  //    _sc_software_configure_build     // #
    #============================================#
    
    def _sc_software_configure_build(self, SC):
        ''' Method to configure the SDN Controller server '''

        dpid_sw_map = dict()
        dpid_port_map = dict()
        dpid_ip_map = dict()

        # // Instantiate the IxpSdn class and execute the _get_switch_maps() method //
        s = IxpSdn()
        s._get_switch_maps()
        s._dpid_port_map()

        # // Get the dpid_port_map using the IxpSdn _dpid_port_map() method //
        dpid_port_map = s.dpid_port_map

        dict_ = {'230': '0'} 
        for c, x in enumerate(range(241, 250)):
            dict_[str(x)] = str(c + 1)

        # // Get dpid_ip_map from SDN Controller //
        f = _IxpLxc._lxc_exec(self, SC, f'cat {dpid_ip_filename}')[1] 
        dpid_ip_map = json.loads(f)

        # // Match DPID to switch number //
        for k, v in dpid_ip_map.items():
            if (v[-3:] in dict_.keys()):
                dpid_sw_map[k] = dict_[v[-3:]]

        # // Get the name and management IP address of the SDN Controller //
        (sc_ipv4, unused_) = _read_db('schema', ['lxc', f"'{SC}'"])[SC]['ipv4_man'].split('/')

        # // Grab data from RESTful API and return it //
        url_ = f'http://{sc_ipv4}:8080/stats/switches'
        
        # // Test SDN Controller and RESTful API are operational //
        try:
            f = urllib.request.urlopen(url_).read()
            list_ = json.loads(f.decode('utf-8'))
            if (len(list_) > 0):
                print (f'SDN Controller and RESTful API on {sc_ipv4} are operational\n')
                _ixp_debug('y',f'ERROR[_sc_software_configure_build]: SDN Controller and RESTful API on {sc_ipv4} are operational')                

        except urllib.error.HTTPError as e:
            print (e.code)
            print (e.read())
            print (f'ERROR: Connection to the RESTful API on {sc_ipv4} failed\n')
            _ixp_debug('y',f'ERROR[_sc_software_configure_build]: Connection to the RESTful API on {sc_ipv4} failed')
            sys.exit(1)

        # // Push dpid port map to the SC //
        with open(f'/tmp/{dpid_port_file}', mode='w', encoding='utf-8') as fh:
            fh.write(json.dumps(dpid_port_map))
        _IxpLxc._lxc_push(self, SC, f'/tmp/{dpid_port_file}', f'{dpid_port_dir}{dpid_port_file}')           

        # // Add drop flows for each peer port //
        for k, v in dpid_port_map.items():
            for z in v:
                s._add_delete_flow('add', k, 99, {'match': { "in_port": z}, 'actions': []})

        print (f'Configuration on {SC} SDN Controller completed successfully\n')
        _ixp_debug('y', f'INFO[_sc_software_configure_build]: Configuration on {SC} SDN Controller completed successfully')

        return (0)

    ## End of _sc_software_configure_build method

    #============================================#
    #  //      _ixp_software_configure        // #
    #============================================#
    
    def _ixp_software_configure(self, args):
        ''' Method to configure software on the servers '''

        ixp_servers['gateway'] = '1'

        for c in args:

            # // Confirm nameserver is global on containers //
            if not (c == self.NS):
                status = _IxpLxc._lxc_status(self, c)

                if (status[1] == 'Running'):
                    self._replace_nameserver(c, upstream_ns[0], upstream_ns[0])
                else:
                    print (f'The {c} server is not \'Running\' and will need to be updated\n')
                    _ixp_debug('y', f'INFO[_ixp_software_install]: The {c} server is not \'Running\' and will need to be updated')
                    sys.exit(1)

                _IxpLxc._lxc_exec(self, c, 'netplan apply') 

            if (c == self.NS):
                self._nameserver_configure(c)   

            elif (c == self.RS):
                self._bird_configure(c)
                self._birdseye_configure(c)

            elif (c == self.CS):
                self._bird_configure(c)
                self._birdseye_configure(c)

            elif (c == self.BS):
                self._bs_software_configure_build(c) 

            elif (c == self.SC):
                self._sc_software_configure_build(c)
            else:
                assert False, 'ERROR: Not sure how we arrived here'

            # // Update nameserver NS in the other running containers //
            if not (c == self.NS):
                status = _IxpLxc._lxc_status(self, c)

                if (status[1] == 'Running'):
                    self._replace_nameserver(c, self.ixp_ns_man_ip, self.ixp_ns_peer_ip)
                else:
                    print (f'The {c} server is not \'Running\' and will need to be updated\n')
                    _ixp_debug('y', f'INFO[_ixp_software_install]: The {c} server is not \'Running\' and will need to be updated')
                    sys.exit(1)

                _IxpLxc._lxc_exec(self, c, 'netplan apply')

        print ('IXP software configuration completed\n')
        _ixp_debug('y', 'INFO[_ixp_software_install]: IXP software configuration completed')

        return (0)

    ## End of _ixp_software_configure method

    #============================================#
    #  //       _ixp_software_install         // #
    #============================================#
    
    def _ixp_software_install(self, ixp_argv):
        ''' Method to install software on the servers '''

        # // Loop through the servers //
        for c in ixp_argv:

            # // Upgrade the servers //
            print (f'Upgrading server {c}, please be patient\n')
            upgrade_test = _IxpLxc._lxc_upgrade(self, c)

            if not (upgrade_test == 0):
                print (f'ERROR: Problem carrying out the repository update and distribution upgrade on {c}\n')
                _ixp_debug('y', f'ERROR[_ixp_software_install]: Problem carrying out the repository update and distribution upgrade in {c}')
                sys.exit(1) 

            # // Install software from list //
            for p in self.software[c[:2]]:  # // c[:2] is the first 2 letters of the server //

                # // Update the repository //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Updating repository on {c}')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y update')
                
                # // Install software //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Installing \'{p}\' on {c}')
                install_test = _IxpLxc._lxc_exec(self, c, f'apt-get -y install {p}')

                if (install_test[0] == 0):
                    dpkg_test = _IxpLxc._lxc_exec(self, c, f'dpkg -s {p} | grep \'Status:\'')

                    if (dpkg_test[0] == 0):
                        _ixp_debug('y', f'INFO[_ixp_software_install]: \'{p}\' on {c} - {dpkg_test[1]}')   

                    else:
                        print (f'ERROR: Problem Installing \'{p}\' on {c} - {dpkg_test[1]}')   
                        _ixp_debug('y', f'ERROR[_ixp_software_install]: Problem installing \'{p}\' on {c} - {dpkg_test[1]}')   
                        sys.exit(1)                  
           
                else:
                    print (f'ERROR: Problem Installing \'{p}\' on {c}\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_install]: Problem installing \'{p}\' on {c}')
                    sys.exit(1)  

                print (f'Installed {p} on {c}\n')

            # // Install Birdseye software on the servers with bird //
            if (c[:2] in ['rs', 'cs', 'bs']):

                # // Autoremove //
                _ixp_debug('y', 'INFO[_ixp_software_install]: apt-get autoremove')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y autoremove')

                # // Add software-properties-common (manages adding repositories) //
                _ixp_debug('y', 'INFO[_ixp_software_install]: Installing \'software-properties-common\'')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y install software-properties-common')
             
                # // Add repository //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Installing \'ppa:ondrej/php\' repository on {c}')
                _IxpLxc._lxc_exec(self, c, 'add-apt-repository -y ppa:ondrej/php')

                # // Update the repository //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Updating repository on {c}')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y update')

                # // Add PHP //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Installing PHP on {c}')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y install php7.0 php7.0-cgi php7.0-mbstring php7.0-xml unzip')

                # // Copy the birdseye archive to the /srv directory //
                if (path.isfile(f'/var/ixp/code/{self.birdseye_file}')):
                    _IxpLxc._lxc_push(self, c, f'/var/ixp/code/{self.birdseye_file}', '/srv/') 
                    _ixp_debug('y', f'INFO[_ixp_software_install]: Created the file /srv/{self.birdseye_file} on {c}') 

                else:
                    print (f'/var/ixp/code/{self.birdseye_file} doesn\'t exist\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_install]: /var/ixp/code/{self.birdseye_file} doesn\'t exist')
                    sys.exit(1)  

                # // Unzip the birdseye archive //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Unzipping the file /srv/{self.birdseye_file} on {c}') 
                _IxpLxc._lxc_exec(self, c, f'tar -xjvf /srv/{self.birdseye_file} -C /srv/')    

                # // Move the birdseye directory //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Creating the /srv/birdseye directory on {c}') 
                birdseye_test = _IxpLxc._lxc_exec(self, c, '[ -d /srv/birdseye/ ]; echo $?')

                if (birdseye_test[1] == '0'):
                    _IxpLxc._lxc_exec(self, c, 'rm -r /srv/birdseye/') 
                try:    
                    _IxpLxc._lxc_exec(self, c, f'mv /srv/{self.birdseye_directory} /srv/birdseye/') 
                except:
                    print (f'ERROR: Failed to create the /srv/birdseye directory on {c}\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_install]: Failed to create the /srv/birdseye directory on {c}')
                    sys.exit(1)        

                # // Change owner of the /srv/birdseye/storage directory //
                _ixp_debug('y', f'INFO[_ixp_software_install]: change owner of /srv/birdseye/storage directory on {c}')
                _IxpLxc._lxc_exec(self, c, 'chown -R www-data: /srv/birdseye/storage')   

                # // Add lighttpd //
                _ixp_debug('y', f'INFO[_ixp_software_install]: Installing lighttpd')
                _IxpLxc._lxc_exec(self, c, 'apt-get -y install lighttpd') 

                print (f'Installed lighthttpd software on {c}\n')
                
                # // Copy the lighttpd.conf file to the /etc/lighttpd directory //
                if (path.isfile(f'{var_dir}{self.lighttpd_conf_template}')):
                    _ixp_debug('y', f'INFO[_ixp_software_install]: Installing lighttpd.conf template on {c}') 
                    _IxpLxc._lxc_exec(self, c, f'cp {self.lighttpd_dir}lighttpd.conf {self.lighttpd_dir}lighttpd.conf.bak') 
                    _IxpLxc._lxc_push(self, c, f'{var_dir}{self.lighttpd_conf_template}', f'{self.lighttpd_dir}lighttpd.conf') 
                    _IxpLxc._lxc_exec(self, c, f'chown root: {self.lighttpd_dir}lighttpd.conf')  
                    _IxpLxc._lxc_exec(self, c, f'chmod 644 {self.lighttpd_dir}lighttpd.conf')                         
                    _ixp_debug('y', f'INFO[_ixp_software_install]: Created the file {self.lighttpd_dir}lighttpd.conf on {c}') 
                else:
                    print ('ERROR: No lighttpd.conf template file exists') 
                    _ixp_debug('y', 'ERROR[_ixp_software_install]: No lighttpd.conf template file exists')                       

                # // Restart the lighttpd service //
                try:
                    _IxpLxc._lxc_exec(self, c, 'lighty-enable-mod fastcgi &> /dev/null; echo 0') 
                    _IxpLxc._lxc_exec(self, c, 'lighty-enable-mod fastcgi-php &> /dev/null; echo 0') 
                    _IxpLxc._lxc_exec(self, c, 'systemctl restart lighttpd.service')
                    _ixp_debug('y', f'INFO[_ixp_software_install]: Reloaded the lighttpd.service on {c}')
                except:
                    _IxpLxc._lxc_exec(self, c, 'systemctl restart lighttpd.service')
                    _ixp_debug('y', f'INFO[_ixp_software_install]: Reloaded the lighttpd.service on {c}')

                print (f'Birdseye is running on {c}\n')
                _ixp_debug('y', f'INFO[_ixp_software_install]: Birdseye is running on {c}')

            # // Install Ryu Controller flowmanager and systemd start scripts //
            if (c[:2] in ['sc']):

                # // Get regular username //
                username = getpass.getuser()

                # // Install flowmanager //
                _ixp_debug('y',f'INFO[_ixp_software_install]: Install \flowmanager\'') 
                flow_mgr = _IxpLxc._lxc_exec(self, c, '[ -d "/srv/flowmanager" ]; echo $?') 
                if (flow_mgr[1] == '0'):
                    _IxpLxc._lxc_exec(self, c, 'rm -r /srv/flowmanager') 
                _IxpLxc._lxc_exec(self, c, 'git clone https://github.com/martimy/flowmanager /srv/flowmanager') 
                _IxpLxc._lxc_exec(self, c, f'chown -R {username}: /srv/flowmanager')          
                
                # // Create the /srv/ryu dir //
                _ixp_debug('y',f'INFO[_ixp_software_install]: Install Ryu subclass')
                srv_ryu_dir = _IxpLxc._lxc_exec(self, c, f"[ -d '{self.srv_ryu_dir}' ]; echo $?") 
                if not (srv_ryu_dir[1] == '0'):
                    _IxpLxc._lxc_exec(self, c, f'mkdir -p {self.srv_ryu_dir}')       

                _IxpLxc._lxc_push(self, c, f'{var_dir}{self.ryu_sub_class}', f'{self.srv_ryu_dir}ixp_switch_13.py')
                _IxpLxc._lxc_exec(self, c, f'chown -R {username}: {self.srv_ryu_dir}') 

                # // Create the Ryu log file //
                ryu_log_dir = _IxpLxc._lxc_exec(self, c, f"[ -d '{self.ryu_log_dir}' ]; echo $?") 
                if not (ryu_log_dir[1] == '0'):
                    _IxpLxc._lxc_exec(self, c, f'mkdir -p {self.ryu_log_dir}')

                # // Copy Ryu Start/Stop Script //
                _ixp_debug('y',f'INFO[_ixp_software_install]: Copy the Ryu start/stop scripts')
                for r in [f'{self.ryu_service_script}', f'{self.ryu_start_script}', f'{self.ryu_stop_script}']:
                    _IxpLxc._lxc_push(self, c, f'{var_dir}{r}_template', f'{self.srv_ryu_dir}{r}')
                    _IxpLxc._lxc_exec(self, c, f'chown -R {username}: {self.srv_ryu_dir}{r}')
                    _IxpLxc._lxc_exec(self, c, f'chmod +x {self.srv_ryu_dir}{r}')

                # // Install Ryu systemd service //                                
                _ixp_debug('y',f'INFO[_ixp_software_install]: Copy the systemctl Ryu service template')
                s = self.ryu_systemd_service
                _IxpLxc._lxc_push(self, c, f'{var_dir}{s}_template', f'/etc/systemd/system/{s}')
                _IxpLxc._lxc_exec(self, c, f'chmod +x /etc/systemd/system/{s}')

                # // Enable and start the ryu service //
                _IxpLxc._lxc_exec(self, c, f'systemctl enable /etc/systemd/system/{self.ryu_service}')
                _IxpLxc._lxc_exec(self, c, f'systemctl start {self.ryu_service}')

                # // Rotate the logs hourly and only keep 24 logs //
                _ixp_debug('y',f'INFO[_ixp_software_install]: Create Ryu logrotate')

                with _ManagedFile('/tmp/ryu_log_rot') as ryu_log_rot:    
                    ryu_log_rot.write(f'{self.ryu_log_dir}{self.ryu_log}')
                    ryu_log_rot.write('\n  hourly')
                    ryu_log_rot.write('\n  missingok')
                    ryu_log_rot.write('\n  rotate 24')
                    ryu_log_rot.write('\n  compress')
                    ryu_log_rot.write('\n  notifempty')
                    ryu_log_rot.write(f'\n  create 0640 {username} {username}')
                    ryu_log_rot.write('\n  sharedscripts')
                    ryu_log_rot.write('\n  postrotate')
                    ryu_log_rot.write(f'\n      systemctl reload {self.ryu_service}')
                    ryu_log_rot.write('\n  endscript')
                    ryu_log_rot.write('\n}')

                _IxpLxc._lxc_push(self, c, '/tmp/ryu_log_rot', f'/etc/logrotate.d/{self.ryu_log_rot_file}')  
                subprocess.getstatusoutput ('rm /tmp/ryu_log_rot')                
                _IxpLxc._lxc_exec(self, c, f'chmod 644 /etc/logrotate.d/{self.ryu_log_rot_file}') 
                _IxpLxc._lxc_exec(self, c, f'chown root: /etc/logrotate.d/{self.ryu_log_rot_file}') 

                # // Put logrotate into cron.hourly instead of cron.daily //
                log_rot = _IxpLxc._lxc_exec(self, c, '[ -f "/etc/cron.daily/logrotate" ]; echo $?') 
                if (log_rot[1] == '0'):
                    _IxpLxc._lxc_exec(self, c, 'mv /etc/cron.daily/logrotate /etc/cron.hourly') 

                print (f'SDN Controller installed on {c}\n')
                _ixp_debug('y',f'INFO[_ixp_software_install]: SDN Controller installed on {c}')

        print ('IXP software installation completed\n')
        _ixp_debug('y', 'INFO[_ixp_software_install]: IXP software installation completed')

        return (0)

    ## End of _ixp_software_install method

    #============================================#
    # //      _ixp_software_test_options      // #
    #============================================#     

    def _ixp_software_test_options(self, args):
        ''' Method to test 'ixp_software' cli options '''

        first_options = ('install', 'configure', 'help')  
        str_ = str()
        list_ = list()
        list2_ = list()
        dict_ = dict()

        # // Iterate over strings in list and lowercase them //
        args = [string.lower() for string in args]

        # // IXP Schema elements //
        e = _ixp_schema_servers()

        # // Lowercase any instance of 'help' and swap 'help' for '?'//
        args = ['help' if x == '?' else x for x in args]
        args = ['help' if x.lower() == 'help'[:len(x)] else x for x in args]

        # // Assign command //
        cmd = args[0]

         # // Handle abbreviations //
        for x in first_options:
            if (cmd == x[:len(cmd)]):      
                cmd = x     
        
        # // Handle no command //
        if not (cmd in first_options):
            print (f'ERROR: {cmd} is not a valid option\n')
            _ixp_debug('y', f'ERROR[_ixp_software_test_options]: {cmd} is not a valid option')
            self._ixp_software_help_handler('help', 1)                   

        # // Check if help called first //
        if (cmd == 'help'):      
            self._ixp_software_help_handler('help', 0)

        # // Handle second level abbreviations //
        if (len(args) >= 2):
            if (args[1] == '?'):
                args[1] = 'help'
            else:
                if (args[1] == 'help'[:len(args[1])]):      
                    args[1] = 'help'

            # // Deal with help //
            if (args[1] == 'help'):
                if (cmd == 'install'):
                    self._ixp_software_help_handler('install', 0)
                elif (cmd == 'configure'):
                    self._ixp_software_help_handler('configure', 0)

        # // Handle no options given //
        if (len(args) == 1):
            print (f'No container options given, assuming \'all\'\n')
            _ixp_debug('y', f'INFO[_ixp_software_test_options]: No container options given, assuming \'all\'')
            list_ = e

        # // Handle 'all'
        elif (len(args) == 2 and args[1] == 'all'):
            list_ = e

        # // Handle 'all' mixed with other options //
        elif (len(args) > 2 and 'all' in args):
            print (f'ERROR: Cannot mix \'all\' with other options\n')
            _ixp_debug('y', f'ERROR[_ixp_software_test_options]: Cannot mix \'all\' with other options')
            self._ixp_software_help_handler('help', 1)

        # // Remove duplicates and test against server list //
        else:
            list_ = args[1:]
            list_ = list(set(list_))  # Remove duplicates  

            # // Test against server list //
            if not (set(list_).issubset(e)):
                list2_ = list()
                for x in list_:
                    if not (x in e):
                        list2_.append(x)

                if (len(list2_) == 1):
                    print (f'ERROR: {list2_[0]} is an illegal option\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_test_options]: {list2_[0]} is an illegal option')
                    self._ixp_software_help_handler('help', 1)         
                elif (len(list2_) > 1):             
                    str_ = ", ".join(str(x) for x in list2_)
                    print (f'ERROR: {str_} are illegal options.\n')
                    _ixp_debug('y', f'ERROR[_ixp_software_test_options]: {str_} are illegal options')
                    self._ixp_software_help_handler('help', 1)       
                else:  
                    assert False, 'ERROR: Not sure how we arrived here'

        # // Test that the container servers are running //        
        for x in list_:
            (start, status) = _IxpLxc._lxc_status_up(self, x)
            print (f'{x} status is {status}', end = ' ')

            # // Carry out Internet conenctivity test //
            status = int(_IxpLxc._lxc_internet_test(self, x)) 
            if (status == 100):
                print ('and has good Internet connectivity\n')     
            elif (status >= 50):
                print ('and has poor Internet connectivity\n')   
            else: 
                print (f'ERROR: {x} has no Internet connectivity, exiting\n')   
                _ixp_debug('y', f'ERROR[_ixp_software_test_options]: {x} has poor Internet connectivity, exiting')   
                sys.exit(1)  

        # // Create return dictionary //
        dict_['cmd'] = cmd
        dict_['ins'] = list_

        # // Return dictionary //
        return (dict_)
              
    ## End _ixp_software_test_options method

    #============================================#
    # //             ixp_software             // #
    #============================================#

    def ixp_software(self, args):
        ''' Method to handle 'ixp software' commands '''

        (dict_) = self._ixp_software_test_options(args)

        # // Check if 'install' called //
        if (dict_['cmd'] == 'install'):

            # // Install software in the servers //
            self._ixp_software_install(dict_['ins'])

        # // Check if 'configure' called //
        if (dict_['cmd'] == 'configure'):

            # // Configure software in the servers //
            self._ixp_software_configure(dict_['ins'])

        sys.exit(0)

    ## End to ixp_software method

########## // End IxpSoftware class // ##########

#################################################
# //               IxpSdn class              // #
#################################################

class IxpSdn:
    ''' IXP SDN Class '''

    def __init__(self):
        '''IxpSdn class constructor method'''

        self.functionality = 'Manage the SDN Controller of the IXP'
        self.dpid_ip_map = dict()
        self.dpid_sw_map = dict()
        self.dpid_port_map = dict()

        # // Get the name and management IP address of the SDN Controller //
        self.SC = [x for x in _ixp_schema_servers() if 'sc' == x[:2]][0]
        (self.sc_ipv4, self.unused_) = _read_db('schema', ['lxc', f"'{self.SC}'"])[self.SC]['ipv4_man'].split('/')
        
        ## End of __init__ method
        
    def __str__(self):
        ''' IxpSdn class __str__ method ''' 

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' IxpSdn class __repr__ method ''' 

        return (f'{self.__class__.__name__} class: {self.functionality}')    

        ## End of __repr__ method  

    #============================================#
    # //         _ixp_sdn_help_handler        // #
    #============================================#

    # // Help handler function //
    def _ixp_sdn_help_handler(self, command, err_code):
        ''' sdn help handler '''

        if (command == 'help'):
            IxpHelp._ixp_sdn_help(self, err_code)
        elif (command == 'list'):
            IxpHelp._ixp_sdn_list_help(self, err_code)
        elif (command == 'switch'):
            IxpHelp._ixp_sdn_list_switch_help(self, err_code)
        elif (command == 'flows'):
            IxpHelp._ixp_sdn_list_flows_help(self, err_code)
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        ## End to _ixp_sdn_help_handler method  

    #============================================#
    # //         _ixp_sdn_list_switch         // #
    #============================================#

    def _ixp_sdn_list_switch(self, kargs):
        ''' Method to list the SDN switches from the SDN Controller '''

        list_ = list()
        dict_ = dict()
        url_ = 'http://<<ip_address>>:8080/stats/desc/'
        self._get_switch_maps()

        # // Get switches //
        if ('switch_number' in kargs.keys()):
            for k, v in self.dpid_sw_map.items():
                if (v == kargs['switch_number']):
                    list_ = [int(k)]
        else:
            # // Get list of dictionary keys based on sort of values //
            list_ = [x[0] for x in sorted(self.dpid_sw_map.items(), key=lambda x: x[1])]

        # // Loop through list of switches //      
        for s in list_:
            dict_ = self._scrape_rest_api(f'{url_}{s}')
            if (self.dpid_sw_map[str(s)] == '0'):
                str_ = '\nSwitch Number: 0 (internal OvS)'
            else:
                str_ = f'\nSwitch Number: {self.dpid_sw_map[str(s)]} (External Sw)'
            print (str_)
            print (f"{'-' * (len(str_) - 1)}")
            print (f"Switch ID     : {s}")
            print (f"Manufacturer  : {dict_[str(s)]['mfr_desc']}")
            print (f"Hardware Desc : {dict_[str(s)]['hw_desc']}")
            print (f"Software Desc : {dict_[str(s)]['sw_desc']}")
            print (f"Serial number : {dict_[str(s)]['serial_num']}")
            print (f"DP Description: {dict_[str(s)]['dp_desc']}")
            print (f"IP Address    : {self.dpid_ip_map[str(s)]}")

        print ('')

        sys.exit(0)

        ## End to _ixp_sdn_list_switch method  

    #============================================#
    # //         _ixp_sdn_list_flows          // #
    #============================================#

    def _ixp_sdn_list_flows(self, kargs):
        ''' Method to list the SDN flows from the SDN Switches '''

        list_ = list()
        url_ = 'http://<<ip_address>>:8080/stats/flow/'

        self._get_switch_maps()

        # // Get switches //
        if ('switch_number' in kargs.keys()):
            for k, v in self.dpid_sw_map.items():
                if (v == kargs['switch_number']):
                    list_ = [int(k)]
        else:
            # // Get list of dictionary keys based on sort of values //
            list_ = [x[0] for x in sorted(self.dpid_sw_map.items(), key=lambda x: x[1])]
       
        for s in list_:
            dict_ = self._scrape_rest_api(f'{url_}{s}')
            if (self.dpid_sw_map[str(s)] == '0'):
                str_ = '\nSwitch Number: 0 (internal OvS)'
            else:
                str_ = f'\nSwitch Number: {self.dpid_sw_map[str(s)]} (External Sw)'
            print (str_)
            print (f"{'-' * (len(str_) - 1)}") 
            for f in dict_[str(s)]:
                print (f"P: {f['priority']} ", end = '')
                print (f"HT: {f['hard_timeout']} ", end = '')
                print (f"IT: {f['idle_timeout']} ", end = '')
                print (f"M: {f['match']} ", end = ' A: ')
                for a in f['actions']:
                    print (a, end = '')
                print ('')

        print ('\nP - Priority, H - Hard timeout, I - Idle timeout, M - Match, A - Actions')

        print ('')

        sys.exit(0)

        ## End to _ixp_sdn_list_flows method  

    #============================================#
    # //           _scrape_rest_api           // #
    #============================================#

    def _scrape_rest_api(self, url_):
        ''' Method to scrape RESTful API '''

        list_ = list()

        # Insert the IP address in the URL //
        url_ = url_.replace('<<ip_address>>', self.sc_ipv4)

        # // Grab data from RESTful API and return it //
        try:
            f = urllib.request.urlopen(url_).read()
            list_ = json.loads(f.decode('utf-8'))

        except urllib.error.HTTPError as e:
            print (e.code)
            print (e.read())
            print (f'ERROR: Connection to the RESTful API on {self.sc_ipv4} failed\n')
            _ixp_debug('y',f'ERROR[_scrape_rest_api]: Connection to the RESTful API on {self.sc_ipv4} failed')
            sys.exit(1)

        return (list_)

        ## End to _scrape_rest_api method  

    #============================================#
    # //           _get_switch_maps           // #
    #============================================#

    def _get_switch_maps(self):
        ''' Method to get list of OpenFlow switches from Controller '''

        list_ = list()
        dict_ = {'230': '0'}   

        for c, x in enumerate(range(241, 250)):
            dict_[str(x)] = str(c + 1)

        list_ = self._scrape_rest_api('http://<<ip_address>>:8080/stats/switches')
        f = _IxpLxc._lxc_exec(self, self.SC, f'cat {dpid_ip_filename}')[1] 
        self.dpid_ip_map = json.loads(f)

        # // Test list from RESTful API and SDN Controller direct are equal //
        if not ([str(x) for x in list_] == list(self.dpid_ip_map.keys())):
            print ('ERROR: RESTful API and SDN Controller reporting different DPIDs\n')
            _ixp_debug('y', 'ERROR[_get_switch_maps]: RESTful API and SDN Controller reporting different DPIDs')
        else:
            _ixp_debug('y', 'INFO[_get_switch_maps]: Received DPIDs from the SDN Controller with associated IP addresses')

        # // Match DPID to switch number //
        for k, v in self.dpid_ip_map.items():
            if (v[-3:] in dict_.keys()):
                self.dpid_sw_map[k] = dict_[v[-3:]]

        return (0)

        ## End to _get_switch_maps method  

    #============================================#
    # //            _dpid_port_map            // #
    #============================================#

    def _dpid_port_map(self):
        ''' Method to get switch and port numbers from the database and match dpid '''

        dpid_ = str()
        dpid2_ = str()
        set_ = set()
        dict_ = dict()

        # // Get switch and port number data from database //
        for x in ['ipv4_peer','ipv6_peer']:
            dict_ = _read_db(x,['function', f"'peer'", 'enabled', 1, 'reserved', 1])
            for v in dict_.values():
                if not (str(v['switch_number']) in self.dpid_sw_map.values()):
                    set_.add(str(v['switch_number'])) 
                else:
                    dpid_ = list(self.dpid_sw_map.keys())[list(self.dpid_sw_map.values()).index(str(v['switch_number']))]
                    if (dpid_ in self.dpid_port_map.keys()):
                        self.dpid_port_map[dpid_].append(v['port_number'])
                    else: 
                        self.dpid_port_map[dpid_] = [v['port_number']] 

        for k, v in self.dpid_port_map.items():
            self.dpid_port_map[k] = sorted(list(set(self.dpid_port_map[k])))

        # // Get the dpid for the internal OvS (0)
        dpid2_ = list(self.dpid_sw_map.keys())[list(self.dpid_sw_map.values()).index('0')]

        # // Add 1000 to the internal OvS port numbers if they exist //
        if (dpid2_ in self.dpid_port_map.keys()):
            self.dpid_port_map[dpid2_][:] = [x + 1000 for x in self.dpid_port_map[dpid2_]]

        # // Report on missing switches //
        if (len(set_) > 0):
            for x in set_:
                print (f"ERROR: There is no OpenFlow switch: {x}\n")
                _ixp_debug('y', f"ERROR[_dpid_port_map]: There is no OpenFlow switch: {x}") 

        return (0)

        ## End to _dpid_port_map method  

    #============================================#
    # //          _add_delete_flow            // #
    #============================================#

    def _add_delete_flow(self, instruction, dpid, priority, kargs):
        ''' Method to add flows to OpenFlow switches via RESTful API '''

        # // Expect kargs --> {'match': {'in_port': <port>}, 'actions':[{"type":"OUTPUT", "port": 2}]

        idle_timeout = 0
        hard_timeout = 0
        flags = 0
        match = dict()     # {"in_port":1}
        actions = list()   # [{"type":"OUTPUT", "port": 2}]

        # // Method maybe called from outside class to instantiate //
        s = IxpSdn()
        s._get_switch_maps()
        match = kargs['match']
        actions = kargs['actions']
        url_ = f'http://{s.sc_ipv4}:8080/stats/flowentry/{instruction}'

        # // Flow template //
        flow = {"dpid": int(dpid),
                "idle_timeout": idle_timeout,
                "hard_timeout": hard_timeout,
                "priority": priority,
                "flags": flags,
                "match": match,
                "actions": actions
               }

        # // Push flow to RESTful API //
        try:
            data = str(flow).encode('utf-8')
            request = urllib.request.Request(url_, data)
            urllib.request.urlopen(request)
            _ixp_debug('y',f'INFO[_add_delete_flow]: {instruction.title()} flow, via the SDN Controller {s.sc_ipv4} RESTful API')

        except urllib.error.HTTPError as e:
            print (e.code, e.read())
            print (f'ERROR: Failed to {instruction} flow, connection to the RESTful API on {s.sc_ipv4} failed\n')
            _ixp_debug('y',f'ERROR[_add_delete_flow]: Failed to {instruction} flow, connection to the RESTful API on {s.sc_ipv4} failed')
            sys.exit(1)

        return (0)

        ## End to _add_delete_flow method  

    #============================================#
    # //             _clear_flows             // #
    #============================================#

    def _clear_flows(self, switch):
        ''' Method to clear all flows from an OpenFlow switch via RESTful API '''

        dpid = 0

        # // Method maybe called from outside class to instantiate //
        s = IxpSdn()
        s._get_switch_maps()

        # // Get DPID from dpid_sw_map //
        if not (switch in s.dpid_sw_map.values()):
            _ixp_debug('y',f'INFO[_clear_flows]: {switch} does not have a DPID in the DPID/Switch map')
            return(0)

        else:
            dpid = list(s.dpid_sw_map.keys())[list(s.dpid_sw_map.values()).index(str(switch))] 

            url_ = f'http://{s.sc_ipv4}:8080/stats/flowentry/clear/{dpid}'

            # // Delete flows via RESTful API //
            try:
                request = urllib.request.Request(url_)
                request.get_method = lambda: "DELETE"        # Override the urllib methods GET and POST
                urllib.request.urlopen(request)
                _ixp_debug('y',f'INFO[_clear_flows]: Flows on switch: {switch} cleared via the SDN Controller {s.sc_ipv4} RESTful API')

            except urllib.error.HTTPError as e:
                print (e.code, e.read())
                print (f'ERROR: Failed to clear flows on switch: {switch}, connection to the RESTful API on {s.sc_ipv4} failed\n')
                _ixp_debug('y',f'ERROR[_clear_flows]: Failed to clear flows on switch: {switch}, connection to the RESTful API on {s.sc_ipv4} failed')
                sys.exit(1)

        return (0)

        ## End to _clear_flows method  

    #============================================#
    # //         _ixp_sdn_test_options        // #
    #============================================#

    def _ixp_sdn_test_options(self, args):
        ''' Method to test 'ixp_sdn' cli options '''

        str_ = str()
        dict_ = dict()
        first_options = ['list', 'help']

        self._get_switch_maps()
        sw_str_ = ', '.join(x for x in list(self.dpid_sw_map.values()))

        # // lowercase all arguments //
        args = [x.lower() for x in args]
 
        # // Replace any instance of 'show' with 'list' //
        args = ['list' if x == 'show' else x for x in args]

        # // Swap 'help' for '?'//
        args = ['help' if x == '?' else x for x in args]
        args = ['help' if x == 'help'[:len(x)] else x for x in args]

        # // Handle abbreviations //
        for x in first_options:
            if (x[:len(args[0])] == args[0]):
                args[0] = x

        # // Assign args to the dictionary //
        if (args[0] in first_options):
            dict_['cmd'] = args[0]
        else:
            str_ = ', '.join(x for x in first_options[:-1])
            print (f"ERROR: Not a valid choice, select '{str_} or {first_options[-1]}'\n")
            _ixp_debug('y',f"ERROR[ixp_sdn]: Not a valid choice, select '{str_} or {first_options[-1]}'")                
            self._ixp_sdn_help_handler('help', 1)            

        # // Handle help //
        if (args[0] == 'help'): 
            self._ixp_sdn_help_handler(dict_['cmd'], 0) 

        # // Handle one element //
        if (len(args) == 1):
            dict_['ins'] = ''

        # // Handle two elements //
        elif (len(args) > 1):
            dict_['ins'] = args[0]
            dict_['elm'] = {}

            # // Handle help //
            if (args[1] == 'help'): 
                self._ixp_sdn_help_handler(dict_['cmd'], 0)  

            # // Handle switch //
            if (args[1] == 'switches'[:len(args[1])]):
                dict_['ins'] = 'switch'

            # // Handle flow //
            if (args[1] == 'flows'[:len(args[1])]):
                dict_['ins'] = 'flows'

            # // Handle help //
            if ('help' in args[2:]):
                self._ixp_sdn_help_handler(dict_['ins'], 0)  

        # // Handle second options //
        if (len(args) == 2):
            return (dict_)

        elif (len(args) == 3):
            if (args[2] == 'all'[:len(args[2])]):
               return (dict_) 
            else:
                print (f'ERROR: Not a valid choice: {args[2]}\n')
                _ixp_debug('y', f'ERROR: Not a valid choice: {args[2]}')
                self._ixp_sdn_help_handler(dict_['ins'], 1)

        elif (len(args) == 4):
            if (args[2] in ['--switch-number', '-s']):
                if (args[3] in self.dpid_sw_map.values()):
                    dict_['elm'] = {'switch_number': args[3]}
                else:
                    print (f'ERROR: Not a legimate option {args[3]}, choose from {sw_str_}\n')
                    _ixp_debug('y',f'ERROR[_ixp_schema_test_options]: Not a legimate option {args[3]}, choose from {sw_str_}')                      
                    self._ixp_sdn_help_handler(dict_['ins'], 1)

            else:
                print (f"ERROR: Not a valid choice '{args[2]}, {args[3]}'\n")
                _ixp_debug('y',f"ERROR[ixp_sdn]: Not a valid choice '{args[2]}, {args[3]}'")                
                self._ixp_sdn_help_handler(dict_['ins'], 1)

        else:
            print (f"ERROR: Choices made are not valid\n")
            _ixp_debug('y',f"ERROR[ixp_sdn]: Choices made are not valid'")  
            if (dict_['cmd'] == 'list'):           
                self._ixp_sdn_help_handler('list', 1)
            else:
                self._ixp_sdn_help_handler('help', 1)

        return (dict_)
 
    ## End _ixp_sdn_test_options method  

    #============================================#
    # //                ixp_sdn               // #
    #============================================#

    def ixp_sdn(self, args):
        ''' Method to handle 'ixp sdn' commands '''

        (dict_) = self._ixp_sdn_test_options(args)

        # // Check for 'list' called //
        if (dict_['cmd'] == 'list'):

            # // handle list switch //
            if (dict_['ins'] == 'switch'):
                self._ixp_sdn_list_switch(dict_['elm'])

            # // handle list flows //
            elif (dict_['ins'] == 'flows'):
                self._ixp_sdn_list_flows(dict_['elm']) 

            else:
                assert False, 'ERROR: Not sure how we arrived here'

        sys.exit(0)

    ## End to ixp_sdn method

############# // End IxpSdn class // ############

#################################################
# //              IxpPeer class              // #
#################################################

class IxpPeer:
    ''' IXP Peer Class '''

    def __init__(self):
        '''IxpPeer class constructor method'''

        self.functionality = 'Manage member peers of the IXP'
        
        self.peer_add_dict = {'-n':'--name',
                              '-a':'--as-number',
                              '-d':'--domain',
                              '-rs':'--route-server',
                              '-bs':'--blackhole-server',
                              '-is':'--interface-speed',                              
                              }

        ## End of __init__ method
        
    def __str__(self):
        ''' IxpPeer class __str__ method ''' 

        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        ''' IxpPeer class __repr__ method ''' 

        return (f'{self.__class__.__name__} class: {self.functionality}')    

        ## End of __repr__ method  

    #============================================#
    # //        _ixp_peer_help_handler        // #
    #============================================#

    # // Help handler method //
    def _ixp_peer_help_handler(self, command, err_code):
        ''' Peer help handler '''

        if (command == 'help'):
            IxpHelp._ixp_peer_help(self, err_code)
        elif (command == 'add'):
            IxpHelp._ixp_peer_add_help(self, err_code)
        elif (command == 'delete'):
            IxpHelp._ixp_peer_delete_help(self, err_code)
        elif (command == 'list'):
            IxpHelp._ixp_peer_list_help(self, err_code)
        elif (command == 'status'):
            IxpHelp._ixp_peer_status_help(self, err_code)
        elif (command == 'route'):
            IxpHelp._ixp_peer_route_help(self, err_code)
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        ## End to _ixp_peer_help_handler method

    #============================================#
    # //            _ixp_peer_sync            // #
    #============================================#

    def _ixp_peer_sync(self, arg = 'noisy'):
        ''' Method to add a peer to a route server '''

        SC = str()
        url_ = str()
        list2_ = list()
        dict_ = dict()
        dict2_ = dict()
        dict3_ = dict()

        # // Get list of applicable servers //
        list_ = [x for x in _ixp_schema_servers() if x[:2] in ['cs','rs','bs']]

        # // Loop through each server with a bird daemon //
        for x in list_:
            file_list = [f"{var_dir}{x}_bird.conf", f"{var_dir}{x}_bird6.conf"]

            # // Loop through IPv4 and IPv6 configurations //
            for c, y in enumerate(['ipv4_peer', 'ipv6_peer']):

                # // Which file //
                file = file_list[c] 

                file_dir_temp = '/tmp/bird.conf_temp_file' 

                if not (_get_file_dir_info(file)['type'] == 'file'):
                    print (f"ERROR: The file {file} doesn't exist, was the 'ixp software install/configure' process followed?\n")
                    _ixp_debug('y',f"ERROR[_ixp_peer_sync]: The file {file} doesn't exist, was the 'ixp software install/configure' process followed?")
                    sys.exit(1)

                # // Copy files to /tmp for editing //
                subprocess.getstatusoutput (f"cp {file} {file_dir_temp}")

                # // Grab data from the database //
                dict_ = _read_db(y, 'function', "'peer'", 'enabled', 2, 'reserved', 1)

                # // Generate a multiline string to append to the template file //
                str_ = str()

                for k, v in dict_.items():
                    if (v['route_server'] == 'no' and x[:2] == 'rs'):
                        continue
                    elif (v['blackhole_server'] == 'no' and x[:2] == 'bs'):
                        continue

                    # // Add to templates //
                    ip, unused_ = k.split('/')

                    str_ = str_ + (f"protocol bgp {v['assigned']} from PEERS {{\n"
                                   f"        neighbor {ip} as {v['as_number']};\n"
                                   f"}}\n\n"
                                  )

                # // Append to the template file //
                with _ManagedFile(file_dir_temp, 'a') as birdfh:
                    birdfh.write(str_) 

                # // Load to the server container //
                if ('4' in y):
                    _IxpLxc._lxc_push(self, x, file_dir_temp, f'{bird_root}{bird_file}')
                    _IxpLxc._lxc_exec(self, x, f"chown bird: {bird_root}{bird_file}")
                    _IxpLxc._lxc_exec(self, x, f"chmod 640 {bird_root}{bird_file}")
                elif ('6' in y):
                    _IxpLxc._lxc_push(self, x, file_dir_temp, f'{bird_root}{bird6_file}')
                    _IxpLxc._lxc_exec(self, x, f"chown bird: {bird_root}{bird6_file}")
                    _IxpLxc._lxc_exec(self, x, f"chmod 640 {bird_root}{bird6_file}")
                else:
                    assert False, 'ERROR: Not sure how we arrived here'

            # // Restart BIRD daemons on the container //
            _IxpLxc._bird_restart(self, x)

            # // Quiet mode //
            if (arg == 'noisy'):
                print (f'Restarting BIRD Server on {x}\n')
            _ixp_debug('y', f'INFO [_ixp_peer_sync]: Restarting BIRD Server on {x}')

        # // Manage inactive ports to drop flows at the SDN Controller //
        if (_switching_type() == 'software-defined'):
            s = IxpSdn()
            s._get_switch_maps()
            s._dpid_port_map()

            # // Get SDN Controller //
            SC = [x for x in _ixp_schema_servers() if x[:2] == 'sc'][0]

            # // Delete all flows with priority 99 (Default drop for peers) //
            list2_ = s.dpid_sw_map.keys()
            for x in list2_:
                url_ = f'http://{s.sc_ipv4}:8080/stats/flow/{x}'             
                dict2_ = s._scrape_rest_api(url_)    
                for k, v in dict2_.items():
                    for y in v:
                        if (y['priority'] == 99):
                            dict3_['match'] = y['match']
                            dict3_['actions'] = []
                            s._add_delete_flow('delete', x, 99, dict3_)
            _ixp_debug('y', f'INFO [_ixp_peer_sync]: Deleting all drop flows from switches')

            # // Add flows for unconfigured ports //
            for k, v in s.dpid_port_map.items():
                for y in v: 
                    s._add_delete_flow('add', k, 99, {'match': {"in_port": y}, 'actions': []})
            _ixp_debug('y', f'INFO [_ixp_peer_sync]: Adding drop flows to switches for inactive peering ports')

            # // Update dpid - port map on SDN Controller // 
            with open(f'/tmp/{dpid_port_file}', mode='w', encoding='utf-8') as fh:
                fh.write(json.dumps(s.dpid_port_map))
            _IxpLxc._lxc_push(self, SC, f'/tmp/{dpid_port_file}', f'{dpid_port_dir}{dpid_port_file}') 
            _ixp_debug('y', f'INFO [_ixp_peer_sync]: Updating DPID to port map on the SDN Controller')

        return(0)

    ## End of _ixp_peer_sync method

    #============================================#
    # //            _ixp_peer_delete          // #
    #============================================#

    def _ixp_peer_delete(self, kargs):
        ''' Method to delete a peer from the database ''' 

        str_ = str()
        list_ = list()
        list2_ = list()
        dict_ = dict()
        dict2_ = dict()
        dict3_ = dict()

        # // Generate 'assigned from 'name' //
        if ('name' in kargs.keys()):
            kargs['assigned'] = kargs['name'].replace(' ', '_')

        # // Confirm kargs has either an assigned name or an AS number //
        flag = 0
        if ('assigned' in kargs.keys()):
                flag += 1
                dict_['assigned'] = kargs['assigned']
                list_.append(kargs['assigned'])
        if ('as_number' in kargs.keys()):
                flag += 1
                dict_['as_number'] = kargs['as_number'] 
                list_.append(kargs['as_number']) 

        if ('domain' in kargs.keys()):
            print (f"INFO: Deleting based on 'domain' not allowed\n")   
            _ixp_debug('y', f"INFO[_ixp_peer_delete]: Deleting based on 'domain' not allowed\n") 

        # // Make sure one of the valid options exist //
        if (flag == 0):
            print (f"ERROR: No valid criteria given to permit peer delete\n")   
            _ixp_debug('y', f"ERROR[_ixp_peer_delete]: No valid criteria given to permit peer delete\n")
            sys.exit(1)

        # // Peer map //
        list2_ = ['function', "'peer'", 'enabled', '2', 'reserved', '1']

        for k, v in dict_.items():
            if (k == 'assigned'):
                list2_.extend([f'{k}',f"'{v}'"])
            else:
                list2_.extend([f'{k}',f'{str(v)}'])

        # // Values dictionary //
        dict2_ = {'name': None, 'assigned': None, 'as_number': None, 
                  'domain': None, 'route_server': None, 'blackhole_server': None, 
                  'enabled': 1, 'reserved': 1}

        # // Get the key (IP address) and update //
        for x in ['ipv4_peer','ipv6_peer']: 
            dict3_ = _read_db(x, list2_)

            if not (_is_empty(dict3_)): 
                str_ = list(dict3_.keys())[0] # IP address key of entry
                _ixp_database_update(x, f"'{str_}'", dict2_) 
                print (f"Deleted the '{str_}' peer from the {x.replace('_',' ')} database table\n")
                _ixp_debug('y', f"INFO[_ixp_peer_delete]:Deleted the '{str_}' peer from the {x.replace('_',' ')} database table")
            else:
                print (f"There is no '{list(kargs.values())[0]}' peer in the {x.replace('_',' ')} database table\n")
                _ixp_debug('y', f"INFO[_ixp_peer_delete]: There is no '{list(kargs.values())[0]}' peer in the {x.replace('_',' ')} database table")

        # // Sync the database quietly //
        self._ixp_peer_sync('quiet')

        return (0)

    ## End of _ixp_peer_delete method

    #============================================#
    # //             _ixp_peer_add            // #
    #============================================#

    def _ixp_peer_add(self, kargs):
        ''' Method to add a peer to a route server '''

        str2_ = str()
        list_ = list()
        dict_ = dict()
        dict2_ = dict()
        dict3_ = dict()

        # // Add default interface speed value if none exists //
        if not ('interface_speed' in kargs.keys()):
            kargs['interface_speed'] = '1G'
        else:
            kargs['interface_speed'] = kargs['interface_speed'].upper()

        # // Assemble a dictionary for upload // 
        dict_ = {'name': '',
                 'assigned': '',
                 'domain': '',
                 'route_server': 'yes',
                 'blackhole_server': 'yes',
                 'as_number': 0,
                 'enabled': 2,
                }

        # // Test for name, AS number and domain at least //
        for x in ['name', 'as_number', 'domain']:
            if not (x in kargs.keys()):
                print (f"ERROR: 'name', 'as_number' and 'domain' are mandatory\n")
                _ixp_debug('y', f"ERROR[_ixp_peer_add]: 'name', 'as_number' and 'domain' are mandatory")     
                self._ixp_peer_help_handler('add', 1)

        # // Generate 'assigned' from 'name' //
        if ('name' in kargs.keys()):
            kargs['assigned'] = kargs['name'].replace(' ', '_')

        # // Testing ISP is not already configured //
        flag = 0
        for x in ['ipv4_peer', 'ipv6_peer']:            
            dict2_ = _read_db(x, 'function', "'peer'", 'enabled', 2, 'reserved', 1)
            list_ = list(dict2_.keys())

            # // Test for repeated items //
            for k, v in kargs.items():
                if (k in ['route_server', 'blackhole_server', 'interface_speed']):
                    continue

                for y in list_:
                    if (v == dict2_[y][k]):
                        _ixp_debug('y', f"ERROR[_ixp_peer_add]: There is already an {k}: {v} in the database")
                        flag += 1

        # // Exiting if there are duplicates //
        if (flag > 0):
            print (f"ERROR: This ISP is already configured in the database, exiting ....\n")
            _ixp_debug('y', f"ERROR[_ixp_peer_add]: This ISP is already configured in the database, exiting ....")     
            sys.exit(1)       

        # // Mapping kargs to the template //
        for k, v in kargs.items():
            dict_[k] = v

        # // Adding new ISP //
        for x in ['ipv4_peer', 'ipv6_peer']:
            dict3_ = dict()
            if (len(_read_db(x, 'function', "'peer'", 'interface_speed', f"'{kargs['interface_speed']}'",'enabled', 1, 'reserved', 1).keys()) > 0):       
                str2_ = list(_read_db(x, 'function', "'peer'", 'interface_speed', f"'{kargs['interface_speed']}'", 'enabled', 1, 'reserved', 1).keys())[0]
            else:
                print (f"ERROR: IXP switches have no suitable ports configured yet, try the 'ixp switch' command first\n")
                _ixp_debug('y', f"ERROR[_ixp_peer_add]: IXP switches have no suitable ports configured yet, try the 'ixp switch' command first")     
                sys.exit(1) 

            # // Push configuration to the database //
            _ixp_database_update(x, f"'{str2_}'", dict_) 

            # // Extract data from the row //
            dict3_ = _read_db(x, 'addr', f"'{str2_}'")[str2_]
        
            print (f"'{dict_['name']}' assigned {x.replace('_', ' ')} address {str2_} " 
                  f"on switch {dict3_['switch_number']}, port {dict3_['port_number']}\n")
            _ixp_debug('y', f"INFO[_ixp_peer_add]:'{dict_['assigned']}' assigned {x.replace('_', ' ')} "   
                            f"address {str2_} on switch {dict3_['switch_number']}, port" 
                            f"{dict3_['port_number']}")

        # // Sync the database quietly //
        self._ixp_peer_sync('quiet')

        return (0)

    ## End of _ixp_peer_add method


    #============================================#
    # //            _ixp_peer_list            // #
    #============================================#

    def _ixp_peer_list(self, kargs):
        ''' Method to get peer data from the database '''

        ip_list = list()

        if (kargs == {}):
            kargs = {'ip_type':'both'}

        if (kargs['ip_type'] == 'both'):
            ip_list = ['ipv4_peer', 'ipv6_peer']
        elif (kargs['ip_type'] == 'ipv4'):
            ip_list = ['ipv4_peer']
        elif (kargs['ip_type'] == 'ipv6'):
            ip_list = ['ipv6_peer']
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        def _ixp_peer_list_v46(ip):
            ''' Method to handle IPv4 or IPv6   '''

            # // Grab database information //

            str_ = str()
            dict_ = dict()

            if (len(_read_db(ip, 'function', "'peer'", 'enabled', 2, 'reserved', 1).keys()) > 0):
                   dict_ = _read_db(ip, 'function', "'peer'", 'enabled', 2, 'reserved', 1)

            if (_is_empty(dict_)):
                print (f'No peers exist in the database\n')
                _ixp_debug('y', f'INFO[_ixp_peer_list]: No peers exist in the database')
                sys.exit(0)

            # // Build and print table //

            # // Determine IP type //
            if ('4' in ip):
                ip_title = f'IPv4 Peering Table'
            elif ('6' in ip):
                ip_title = f'IPv6 Peering Table'
            else:
                assert False, 'ERROR: Not sure how we arrived here'

            padder = {'p0':4,'p1':3,'p2':6,'p3':4,'p4':4,'p5':6,'p6':5,'p7':5,'p8':10}
            list2_ = ['Name','ASN','Switch','Port','speed','Domain','RS', 'AS112', 'IP Address']

            # // Dictionary Keys //
            keys_ = list(dict_.keys())

            keys2_ = ['name', 'as_number', 'switch_number', 'port_number', 'interface_speed', 'domain', 'route_server', 'blackhole_server']

            # // Loop through and update padder //
            for x in keys_:        
                if len(x) > padder['p8']: padder['p8'] = len(x)
                for c, y in enumerate(keys2_):
                    if len(str(dict_[x][y])) > padder[f'p{c}']: padder[f'p{c}'] = len(str(dict_[x][y]))

            # // Top line //
            top_line = f"  + {'-' * (sum(padder.values()) + 42)} +"
            print (top_line)

            # // Title //
            print (f"  |{ip_title:^{sum(padder.values()) + 43}} |")

            # // Create line //
            list3_ = [' ']
            for c in range(0, 9):
                list3_.append(f" + {'-' * (padder[f'p{c}'] + 2)}")
            list3_.append(' +')
            str_ = ''.join(x for x in list3_)

            # // print line //
            print (str_)

            # // Print Headings //
            print (' ', end = '')
            for c, x in enumerate (list2_):
                print (f" | {x:^{padder[f'p{c}'] + 2}}", end = '')
            print (' |')

            # // print line //
            print (str_)

            # // Loop through and print data //
            for x in keys_: 
                print (' ', end = '')       
                for c, y in enumerate(keys2_):
                    if (0 < c < 8):
                        print (f" | {dict_[x][y]:^{padder[f'p{c}'] + 2}}", end = '')
                    else:
                        print (f" | {dict_[x][y]:<{padder[f'p{c}'] + 2}}", end = '') 

                # // Add IP address  
                print (f" | {x:<{padder['p8'] + 2}} ", end = '')                        
                print ('|')

            # // print line //
            print (str_)

            print ('')

            return (0)

            ## End of _ixp_peer_list_v46 method

        # // Handle tables //
        for x in ip_list:
            _ixp_peer_list_v46(x)

        return (0)

    ## End of _ixp_peer_list method

    #============================================#
    # //          _ixp_peer_status            // #
    #============================================#

    def _ixp_peer_status(self, kargs):
        ''' Method to get BGP status from BIRD servers '''

        # // Determine IPv4 or IPv6 or both //

        if (kargs == {}):
            kargs = {'ip_type':'both'}

        if (kargs['ip_type'] == 'both'):
            ip_list = ['ipv4', 'ipv6']
        elif (kargs['ip_type'] == 'ipv4'):
            ip_list = ['ipv4']
        elif (kargs['ip_type'] == 'ipv6'):
            ip_list = ['ipv6']
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        def _ixp_peer_status_v46(ip):
            ''' Method to get peer status for IPv4 or IPv6   '''

            dict_ = dict()

            # // Start padder with the width of the labels //
            padder = {'p0':6,'p1':4,'p2':5,'p3':5}

            # // Get list of applicable servers //
            servers = [x for x in _ixp_schema_servers() if x[:2] in ['rs','cs','bs']]

            # // Loop through the three containers with BIRD daemons //
            for s in servers:
                str_ = str()
                str2_ = str()
                list_ = list()
                list2_ = list()
                list3_ = list()

                if (len(s) > padder['p0']):
                    padder['p0'] = len(s) 

                # // Grab detail from the bird daemons //
                if (ip == 'ipv4'):
                    str_ = _IxpLxc._lxc_exec(self, s, "birdc show protocol")[1]
                elif (ip == 'ipv6'):
                    str_ = _IxpLxc._lxc_exec(self, s, "birdc6 show protocol")[1]   
                else:
                    assert False, 'ERROR: Not sure how we arrived here'
                                     
                list_ = [x for x in str_.split('\n') if 'BGP' in x]

                # // Break data down into dictionaries //
                for y in list_:
                    a = y.strip().split()[0]
                    b = y.strip().split()[3]
                    c = y.strip().split()[5]
                    if len(a) > padder['p1']: padder['p1'] = len(a)
                    if len(b) > padder['p2']: padder['p2'] = len(b)
                    if len(c) > padder['p3']: padder['p3'] = len(c)
                    list2_.append(a); list2_.append(b); list2_.append(c)
                dict_[s] = list2_

            # // Add 2 to each pad //
            list3_ = padder.values()
            list3_ = [x + 2 for x in list3_ ] 

            # // Create line //
            list2_ = [' ']
            for x in list3_:
                list2_.append(f" + {'-' * x}")
            list2_.append(' +')
            str_ = ''.join(x for x in list2_)

            # // Top line //
            print (f"  + {'-' * (sum(list3_) + 9)} +")

            # // Print Header //
            str2_ = f"IPv{ip[-1]} BGP State Table" 
            print (f"  |{str2_:^{(sum(list3_) + 11)}}|")

            # // Print line //
            print (str_)

            # // Print data //
            print ('  | ', end = '')
            for c, x in enumerate(['Server', 'Name', 'State', 'Info']):
                print (f"{x:^{padder[f'p{c}'] + 2}} | ", end = '')
            print ('')

            # // print line //
            print (str_)

            # // Loop through servers //
            for x in servers:
                for y in range(0,len(dict_[x]), 3):
                    if (y == 0):
                        print (f"  | {x:^{list3_[0]}} ", end = '')
                    else:
                        print (f"  | {' ':^{list3_[0]}} ", end = '')

                    print (f"| {dict_[x][y]:<{list3_[1]}} ", end = '')
                    print (f"| {dict_[x][y + 1]:<{list3_[2]}} ", end = '')
                    print (f"| {dict_[x][y + 2]:<{list3_[3]}} |", end = '')
                    print ('')

                # // print line //
                print (str_)

            print ('')

            return (0)

            ## End of _ixp_peer_status_v46 method

        # // Handle tables //
        for x in ip_list:
            _ixp_peer_status_v46(x)

        sys.exit(0)

    ## End of _ixp_peer_status method

    #============================================#
    # //          _ixp_peer_route             // #
    #============================================#

    def _ixp_peer_route(self, kargs):
        ''' Method to get route tables from BIRD servers '''

        # // Method to determine IPv4 or IPv6 or both //

        if (kargs == {}):
            kargs = {'ip_type':'both'}

        if (kargs['ip_type'] == 'both'):
            ip_list = ['ipv4', 'ipv6']
        elif (kargs['ip_type'] == 'ipv4'):
            ip_list = ['ipv4']
        elif (kargs['ip_type'] == 'ipv6'):
            ip_list = ['ipv6']
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        def _ixp_peer_route_v46(ip):
            ''' Method to get IPv4 or IPv6 routes tables '''

            str_ = str()
            dict_ = dict()
            dict2_ = dict()
            dict3_ = dict()

            # // Start padder with the width of the labels //
            padder = {'p0':6,'p1':5,'p2':3,'p3':9,'p4':6,'p5':6,'p6':3}

            # // Get list of applicable servers //
            servers = [x for x in _ixp_schema_servers() if x[:2] in ['rs','cs','bs']]

            # // Loop through the three containers with BIRD daemons //
            for s in servers:
                str2_ = str()
                list_ = list()
                list2_ = list()
                list3_ = list()      
                list4_ = list()  
                list5_ = list()        
                dict_[s] = list()
                dict2_[s] = list()
                list3_ = ['IP', 'GW', 'INT', 'ISP', 'LP', 'AS']
                list5_ = ['Server', 'Address','Gateway','Interface','Member','LPref','ASN']

                if (len(s) > padder['p0']):
                    padder['p0'] = len(s)              

                # // Grab detail from the bird daemons //
                if (ip == 'ipv4'):
                    str_ = _IxpLxc._lxc_exec(self, s, "birdc show route")[1]
                elif (ip == 'ipv6'):
                    str_ = _IxpLxc._lxc_exec(self, s, "birdc6 show route")[1]   
                else:
                    assert False, 'ERROR: Not sure how we arrived here'

                # //Split out routes //
                for x in str_.split('\n'):
                    list_ = x.strip().split()
                    flag = 0
                    try:
                        ipaddress.ip_interface(list_[0])
                        flag = 1
                    except:
                        flag = 0

                    if (flag == 1):
                        dict2_[s].append(x.strip().split())
                    else:
                        pass

            # // Create final dictionary for printing //
            for k, v in dict2_.items():
                for l in v:
                    if (l[1] == 'via'):
                        dict3_ = dict()                      
                        dict3_['IP'] = l[0]
                        dict3_['GW'] = l[2]
                        dict3_['INT'] = l[4]
                        dict3_['ISP'] = l[5][1:].replace('_', ' ')
                        dict3_['LP'] = l[8][1:-1]
                        dict3_['AS'] = l[9][3:-2]

                        for c, x in enumerate(list3_):
                            if len(dict3_[x]) > padder[f'p{c + 1}']: padder[f'p{c + 1}'] = len(dict3_[x])
                        dict_[k].append(dict3_)
                        
                    elif (l[1] == 'dev'):
                        dict3_ = dict()                      
                        dict3_['IP'] = l[0]
                        dict3_['GW'] = 'Local'
                        dict3_['INT'] = l[2]

                        for c, x in enumerate(list3_):
                            if (x in dict3_.keys()):
                                if len(dict3_[x]) > padder[f'p{c + 1}']: padder[f'p{c + 1}'] = len(dict3_[x])
                            else:
                                pass
                        dict_[k].append(dict3_)
                        
                    else:
                        pass

            # // Add 2 to each pad //
            list2_ = padder.values()
            list2_ = [x + 2 for x in list2_ ] 

            # // Create line //
            list4_ = [' ']
            for x in list2_:
                list4_.append(f" + {'-' * x}")
            list4_.append(' +')
            str_ = ''.join(x for x in list4_)

            # // Top line //
            print (f"  + {'-' * (sum(list2_) + 18)} +")

            # // Title //
            str2_ = f"IPv{ip[-1]} Route Table" 
            print (f"  |{str2_:^{(sum(list2_) + 20)}}|")

            # // Print line //
            print (str_)  

            # // Print table headers //
            print ('  |', end = '')
            for c, z in enumerate(list5_):
                print (f" {z:^{list2_[c]}} |", end = '')
            print ('')

            # // Print line //
            print (str_)  

            # // Progress through table data //
            for k, v in dict_.items():
                if (_is_empty(v)):
                    v = [{'IP':'No routes'}]

                # // Put something in empty keys //
                for c2, x in enumerate(v):
                    for y in list3_:
                        if not (y in x.keys()):
                            x[y] = ''

                    # // Print data //
                    print ('  | ', end = '')

                    if (c2 == 0):
                        print (f"{k:^{list2_[0]}} | ", end = '')
                    else:
                        print (f"{' ':^{list2_[0]}} | ", end = '') 
                    for c, z in enumerate(list3_):
                        if (c in [0,1,2,3]):
                            print (f"{x[z]:<{list2_[c + 1]}} | ", end = '')
                        else:
                            print (f"{x[z]:^{list2_[c + 1]}} | ", end = '')                            
                    print ('')

                # // Print line //
                print (str_)      

            print ('')                  

            return (0)

            ## End of _ixp_peer_route_v46 method

        # // Handle tables //
        for x in ip_list:
            _ixp_peer_route_v46(x)

        sys.exit(0)

    ## End of _ixp_peer_route method

    #============================================#
    # //        _ixp_peer_test_options        // #
    #============================================#

    def _ixp_peer_test_options(self, args):
        ''' Method to test 'ixp_peer' cli options '''

        str_ = str()
        dict_ = dict()
        dict2_ = dict()
        first_options = ['add', 'delete', 'list', 'status', 'route', 'sync', 'help']
 
        # // Replace any instance of 'show' with 'list' //
        args = ['list' if x == 'show' else x for x in args]

        # // Handle abbreviations //
        for x in first_options:
            if (x[:len(args[0])] == args[0]):
                args[0] = x

        # // Assign args to the dictionary //
        if (args[0] in first_options):
            dict_['cmd'] = args[0]
        else:
            str_ = ', '.join(x for x in first_options[:-1])
            print (f"ERROR: Not a valid choice, select '{str_} or {first_options[-1]}'\n")
            _ixp_debug('y',f"ERROR[_ixp_peer_test_options]: Not a valid choice, select '{str_} or {first_options[-1]}'")                
            self._ixp_peer_help_handler('help', 1)            

        # // Handle one element //
        if (len(args) == 1):
            dict_['ins'] = ''
            dict_['elm'] = {}

        # // Handle two elements //
        elif (len(args) == 2):
            if (args[1] in ['status', 'list', 'route', 'help']):
                dict_['ins'] = ''
                dict_['elm'] = {}

            # // Handle help //
            if (args[1] == 'help'):
                self._ixp_peer_help_handler(dict_['cmd'], 0)  

            # // Handle sync //
            if (args[1] == 'sync'):
                return (dict_)  

            else:
                print (f"ERROR: Not a valid choice '{args[1]}'\n")
                _ixp_debug('y',f"ERROR[_ixp_peer_test_options]: Not a valid choice '{args[1]}'")                
                self._ixp_peer_help_handler(dict_['cmd'], 1)

        # // Handle more that 2 elements //
        elif (len(args) > 2):
            dict_['elm'] = args[1:]

            # // Handle help //
            if ('help' in args[1:]):
                self._ixp_peer_help_handler(dict_['cmd'], 0)  

            if (dict_['cmd'] in ['list', 'status', 'route']):
                if (dict_['elm'][0] == '-ip' or dict_['elm'][0] == '--ip-type'):
                    if (dict_['elm'][1] in ['ipv4', 'ipv6', 'both']):
                        dict2_ = {'ip_type':dict_['elm'][1]}
                        del dict_['elm']
                        dict_['elm'] = dict2_
                    else:
                        print (f"ERROR: Not valid 'ixp peer {dict_['cmd']}' option value\n")
                        _ixp_debug('y', f"ERROR[_ixp_peer_test_options]: Not valid 'ixp peer {dict_['cmd']}' option value")
                        self._ixp_peer_help_handler('list', 1)                        
                else:
                    print (f"ERROR: Not valid 'ixp peer {dict_['cmd']}' option\n")
                    _ixp_debug('y', f"ERROR[_ixp_peer_test_options]: Not valid 'ixp peer {dict_['cmd']}' option")
                    self._ixp_peer_help_handler('list', 1)

                return (dict_)

            # // Create element dictionary //
            dict_['elm'] = _list_to_dict(args[1:])

            # // Test the route and blackhole server options //            
            if (dict_['cmd'] == 'add'):
                for k, v in dict_['elm'].items():
                    if (k == '-rs' or k == '--route-server'):  
                        if (v.lower() == 'yes'[:len(v)]):
                            dict_['elm'][k] = 'yes'
                        elif (v.lower() == 'no'[:len(v)]):
                            dict_['elm'][k] = 'no'
                        else:
                            print (f"ERROR: '{v}' is not valid for route server option\n")
                            _ixp_debug('y', f"ERROR[_ixp_peer_test_options]: '{v}' is not valid for route server option")                            
                            self._ixp_peer_help_handler(dict_['cmd'], 1) 

                    if (k == '-bs' or k == '--blackhole-server'):
                        if (v.lower() == 'yes'[:len(v)]):
                            dict_['elm'][k] = 'yes'
                        elif (v.lower() == 'no'[:len(v)]):
                            dict_['elm'][k] = 'no'
                        else:
                            print (f"ERROR: '{v}' is not valid for blackhole server option\n")
                            _ixp_debug('y', f"ERROR[_ixp_peer_test_options]: '{v}' is not valid for blackhole server option")
                            self._ixp_peer_help_handler(dict_['cmd'], 1) 

            # // Verify elements //
            dict_['elm'] = _validate_options(self.peer_add_dict, dict_['elm'])

        # //Handle 'help' //
        if (dict_['cmd'] == 'help'):
            self._ixp_peer_help_handler('help', 0)

        # // Return dictionary //

        return (dict_)
 
    ## End _ixp_peer_test_options

    #============================================#
    # //               ixp_peer               // #
    #============================================#

    def ixp_peer(self, args):
        ''' Method, handles 'ixp peer' command '''

        dict_ = dict()

        dict_ = self._ixp_peer_test_options(args)

        # // Handle 'add' //
        if (dict_['cmd'] == 'add'):       

            self._ixp_peer_add(dict_['elm'])  

        # // Handle 'delete' //
        if (dict_['cmd'] == 'delete'):

            self._ixp_peer_delete(dict_['elm']) 

        # // Handle 'list' //
        if (dict_['cmd'] == 'list'):

            self._ixp_peer_list(dict_['elm']) 

        # // Handle 'sync' (hidden command) //
        if (dict_['cmd'] == 'sync'):

            self._ixp_peer_sync() 

        # // Handle 'status' //
        if (dict_['cmd'] == 'status'):

            self._ixp_peer_status(dict_['elm']) 

        # // Handle 'route' //
        if (dict_['cmd'] == 'route'):

            self._ixp_peer_route(dict_['elm'])

        sys.exit(0)

    ## End to ixp_peer method

############ // End IxpPeer class // ############

#################################################
# //             IxpRemote class             // #
#################################################

class IxpRemote:
    ''' IXP Remote Class '''

    def __init__(self):
        '''IxpRemote class constructor method'''

        self.functionality = 'Manage Access to mini IXPs'

        # // Get the site type //
        self.site_number = _site_number()
        self.site_type = _site_type()
        self.key_store = '/srv/keys/'
        self.username = getpass.getuser()
        self.remote_site_dict = {'-sn':'--site-number',
                                 '-se':'--site-name', 
                                 '-l':'--location', 
                                 '-h4':'--host-v4',
                                 '-h6':'--host-v6',
                                 '-u':'--remote-user',
                                 '-p':'--tun-port'
                                }    

        self.remote_temp_dict = {'site_number': 'INTEGER PRIMARY KEY',
                                 'site_name': 'TEXT',
                                 'location': 'TEXT',     
                                 'host_v4': 'TEXT',      
                                 'host_v6': 'TEXT',   
                                 'remote_user':'TEXT', 
                                 'tun_port':'TEXT'
                                }

        self.ixp_peer_add_dict = {'-n':'--name',
                                  '-a':'--as-number',
                                  '-d':'--domain',
                                  '-rs':'--route-server',
                                  '-bs':'--blackhole-server'
                                 }

        self.ixp_peer_status_route_dict = {'-ip': '--ip-type'}

        # // Test if remote database exists and create if it doesn't //
        _ixp_database_create('remote', self.remote_temp_dict)

        ## End of __init__ method
        
    def __str__(self):
        return (self.functionality)

        ## End of __str__ method
 
    def __repr__(self):
        return (f'{self.__class__.__name__} class: {self.functionality}')    

        ## End of __repr__ method  

    #============================================#
    # //       _ixp_remote_help_handler       // #
    #============================================#

    def _ixp_remote_help_handler(self, command, err_code):
        ''' remote help handler '''

        if (command == 'help'):
            IxpHelp._ixp_remote_help(self, err_code)
        elif (command == 'keys'):
            IxpHelp._ixp_remote_keys_help(self, err_code)
        elif (command == 'site'):
            IxpHelp._ixp_remote_site_help(self, err_code)
        elif (command == 'site_add'):
            IxpHelp._ixp_remote_site_add_help(self, err_code)
        elif (command == 'site_delete'):
            IxpHelp._ixp_remote_site_delete_help(self, err_code)
        elif (command == 'command'):
            IxpHelp._ixp_remote_command_help(self, err_code)
        else:
            assert False, 'ERROR: Not sure how we arrived here'

        ## End to _ixp_remote_help_handler method 

    #============================================#
    # //         _ixp_remote_command          // #
    #============================================#

    def _ixp_remote_command(self, kargs):
        ''' Method to execute remote commands '''

        str_ = str()
        err_ = 'ERROR: Failed to connect to remote mIXP\n'
        ip_addr = str()
        dict_ = dict()

        site_number = kargs['ins']
        command = kargs['elm']

        # // Extract site data from database //
        dict_ = _read_db('remote')[site_number]

        # // Get username //
        remote_user = dict_['remote_user']

        # // Check for valid IP address //
        if (public_ipv6_net == False):
            if not ('host_v4' in dict_.keys()):
                print (f'ERROR: No IPv6 network and no IPv4 address available for site no. {site_number}\n')
                _ixp_debug ('y', f'ERROR[_ixp_remote_command]: No IPv6 network and no IPv4 address available for site no. {site_number}')
                sys.exit(1)
            else:
                ip_addr = dict_['host_v4']
        else:
            if not ('host_v6' in dict_.keys()):
                print (f'ERROR: IPv6 network but no IPv6 address available for site no. {site_number}\n')
                _ixp_debug ('y', f'ERROR[_ixp_remote_command]: IPv6 network but no IPv6 address available for site no. {site_number}')
                sys.exit(1)
            else:
                ip_addr = dict_['host_v6']

        # // Connect to mIXP //
        client = SSHClient()
        client.set_missing_host_key_policy(AutoAddPolicy())

        try:
            client.connect(ip_addr, tun_port, remote_user)

        except Exception as e:
            print (e)
            print (f'ERROR: Failed to connect to remote mIXP at site no. {site_number}\n')
            _ixp_debug ('y', f'ERROR[_ixp_remote_command]: Failed to connect to remote mIXP at site no. {site_number}')
            sys.exit(1)            

        # // Make connection to remote shell //       
        shell = client.invoke_shell()

        # // Execute command and get results //
        unused_, stdout, stderr  = client.exec_command(command)
        str_ = stdout.read().decode()
        err_ = stderr.read().decode('utf-8')

        # // Return results to terminal //
        if not err_:
            print (f'\n## Executing on remote mIXP site: {site_number} \n\n  [ mIXP: {site_number} ]~$ {command}\n')
            print (str_)
            shell.close()
            client.close()
            return (0)
        else:
            print (f'\n## Executing on remote mIXP site: {site_number} \n\n  [ mIXP: {site_number} ]~$ {command}\n')
            print (f'ERROR: {err_}')
            shell.close()
            client.close()
            _ixp_debug ('y', f'ERROR[_ixp_remote_command]: Failed to execute command on remote mIXP at site no. {site_number}')
            return (1)            

        ## End to _ixp_remote_command method 

    #============================================#
    # //         _ixp_remote_site_add         // #
    #============================================#

    def _ixp_remote_site_add(self, kargs):
        ''' Method to add remote mIXP database entries '''

        # // Insert defaults if none given //
        if not ('remote_user' in kargs.keys()):
            kargs['remote_user'] = f"'{self.username}'"

        if not ('tun_port' in kargs.keys()):
            kargs['tun_port'] = f"'{tun_port}'"

        # // Load data to database //
        try:
            _ixp_database_insert('remote', kargs)
            print (f"Loaded site {kargs['site_number']} data to the database\n")
            _ixp_debug ('y', f"INFO[_ixp_remote_site_add]: Loaded site {kargs['site_number']} data to the database") 
        except:
            print (f"Remote site {kargs['site_number']} already exists in the database")
            _ixp_debug ('y', f"INFO[_ixp_remote_site_add]: Remote site {kargs['site_number']} already exists in the database") 

        return (0)

        ## End to _ixp_remote_site_add method  

    #============================================#
    # //       _ixp_remote_site_delete        // #
    #============================================#

    def _ixp_remote_site_delete(self, kargs):
        ''' Method to delete remote mIXP database entries '''

        site_number = int(kargs['site_number'])
        if not (site_number in list(_read_db('remote').keys())):
            print (f'ERROR: There is no site number: {site_number}\n')
            _ixp_debug ('y', f'ERROR[_ixp_remote_site_delete]: There is no site number: {site_number}')   
            self._ixp_remote_help_handler('site_delete', 1) 

        # // Delete line from database //
        _ixp_database_delete('remote', 'site_number', site_number)

        print (f'Site number: {site_number} removed from the database\n')
        _ixp_debug ('y', f'ERROR[_ixp_remote_site_delete]: Site number: {site_number} removed from the database')  

        return (0)

        ## End to _ixp_remote_site_delete method  

    #============================================#
    # //        _ixp_remote_site_show         // #
    #============================================#

    def _ixp_remote_site_show(self):
        ''' Method to show remote mIXP database entries '''

        str_ = str()
        str2_ = str()
        dict_ = _read_db('remote')
        list_ = ['Site No.', 'Site Name', 'Location', 'IPv4' , 'IPv6', 'User', 'Port']
        list2_ = list()
        list3_ = list()

        # // Start padder with the width of the labels //
        padder = {'p0': 8, 'p1':11,'p2':10,'p3':5,'p4':5,'p5':4,'p6':4}

        # // Adjust padder to match data //
        for k,v in dict_.items():
            c = 1
            for kk,vv in v.items():
                if (len(str(vv)) > padder[f'p{c}']):
                    padder[f'p{c}'] = len(vv) + 2
                c += 1

        # // Add 2 to each pad //
        list2_ = padder.values()
        list2_ = [x + 2 for x in list2_ ] 

        # // Create line //
        list3_ = [' ']
        for x in list2_:
            list3_.append(f" + {'-' * x}")
        list3_.append(' +')
        str_ = ''.join(x for x in list3_)

        # // Top line //
        print (f"  + {'-' * (sum(list2_) + 18)} +")

        # // Title //
        str2_ = f"Remote mIXP sites" 
        print (f"  |{str2_:^{(sum(list2_) + 20)}}|")

        # // Print line //
        print (str_)  

        # // Print table headers //
        print ('  |', end = '')
        for c, x in enumerate(list_):
            print (f" {x:^{list2_[c]}} |", end = '')
        print ('')

        # // Print line //
        print (str_) 

        # // Print Data //
        for k,v in dict_.items():

            print (f"  | {k:^{list2_[0]}}", end = '')
            c = 1
            for kk,vv in v.items():
                if (vv == None):
                    vv = ' '
                print (f" | {vv:^{list2_[c]}}", end= '')
                c += 1
            print (' |')

        # // Print line //
        print (f'{str_}\n') 

        ## End to _ixp_remote_site_show method  

    #============================================#
    # //          _ixp_remote_keygen          // #
    #============================================#

    def _ixp_remote_keygen(self):
        ''' Method to generate SSH key pair '''

        str_ = str()

        # // Test for cIXP //
        if (self.site_type == 'mini'):
            print ('ERROR: Encryption keys are only generated on core IXPs (cIXP)\n')
            _ixp_debug ('y', 'ERROR[_ixp_remote_keygen]: Encryption keys are only generated on core IXPs (cIXP)')
            sys.exit(1)

        # // Gererate RSA key pair //
        key = RSA.generate(2048)

        # // Write private key //
        with open(f'{self.key_store}private.key', 'wb') as fh:
            subprocess.getstatusoutput (f'chmod {key_perm} {self.key_store}private.key')
            fh.write(key.exportKey('PEM'))

        # // Write public key //
        pubkey = key.publickey()
        with open(f'{self.key_store}public.key', 'wb') as fh:
            fh.write(pubkey.exportKey('OpenSSH'))

        # // Check if .ssh and authorised keys file exist //
        if (_get_file_dir_info(f'/home/{self.username}/.ssh')['type'] == 'not_exist'):
            subprocess.getstatusoutput (f'mkdir -m 700 /home/{self.username}/.ssh')  

        # // Copy the new keys to the .ssh directory //
        subprocess.getstatusoutput (f'cp /srv/keys/private.key /home/{self.username}/.ssh/id_rsa') 
        subprocess.getstatusoutput (f'cp /srv/keys/public.key /home/{self.username}/.ssh/id_rsa.pub')  

        # // Disable Strict Host Key Checking //
        str_ = (f'Host *\n'
                f'    StrictHostKeyChecking no\n'
                f'    UserKnownHostsFile=/dev/null\n') 

        with open(f'/home/{self.username}/.ssh/config', 'w') as fh:
            fh.write(str_)

        subprocess.getstatusoutput (f'chmod 700 /home/{self.username}/.ssh/config')

        print ('Generated encryption keys for core IXP (cIXP)\n')
        _ixp_debug ('y', 'INFO[_ixp_remote_keygen]: Generated encryption keys for core IXP (cIXP)')

        ## End to _ixp_remote_keygen method 

    #============================================#
    # //         _ixp_remote_key_enter        // #
    #============================================#

    def _ixp_remote_key_enter(self):
        ''' Method to add SSH public key to mIXP '''

        # // Test for mIXP //
        if (self.site_type == 'core'):
            print ('ERROR: Encryption keys are added on mini IXPs (mIXP) only\n')
            _ixp_debug ('y', 'ERROR [_ixp_remote_key_enter]: Encryption keys are added on mini IXPs (mIXP) only')
            sys.exit(1)

        # // Get public key //
        public_key = input('\n  Paste the public key test here: ')

        with open(f'{self.key_store}public.key', 'w') as fh:
            fh.write(public_key)

        # // Check if .ssh and authorised keys file exist //
        if (_get_file_dir_info(f'/home/{self.username}/.ssh')['type'] == 'not_exist'):
            subprocess.getstatusoutput (f'mkdir -m 700 /home/{self.username}/.ssh')

        if (_get_file_dir_info(f'/home/{self.username}/.ssh/authorized_keys')['type'] == 'not_exist'):
            subprocess.getstatusoutput (f'touch /home/{self.username}/.ssh/authorized_keys')
            subprocess.getstatusoutput (f'chmod 700 /home/{self.username}/.ssh/authorized_keys')

        # // Check if key already exists in authorised keys //
        if (f'{public_key}' in open(f'/home/{self.username}/.ssh/authorized_keys').read()):
            print ('Key already exists in authorised keys\n')
            _ixp_debug ('y', 'INFO[_ixp_remote_key_enter]: Key already exists in authorised keys')
        else:
            # // Add public key to authorised keys // 
            subprocess.getstatusoutput (f'echo >> /home/{self.username}/.ssh/authorized_keys')
            subprocess.getstatusoutput (f'cat /srv/keys/public.key >> /home/{self.username}/.ssh/authorized_keys')
            print ('Added public key to mini IXP (mIXP)')
            _ixp_debug ('y', 'INFO[_ixp_remote_key_enter]: Added public key to mini IXP (mIXP)')

        ## End to _ixp_remote_key_enter method 

    #============================================#
    # //         _ixp_remote_key_show         // #
    #============================================#

    def _ixp_remote_key_show(self):
        ''' Method to show the SSH public key '''

        with open(f'{self.key_store}public.key', 'r') as fh:
            print (fh.read())

        ## End to _ixp_remote_key_show method 

    #============================================#
    # //       _ixp_remote_test_options       // #
    #============================================#

    def _ixp_remote_test_options(self, args):
        ''' Method to test 'ixp_remote' cli options '''

        str_ = str()
        str2_ = str()
        list_ = list()
        list2_ = list()
        list3_ = list()
        dict_ = dict()
        dict2_ = dict()
        dict3_ = dict()
        dict4_ = dict()

        first_options = ['keys', 'site', 'command', 'cmd', 'help']
        second_options = ['schema', 'host', 'switch', 'server', 'peer', 'sdn']
        third_options = ['show', 'list', 'add', 'delete', 'status', 'route']
        fourth_options = ['switches', 'flows']
        remote_cmds = ['ixp schema show',
                       'ixp host show',
                       'ixp switch show',
                       'ixp server show',
                       'ixp schema list',
                       'ixp host list',
                       'ixp switch list',
                       'ixp sdn show switches',
                       'ixp sdn show flows',
                       'ixp peer add',
                       'ixp peer delete', 
                       'ixp peer list', 
                       'ixp peer show',
                       'ixp peer status', 
                       'ixp peer route'
                      ]

        # // lowercase first two arguments //
        args[0] = args[0].lower()
        if (len(args) > 1):
            args[1] = args[1].lower()

        # // Replace any instance of 'list' with 'show' //
        args = ['show' if x == 'list' else x for x in args]

        # // Swap 'help' for '?'//
        args = ['help' if x == '?' else x for x in args]
        args = ['help' if x == 'help'[:len(x)] else x for x in args]

        # // Handle abbreviations //
        for x in first_options:
            if (x[:len(args[0])] == args[0]):
                args[0] = x

        # // Assign args to the dictionary //
        if (args[0] in first_options):
            dict_['cmd'] = args[0]
        else:
            str_ = ', '.join(x for x in first_options[:-1])
            print (f"ERROR: Not a valid choice, select '{str_} or {first_options[-1]}'\n")
            _ixp_debug ('y',f"ERROR[_ixp_remote_test_options]: Not a valid choice, select '{str_} or {first_options[-1]}'")                
            self._ixp_remote_help_handler('help', 1)            

        # // Handle help //
        if (args[0] == 'help'): 
            self._ixp_remote_help_handler(dict_['cmd'], 0) 

        # // Handle one element //
        if (len(args) == 1):
            self._ixp_remote_help_handler(args[0], 1)

        # // Handle greater that 1 element //
        elif (len(args) > 1):
            dict_['ins'] = args[0]
            dict_['elm'] = ''

            # // Convert 'cmd' to 'command' //
            if (args[0] == 'cmd'[:len(args[0])]):   
                 args[0] = 'command'        

            # // Handle keys //
            if (args[0] == 'keys'[:len(args[0])]):
                if (args[1] == 'help'):
                    self._ixp_remote_help_handler('keys', 0)

                # // Handle keys generate //
                elif (args[1] == 'generate'[:len(args[1])]):
                    if not (self.site_type == 'core'):
                        print ('ERROR: Generating RSA keys is only available at a cIXP\n')
                        _ixp_debug ('y', 'ERROR[_ixp_remote_test_options]: Generating RSA keys is only available at a cIXP')
                        sys.exit()  
                    dict_['ins'] = 'generate'

                # // Handle keys enter //
                elif (args[1] == 'enter'[:len(args[1])]):
                    dict_['ins'] = 'enter'
                    if not (self.site_type == 'mini'):
                        print ('ERROR: Entering a public key is only available at an mIXP\n')
                        _ixp_debug ('y', 'ERROR[_ixp_remote_test_options]: Entering a public key is only available at an mIXP')
                        sys.exit()  

                # // Handle keys show //
                elif (args[1] == 'show'[:len(args[1])]):
                    dict_['ins'] = 'show'
                else:
                    print (f'ERROR: {args[1]} is not a recognoised option\n')
                    _ixp_debug ('y', f'ERROR[_ixp_remote_test_options]: {args[1]} is not a recognoised option')
                    self._ixp_remote_help_handler('keys', 1)

            # // Handle site //    
            elif (args[0] == 'site'[:len(args[0])]):
                if not (self.site_type == 'core'):
                    print ('ERROR: Remote database options only available at a cIXP\n')
                    _ixp_debug ('y', 'ERROR[_ixp_remote_test_options]: Remote database options only available at a cIXP')
                    sys.exit()                   
                elif (args[1] == 'help'):
                    self._ixp_remote_help_handler('site', 0)

                # // Handle site add //
                elif (args[1] == 'add'[:len(args[1])]):
                    if not (len(args) > 2):
                        print ("ERROR: 'ixp remote site add' requires options\n")
                        _ixp_debug ('y', "ERROR[_ixp_remote_test_options]: 'ixp remote site add' requires options")  
                        self._ixp_remote_help_handler('site_add', 1)   

                    # // Check for help //
                    if ('help' in args[2:]):
                        self._ixp_remote_help_handler('site_add', 0) 

                    dict_['ins'] = 'add'

                    # // Validate switch options //
                    dict_['elm'] = _validate_options(self.remote_site_dict, _list_to_dict(args[2:]))

                    # // Check for the site number //
                    if not ('site_number' in dict_['elm'].keys()):
                        print ('ERROR: The --site-number and at least one IP address switch is required\n')
                        _ixp_debug ('y','ERROR[_ixp_remote_test_options]: The --site-number and at least one IP address switch is required')
                        self._ixp_remote_help_handler('site_add', 1) 

                    # // Check for an IP address //
                    if not (('host_v4' in dict_['elm'].keys()) or ('host_v6' in dict_['elm'].keys())):
                        print ('ERROR: An IP address is required\n')
                        _ixp_debug ('y','ERROR[_ixp_remote_test_options]: An IP address is required')
                        self._ixp_remote_help_handler('site_add', 1) 

                    # // Put quotes around text elements //
                    for k, v in dict_['elm'].items():
                        if (self.remote_temp_dict[k] == 'TEXT'):
                            dict_['elm'][k] = f"'{dict_['elm'][k]}'"

                # // Handle site delete //
                elif (args[1] == 'delete'[:len(args[1])]):
                    if not (len(args) > 2):
                        print ('ERROR: The --site-number option is required\n')
                        _ixp_debug ('y', 'ERROR[_ixp_remote_test_options]: The --site-number option is required')  
                        self._ixp_remote_help_handler('site_delete', 1)     

                    # // Check for help //
                    if ('help' in args[2:]):
                        self._ixp_remote_help_handler('site_delete', 0)                                           

                    dict_['ins'] = 'delete'

                    # // Validate switch options //
                    dict2_ = _validate_options(self.remote_site_dict, _list_to_dict(args[2:]))

                    # // Check for --site-number //
                    if not ('site_number' in dict2_.keys()):
                        print ('ERROR: --site-number is a required argument\n')
                        _ixp_debug ('y', 'ERROR[_ixp_remote_test_options]: --site-number is a required argument')                        
                        self._ixp_remote_help_handler('site_delete', 1) 

                    dict_['elm'] = {'site_number': dict2_['site_number']}

                # // Handle site show //
                elif (args[1] == 'show'[:len(args[1])]):
                    dict_['ins'] = 'show'               

                else:
                    print (f'ERROR: {args[1]} is not a recognoised option\n')
                    _ixp_debug ('y', f'ERROR[_ixp_remote_test_options]: {args[1]} is not a recognoised option')
                    self._ixp_remote_help_handler('site', 1)

            # // Handle remote commands //    
            elif (args[0] == 'command'[:len(args[0])]):
                if not (self.site_type == 'core'):
                    print ('ERROR: Remote database commands are only available at a cIXP\n')
                    _ixp_debug ('y', 'ERROR[_ixp_remote_test_options]: Remote database commands are only available at a cIXP')
                    sys.exit()  

                dict_['cmd'] = 'command'

                if not (len(args) > 5):
                    print ('ERROR: A site number and commands are required\n')
                    _ixp_debug ('y', 'ERROR[_ixp_remote_test_options]: A site number and commands are required')  
                    self._ixp_remote_help_handler('command', 1)    

                # // Test for help //
                if ('help' in args[1:]):
                    self._ixp_remote_help_handler('command', 0)                    

                # // Test remote sites from the database //
                list_ = list(_read_db('remote').keys())
                str_ = ', '.join([str(x) for x in list_])
                if not (int(args[1]) in list_):
                    print (f'ERROR: {args[1]} is not a valid remote site, choose from sites {str_}\n')
                    _ixp_debug ('y', f'ERROR[_ixp_remote_test_options]: {args[1]} is not a valid remote site, choose from sites {str_}')  
                    self._ixp_remote_help_handler('command', 1) 
                else:
                    dict_['ins'] = int(args[1])

                # // Test for separator '--'' //
                if not (args[2] == '--'):
                    print ('ERROR: No valid command separator\n')
                    _ixp_debug ('y', 'ERROR[_ixp_remote_test_options]: No valid command separator')  
                    self._ixp_remote_help_handler('command', 1)    

                # // Remove abbreviations //
                if not args[3] == 'ixp'[:len(args[3])]:
                    print (f'ERROR: {args[3]} is not a valid remote command\n')
                    _ixp_debug ('y', f'ERROR[_ixp_remote_test_options]: {args[3]} is not a valid remote command')
                
                for x in second_options:
                    if (args[4] == x[:len(args[4])]):
                        args[4] = x

                for x in third_options:
                    if (args[5] == x[:len(args[5])]):
                        args[5] = x

                if (len(args) == 7):
                    for x in fourth_options:
                        if (args[6] == x[:len(args[6])]):
                            args[6] = x
                    str2_ = ' '.join(args[3:7])
                else:
                    str2_ = ' '.join(args[3:6])

                # // Test command is an allowed remote command //
                if not (str2_ in remote_cmds):
                    print (f'ERROR: {str2_} is not a valid remote command\n')
                    _ixp_debug ('y', f'ERROR[_ixp_remote_test_options]: {str2_} is not a valid remote command')  
                    self._ixp_remote_help_handler('command', 1)                                 

                # // Test elements from an ixp peer add and delete //
                if (str2_ == 'ixp peer add' or str2_ == 'ixp peer delete' ):

                    # // Verify elements and create command //
                    dict3_ = _list_to_dict(args[6:])
                    for k, v in _validate_options(self.ixp_peer_add_dict, dict3_).items():
                        k = f"--{k.replace('_', '-')}"
                        list2_.append(k)
                        list2_.append(v)  

                    dict_['elm'] = f"{str2_} {' '.join(list2_)}"  

                # // Test elements from an ixp peer status //
                elif (str2_ == 'ixp peer status' or str2_ == 'ixp peer route'):

                    # // Verify elements and create command //
                    dict4_ = _list_to_dict(args[6:])

                    for k, v in _validate_options(self.ixp_peer_status_route_dict, dict4_).items():
                        k = f"--{k.replace('_', '-')}"
                        list3_.append(k)
                        list3_.append(v)  

                    dict_['elm'] = f"{str2_} {' '.join(list3_)}"                   

                else:
                    dict_['elm'] = ' '.join(args[3:])

            else:
                print (f'ERROR: {args[0]} is not a valid option\n')
                _ixp_debug ('y', f'ERROR[_ixp_remote_test_options]: {args[0]} is not a valid option')
                self._ixp_remote_help_handler('help', 1)

        return (dict_)
 
    ## End _ixp_remote_test_options method  

    #============================================#
    # //               ixp_remote             // #
    #============================================#

    def ixp_remote(self, args):
        ''' Method to handle 'ixp remote' commands '''

        dict_ = dict()
        
        dict_ = self._ixp_remote_test_options(args)

        # // handle keys //
        if (dict_['cmd'] == 'keys'):
            if (dict_['ins'] == 'generate'):
                self._ixp_remote_keygen()
            elif (dict_['ins'] == 'enter'):
                self._ixp_remote_key_enter()                
            elif (dict_['ins'] == 'show'):
                self._ixp_remote_key_show()
            else:
                print (f"ERROR: {dict_['ins']} is not a valid option of 'ixp remote {dict_['cmd']}'\n")
                _ixp_debug ('y', f"ERROR[ixp_remote]: {dict_['ins']} is not a valid option of 'ixp remote {dict_['cmd']}'")
                self._ixp_remote_help_handler('keys', 1)

        # // handle site //
        elif (dict_['cmd'] == 'site'):
            if (dict_['ins'] == 'add'):
                self._ixp_remote_site_add(dict_['elm'])
            elif (dict_['ins'] == 'delete'):
                self._ixp_remote_site_delete(dict_['elm'])
            elif (dict_['ins'] == 'show'):
                self._ixp_remote_site_show()
            else:
                print (f"ERROR: {dict_['ins']} is not a valid option of 'ixp remote {dict_['cmd']}'\n")
                _ixp_debug ('y', f"ERROR[ixp_remote]: {dict_['ins']} is not a valid option of 'ixp remote {dict_['cmd']}'")
                self._ixp_remote_help_handler('site', 1)

        # // handle command //
        elif (dict_['cmd'] == 'command'):
            self._ixp_remote_command(dict_)

        else:
            assert False, 'ERROR: Not sure how we arrived here'

        sys.exit(0)

    ## End to ixp_remote method

########### // End IxpRemote class // ###########