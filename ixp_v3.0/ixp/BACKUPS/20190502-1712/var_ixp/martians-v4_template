function avoid_martians_v4()
prefix set martiansv4;
{
    # RFC6890 Special-Purpose Address Registry:
    martiansv4 = [
        10.0.0.0/8+,            # RFC1918 - Private use
        100.64.0.0/10+,         # RFC6598 - Shared address space
        127.0.0.0/8+,           # RFC1122 - Loopback
        169.254.0.0/16+,        # RFC3927 - Link-local
        172.16.0.0/12+,         # RFC1918 - Private use
        192.0.0.0/24+,          # multiple RFCs
        192.0.2.0/24+,          # RFC5737 - Documentation - TEST-NET-1
        192.168.0.0/16+,        # RFC1918 - Private use
        198.18.0.0/15+,         # RFC2544 - Benchmarking
        198.51.100.0/24+,       # RFC5737 - Documentation - TEST-NET-2
        203.0.113.0/24+,        # RFC5737 - Documentation - TEST-NET-3
        224.0.0.0/4+,           # RFC3171 - Multicast
        240.0.0.0/4+,           # RFC1112 - Reserved
        0.0.0.0/32-,
        0.0.0.0/0{25,32},
        0.0.0.0/0{0,7}
    ];

    # Avoid RFC1918 and similar networks
    if net ~ martiansv4 then
        return false;

    return true;
}

filter import_policy_v4{

    if !(avoid_martians_v4()) then
        reject;

    accept;
}
