# """ ixp_Switch for OpenFlow v1.3 """

__author__      = "Diarmuid O'Briain"
__copyright__   = "Copyright 2019, C²S Consulting"
__license__     = "European Union Public Licence v1.2"
__version__     = "Phase 3.0, Version 5.0"

#
# Copyright 2019 C²S Consulting
# 
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved 
# by the European Commission - subsequent versions of the EUPL (the "Licence");
#
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
# 
# https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
# 
# Unless required by applicable law or agreed to in writing, software distributed
# under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR 
# CONDITIONS OF ANY KIND, either express or implied.
# 
# See the Licence for the specific language governing permissions and limitations 
# under the Licence.
#

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
import json
import subprocess
import os

dpid_ip_path_file = '/srv/ryu/.dpid_to_ip.map'
dpid_port_path_file = '/srv/ryu/.dpid_to_port.map'

class IxpSwitch13(app_manager.RyuApp):
    ''' IXP switch v1.3 class '''

    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        ''' IxpSwitch13 class constructor method '''

        super(IxpSwitch13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.dpid_to_port = {}
        self.dpid_to_ip = {}
        self.padder = 0    

        # // Read DPID to Port map //
        if (os.path.isfile(dpid_port_path_file)):
            with open(dpid_port_path_file, 'r') as fh1:
                self.dpid_to_port = json.loads(fh1.read())

    #============================================#
    #  //       _switch_features_handler      // #
    #============================================#

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        ''' Method for initial communications with switch '''

        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        self.dpid_to_ip[datapath.id] = datapath.address[0]
        fh = open(dpid_ip_path_file, mode='w', encoding='utf-8') 
        fh.write(json.dumps(self.dpid_to_ip))
        fh.close()

        # // Change file parameters //
        subprocess.getstatusoutput(f"chmod 660 {dpid_ip_path_file}")

        # // Clear existing flows in switch //
        match = parser.OFPMatch(None)
        actions = []
        self._clear_flows(datapath)     

        # // Install table-miss flow entry //
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self._add_flow(datapath, 0, match, actions)
        self.logger.info(f"ADD FLOW: {datapath.id} ({self.dpid_to_ip[datapath.id]}) | Table miss")

        # // Blocking blocked ports from DPID to Port map //
        actions = []
        if (str(datapath.id) in self.dpid_to_port.keys()):
            for port in self.dpid_to_port[str(datapath.id)]:
                match = parser.OFPMatch(in_port=port)
                self._add_flow(datapath, 99, match, actions)
                self.logger.info(f"ADD FLOW: {datapath.id} ({match}) | A:")
                
        len_ = len(str(datapath.id)) + len(self.dpid_to_ip[datapath.id]) + 2
        if (len_ > self.padder):
            self.padder = len_     

    ## End of _switch_features_handler method

    #============================================#
    #  //          _packet_in_handler         // #
    #============================================#

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        ''' Method to handle Packet in messages '''

        # // "miss_send_length" too short in switch //
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug(f"packet truncated: only {ev.msg.msg_len} of {ev.msg.total_len} bytes")

        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst_mac = eth.dst
        src_mac = eth.src

        dpid = datapath.id
        dpid_tag = f"{dpid} ({self.dpid_to_ip[dpid]})"
        self.mac_to_port.setdefault(dpid, {})

        self.logger.debug(f"packet in {dpid_tag}), {src_mac}, {dst_mac}, {in_port}")

        # // Drop 10 Decnet MOP //
        if (dst_mac == 'ab:00:00:02:00:00'):
            return

        # // Drop packet in/out of the same port //
        if (src_mac == dst_mac):
            return

        # // Learn MAC address to avoid FLOOD in future //
        self.mac_to_port[dpid][src_mac] = in_port

        if dst_mac in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst_mac]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [parser.OFPActionOutput(out_port)]

        # // Install a flow in the switch to handle such events in future //
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_src=src_mac, eth_dst=dst_mac)

            # // If a valid buffer_id, avoid sending both a flow_mod & packet_out //
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:                
                self._add_flow(datapath, 1, match, actions, msg.buffer_id)
                self.logger.info(f"ADD FLOW: {dpid_tag:<{self.padder}} | priority: 1, match | " 
                                 f"{in_port:<{3}} - {src_mac} - {dst_mac} | actions:  "
                                 f"{out_port:<{3}} | buffer ID: {msg.buffer_id}")
                return
            else:
                self._add_flow(datapath, 1, match, actions)
                self.logger.info(f"ADD FLOW: {dpid_tag:<{self.padder}} | priority: 1, match | " 
                                 f"{in_port:<{3}} - {src_mac} - {dst_mac} | actions:  "
                                 f"{out_port:<{3}} | buffer ID: {msg.buffer_id}")
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        datapath.send_msg(out)

    ## End of packet_in_handler method

    #============================================#
    #  //              _add_flow              // #
    #============================================#

    def _add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ''' Method to add flow_mod to the switch '''

        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    ## End of _add_flow method

    #============================================#
    #  //            _clear_flows             // #
    #============================================#

    def _clear_flows(self, datapath):
        ''' Method to clear flows from the switch '''
         
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        self.logger.info(f"CLEAR FLOW: {datapath.id} ({self.dpid_to_ip[datapath.id]})")

        match = parser.OFPMatch({})

        mod = parser.OFPFlowMod(datapath=datapath,match=match,
                                out_port=ofproto.OFPP_ANY, 
                                out_group=ofproto.OFPG_ANY,
                                command=ofproto.OFPFC_DELETE)
        datapath.send_msg(mod)

    ## End of _clear_flows method



