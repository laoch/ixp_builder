#!/bin/bash

### ixp-backup.py: Backup IXP Builder during development. ###

# author     : "Diarmuid O'Briain"
# copyright  : "Copyright 2019, C²S Consulting"
# license    : "European Union Public Licence v1.2"
# version    : "Phase 3.0, version 5.2"


# // Definition of files to be backed up //

py3_site=$(python3 -m site | grep '/usr/local/lib' | awk -v FS="('|')" '{print $2}')
BACKUP_FILES=('./ixp-install.sh' '/opt/ixp/ixp' "$py3_site/ixp.py" 
              '/var/log/ixp' '/var/ixp' '/var/ixp/code')

# // Check user is a regular user //

USER=$(whoami)
if [ "$USER" == 'root' ]
then
    echo -e "\nRun script as a regular user with group 'ixp' membership not 'sudo'.\n\n$ $0\n"
    exit 1
fi

# // Definitions //

# // Deal with the problem of hour < 10 as ' 9', make '09'

HOUR="$(/bin/date +%k)"

if [[ "$HOUR" =~ ^\ [0-9]$ ]]; then
    num=$(echo "$HOUR" | sed "s/^\ \([0-9]\)$/\1/")
    HOUR="0$num"
fi

TIMESTAMP="$(/bin/date +%Y%m%d-$HOUR%M)"
echo "$TIMESTAMP"
 
BACKUP_DIR="/home/$USER/ixp/BACKUPS/$TIMESTAMP"

# // Make backup directory //

mkdir -p "$BACKUP_DIR"

# // Make README file in backup directory

touch "$BACKUP_DIR/README.txt"
echo -e "\nBacked up file paths" > "$BACKUP_DIR/README.txt" 
printf "%0.s-" {1..20} >> "$BACKUP_DIR/README.txt" 
echo -e "\n" >> "$BACKUP_DIR/README.txt"

# // Banner //

echo -e "\nBackup script for IXP builder package" 
printf "%0.s-" {1..37}; echo

# // Backup files //

for files in "${BACKUP_FILES[@]}"; do

   if [ -f "$files" ]; then
       cp "$files" "$BACKUP_DIR"
       echo "Backing up: $files"
       echo "FILE: $files"  >> "$BACKUP_DIR/README.txt"
   else 
       files_file=${files//\//\_}      # Replace '/' with '_'
       files_file="${files_file:1}"    # Remove the first character i.e. '_'
       mkdir -p "$files" "$BACKUP_DIR/$files_file"
       cp -r "$files"/* "$BACKUP_DIR/$files_file"
       echo "Backing up files in $files in $BACKUP_DIR/$files_file"
       echo "DIRECTORY (+ FILES): $files"  >> "$BACKUP_DIR/README.txt"
   fi
done

echo -e "\n"  >> "$BACKUP_DIR/README.txt"

# // Confirm process is completed //

echo -e "IXP Builder package files backed up in $BACKUP_DIR\n"

# End
