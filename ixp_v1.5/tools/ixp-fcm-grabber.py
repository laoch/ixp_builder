#! /usr/bin/env python3
# -*- coding: utf-8 -*-

'''Grabs ordered list of functions in ixp.py'''

__author__      = "Diarmuid O'Briain"
__copyright__   = "Copyright 2019, C²S Consulting"
__license__     = "European Union Public Licence v1.2"
__version__     = "Phase 1.5, version 3.0"

#
# Copyright 2019 C²S Consulting
# 
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved 
# by the European Commission - subsequent versions of the EUPL (the "Licence");
#
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
# 
# https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
# 
# Unless required by applicable law or agreed to in writing, software distributed
# under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR 
# CONDITIONS OF ANY KIND, either express or implied.
# 
# See the Licence for the specific language governing permissions and limitations 
# under the Licence.
#

import re
import datetime
import subprocess
import sys

ixp_argv = [string.lower() for string in sys.argv]
ixp_mod = '/usr/local/lib/python3.6/dist-packages/ixp.py'
def_list_file = './ixp_functions.txt'

#====================================#
# //           functions          // #
#====================================#

def search_ixp(arg):
    ''' Search function '''

    # // Function scope variables //
    s = arg
    l = list()
    c = 0

    # // Open the module for reading //
    fhi = open(ixp_mod, mode='r')

    # // Find lines matching pattern given //
    for line in fhi:
        m = re.findall(rf'{s}', line, flags=re.MULTILINE)
        if (len(m) != 0):
            l.append({c : m[0]})
        c += 1

    # // Close filehandle //
    fhi.close()
    
    # // Return list of matched patterns with line numbers //
    return(l)

    ## End of search_ixp() function ##

def main():
    ''' Main function '''

    # Function variables //
    class_lines = list()
    class_range = list()

    if (len(ixp_argv) == 1):
        ixp_argv.append('line-numbers')
        ixp_argv.append('true')

    if (ixp_argv[1].lower() in ['help', '--help', '?']):
        print(f'''Usage: {ixp_argv[0]} {{ help | ? }} [-l|--line-numbers] [true|false]

        ?, --help  -  This help message.

        -l, --line-numbers - Line numbers.

        ARGUMENT(s) [list]:
        true  - Add line numbers to output in parenthesis.
        false - Do not add line numbers to output in parenthesis.
        ''')
        sys.exit(0)

    # // Handle abbreviations //
    if (ixp_argv[1] == '--line-numbers'[:len(ixp_argv[1])]):      
        ixp_argv[1] = 'line-numbers'
    elif (ixp_argv[1] == '-l'):
        ixp_argv[1] = 'line-numbers'
    else:
        print(f'{ixp_argv[1]} is not a valid switch option\n')

    # // Handle only two argv //
    if (len(ixp_argv) == 2):
        print('Not enough arguments given, try --line-numbers [true|false]')
        sys.exit(1)

    # // Handle greater than three argv //
    if (len(ixp_argv) > 3):
        print('Too many arguments given, try --line-numbers [true|false]')
        sys.exit(1)

    # // Handle abbreviation //
    for x in ['true', 'false']:
        if (ixp_argv[2] == x[:len(ixp_argv[2])]):      
            ixp_argv[2] = x    

    # // Define line numbers or not //
    if (ixp_argv[1] == 'line-numbers'):
        if (ixp_argv[2] == 'true'):
            line_numbers = True
        elif (ixp_argv[2] == 'false'):
            line_numbers = False
        else:
            print('ERROR: Illegal option')
            sys.exit(1)
    else:
        print('ERROR: Illegal option')
        sys.exit(1)

    # // Open the module for reading and make list of lines //
    fhi = open(ixp_mod, mode='r')
    lines=fhi.readlines()
    fhi.close()

    # // Generate timestamp //
    tstamp = '{:%Y%m%d-%H%M%S}'.format(datetime.datetime.now())

    # // Open file to output data //
    if (line_numbers == True):
        fho = open(f'../{tstamp}_def_list_with_line_numbers.txt', mode='w')
    else:
        fho = open(f'../{tstamp}_def_list.txt', mode='w')

    fho.write(f'Timestamp : {tstamp}')
    fho.write('\n')

    # // Search for functions, classes and methods //
    
    ixp_functions = search_ixp('^def\s(.*?)\(')
    ixp_classes = search_ixp('^class\s(.*?):')
    ixp_methods = search_ixp('^    def\s(.*?)\(')

    # // Process functions 
    fho.write('\n=========================\n')
    fho.write('= Independent functions =\n')
    fho.write('=========================\n\n')

    count = 0
    for e in ixp_functions:        
        count += 1
        for k,v in e.items():
            if (line_numbers == True):
                fho.write(f'{count:<4}({k}) {v}()\n')
            else:
                fho.write(f'{count:<4} {v}()\n')
            m = re.findall(r"'''\s*(.*?)\s*'''", lines[k + 1])
            if (len(m) > 0):
                fho.write(f'    {m[0]}.\n')
        fho.write('\n')

    # // Get the Class names and first and last lines of them //

    for e in ixp_classes:
        for k,v in e.items():
            class_lines.append(k)
      
    count = 0
    for e in ixp_classes:
        count += 1
        if (count == len(class_lines)):
            for k,v in e.items():
                class_range.append([v,k, len(lines)])   
        else:
            for k,v in e.items():
                next_class = count
                class_range.append([v,k, class_lines[next_class] - 1])

    # // Process through classes //

    fho.write('=======================\n')
    fho.write('= Classes and methods =\n')
    fho.write('=======================\n\n')

    count = 0
    for e in ixp_classes:        
        count += 1

        for k,v in e.items():
            if (line_numbers == True):
                fho.write(f'{count:<4}: ({k}) {v}\n')
            else:
                fho.write(f'{count:<4}: {v}\n')
            m = re.findall(r"'''\s*(.*?)\s*'''", lines[k + 1])
            if (len(m) > 0):
                fho.write(f'     {m[0]}.\n')
        fho.write('\n')

        mcount = 0        
        for e in ixp_methods:
            for k,v in e.items():
                if(class_range[count - 1][1] < k < class_range[count - 1][2]):
                    mcount += 1
                    if (line_numbers == True):
                        fho.write(f'    {mcount:<4}: ({k}) {v}()\n')
                    else:
                        fho.write(f'    {mcount:<4}: {v}()\n')
                    m = re.findall(r"'''\s*(.*?)\s*'''", lines[k + 1])
                    if (len(m) > 0):
                        fho.write(f'          {m[0]}.\n')
                    fho.write('\n')
        fho.write('\n')

    # // close output filehandle //
    fho.close()

    # // Read the output to the terminal from the file //

    print('\nLists of functions, classes and associated methods')
    print(f'have been saved to ../{tstamp}_def_list.txt\n')
     
    # // Unhash next line to see output to terminal also //  
    """    
    if (line_numbers == True):
        subprocess.call(['cat', f'../{tstamp}_def_list_with_ln.txt'])
    else:
        subprocess.call(['cat', f'../{tstamp}_def_list.txt'])
    """

    ## End of main() function ##

if __name__ == "__main__":
    main()

# // End //
sys.exit(0)

